/* GtkQuery
 * Copyright (C) 1998 Tim Janik
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#include	<gtk/gtk.h>
#include	<stdio.h>
#include	<stdlib.h>
#include	<unistd.h>
#include	<sys/stat.h>
#include	<fcntl.h>

static gchar *indent_inc = NULL;
static guint spacing = 1;
static FILE *f_out = NULL;
static GtkType root = GTK_TYPE_OBJECT;
static gboolean recursion = TRUE;

static gchar*
arg_name (GtkType type)
{
  static gchar buffer[2048];
  
  switch (GTK_FUNDAMENTAL_TYPE (type))
    {
    case  GTK_TYPE_BOOL:
      return "gboolean";

    case  GTK_TYPE_NONE:
      return "void";
      
    case  GTK_TYPE_STRING:
    case  GTK_TYPE_BOXED:
    case  GTK_TYPE_ARGS:
    case  GTK_TYPE_OBJECT:
      sprintf (buffer, "%s *", gtk_type_name (type));
      break;
      
    case  GTK_TYPE_FOREIGN:
    case  GTK_TYPE_SIGNAL:
    case  GTK_TYPE_CALLBACK:
      sprintf (buffer, "?%s?", gtk_type_name (type));
      break;

    case  GTK_TYPE_C_CALLBACK:
      return "GtkCallback";

    case  GTK_TYPE_ENUM:
    case  GTK_TYPE_FLAGS:
      return gtk_type_name (type);
      
    default:
      sprintf (buffer, "g%s", gtk_type_name (type));
      break;
    }
  
  return buffer;
}

static void
show_signals (GtkType type,
	      const gchar *indent)
{
  GtkObjectClass *class;
  guint i, sig, j;

  class = gtk_type_class (type);
  if (!class || class->nsignals == 0)
    return;

  if (type != root)
    for (i = 0; i < spacing; i++)
      fprintf (f_out, "%s\n", indent);
  fprintf (f_out, "%s%s\n",
	   indent,
	   gtk_type_name (type));
  
  fputs (indent, f_out);
  for (i = 0; i < strlen (gtk_type_name (type)); i++)
    fputc ('=', f_out);
  fputc ('\n', f_out);

  
  for (sig = 0; sig < class->nsignals; sig++)
    {
      GtkSignalQuery *query;
      guint n;
      
      if (!class->signals[sig])
	{
	  fprintf (f_out, "%sSignal slot [%u] is empty\n", indent, sig);
	  continue;
	}
      
      query = gtk_signal_query (class->signals[sig]);
      fputs (indent, f_out);

      n = fprintf (f_out, "%s %s::%s",
		   arg_name (query->return_val),
		   gtk_type_name (type),
		   query->signal_name);
      fprintf (f_out, "\t(%s *,\n",
	       gtk_type_name (type));

      for (j = 0; j < query->nparams; j++)
	{
	  i = n;
	  
	  fputs (indent, f_out);
	  while (i--)
	    fputc (' ', f_out);
	  
	  fprintf (f_out, "\t %s,\n",
		   arg_name (query->params[j]));
	}
      
      fputs (indent, f_out);
      i = n;
      while (i--)
	fputc (' ', f_out);
      fprintf (f_out, "\t gpointer);\n");
    }
}

static void
signal_traverse (GtkType type,
		 const gchar *indent)
{
  GList *list;

  if (!type || !recursion)
    return;

  list = gtk_type_children_types (type);
  for (; list ; list = list->next)
    {
      GtkType child_type;

      child_type = (GtkType) list->data;

      show_signals (child_type, indent);
    }

  list = gtk_type_children_types (type);
  for (; list; list = list->next)
    {
      type = (GtkType) list->data;

      signal_traverse (type, indent);
    }
}

/*
  #define	O_SPACE	"\\as"
  #define	O_ESPACE " "
  #define	O_BRANCH "\\aE"
  #define	O_VLINE "\\al"
  #define	O_LLEAF	"\\aL"
  #define	O_KEY_FILL "_"
*/

#define	O_SPACE	" "
#define	O_ESPACE ""
#define	O_BRANCH "+"
#define	O_VLINE "|"
#define	O_LLEAF	"`"
#define	O_KEY_FILL "_"

static void
show_nodes (GtkType type,
	    GtkType sibling,
	    const gchar *indent)
{
  GList *list;
  guint i;

  if (!type)
    return;

  list = gtk_type_children_types (type);

  if (type != root)
    for (i = 0; i < spacing; i++)
      fprintf (f_out, "%s%s\n", indent, O_VLINE);
  
  fprintf (f_out, "%s%s%s%s",
	   indent,
	   sibling ? O_BRANCH : (type != root ? O_LLEAF : O_SPACE),
	   O_ESPACE,
	   gtk_type_name (type));

  for (i = strlen (gtk_type_name (type)); i <= strlen (indent_inc); i++)
    fputs (O_KEY_FILL, f_out);
  fputc ('\n', f_out);
  
  if (list && recursion)
    {
      gchar *new_indent;

      if (sibling)
	new_indent = g_strconcat (indent, O_VLINE, indent_inc, NULL);
      else
	new_indent = g_strconcat (indent, O_SPACE, indent_inc, NULL);

      for (; list; list = list->next)
	{
	  GtkType child;

	  child = (GtkType) list->data;

	  show_nodes (child,
		      list->next ? (GtkType) list->next->data : 0,
		      new_indent);
	}

      g_free (new_indent);
    }
}

static gchar*
mk_palette_type (gchar *name)
{
  gchar *string;
  gchar *s, *n;
  gint c;

  string = g_new (gchar, strlen (name) * 2 + 1);
  n = name;
  string[0] = 0;
  s = string + 1;

  c = 0;
  while (*n)
    {
      if (*n >= 'A' && *n <= 'Z')
	{
	  if (c > 0)
	    *s++ = '_';
	  *s = *n - 'A' + 'a';
	  c = -1;
	}
      else
	*s = *n;
      c++;
      n++;
      s++;
    }
  *s = 0;

  s = string;
  string = g_strdup (s + 1);
  g_free (s);

  return string;
}

static void
show_palette_nodes (GScanner *scanner,
		    GtkType type,
		    const gchar *indent)
{
  GList *list;
  guint i;

  if (!type)
    return;

  list = gtk_type_children_types (type);

  if (!g_scanner_lookup_symbol (scanner, gtk_type_name (type)))
    {
      gchar *name;
      
      if (type != root)
	for (i = 0; i < spacing; i++)
	  fprintf (f_out, "\n");
      
      name = mk_palette_type (gtk_type_name (type));
      fprintf (f_out, "%s%s_get_type,\n",
	       indent,
	       name);
      
      g_free (name);
    }
  
  if (list && recursion)
    {
      gchar *new_indent;

      new_indent = g_strconcat (indent, indent_inc, NULL);

      for (; list; list = list->next)
	{
	  GtkType child;

	  child = (GtkType) list->data;

	  show_palette_nodes (scanner,
			      child,
			      new_indent);
	}

      g_free (new_indent);
    }
}

static void
parse_kill_file (GScanner *scanner,
		 gchar	  *kill_file)
{
  int fd;

  fd = open (kill_file, O_RDONLY);
  if (fd < 0)
    return;
  
  g_scanner_input_file (scanner, fd);
  while (!g_scanner_eof (scanner))
    {
      g_scanner_get_next_token (scanner);
      if (scanner->token == G_TOKEN_IDENTIFIER)
	g_scanner_add_symbol (scanner, scanner->value.v_identifier, (void*) 1);
    }

  close (fd);
}

static gint
help (gchar *arg)
{
  fprintf (stderr, "usage: gtkquery [-r <type>] [-{i|b|k} \"\"] [-s #] [-h] <qualifier>\n");
  fprintf (stderr, "       -r       specifiy root object type\n");
  fprintf (stderr, "       -b       specifiy indent string\n");
  fprintf (stderr, "       -i       specifiy incremental indent string\n");
  fprintf (stderr, "       -s       specifiy line spacing\n");
  fprintf (stderr, "       -n       no recursion\n");
  fprintf (stderr, "       -k       kill-file\n");
  fprintf (stderr, "       -h       guess what ;)\n");
  fprintf (stderr, "qualifiers:\n");
  fprintf (stderr, "       signals  dump object signals\n");
  fprintf (stderr, "       tree     show object tree\n");
  fprintf (stderr, "       palette  print widget palette types\n");
  

  return arg != NULL;
}

int
main (gint argc, gchar *argv[])
{
  gboolean gen_tree = 0;
  gboolean gen_palette = 0;
  gboolean gen_signals = 0;
  guint i;
  gchar *iindent = "";
  gchar *kill_file = NULL;
  
  f_out = stdout;

  gtk_type_init ();

# define GLE_TYPE_CALL(type_func) type_func ();
#   include <gle/gletypecalls.h>
# undef  GLE_TYPE_CALL

  for (i = 1; i < argc; i++)
    {
      if (strcmp ("-s", argv[i]) == 0)
	{
	  i++;
	  if (i < argc)
	    spacing = atoi (argv[i]);
	}
      else if (strcmp ("-i", argv[i]) == 0)
	{
	  i++;
	  if (i < argc)
	    {
	      char *p;
	      guint n;
	      
	      p = argv[i];
	      while (*p)
		p++;
	      n = p - argv[i];
	      indent_inc = g_new (gchar, n * strlen (O_SPACE) + 1);
	      *indent_inc = 0;
	      while (n)
		{
		  n--;
		  strcpy (indent_inc, O_SPACE);
		}
	    }
	}
      else if (strcmp ("-b", argv[i]) == 0)
	{
	  i++;
	  if (i < argc)
	    iindent = argv[i];
	}
      else if (strcmp ("-k", argv[i]) == 0)
	{
	  i++;
	  if (i < argc)
	    kill_file = argv[i];
	}
      else if (strcmp ("-r", argv[i]) == 0)
	{
	  i++;
	  if (i < argc)
	    root = gtk_type_from_name (argv[i]);
	}
      else if (strcmp ("-n", argv[i]) == 0)
	{
	  recursion = FALSE;
	}
      else if (strcmp ("tree", argv[i]) == 0)
	{
	  gen_tree = 1;
	}
      else if (strcmp ("palette", argv[i]) == 0)
	{
	  gen_palette = 1;
	}
      else if (strcmp ("signals", argv[i]) == 0)
	{
	  gen_signals = 1;
	}
      else if (strcmp ("-h", argv[i]) == 0)
	{
	  return help (NULL);
	}
      else if (strcmp ("--help", argv[i]) == 0)
	{
	  return help (NULL);
	}
      else
	return help (argv[i]);
    }
  if (!gen_tree && !gen_palette && !gen_signals)
    return help (argv[i-1]);

  if (!indent_inc)
    {
      indent_inc = g_new (gchar, strlen (O_SPACE) + 1);
      *indent_inc = 0;
      strcpy (indent_inc, O_SPACE);
      strcpy (indent_inc, O_SPACE);
      strcpy (indent_inc, O_SPACE);
    }

  if (gen_tree)
    show_nodes (root, 0, iindent);


  if (gen_palette)
    {
      GScanner *scanner;

      scanner = g_scanner_new (NULL);
      if (kill_file)
	parse_kill_file (scanner, kill_file);
      show_palette_nodes (scanner, root, iindent);
      g_scanner_destroy (scanner);
    }

  if (!gen_signals)
    return 0;

  gtk_init (NULL, NULL);

# define GLE_TYPE_CALL(type_func) gtk_type_class (type_func ());
#   include <gle/gletypecalls.h>
# undef  GLE_TYPE_CALL

  if (gen_signals)
    {
      show_signals (root, iindent);
      signal_traverse (root, iindent);
    }
  
  return 0;
}
