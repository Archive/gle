/* GleTest
 * Copyright (C) 1999 Tim Janik
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#include	<gle/gle.h>
#include	<gle/gleshell.h>
#include	<gle/gtkcluehunter.h>
#include	<gle/gtkhwrapbox.h>
#include	<gle/gtkvwrapbox.h>

int
main (gint   argc,
      gchar *argv[])
{
  gle_init (&argc, &argv);
  
  gtk_widget_new (GTK_TYPE_WINDOW,
		  "title", "GLE Test",
		  "child", gtk_widget_new (GTK_TYPE_VBOX,
					   "visible", TRUE,
					   "child", gtk_widget_new (GTK_TYPE_LABEL,
								    "visible", TRUE,
								    "label",
								    "Press CTRL+"
								    "ALT+Tab",
								    NULL),
					   "child", gtk_widget_new (GTK_TYPE_BUTTON,
								    "visible", TRUE,
								    "label",
								    "or Click Here",
								    "signal::clicked",
								    gle_shell_popup,
								    NULL,
								    NULL),
					   NULL),
		  "visible", TRUE,
		  "signal::destroy", gtk_main_quit, NULL,
		  NULL);

  gtk_clue_hunter_get_type ();
  gtk_hwrap_box_get_type ();
  gtk_vwrap_box_get_type ();

  gtk_main ();
  
  return 0;
}
