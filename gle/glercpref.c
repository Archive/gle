/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#include	<gle/gleconfig.h>

#include	"glercpref.h"
#include	<stdio.h>
#include	<stdlib.h>




/* --- prototypes --- */
static void		gle_rc_pref_class_init		(GleRcPrefClass	*class);
static void		gle_rc_pref_init		(GleRcPref	*rc_pref);
static void		gle_rc_pref_destroy		(GtkObject	*object);
static void		gle_rc_pref_create_field	(GleRcPref	*rc_pref,
							 GtkBox		*parent_box,
							 GleRcArg	*rc_arg);
static void		gle_rc_pref_refresh_field	(GtkWidget	*widget,
							 GleRcArg	*rc_arg);
static void		gle_rc_pref_apply_field		(GtkWidget	*widget,
							 GleRcArg	*rc_arg);


/* --- variables --- */
static GtkAlignmentClass	*parent_class = NULL;


/* --- functions --- */
GtkType
gle_rc_pref_get_type (void)
{
  static GtkType rc_pref_type = 0;
  
  if (!rc_pref_type)
    {
      GtkTypeInfo rc_pref_info =
      {
	"GleRcPref",
	sizeof (GleRcPref),
	sizeof (GleRcPrefClass),
	(GtkClassInitFunc) gle_rc_pref_class_init,
	(GtkObjectInitFunc) gle_rc_pref_init,
	/* reserved_1 */ NULL,
	/* reserved_2 */ NULL,
	(GtkClassInitFunc) NULL,
      };
      
      rc_pref_type = gtk_type_unique (GTK_TYPE_ALIGNMENT, &rc_pref_info);
    }
  
  return rc_pref_type;
}

static void
gle_rc_pref_class_init (GleRcPrefClass *class)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;
  
  object_class = (GtkObjectClass*) class;
  widget_class = (GtkWidgetClass*) class;
  
  parent_class = gtk_type_class (GTK_TYPE_ALIGNMENT);
  
  object_class->destroy = gle_rc_pref_destroy;
}

static void
gle_rc_pref_init (GleRcPref	 *rc_pref)
{
  rc_pref->args = NULL;
  rc_pref->tooltips = NULL;
  rc_pref->notebook =
    gtk_widget_new (GTK_TYPE_NOTEBOOK,
		    "visible", TRUE,
		    "scrollable", TRUE,
		    "tab_border", 3,
		    "show_border", FALSE,
		    "enable_popup", TRUE,
		    NULL);
  gtk_widget_new (GTK_TYPE_VBOX,
		  "visible", TRUE,
		  "parent", rc_pref,
		  "child", rc_pref->notebook,
		  NULL);
}

static void
gle_rc_pref_destroy (GtkObject	*object)
{
  GleRcPref *rc_pref;
  
  g_return_if_fail (object != NULL);
  g_return_if_fail (GLE_IS_RC_PREF (object));
  
  rc_pref = GLE_RC_PREF (object);
  if (rc_pref->tooltips)
    {
      gtk_object_unref (GTK_OBJECT (rc_pref->tooltips));
      rc_pref->tooltips = NULL;
    }
  
  /* FIXME free args */
  
  GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

typedef struct _GroupNode GroupNode;
struct _GroupNode
{
  GtkWidget *widget;
  guint	     group_id;
  GroupNode *next;
};

void
gle_rc_pref_construct (GleRcPref      *rc_pref,
		       GleRcHandle    *rc_handle)
{
  GList *list;
  GroupNode *groups;
  
  g_return_if_fail (rc_pref != NULL);
  g_return_if_fail (GLE_IS_RC_PREF (rc_pref));
  g_return_if_fail (rc_handle != NULL);
  g_return_if_fail (rc_pref->args == NULL);
  
  groups = NULL;
  for (list = gle_rc_get_arg_list (rc_handle); list; list = list->next)
    {
      GleRcArg *arg;
      GroupNode *node;
      
      arg = list->data;
      if (!arg->any.group_id || !*arg->any.group_id)
	continue;
      
      node = groups;
      while (node)
	{
	  if (node->group_id == *arg->any.group_id)
	    break;
	  node = node->next;
	}
      
      if (!node)
	{
	  node = g_new (GroupNode, 1);
	  node->widget = gtk_widget_new (GTK_TYPE_VBOX,
					 "visible", TRUE,
					 NULL);
	  gtk_container_add_with_args (GTK_CONTAINER (rc_pref->notebook), node->widget,
				       "tab_label", g_quark_to_string (*arg->any.group_id),
				       NULL);
	  node->group_id = *arg->any.group_id;
	  node->next = groups;
	  groups = node;
	}
      
      gle_rc_pref_create_field (rc_pref, GTK_BOX (node->widget), arg);
    }
  
  while (groups)
    {
      GroupNode *node;
      
      node = groups;
      groups = node->next;
      g_free (node);
    }
}

void
gle_rc_pref_apply_args (GleRcPref      *rc_pref)
{
  GleRcArgW *arg;
  
  g_return_if_fail (rc_pref != NULL);
  g_return_if_fail (GLE_IS_RC_PREF (rc_pref));
  g_return_if_fail (rc_pref->args != NULL);
  
  for (arg = rc_pref->args; arg; arg = arg->next)
    {
      gle_rc_pref_apply_field (arg->widget, arg->rc_arg);
    }
}

void
gle_rc_pref_refresh_args (GleRcPref	 *rc_pref)
{
  GleRcArgW *arg;
  
  g_return_if_fail (rc_pref != NULL);
  g_return_if_fail (GLE_IS_RC_PREF (rc_pref));
  g_return_if_fail (rc_pref->args != NULL);
  
  for (arg = rc_pref->args; arg; arg = arg->next)
    {
      gle_rc_pref_refresh_field (arg->widget, arg->rc_arg);
    }
}

void
gle_rc_pref_reset_args (GleRcPref      *rc_pref)
{
  GleRcArgW *arg;
  
  g_return_if_fail (rc_pref != NULL);
  g_return_if_fail (GLE_IS_RC_PREF (rc_pref));
  g_return_if_fail (rc_pref->args != NULL);
  
  for (arg = rc_pref->args; arg; arg = arg->next)
    {
      GleRcArg *rc_arg;
      
      rc_arg = arg->rc_arg;
      
      switch (rc_arg->any.type)
	{
	case GLE_RC_BOOL:
	  rc_arg->abool.value = rc_arg->abool.default_value;
	  break;
	  
	case GLE_RC_LONG:
	  rc_arg->along.value = rc_arg->along.default_value;
	  break;
	  
	case GLE_RC_STRING:
	  g_free (rc_arg->astring.value);
	  rc_arg->astring.value = g_strdup (rc_arg->astring.default_value);
	  break;
	  
	default:
	  break;
	}
      gle_rc_pref_refresh_field (arg->widget, arg->rc_arg);
    }
}

static void
activate_toplevel (GtkWidget *widget)
{
  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_WIDGET (widget));
  
  widget = gtk_widget_get_toplevel (widget);
  
  if (widget && GTK_IS_WINDOW (widget))
    gtk_window_activate_default (GTK_WINDOW (widget));
}

static void
gle_tb_sensitize (GtkToggleButton *tb,
		  GtkWidget	  *widget)
{
  gtk_widget_set_sensitive (widget, tb->active);
}

static void
gle_rc_pref_create_field (GleRcPref   *rc_pref,
			  GtkBox      *parent_box,
			  GleRcArg    *rc_arg)
{
  GleRcArgW *arg;
  GtkWidget *widget;
  
  arg = g_new (GleRcArgW, 1);
  arg->next = rc_pref->args;
  rc_pref->args = arg;
  arg->rc_arg = rc_arg;
  
  switch (rc_arg->any.type)
    {
      GtkWidget *box;
      GtkWidget *label;
      GtkWidget *button;
      
    case GLE_RC_BOOL:
      widget = gtk_widget_new (GTK_TYPE_CHECK_BUTTON,
			       "label", rc_arg->name,
			       "visible", TRUE,
			       NULL);
      gtk_misc_set_alignment (GTK_MISC (GTK_BIN (widget)->child), 0, 0.5);
      gtk_box_pack_start (parent_box, widget, FALSE, FALSE, 0);
      break;
      
    case GLE_RC_LONG:
      box = gtk_widget_new (GTK_TYPE_HBOX,
			    "homogeneous", FALSE,
			    "spacing", 10,
			    "border_width", 0,
			    "visible", TRUE,
			    NULL);
      gtk_box_pack_start (parent_box, box, FALSE, TRUE, 0);
      label = gtk_widget_new (GTK_TYPE_LABEL,
			      "label", rc_arg->name,
			      "justify", GTK_JUSTIFY_LEFT,
			      "visible", TRUE,
			      NULL);
      gtk_box_pack_start (GTK_BOX (box), label, FALSE, TRUE, 0);
      widget = gtk_widget_new (GTK_TYPE_ENTRY,
			       "width", 50,
			       "visible", TRUE,
			       "signal::activate", activate_toplevel, NULL,
			       NULL);
      gtk_box_pack_end (GTK_BOX (box), widget, FALSE, TRUE, 0);
      break;
      
    case GLE_RC_STRING:
      box = gtk_widget_new (GTK_TYPE_HBOX,
			    "visible", TRUE,
			    "homogeneous", FALSE,
			    "spacing", 10,
			    "border_width", 0,
			    NULL);
      gtk_box_pack_start (parent_box, box, FALSE, TRUE, 0);
      label = gtk_widget_new (GTK_TYPE_LABEL,
			      "visible", TRUE,
			      "label", rc_arg->name,
			      "justify", GTK_JUSTIFY_LEFT,
			      NULL);
      gtk_box_pack_start (GTK_BOX (box), label, FALSE, TRUE, 0);
      button = gtk_widget_new (GTK_TYPE_CHECK_BUTTON,
			       "visible", TRUE,
			       NULL);
      widget = gtk_widget_new (GTK_TYPE_ENTRY,
			       "visible", TRUE,
			       "width", 140,
			       "signal::activate", activate_toplevel, NULL,
			       "user_data", button,
			       NULL);
      gtk_signal_connect (GTK_OBJECT (button),
			  "clicked",
			  GTK_SIGNAL_FUNC (gle_tb_sensitize),
			  widget);
      gtk_box_pack_end (GTK_BOX (box), widget, FALSE, TRUE, 0);
      gtk_box_pack_end (GTK_BOX (box), button, FALSE, FALSE, 0);
      break;
      
    default:
      label =
	gtk_widget_new (GTK_TYPE_LABEL,
			"visible", TRUE,
			"label", rc_arg->name,
			"xalign", 0.0,
			"parent", parent_box,
			"sensitive", FALSE,
			NULL);
      widget = gtk_widget_new (GTK_TYPE_EVENT_BOX,
			       "visible", TRUE,
			       "child", label,
			       NULL);
      break;
    }
  
  arg->widget = widget;
  /* gtk_object_set_data (GTK_OBJECT (widget), "gle-rc-pref-arg", arg); */
  
  if (rc_arg->any.description && rc_arg->any.description[0])
    {
      while (GTK_WIDGET_NO_WINDOW (widget))
	widget = widget->parent;
      if (!rc_pref->tooltips)
	{
	  rc_pref->tooltips = gtk_tooltips_new ();
	  gtk_object_ref (GTK_OBJECT (rc_pref->tooltips));
	  gtk_object_sink (GTK_OBJECT (rc_pref->tooltips));
	  gtk_tooltips_force_window (rc_pref->tooltips);
	  gtk_widget_set_name (rc_pref->tooltips->tip_window, "GLE-TipWindow");
	}
      gtk_tooltips_set_tip (rc_pref->tooltips, widget, rc_arg->any.description, NULL);
    }
  
  gle_rc_pref_refresh_field (arg->widget, rc_arg);
}

static void
gle_rc_pref_refresh_field (GtkWidget   *widget,
			   GleRcArg    *rc_arg)
{
  switch (rc_arg->any.type)
    {
      GtkWidget *button;
      gchar buffer[64];
      gchar *string;
      
    case GLE_RC_BOOL:
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget),
				    rc_arg->abool.value);
      break;
      
    case GLE_RC_LONG:
      sprintf (buffer, "%ld", rc_arg->along.value);
      if (!g_str_equal (gtk_entry_get_text (GTK_ENTRY (widget)), buffer))
	{
	  gtk_entry_set_text (GTK_ENTRY (widget), buffer);
	  gtk_entry_set_position (GTK_ENTRY (widget), 0);
	}
      break;
      
    case GLE_RC_STRING:
      string = rc_arg->astring.value;
      if (!string || !g_str_equal (gtk_entry_get_text (GTK_ENTRY (widget)), string))
	{
	  gtk_entry_set_text (GTK_ENTRY (widget), string ? string : "");
	  gtk_entry_set_position (GTK_ENTRY (widget), 0);
	}
      button = gtk_object_get_user_data (GTK_OBJECT (widget));
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), string != NULL);
      gtk_widget_set_sensitive (widget, string != NULL);
      break;
      
    default:
      break;
    }
}

static void
gle_rc_pref_apply_field (GtkWidget   *widget,
			 GleRcArg    *rc_arg)
{
  switch (rc_arg->any.type)
    {
      GtkWidget *button;
      glong along;
      gchar *dummy;
      
    case GLE_RC_BOOL:
      rc_arg->abool.value = (GTK_TOGGLE_BUTTON (widget)->active != FALSE);
      break;
      
    case GLE_RC_LONG:
      along = strtol (gtk_entry_get_text (GTK_ENTRY (widget)), &dummy, 10);
      rc_arg->along.value = CLAMP (along, rc_arg->along.minimum, rc_arg->along.maximum);
      break;
      
    case GLE_RC_STRING:
      g_free (rc_arg->astring.value);
      button = gtk_object_get_user_data (GTK_OBJECT (widget));
      if (GTK_TOGGLE_BUTTON (button)->active)
	rc_arg->astring.value = g_strdup (gtk_entry_get_text (GTK_ENTRY (widget)));
      else
	rc_arg->astring.value = NULL;
      break;
      
    default:
      break;
    }
  
  gle_rc_pref_refresh_field (widget, rc_arg);
}

GtkWidget*
gle_rc_pref_dialog_new (GleRcHandle *rc_handle,
			const gchar *window_title)
{
  GtkWidget *window;
  GtkWidget *main_vbox;
  GtkWidget *any;
  GtkWidget *button;
  GtkWidget *rc_pref;
  GtkWidget *hbox;
  
  g_return_val_if_fail (rc_handle != NULL, NULL);
  
  window =
    gtk_widget_new (GTK_TYPE_WINDOW,
		    "title", window_title ? window_title : "RcPreferences",
		    "allow_shrink", FALSE,
		    "allow_grow", TRUE,
		    NULL);
  main_vbox =
    gtk_widget_new (GTK_TYPE_VBOX,
		    "homogeneous", FALSE,
		    "spacing", 0,
		    "border_width", 0,
		    "parent", window,
		    "visible", TRUE,
		    NULL);
  rc_pref =
    gtk_widget_new (GLE_TYPE_RC_PREF,
		    "visible", TRUE,
		    "border_width", 5,
		    "parent", main_vbox,
		    NULL);
  gle_rc_pref_construct (GLE_RC_PREF (rc_pref), rc_handle);
  hbox =
    gtk_widget_new (GTK_TYPE_HBOX,
		    "homogeneous", TRUE,
		    "spacing", 5,
		    "border_width", 5,
		    "visible", TRUE,
		    NULL);
  gtk_box_pack_start (GTK_BOX (main_vbox), hbox, FALSE, TRUE, 0);
  button =
    gtk_widget_new (GTK_TYPE_BUTTON,
		    "visible", TRUE,
		    "label", "Reset", /* "Defaults", */
		    "parent", hbox,
		    "object_signal::clicked", gle_rc_pref_reset_args, rc_pref,
		    NULL);
  button =
    gtk_widget_new (GTK_TYPE_BUTTON,
		    "visible", TRUE,
		    "label", "Refresh",
		    "parent", hbox,
		    "object_signal::clicked", gle_rc_pref_refresh_args, rc_pref,
		    NULL);
  any =
    gtk_widget_new (GTK_TYPE_HSEPARATOR,
		    "visible", TRUE,
		    NULL);
  gtk_box_pack_start (GTK_BOX (main_vbox), any, FALSE, TRUE, 0);
  hbox =
    gtk_widget_new (GTK_TYPE_HBOX,
		    "homogeneous", TRUE,
		    "spacing", 5,
		    "border_width", 5,
		    "visible", TRUE,
		    NULL);
  gtk_box_pack_start (GTK_BOX (main_vbox), hbox, FALSE, TRUE, 0);
  button =
    gtk_widget_new (GTK_TYPE_BUTTON,
		    "visible", TRUE,
		    "label", "Save",
		    "parent", hbox,
		    "can_default", TRUE,
		    "has_default", TRUE,
		    "object_signal::clicked", gle_rc_pref_apply_args, rc_pref,
		    "object_signal::clicked", gtk_widget_destroy, window,
		    NULL);
  button =
    gtk_widget_new (GTK_TYPE_BUTTON,
		    "visible", TRUE,
		    "label", "Close",
		    "parent", hbox,
		    "can_default", TRUE,
		    "object_signal::clicked", gtk_widget_destroy, window,
		    NULL);
  
  return window;
}
