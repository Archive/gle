/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#include        <gle/gleconfig.h>

#include	"glednd.h"
#include	"gleted.h"
#include	"glegcontainer.h"
#include	"gleprivate.h"
#include	"glestock.h"


/* --- prototypes --- */
static gint		gle_dnd_event_handler		(GtkWidget	*widget,
							 GdkEvent	*event,
							 gpointer   	 data);
static void		gle_dnd_destroy_source	 	(GtkWidget	*widget,
							 gpointer	 data);
static void		gle_dnd_destroy_notifier 	(GtkWidget	*widget,
							 gpointer	 data);
static gboolean		gle_dnd_drag			(GtkWidget	*widget,
							 GdkEventButton *event);
static void		gle_dnd_finish			(gpointer	 drag_failed);
static GtkWidget*	gle_dnd_window			(GtkType	 type);
static void		gle_dnd_candidate_check		(GleSelector	*selector,
							 GtkWidget     **new_candidate,
							 gint           *candidate_ok,
							 gpointer	 data);
static void		gle_dnd_candidate_selected	(GleSelector	*selector,
							 GtkWidget	*new_candidate,
							 gpointer	 data);
static void		gle_dnd_notify			(GleGObject	*gobject);


/* --- variables -- */
static GQuark	   gquark_dnd_start = 0;
static GQuark	   gquark_dnd_stop = 0;
static GQuark	   gquark_dnd_check = 0;
static GQuark	   gquark_dnd_notify = 0;
static GleGWidget *dnd_drag_thing = NULL;
static GtkWidget  *dnd_drag_source = NULL;
static GtkWidget  *dnd_selector = NULL;
static GSList	  *dnd_notifiers = NULL;
static GSList	  *dnd_tmp_notifiers = NULL;


/* --- functions --- */
void
gle_dnd_add_notifier (GtkWidget   *widget,
		      GleDndNotify dnd_notify)
{
  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_WIDGET (widget));
  g_return_if_fail (dnd_notify != NULL);

  if (!gquark_dnd_notify)
    gquark_dnd_notify = g_quark_from_static_string (GLE_PRIVATE_KEY (dnd-notify));

  if (gtk_object_get_data_by_id (GTK_OBJECT (widget), gquark_dnd_notify))
    {
      g_warning ("widget already registered as dnd notifier");
      return;
    }

  gtk_object_set_data_by_id (GTK_OBJECT (widget), gquark_dnd_notify, dnd_notify);

  dnd_notifiers = g_slist_prepend (dnd_notifiers, widget);

  gtk_signal_connect (GTK_OBJECT (widget),
		      "destroy",
		      GTK_SIGNAL_FUNC (gle_dnd_destroy_notifier),
		      NULL);
}

static void
gle_dnd_destroy_notifier (GtkWidget *widget,
			  gpointer   data)
{
  gtk_signal_disconnect_by_func (GTK_OBJECT (widget),
				 GTK_SIGNAL_FUNC (gle_dnd_destroy_notifier),
				 data);
  gtk_object_remove_data_by_id (GTK_OBJECT (widget), gquark_dnd_notify);
  
  dnd_notifiers = g_slist_remove (dnd_notifiers, widget);
  dnd_tmp_notifiers = g_slist_remove (dnd_tmp_notifiers, widget);
}

void
gle_dnd_remove_notifier (GtkWidget   *widget)
{
  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_WIDGET (widget));

  if (!gtk_object_get_data_by_id (GTK_OBJECT (widget), gquark_dnd_notify))
    {
      g_warning ("widget not registered as dnd notifier");
      return;
    }

  gle_dnd_destroy_notifier (widget, NULL);
}

static void
gle_dnd_notify (GleGObject *gobject)
{
  g_return_if_fail (dnd_tmp_notifiers == NULL);

  while (dnd_notifiers)
    {
      GSList *slist;
      GleDndNotify dnd_notify;

      slist = dnd_notifiers;
      dnd_notifiers = slist->next;
      slist->next = dnd_tmp_notifiers;
      dnd_tmp_notifiers = slist;

      dnd_notify = gtk_object_get_data_by_id (slist->data, gquark_dnd_notify);
      dnd_notify (slist->data, gobject);
    }
  dnd_notifiers = dnd_tmp_notifiers;
  dnd_tmp_notifiers = NULL;
}

static void
gle_dnd_destroy_source (GtkWidget *widget,
			gpointer   data)
{
  gtk_signal_disconnect_by_func (GTK_OBJECT (widget),
				 GTK_SIGNAL_FUNC (gle_dnd_event_handler),
				 data);
  gtk_signal_disconnect_by_func (GTK_OBJECT (widget),
				 GTK_SIGNAL_FUNC (gle_dnd_destroy_source),
				 data);

  if (widget == dnd_drag_source)
    gle_selector_abort (GLE_SELECTOR (dnd_selector));

  gtk_object_remove_data_by_id (GTK_OBJECT (widget), gquark_dnd_start);
  gtk_object_remove_data_by_id (GTK_OBJECT (widget), gquark_dnd_stop);
}

void
gle_dnd_add_source (GtkWidget  *widget,
		    guint	button_mask,
		    GleDndStart dnd_start,
		    GleDndStop  dnd_stop)
{
  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_WIDGET (widget));
  g_return_if_fail (dnd_start != NULL);

  if (!gquark_dnd_start)
    {
      gquark_dnd_start = g_quark_from_static_string (GLE_PRIVATE_KEY (dnd-start));
      gquark_dnd_stop = g_quark_from_static_string (GLE_PRIVATE_KEY (dnd-stop));
    }

  if (gtk_object_get_data_by_id (GTK_OBJECT (widget), gquark_dnd_start))
    {
      g_warning ("widget already registered as dnd source");
      return;
    }

  gtk_widget_add_events (widget, GDK_BUTTON_PRESS_MASK);
  gtk_object_set_data_by_id (GTK_OBJECT (widget), gquark_dnd_start, dnd_start);
  gtk_object_set_data_by_id (GTK_OBJECT (widget), gquark_dnd_stop, dnd_stop);

  gtk_signal_connect (GTK_OBJECT (widget),
		      "button_press_event",
		      GTK_SIGNAL_FUNC (gle_dnd_event_handler),
		      GUINT_TO_POINTER (button_mask));
  gtk_signal_connect (GTK_OBJECT (widget),
		      "destroy",
		      GTK_SIGNAL_FUNC (gle_dnd_destroy_source),
		      GUINT_TO_POINTER (button_mask));
}

void
gle_dnd_add_target (GtkWidget  *widget,
		    GleDndCheck dnd_check)
{
  g_return_if_fail (widget != NULL);
  g_return_if_fail (GTK_IS_WIDGET (widget));
  g_return_if_fail (dnd_check != NULL);

  if (!gquark_dnd_check)
    gquark_dnd_check = g_quark_from_static_string (GLE_PRIVATE_KEY (dnd-check));

  if (gtk_object_get_data_by_id (GTK_OBJECT (widget), gquark_dnd_check))
    {
      g_warning ("widget already registered as dnd target");
      return;
    }

  gtk_object_set_data_by_id (GTK_OBJECT (widget), gquark_dnd_check, dnd_check);
}

static gboolean
gle_dnd_drag (GtkWidget      *widget,
	      GdkEventButton *event)
{
  GleDndStart dnd_start;
  GtkWidget *dnd_window;
  gboolean drag_handled;
  
  drag_handled = FALSE;
  
  gtk_widget_ref (widget);
  dnd_start = gtk_object_get_data_by_id (GTK_OBJECT (widget), gquark_dnd_start);
  if (dnd_start)
    dnd_drag_thing = (GleGWidget*) dnd_start (widget, event->x_root, event->y_root);
  if (!dnd_drag_thing || !GLE_IS_GWIDGET (dnd_drag_thing) ||
      GLE_GOBJECT_DESTROYED (dnd_drag_thing))
    dnd_drag_thing = NULL;
  
  if (dnd_drag_thing)
    {
      drag_handled = TRUE;
      
      gle_gwidget_ref (dnd_drag_thing);
      
      if (!dnd_selector)
	{
	  dnd_selector = gle_selector_new ("GLE Drop Target", "Select tentative Container widget");
	  GLE_TAG (dnd_selector, "GLE-DndSelector");
	  GLE_TAG_WEAK (GLE_SELECTOR (dnd_selector)->warning_window);
	  gtk_widget_set (dnd_selector,
			  "signal::candidate_check", gle_dnd_candidate_check, NULL,
			  "signal::candidate_selected", gle_dnd_candidate_selected, NULL,
			  "signal::abort", gle_dnd_finish, dnd_selector,
			  "signal::destroy", gtk_widget_destroyed, &dnd_selector,
			  NULL);
	  gle_selector_set_cursor (GLE_SELECTOR (dnd_selector), gdk_cursor_new (GDK_TOP_LEFT_ARROW));
	}
      
      dnd_window = gle_dnd_window (GLE_GOBJECT_OTYPE (dnd_drag_thing));
      dnd_drag_source = widget;
      gle_selector_extended_selection (GLE_SELECTOR (dnd_selector),
				       GTK_WINDOW (dnd_window),
				       TRUE);
      gtk_widget_destroy (dnd_window);
    }

  gtk_widget_unref (widget);
  
  return drag_handled;
}

static void
gle_dnd_finish (gpointer drag_failed)
{
  GleDndStop dnd_stop;

  dnd_stop = gtk_object_get_data_by_id (GTK_OBJECT (dnd_drag_source), gquark_dnd_stop);
  if (dnd_stop)
    dnd_stop (dnd_drag_source, GLE_GOBJECT (dnd_drag_thing), !drag_failed);
  dnd_drag_source = NULL;

  gle_gwidget_unref (dnd_drag_thing);
  dnd_drag_thing = NULL;

  /* notify notifiers */
  gle_dnd_notify (NULL);
}

static gint
gle_dnd_event_handler (GtkWidget *widget,
		       GdkEvent  *event,
		       gpointer   data)
{
  gboolean handled;
  guint button_mask;

  button_mask = GPOINTER_TO_UINT (data);

  handled = FALSE;

  if (event->type == GDK_BUTTON_PRESS &&
      ((button_mask & 1 && event->button.button == 1) ||
       (button_mask & 2 && event->button.button == 2) ||
       (button_mask & 4 && event->button.button == 3)))
    {
      if (dnd_drag_thing)
	{
	  handled = TRUE;
	  gle_selector_abort (GLE_SELECTOR (dnd_selector));
	}
      else
	{
	  handled = gle_dnd_drag (widget, (GdkEventButton*) event);
	  
	  if (handled)
	    gtk_signal_emit_stop_by_name (GTK_OBJECT (widget), "button-press-event");
	}
    }
  
  return handled;
}

static GtkWidget*
gle_dnd_window (GtkType type)
{
  GtkWidget *window;
  GtkWidget *pixmap;
  GtkWidget *frame;
  GtkWidget *label;
  GtkWidget *xbox;
  GdkPixmap *dpix;
  GdkBitmap *dmask;
  gchar **xpm;

  g_return_val_if_fail (gtk_type_is_a (type, GTK_TYPE_WIDGET), NULL);
  
  window =
    gtk_widget_new (GTK_TYPE_WINDOW,
		    "type", GTK_WINDOW_POPUP,
		    "window_position", GTK_WIN_POS_NONE,
		    "allow_shrink", FALSE,
		    "allow_grow", FALSE,
		    NULL);
  GLE_TAG (window, "GLE-DndWindow");
  frame =
    gtk_widget_new (GTK_TYPE_FRAME,
		    "visible", TRUE,
		    "shadow", GTK_SHADOW_IN,
		    "parent", window,
		    NULL);
  xbox =
    gtk_widget_new (GTK_TYPE_VBOX,
		    "visible", TRUE,
		    "spacing", 0,
		    "border_width", 0,
		    "parent", frame,
		    NULL);
  frame =
    gtk_widget_new (GTK_TYPE_FRAME,
		    "visible", TRUE,
		    "shadow", GTK_SHADOW_ETCHED_OUT,
		    NULL);
  gtk_container_add_with_properties (GTK_CONTAINER (xbox), frame,
				     "expand", FALSE,
				     "pad", 6,
				     NULL);
  label =
    gtk_widget_new (GTK_TYPE_LABEL,
		    "visible", TRUE,
		    "label", gtk_type_name (type),
		    "xpad", 5,
		    "parent", xbox,
		    NULL);
  gtk_widget_realize (frame);

  xpm = gle_stock_get_xpm (gtk_type_name (type));
  if (!xpm)
    xpm = gle_stock_get_xpm_unknown ();

  dpix = gdk_pixmap_create_from_xpm_d (frame->window,
				       &dmask,
				       &frame->style->bg[GTK_STATE_NORMAL],
				       xpm);
  pixmap = gtk_pixmap_new (dpix, dmask);
  gtk_widget_show (pixmap);
  gtk_container_add (GTK_CONTAINER (frame), pixmap);
  gdk_pixmap_unref (dpix);
  gdk_bitmap_unref (dmask);

  return window;
}

static void
gle_dnd_candidate_selected (GleSelector	*selector,
			    GtkWidget	*candidate,
			    gpointer	 data)
{
  GtkWidget *widget;

  if (!GLE_GOBJECT_IS_INSTANTIATED (dnd_drag_thing))
    {
      gle_gobject_instantiate (GLE_GOBJECT (dnd_drag_thing));
      gle_gobject_apply_object_args (GLE_GOBJECT (dnd_drag_thing));
    }

  widget = GLE_GWIDGET_WIDGET (dnd_drag_thing);

  gtk_widget_ref (widget);

  if (widget->parent)
    gtk_container_remove (GTK_CONTAINER (widget->parent), widget);

  gtk_container_add (GTK_CONTAINER (candidate), widget);

  gtk_widget_unref (widget);

  gle_dnd_finish (NULL);
}

static void
gle_dnd_candidate_check (GleSelector *selector,
			 GtkWidget  **new_candidate,
			 gboolean    *candidate_ok,
			 gpointer     data)
{
  GtkWidget *toplevel;

  /* feature dnd targets */
  if (*candidate_ok)
    {
      GleDndCheck dnd_check;
      
      dnd_check = gtk_object_get_data_by_id (GTK_OBJECT (*new_candidate), gquark_dnd_check);
      if (dnd_check)
	{
	  GleGObject *gobject;
	  GdkEvent *event;
	  
	  event = gtk_get_current_event ();
	  if (event->type != GDK_MOTION_NOTIFY)
	    *candidate_ok = FALSE;
	  else
	    {
	      gobject = dnd_check (*new_candidate,
				   GLE_GOBJECT_OTYPE (dnd_drag_thing),
				   event->motion.x_root,
				   event->motion.y_root);
	      if (!gobject || !GLE_IS_GWIDGET (gobject) || !GLE_GOBJECT_IS_INSTANTIATED (gobject))
		*candidate_ok = FALSE;
	      else
		*new_candidate = GLE_GWIDGET_WIDGET (gobject);
	    }
	}
    }

  /* eliminate GLE windows */
  toplevel = *new_candidate ? gtk_widget_get_toplevel (*new_candidate) : NULL;
  *candidate_ok &= toplevel && !GLE_HAS_TAG (toplevel);

  /* check if the candidate belongs to the drag widgets tree */
  if (*candidate_ok && GLE_GOBJECT_IS_INSTANTIATED (dnd_drag_thing))
    {
      GtkWidget *widget;
      GtkWidget *drag_widget;

      drag_widget = GLE_GWIDGET_WIDGET (dnd_drag_thing);
      for (widget = *new_candidate; widget; widget = widget->parent)
	if (widget == drag_widget)
	  {
	    *new_candidate = widget->parent;
	    break;
	  }
      *candidate_ok &= *new_candidate != NULL;
    }

  /* find a container that would accept the target */
  if (*candidate_ok)
    {
      GtkWidget *container;
      
      if (GTK_IS_CONTAINER (*new_candidate))
	container = *new_candidate;
      else
	container = (*new_candidate)->parent;

      /* walk up the widget tree until we find a container that
       * accepts the target and has a Gle proxy
       */
      while (container &&
	     !(gtk_type_is_a (GLE_GOBJECT_OTYPE (dnd_drag_thing),
			      gtk_container_child_type (GTK_CONTAINER (container))) &&
	       gle_object_get_gobject (GTK_OBJECT (container))))
	container = container->parent;
      
      if (container)
	*new_candidate = container;
      else
	*candidate_ok = FALSE;
    }

  /* display only toplevels if we have no candidate */
  if (!*candidate_ok)
    *new_candidate = toplevel;

  /* notify notifiers */
  gle_dnd_notify (*candidate_ok ? gle_object_get_gobject (GTK_OBJECT (*new_candidate)) : NULL);
}
