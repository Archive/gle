/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * GtkClueHunter: Completion popup with pattern matching for GtkEntry
 * Copyright (C) 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GTK_CLUE_HUNTER_H__
#define __GTK_CLUE_HUNTER_H__


#include	<gtk/gtk.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* --- type macros --- */
#define	GTK_TYPE_CLUE_HUNTER		(gtk_clue_hunter_get_type ())
#define	GTK_CLUE_HUNTER(object)	        (GTK_CHECK_CAST ((object), GTK_TYPE_CLUE_HUNTER, GtkClueHunter))
#define	GTK_CLUE_HUNTER_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GTK_TYPE_CLUE_HUNTER, GtkClueHunterClass))
#define	GTK_IS_CLUE_HUNTER(object)	(GTK_CHECK_TYPE ((object), GTK_TYPE_CLUE_HUNTER))
#define GTK_IS_CLUE_HUNTER_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GTK_TYPE_CLUE_HUNTER))
#define GTK_CLUE_HUNTER_GET_CLASS(obj)  (GTK_CHECK_GET_CLASS ((obj), GTK_TYPE_CLUE_HUNTER, GtkClueHunterClass))

/* --- typedefs --- */
typedef	struct	_GtkClueHunterClass GtkClueHunterClass;
typedef	struct	_GtkClueHunter	    GtkClueHunter;


/* --- structures --- */
struct	_GtkClueHunter
{
  GtkWindow	 window;

  guint		 popped_up : 1;
  guint		 completion_tag : 1;
  guint		 pattern_matching : 1;
  guint		 keep_history : 1;
  guint		 align_width : 1;
  guint		 clist_column : 16;

  gchar		*cstring;

  GtkWidget	*scw;
  GtkWidget	*clist;
  GtkWidget	*entry;
};
struct	_GtkClueHunterClass
{
  GtkWindowClass	parent_class;

  void	(*activate)	(GtkClueHunter	*clue_hunter);
  void	(*popup)	(GtkClueHunter	*clue_hunter);
  void	(*popdown)	(GtkClueHunter	*clue_hunter);
  void	(*select_on)	(GtkClueHunter	*clue_hunter,
			 const gchar	*string);
};


/* --- prototypes --- */
GtkType	   gtk_clue_hunter_get_type	        (void);
void	   gtk_clue_hunter_popup	        (GtkClueHunter	*clue_hunter);
void	   gtk_clue_hunter_set_clist	        (GtkClueHunter	*clue_hunter,
						 GtkWidget	*clist,
						 guint16	 column);
void	   gtk_clue_hunter_set_entry	        (GtkClueHunter	*clue_hunter,
						 GtkWidget	*entry);
void	   gtk_clue_hunter_add_string	        (GtkClueHunter	*clue_hunter,
						 const gchar	*string);
void	   gtk_clue_hunter_remove_string	(GtkClueHunter	*clue_hunter,
						 const gchar	*string);
void	   gtk_clue_hunter_remove_matches	(GtkClueHunter	*clue_hunter,
						 const gchar	*pattern);
void	   gtk_clue_hunter_set_pattern_matching (GtkClueHunter	*clue_hunter,
						 gboolean	 on_off);
void	   gtk_clue_hunter_set_keep_history     (GtkClueHunter	*clue_hunter,
						 gboolean	 on_off);
void	   gtk_clue_hunter_set_align_width      (GtkClueHunter	*clue_hunter,
						 gboolean	 on_off);
void	   gtk_clue_hunter_select_on	        (GtkClueHunter	*clue_hunter,
						 const gchar	*string);
gchar*	   gtk_clue_hunter_try_complete	        (GtkClueHunter	*clue_hunter);
GtkWidget* gtk_clue_hunter_create_arrow		(GtkClueHunter	*clue_hunter);
GtkClueHunter* gtk_clue_hunter_from_entry	(GtkWidget	*entry);





#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif	/* __GTK_CLUE_HUNTER_H__ */
