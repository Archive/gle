/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef __GLE_DUMPER_H__
#define __GLE_DUMPER_H__


#include	<gle/glegtoplevel.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


typedef enum
{
  GLE_DUMP_NONE			=  0 << 0,
  GLE_DUMP_READWRITE_ARGS	=  1 << 0,
  GLE_DUMP_CONSTRUCT_ARGS	=  1 << 1,
  GLE_DUMP_RECURSIVE		=  1 << 2,
  GLE_DUMP_MASK			= 0x0f,

  GLE_DUMP_ALL_ARGS		= (GLE_DUMP_READWRITE_ARGS |
				   GLE_DUMP_CONSTRUCT_ARGS)
} GleDumpFlags;


gboolean	gle_dump_gobject	(gpointer       stream,
					 const gchar   *indent,
					 gboolean       need_break,
					 GleGObject    *gobject,
					 GleDumpFlags   flags);
gboolean	gle_dump_gwidget	 (gpointer	stream,
					  const gchar  *indent,
					  gboolean      need_break,
					  GleGWidget   *gwidget,
					  GleDumpFlags  flags);



#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif  /* __GLE_DUMPER_H__ */
