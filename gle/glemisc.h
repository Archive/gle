/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef __GLEMISC_H__
#define __GLEMISC_H__


#include	<gtk/gtk.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */



/* --- GLE functions --- */
void		gle_set_flash_widget		(GtkWidget	*widget);
GtkWidget*	gle_get_flash_widget		(void);
void		gle_widget_make_sensitive	(GtkWidget	*widget);
void		gle_widget_make_insensitive	(GtkWidget	*widget);
void		gle_item_factory_adjust		(GtkItemFactory *ifactory,
						 guint           action,
						 gboolean        sensitive,
						 gpointer        user_data);
GtkWidget*	gle_info_window			(const gchar	*title,
						 const gchar	*text);

/* work around Gtk stupidities */
void		gle_file_selection_heal		(GtkFileSelection *fs);
GtkType		gle_type_from_name		(const gchar	*type_name);
       

/* unimplemented gtk stuff
 */
typedef	void	(*GtkCListRowFunc)	(GtkCList	*clist,
					 guint		 row,
					 gpointer	*row_data_p,
					 gpointer	 func_data);
void	gtk_clist_foreach_row	(GtkCList		*clist,
				 GtkCListRowFunc	func,
				 gpointer		func_data);
GList*		gtk_widget_query_arg_list	(GtkWidget *widget,
						 GtkType    class_type);
void		gtk_widget_queue_destroy	(GtkWidget	*widget);
gint		gtk_clist_find_row		(GtkCList	*clist,
						 gpointer	 data);
GtkType		gtk_type_from_arg_name		(const gchar	*arg_name,
						 const gchar	**class_name_p);
void		gtk_idle_show_widget		(GtkWidget	*widget);

gint		gle_clist_selection_info	(GtkCList	*clist,
						 GdkWindow	*window,
						 gint		 x,
						 gint		 y,
						 gint		*row,
						 gint		*column);
gchar*		g_str_canonicalize	(gchar	*string,
					 gchar	 subsitutor,
					 gchar	*charsets,
					 ...); /* NULL terminated */






#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif	/* __GLEMISC_H__ */
