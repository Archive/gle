/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001-2002 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#define GTK_ENABLE_BROKEN
#include "gleutils.h"

#include "gleshell.h"



/* --- variables --- */
static GQuark quark_gle_tag = 0;


/* --- functions --- */
static inline GScanner*
get_scanner (void)
{
  static GScanner *scanner = NULL;

  if (!scanner)
    {
      scanner = g_scanner_new (NULL);
    }
  return scanner;
}

glong
gle_util_parse_long (const gchar *string)
{
  GScanner *scanner = get_scanner ();

  g_scanner_input_text (scanner, string, strlen (string));
  if (g_scanner_get_next_token (scanner) != G_TOKEN_INT)
    return 0;
  return scanner->value.v_int;
}

gdouble
gle_util_parse_double (const gchar *string)
{
  GScanner *scanner = get_scanner ();

  g_scanner_input_text (scanner, string, strlen (string));
  switch (g_scanner_get_next_token (scanner))
    {
    case G_TOKEN_INT:
      return scanner->value.v_int;
    case G_TOKEN_FLOAT:
      return scanner->value.v_float;
    default:
      return 0;
    }
}

GtkTooltips*
gle_util_get_tooltips (void)
{
  static GtkTooltips *tooltips = NULL;

  if (!tooltips)
    {
      tooltips = gtk_tooltips_new ();
      gtk_tooltips_force_window (tooltips);
      GLE_TAG (tooltips->tip_window);
    }
  return tooltips;
}

gint
gle_clist_selection_info (GtkCList  *clist,
			  GdkWindow *window,
			  gint       x,
			  gint       y,
			  gint      *row,
			  gint      *column)
{
  g_return_val_if_fail (clist != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_CLIST (clist), FALSE);

  if (window && window == clist->clist_window)
    return gtk_clist_get_selection_info (clist, x, y, row, column);

  return FALSE;
}

void
gle_item_factory_adjust (GtkItemFactory         *ifactory,
			 guint                   action,
			 GleItemFactoryItemState state,
			 gpointer                user_data)
{
  GtkWidget *widget;

  g_return_if_fail (GTK_IS_ITEM_FACTORY (ifactory));

  widget = gtk_item_factory_get_widget_by_action (ifactory, action);
  if (widget)
    {
      gtk_widget_set_sensitive (widget, state != GLE_ITEM_FACTORY_ITEM_INSENSITIVE);
      (state != GLE_ITEM_FACTORY_ITEM_HIDDEN ? gtk_widget_show : gtk_widget_hide) (widget);
      gtk_object_set_user_data (GTK_OBJECT (widget), user_data);
    }
}

void
gle_util_set_flash_widget (GtkWidget *widget)
{
}

GtkWidget*
gle_info_window (const gchar *title,
		 const gchar *info_text)
{
  GtkWidget *window;
  GtkWidget *main_vbox;
  GtkWidget *any;
  GtkWidget *sb;
  GtkText *text;

  window = g_object_new (GTK_TYPE_WINDOW,
			 "title", title,
			 "allow_shrink", FALSE,
			 "allow_grow", TRUE,
			 NULL);
  main_vbox = g_object_new (GTK_TYPE_VBOX,
			    "homogeneous", FALSE,
			    "spacing", 0,
			    "border_width", 0,
			    "parent", window,
			    "visible", TRUE,
			    NULL);
  any = g_object_new (GTK_TYPE_HBOX,
		      "homogeneous", FALSE,
		      "spacing", 0,
		      "border_width", 5,
		      "parent", main_vbox,
		      "visible", TRUE,
		      NULL);
  sb = g_object_new (GTK_TYPE_VSCROLLBAR,
		     "visible", TRUE,
		     NULL);
  gtk_box_pack_end (GTK_BOX (any), sb, FALSE, TRUE, 0);
  text = g_object_new (GTK_TYPE_TEXT,
		       "visible", TRUE,
		       "vadjustment", GTK_RANGE (sb)->adjustment,
		       "editable", FALSE,
		       "word_wrap", TRUE,
		       "line_wrap", TRUE,
		       /* "object_signal::activate", gtk_window_activate_default, window, */
		       NULL);
  gtk_text_insert (text, NULL, NULL, NULL, info_text, strlen (info_text));
  gtk_widget_set_usize (GTK_WIDGET (text), 300, 400);
  gtk_container_add (GTK_CONTAINER (any), GTK_WIDGET (text));

  any = g_object_new (GTK_TYPE_HSEPARATOR,
		      "visible", TRUE,
		      NULL);
  gtk_box_pack_start (GTK_BOX (main_vbox), any, FALSE, TRUE, 0);
  any = g_object_new (GTK_TYPE_BUTTON,
		      "label", "Close",
		      "border_width", 5,
		      "visible", TRUE,
		      "can_default", TRUE,
		      NULL);
  g_object_connect (any,
		    "swapped_signal::clicked", gtk_widget_destroy, window,
		    NULL);
  gtk_box_pack_start (GTK_BOX (main_vbox), any, FALSE, TRUE, 0);
  gtk_widget_grab_default (any);

  return window;
}

void
gle_util_tag_object (GObject     *object,
		     const gchar *string)
{
  gchar *name;
  g_return_if_fail (G_IS_OBJECT (object));
  g_return_if_fail (string != NULL);
  
  if (!quark_gle_tag)
    quark_gle_tag = g_quark_from_string ("GLE-Internal-Tag");

  /* we tag objects for three reasons:
   * - we don't want GLE to introspect its own objects
   * - GLE widgets need a different style to be distinguished
   *   from the rest of the app and to make things like emphasising
   *   itmes in lists work. for this window names need to be prefixed
   *   with GLE-* so rc-style settings can apply.
   * - no GLE dialog should survive the GLE shell
   */
  name = g_strconcat ("GLE-", string, NULL);
  g_object_set_qdata_full (object, quark_gle_tag, name, g_free);
  if (GTK_IS_WIDGET (object))
    gtk_widget_set_name (GTK_WIDGET (object), name);
  if (GTK_IS_OBJECT (object))
    gle_shell_manage_object (GTK_OBJECT (object));
}

gboolean
gle_util_check_tag (GObject *object)
{
  return g_object_get_qdata (object, quark_gle_tag) != NULL;
}

void
gle_file_selection_heal (GtkFileSelection *fs)
{
  GtkWidget *main_vbox;
  GtkWidget *hbox;
  GtkWidget *any;

  g_return_if_fail (fs != NULL);
  g_return_if_fail (GTK_IS_FILE_SELECTION (fs));

  /* button placement
   */
  gtk_container_set_border_width (GTK_CONTAINER (fs), 0);
  gtk_file_selection_hide_fileop_buttons (fs);
  gtk_widget_ref (fs->main_vbox);
  gtk_container_remove (GTK_CONTAINER (fs), fs->main_vbox);
  gtk_box_set_spacing (GTK_BOX (fs->main_vbox), 0);
  gtk_container_set_border_width (GTK_CONTAINER (fs->main_vbox), 5);
  main_vbox = gtk_widget_new (GTK_TYPE_VBOX,
			      "homogeneous", FALSE,
			      "spacing", 0,
			      "border_width", 0,
			      "parent", fs,
			      "visible", TRUE,
			      NULL);
  gtk_box_pack_start (GTK_BOX (main_vbox), fs->main_vbox, TRUE, TRUE, 0);
  gtk_widget_unref (fs->main_vbox);
  gtk_widget_hide (fs->ok_button->parent);
  hbox = gtk_widget_new (GTK_TYPE_HBOX,
			 "homogeneous", TRUE,
			 "spacing", 0,
			 "border_width", 5,
			 "visible", TRUE,
			 NULL);
  gtk_box_pack_end (GTK_BOX (main_vbox), hbox, FALSE, TRUE, 0);
  gtk_widget_reparent (fs->ok_button, hbox);
  gtk_widget_reparent (fs->cancel_button, hbox);
  gtk_widget_grab_default (fs->ok_button);
  // gtk_label_set_text (GTK_LABEL (GTK_BIN (fs->ok_button)->child), "Ok");
  // gtk_label_set_text (GTK_LABEL (GTK_BIN (fs->cancel_button)->child), "Cancel");

  /* heal the action_area packing so we can customize children
   */
  gtk_box_set_child_packing (GTK_BOX (fs->action_area->parent),
			     fs->action_area,
			     FALSE, TRUE,
			     5, GTK_PACK_START);

  any = gtk_widget_new (GTK_TYPE_HSEPARATOR,
			"visible", TRUE,
			NULL);
  gtk_box_pack_end (GTK_BOX (main_vbox), any, FALSE, TRUE, 0);
  gtk_widget_grab_focus (fs->selection_entry);
}


/* --- compile marshallers --- */
#include "glemarshal.h"
#include "glemarshal.c"
