/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#include	<gle/gleconfig.h>

#include	"gledumper.h"
#include	"glegarg.h"
#include	"glehandler.h"
#include	<stdio.h>


/* --- macros --- */
#define	CARE_BREAK()	G_STMT_START { \
  if (need_break) \
    { \
      fputc ('\n', f_out); \
      INDENT (); \
      need_break = FALSE; \
    } \
} G_STMT_END
#define	INDENT()	fputs (indent, f_out)
#define	NEW_INDENT()	G_STMT_START { \
  old_indent = indent; \
  indent = g_strconcat (old_indent, "  ", NULL); \
} G_STMT_END
#define	RESTORE_INDENT()	G_STMT_START { \
  g_free (indent); \
  indent = old_indent; \
  old_indent = NULL; \
} G_STMT_END
#define	LINE_BREAK()		G_STMT_START { \
  fputc ('\n', f_out); \
  need_break = FALSE; \
} G_STMT_END
#define	DUMP_BUFFER_LEN	(64)


/* --- functions --- */
static gboolean
gle_dump_gobject_header (gpointer	 stream,
			 gchar		*indent,
			 gboolean	 need_break,
			 GleGObject	*gobject,
			 GleDumpFlags	 flags)
{
  register FILE *f_out = stream;

  LINE_BREAK ();

  INDENT ();

  fputc ('(', f_out);
  fputs (gtk_type_name (GLE_GOBJECT_OTYPE (gobject)), f_out);
  fputs (" \"", f_out);
  if (gobject->gname)
    fputs (gobject->gname, f_out);
  fputc ('"', f_out);

  return need_break;
}

static gchar*
gle_dup_quoted_string (const gchar *string)
{
  GString *gstring;
  gchar *str;

  if (!string)
    return g_strdup ("NULL");

  gstring = g_string_new ("\"");
  while (*string)
    {
      switch (*string)
	{
	case '\\':
	  g_string_append (gstring, "\\\\");
	  break;
	case '\t':
	  g_string_append (gstring, "\\t");
	  break;
	case '\n':
	  g_string_append (gstring, "\\n");
	  break;
	case '\r':
	  g_string_append (gstring, "\\r");
	  break;
	case '\b':
	  g_string_append (gstring, "\\b");
	  break;
	case '\f':
	  g_string_append (gstring, "\\f");
	  break;
	default:
	  if (*string > 126 || *string < 32)
	    g_string_sprintfa (gstring, "\\%03o", *string);
	  else
	    g_string_append_c (gstring, *string);
	  break;
	}
      string++;
    }
  g_string_append_c (gstring, '"');

  str = gstring->str;
  g_string_free (gstring, FALSE);

  return str;
}

static gboolean
gle_dump_garg (gpointer		 stream,
	       gchar		*indent,
	       gboolean		 need_break,
	       GleGArg		*garg,
	       GleDumpFlags	 flags)
{
  register FILE *f_out = stream;
  GString *gstring;

  if (!GLE_GARG_IS_CONSTRUCT_ONLY (garg) &&
      gle_arg_values_equal (garg->info->arg, &garg->object_arg))
    return need_break;

  LINE_BREAK ();
  INDENT ();

  gstring = g_string_new ("(");
  if (GLE_GARG_IS_CHILD_ARG (garg))
    g_string_append (gstring, "child-");
  g_string_append (gstring, "arg \"");
  g_string_append (gstring, garg->arg_name);
  g_string_append (gstring, "\" ");

  switch (GTK_FUNDAMENTAL_TYPE (GLE_GARG_ARG_TYPE (garg)))
    {
      gchar buffer[DUMP_BUFFER_LEN + 1];
      guint i, j;
      guint v;
      GtkEnumValue *ev;
      gchar *tmp;

    case GTK_TYPE_DOUBLE:
      i = g_snprintf (buffer, DUMP_BUFFER_LEN, "%.9f", GTK_VALUE_DOUBLE (garg->object_arg));
      j = i - 1;
      while (buffer[j] == '0')
	buffer[j--] = 0;
      g_string_append (gstring, buffer);
      break;
    case GTK_TYPE_FLOAT:
      i = g_snprintf (buffer, DUMP_BUFFER_LEN, "%.6f", GTK_VALUE_FLOAT (garg->object_arg));
      j = i - 1;
      while (buffer[j] == '0')
	buffer[j--] = 0;
      g_string_append (gstring, buffer);
      break;
    case GTK_TYPE_LONG:
      g_string_sprintfa (gstring, "%ld", GTK_VALUE_LONG (garg->object_arg));
      break;
    case GTK_TYPE_ULONG:
      g_string_sprintfa (gstring, "%lu", GTK_VALUE_ULONG (garg->object_arg));
      break;
    case GTK_TYPE_INT:
      g_string_sprintfa (gstring, "%d", GTK_VALUE_INT (garg->object_arg));
      break;
    case GTK_TYPE_UINT:
      g_string_sprintfa (gstring, "%u", GTK_VALUE_UINT (garg->object_arg));
      break;
    case GTK_TYPE_STRING:
      tmp = gle_dup_quoted_string (GTK_VALUE_STRING (garg->object_arg));
      g_string_append (gstring, tmp);
      g_free (tmp);
      break;
    case GTK_TYPE_BOOL:
      g_string_append (gstring, GTK_VALUE_BOOL (garg->object_arg) ? "true" : "false");
      break;
    case GTK_TYPE_ENUM:
      ev = gtk_type_enum_get_values (GLE_GARG_ARG_TYPE (garg));
      v = GTK_VALUE_ENUM (garg->object_arg);
      if (ev)
	while (ev->value_name)
	  {
	    if (ev->value == v)
	      break;
	    ev++;
	  }
      if (ev)
	g_string_sprintfa (gstring, "%s", ev->value_nick);
      else
	g_string_sprintfa (gstring, "%u", v);
      break;
    case GTK_TYPE_FLAGS:
      ev = gtk_type_enum_get_values (GLE_GARG_ARG_TYPE (garg));
      v = GTK_VALUE_FLAGS (garg->object_arg);
      if (ev)
	{
	  guint fill_len = 0;
	  gboolean wrote_value = FALSE;

	  while (ev->value_name)
	    {
	      if (ev->value & v)
		{
		  if (fill_len)
		    {
		      guint fl;

		      g_string_append_c (gstring, '\n');
		      g_string_append (gstring, indent);

		      for (fl = 0; fl < fill_len; fl++)
			g_string_append_c (gstring, ' ');
		    }
		  else
		    fill_len = gstring->len;

		  g_string_append (gstring, ev->value_nick);
		  wrote_value = TRUE;
		}
	      ev++;
	    }
	  if (!wrote_value)
	    g_string_append (gstring, "0x00");
	}
      else
	g_string_sprintfa (gstring, "0x%x", v);
      break;
    default:
      need_break = TRUE;
      g_string_prepend (gstring, "; ");
      g_string_append_c (gstring, '?');
      break;
    }
  g_string_append_c (gstring, ')');

  if (GLE_GARG_IS_CONSTRUCT_ONLY (garg))
    {
      g_string_append (gstring, " ; construct");
      need_break = TRUE;
    }

  fputs (gstring->str, f_out);
  g_string_free (gstring, TRUE);

  return need_break;
}

static gboolean
gle_dump_gobject_data (gpointer		 stream,
		       gchar		*indent,
		       gboolean		 need_break,
		       GleGObject	*gobject,
		       GleDumpFlags	 flags)
{
  register FILE *f_out = stream;
  GleGArgGroup *group;
  GSList *slist;

  if (flags & GLE_DUMP_CONSTRUCT_ARGS)
    for (group = gobject->garg_groups;
	 group < gobject->garg_groups + gobject->n_garg_groups;
	 group++)
      for (slist = group->gargs; slist; slist = slist->next)
	{
	  GleGArg *garg;
	  
	  garg = slist->data;
	  if (GLE_GARG_IS_CONSTRUCT_ONLY (garg))
	    need_break = gle_dump_garg (f_out, indent, need_break, garg, flags);
	}
  
  if (flags & GLE_DUMP_READWRITE_ARGS)
    for (group = gobject->garg_groups;
	 group < gobject->garg_groups + gobject->n_garg_groups;
	 group++)
      for (slist = group->gargs; slist; slist = slist->next)
	{
	  GleGArg *garg;
	  
	  garg = slist->data;
	  
	  if (GLE_GARG_IS_CONSTRUCT_ONLY (garg))
	    continue;
	  
	  if (GLE_GARG_IS_READWRITE (garg) && !GLE_GARG_IS_CONSTRUCT_ONLY (garg))
	    need_break = gle_dump_garg (f_out, indent, need_break, garg, flags);
	}
  
  return need_break;
}

static gboolean
gle_dump_connection (gpointer		 stream,
		     gchar		*indent,
		     gboolean		 need_break,
		     GleConnection	*connection,
		     GleDumpFlags	 flags)
{
  FILE *f_out = stream;
  gchar *buffer;

  LINE_BREAK ();
  INDENT ();

  buffer = g_strdup_printf ((connection->data ?
			     "(connect%s%s ::%s \"%s\" %p)" :
			     "(connect%s%s ::%s \"%s\")"),
			    (connection->ctype & GLE_CONNECTION_OBJECT) ? "-object" : "",
			    (connection->ctype & GLE_CONNECTION_AFTER) ? "-after" : "",
			    gtk_signal_name (connection->signal_id),
			    connection->handler,
			    connection->data);

  fputs (buffer, f_out);
  g_free (buffer);
  
  return need_break;
}

static gboolean
gle_dump_gobject_connections (gpointer		 stream,
			      gchar		*indent,
			      gboolean		 need_break,
			      GleGObject	*gobject,
			      GleDumpFlags	 flags)
{
  FILE *f_out = stream;
  GleConnection *connection;

  for (connection = gobject->connections; connection; connection = connection->next)
    need_break = gle_dump_connection (f_out, indent, need_break, connection, flags);

  return need_break;
}

static gboolean
gle_dump_gwidget_data (gpointer		 stream,
		       gchar		*indent,
		       gboolean		 need_break,
		       GleGWidget	*gwidget,
		       GleDumpFlags	 flags)
{
  return need_break;
}

static gboolean
gle_dump_gobject_flags (gpointer	 stream,
			gchar		*indent,
			gboolean	 need_break,
			GleGObject	*gobject,
			GleDumpFlags	 flags)
{
  register FILE *f_out = stream;

  if (!GLE_GOBJECT_GENERIC_GNAME (gobject))
    {
      LINE_BREAK ();
      INDENT ();
      fputs ("(user-gname)", f_out);
    }

  return need_break;
}

gboolean
gle_dump_gobject (gpointer	 stream,
		  const gchar	*__indent,
		  gboolean	 need_break,
		  GleGObject	*gobject,
		  GleDumpFlags	 flags)
{
  register FILE *f_out = stream;
  gchar *indent;
  gchar *old_indent = NULL;

  indent = g_strdup (__indent);

  gle_gobject_update_all_args (gobject);

  need_break = gle_dump_gobject_header (stream, indent, need_break, gobject, flags);

  NEW_INDENT ();

  need_break = gle_dump_gobject_flags (stream, indent, need_break, gobject, flags);
  need_break = gle_dump_gobject_data (stream, indent, need_break, gobject, flags);
  need_break = gle_dump_gobject_connections (stream, indent, need_break, gobject, flags);

  RESTORE_INDENT ();

  CARE_BREAK ();
  fputc (')', f_out);

  g_free (indent);

  return need_break;
}

gboolean
gle_dump_gwidget (gpointer	 stream,
		  const gchar	*__indent,
		  gboolean	 need_break,
		  GleGWidget	*gwidget,
		  GleDumpFlags	 flags)
{
  register FILE *f_out = stream;
  gchar *indent;
  gchar *old_indent = NULL;
  register GleGObject *gobject = (GleGObject*) gwidget;
  register GList *list;

  indent = g_strdup (__indent);

  gle_gobject_update_all_args (gobject);

  need_break = gle_dump_gobject_header (stream, indent, need_break, gobject, flags);

  NEW_INDENT ();

  need_break = gle_dump_gobject_flags (stream, indent, need_break, gobject, flags);
  need_break = gle_dump_gobject_data (stream, indent, need_break, gobject, flags);
  need_break = gle_dump_gobject_connections (stream, indent, need_break, gobject, flags);

  need_break = gle_dump_gwidget_data (stream, indent, need_break, gwidget, flags);

  if ((flags & GLE_DUMP_RECURSIVE) && GLE_IS_GCONTAINER (gwidget))
    for (list = GLE_GCONTAINER (gwidget)->children; list; list = list->next)
      need_break = gle_dump_gwidget (stream, indent, need_break, list->data, flags);

  RESTORE_INDENT ();

  CARE_BREAK ();
  fputc (')', f_out);

  g_free (indent);

  return need_break;
}
