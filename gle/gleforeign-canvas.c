/* GLE - GObject Layout Editor
 * Copyright (C) 2002 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include "gleforeign.h"


/* --- GtkWidget --- */
static GParamSpec**
foreign_canvas_item_list_object_pspecs (GObjectClass *oclass,
					guint        *n_pspecs)
{
  return g_object_class_list_properties (oclass, n_pspecs);
}

static GQuark gle_foreign_canvas_quark = 0;

static GleProxy*
foreign_canvas_item_get_proxy (GObject *item)
{
  return g_object_get_qdata (item, gle_foreign_canvas_quark);
}

static void
foreign_canvas_item_set_proxy (GObject  *item,
			       GleProxy *proxy)
{
  if (!gle_foreign_canvas_quark)
    gle_foreign_canvas_quark = g_quark_from_static_string ("GLE-foreign-GnomeCanvas-proxy");
  
  g_object_set_qdata (item, gle_foreign_canvas_quark, proxy);
  if (proxy)
    {
      g_signal_connect_swapped (item, "notify", G_CALLBACK (gle_foreign_update_object_params), proxy);
      g_signal_connect_swapped (item, "destroy", G_CALLBACK (gle_foreign_detach_object), proxy);
    }
  else
    {
      GSignalMatchType match = G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA;

      g_signal_handlers_disconnect_matched (item, match, 0, 0, NULL, G_CALLBACK (gle_foreign_update_object_params), proxy);
      g_signal_handlers_disconnect_matched (item, match, 0, 0, NULL, G_CALLBACK (gle_foreign_detach_object), proxy);
    }
}

static GObject*
foreign_canvas_item_construct (GObjectClass *oclass)
{
  GObject *item;

  item = g_object_new (G_OBJECT_CLASS_TYPE (oclass), NULL);
  g_object_ref (item);
  gtk_object_sink (GTK_OBJECT (item));

  return item;
}

static void
foreign_canvas_item_attach (GObject *item)
{
  g_object_ref (item);
  gtk_object_sink (GTK_OBJECT (item));
}

static void
foreign_canvas_item_detach (GObject *item)
{
  g_object_unref (item);
}

static void
foreign_canvas_item_destruct (GObject *item)
{
  gtk_object_destroy (GTK_OBJECT (item));
  g_object_unref (item);
}

static void
foreign_canvas_item_get_object_param (GObject    *item,
				      GParamSpec *pspec,
				      GValue     *value)
{
  g_object_get_property (item, pspec->name, value);
}

static void
foreign_canvas_item_set_object_param (GObject      *item,
				      GParamSpec   *pspec,
				      const GValue *value)
{
  g_object_set_property (item, pspec->name, value);
}

static GObject*
foreign_canvas_item_get_parent (GObject *object)
{
  GObject *parent = NULL;

  g_object_get (object, "parent", &parent, NULL);
  
  return parent ? G_OBJECT (parent) : NULL;
}

static void
foreign_canvas_item_set_parent (GObject *object,
				GObject *parent)
{
  GObject *old_parent = NULL;

  g_object_get (object, "parent", &old_parent, NULL);

  if (!old_parent)
    g_object_set (object, "parent", parent, NULL);
}

static gboolean
foreign_canvas_item_check_toplevel (GObject *object)
{
  return FALSE;
}

static GSList*
foreign_canvas_item_list_ref_children (GObject *object)
{
  GType titem = g_type_from_name ("GnomeCanvasItem");
  GType tgroup = g_type_from_name ("GnomeCanvasGroup");

  if (tgroup && g_type_is_a (G_OBJECT_TYPE (object), tgroup) &&
      g_type_parent (tgroup) == titem)
    {
      GTypeQuery qitem, qgroup;
      guint8 *p;
      gpointer *pointer;
      GSList *node, *slist = NULL;

      g_type_query (titem, &qitem);
      g_type_query (tgroup, &qgroup);
      if (qitem.instance_size + 2 * sizeof (gpointer) != qgroup.instance_size)
	{
	  g_warning ("Binary size mismatch for GnomeCanvasGroup (%d + %d != %d",
		     qitem.instance_size, 2 * sizeof (gpointer), qgroup.instance_size);
	  return NULL;
	}

      p = (guint8*) object;
      p += qitem.instance_size;
      pointer = (gpointer*) p;
      for (node = *pointer; node; node = node->next)
	slist = g_slist_prepend (slist, g_object_ref (node->data));
      return slist;
    }
  return NULL;
}

enum {
  ITEM_SIZE
};

static GleForeignAction canvas_actions[] = {
  { ITEM_SIZE,		"Print Item Size", },
};

static GleForeignActionState
foreign_canvas_item_check_action (GObject          *object,
				  GleForeignAction *action,
				  GleProxy         *proxy)
{
  switch (action->action_type)
    {
    case ITEM_SIZE:
      return TRUE;
    default:
      g_assert_not_reached ();
      return GLE_FOREIGN_ACTION_HIDE;	/* silence compiler */
    }
}

static void
foreign_canvas_item_exec_action (GObject          *object,
				 GleForeignAction *action,
				 GleProxy         *proxy)
{
  switch (action->action_type)
    {
      GTypeQuery query;
    case ITEM_SIZE:
      g_type_query (G_OBJECT_TYPE (object), &query);
      g_print ("Item %p of type `%s' allocates %u bytes.\n", object, G_OBJECT_TYPE_NAME (object),
	       query.type ? query.instance_size : 0);
      break;
    default:
      g_assert_not_reached ();
    }
}

static GleForeign foreign_canvas_item = {
  foreign_canvas_item_list_object_pspecs,
  NULL,	/* list_link_pspecs */
  foreign_canvas_item_get_proxy,
  foreign_canvas_item_set_proxy,
  foreign_canvas_item_construct,
  foreign_canvas_item_attach,
  foreign_canvas_item_detach,
  foreign_canvas_item_destruct,
  foreign_canvas_item_get_object_param,
  foreign_canvas_item_set_object_param,
  NULL, /* get_link_param */
  NULL, /* set_link_param */
  foreign_canvas_item_get_parent,
  foreign_canvas_item_set_parent,
  foreign_canvas_item_check_toplevel,
  foreign_canvas_item_list_ref_children,
  NULL, /* state_hint */
  G_N_ELEMENTS (canvas_actions),
  canvas_actions,
  foreign_canvas_item_check_action,
  foreign_canvas_item_exec_action
};
