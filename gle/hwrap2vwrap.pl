#!/usr/bin/perl -w
#-i.bak

my $skip_level = 0;

while (<>) {

    if (m@/\*<h2v-off>\*/@x) {
	$skip_level += 1;
    }
    if (m@/\*<h2v-on>\*/@x && $skip_level) {
	$skip_level -=1;
    }
    next if (m@/\*<h2v-skip>\*/@x);

    if (!$skip_level || m@/\*<h2v-trans>\*/@x) {
	
	s/Horizontal/Vertical/g;
	s/HWrap/VWrap/g;
	s/HWRAP/VWRAP/g;
	s/hwrap/vwrap/g;
	s/hwbox/vwbox/g;
	s/row/col/g;
	
	s/vexpand/PLACEHOLDER/g;
	s/hexpand/vexpand/g;
	s/PLACEHOLDER/hexpand/g;
	
	s/vfill/PLACEHOLDER/g;
	s/hfill/vfill/g;
	s/PLACEHOLDER/hfill/g;
	
	s/vspacing/PLACEHOLDER/g;
	s/hspacing/vspacing/g;
	s/PLACEHOLDER/hspacing/g;
	
	s/width/PLACEHOLDER/g;
	s/height/width/g;
	s/PLACEHOLDER/height/g;
	
	s/\bx\b/PLACEHOLDER/g;
	s/\by\b/x/g;
	s/PLACEHOLDER/y/g;
    }
    
} continue {
    print or die "-p destination: $!\n";
}
