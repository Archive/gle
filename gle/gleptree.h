/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001-2002 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GLE_PTREE_H__
#define __GLE_PTREE_H__


#include	<gle/gleproxy.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* --- type macros --- */
#define GLE_TYPE_PTREE                  (gle_ptree_get_type ())
#define GLE_PTREE(obj)                  (GTK_CHECK_CAST ((obj), GLE_TYPE_PTREE, GlePTree))
#define GLE_PTREE_CLASS(klass)          (GTK_CHECK_CLASS_CAST ((klass), GLE_TYPE_PTREE, GlePTreeClass))
#define GLE_IS_PTREE(obj)               (GTK_CHECK_TYPE ((obj), GLE_TYPE_PTREE))
#define GLE_IS_PTREE_CLASS(klass)       (GTK_CHECK_CLASS_TYPE ((klass), GLE_TYPE_PTREE))
#define GLE_PTREE_GET_CLASS(obj)        (GTK_CHECK_GET_CLASS ((obj), GLE_TYPE_PTREE, GlePTreeClass))

#define	GLE_RCVAL_PTREE_WIDTH		(300)
#define	GLE_RCVAL_PTREE_HEIGHT		(512)
#define	GLE_RCVAL_PTREE_RESET_COLUMNS	(TRUE)
#define	GLE_RCVAL_PTREE_EXPAND_DEPTH	(3)


/* --- PTree columns --- */
typedef enum
{
  GLE_PTREE_COL_OTYPE,
  GLE_PTREE_COL_STATE,
  GLE_PTREE_COL_PNAME,
  GLE_PTREE_N_COLS
} GlePTreeColumnType;


/* --- typedefs --- */
typedef struct	_GlePTree		GlePTree;
typedef	struct	_GlePTreeClass		GlePTreeClass;
typedef	struct	_GlePTreeRowData	GlePTreeRowData;


/* --- structures --- */
struct	_GlePTree
{
  GtkCTree	   ctree;
  GleProxy	  *proxy;
  GlePTreeRowData *shown_row_data;
  guint		   thaw_handler;
};
struct	_GlePTreeClass
{
  GtkCTreeClass		parent_class;
  
  gboolean		(*button_click)	(GlePTree   *ptree,
					 GleProxy   *proxy,
					 guint	     root_x,
					 guint	     root_y,
					 guint	     button,
					 guint	     time);
};
struct _GlePTreeRowData
{
  GlePTree	*ptree;
  GleProxy	*proxy;
  guint		 is_connected : 1;
  GtkCTreeNode	*ct_node;
};


/* --- prototypes --- */
GtkType		gle_ptree_get_type		(void);
GtkWidget*	gle_ptree_new			(void);
void		gle_ptree_set_proxy		(GlePTree	*ptree,
						 GleProxy	*proxy);
void		gle_ptree_update		(GlePTree	*ptree);
void		gle_ptree_rebuild		(GlePTree	*ptree);
gboolean	gle_ptree_mark_proxy		(GlePTree	*ptree,
						 GleProxy	*proxy);
GleProxy*	gle_ptree_proxy_from_node	(GlePTree	*ptree,
						 GtkCTreeNode	*ct_node);
GSList*		gle_ptree_selected_proxies	(GlePTree	*ptree);
GlePTreeRowData*gle_ptree_row_data_from_proxy	(GlePTree	*ptree,
						 GleProxy	*proxy);
void		gle_ptree_insert_proxy		(GlePTree	*ptree,
						 GleProxy	*proxy);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif	/* __GLE_PTREE_H__ */
