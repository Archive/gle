/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#include        <gle/gleconfig.h>

#include        "glefiledialog.h"
#include	"gleshell.h"
#include	"gleparser.h"
#include	"gleprivate.h"
#include	"gledumper.h"
#include	<stdio.h>


/* --- prototypes --- */
static void		gle_file_dialog_class_init            (GleFileDialogClass	*class);
static void		gle_file_dialog_init                  (GleFileDialog		*fd);
static void		gle_file_dialog_destroy               (GtkObject		*object);


/* --- variables --- */
static GtkFileSelection *parent_class = NULL;


/* --- functions --- */
GtkType
gle_file_dialog_get_type (void)
{
  static GtkType file_dialog_type = 0;

  if (!file_dialog_type)
    {
      GtkTypeInfo file_dialog_info =
      {
	"GleFileDialog",
	sizeof (GleFileDialog),
	sizeof (GleFileDialogClass),
	(GtkClassInitFunc) gle_file_dialog_class_init,
	(GtkObjectInitFunc) gle_file_dialog_init,
	/* reserved_1 */ NULL,
	/* reserved_2 */ NULL,
	(GtkClassInitFunc) NULL,
      };

      file_dialog_type = gtk_type_unique (GTK_TYPE_FILE_SELECTION, &file_dialog_info);
    }

  return file_dialog_type;
}

static void
gle_file_dialog_class_init (GleFileDialogClass	*class)
{
  parent_class = gtk_type_class (GTK_TYPE_FILE_SELECTION);
}

static void
gle_file_dialog_init (GleFileDialog *fd)
{
  GLE_TAG (fd, "GLE-FileDialog");

  gtk_widget_set (GTK_WIDGET (fd),
		  "GtkWindow::title", "GleFileDialog",
		  NULL);
  
  gle_file_selection_heal (GTK_FILE_SELECTION (fd));
}

static void
gle_file_dialog_destroy (GtkObject *object)
{
  GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static void
gle_file_dialog_open (GleFileDialog *fd)
{
  gchar *file_name;
  GleParserData *pdata;
  GSList *slist;

  file_name = g_strdup (gtk_file_selection_get_filename (GTK_FILE_SELECTION (fd)));

  pdata = gle_parser_data_from_file (file_name);
  if (!pdata)
    {
      g_message ("No souch resource \"%s\"", file_name);
      g_free (file_name);
      return;
    }
  g_free (file_name);

  for (slist = pdata->gtoplevels; slist; slist = slist->next)
    gle_shell_own_gtoplevel (slist->data);

  gle_parser_data_destroy (pdata);

  gtk_widget_destroy (GTK_WIDGET (fd));
}

static void
gle_file_dialog_save (GleFileDialog *fd)
{
  gchar *file_name;
  FILE *f_out;
  GList *list, *gtoplevels;
  GtkWidget *radio;

  file_name = g_strdup (gtk_file_selection_get_filename (GTK_FILE_SELECTION (fd)));

  f_out = fopen (file_name, "w");
  if (!f_out)
    {
      g_message ("Can not save \"%s\"", file_name);
      g_free (file_name);
      return;
    }

  radio = gtk_object_get_data (GTK_OBJECT (fd), "radio-all");
  if (radio && GTK_TOGGLE_BUTTON (radio)->active)
    gtoplevels = gle_shell_list_all ();
  else
    gtoplevels = gle_shell_list_selected ();
  if (!gtoplevels)
    {
      g_message ("Nothing to save for \"%s\"", file_name);
      g_free (file_name);
      return;
    }
  g_free (file_name);
  
  for (list = gtoplevels; list; list = list->next)
    gle_dump_gwidget (f_out,
		      "", FALSE,
		      list->data,
		      GLE_DUMP_ALL_ARGS | GLE_DUMP_RECURSIVE);
  g_list_free (gtoplevels);

  fputc ('\n', f_out);
  fclose (f_out);

  gtk_widget_destroy (GTK_WIDGET (fd));
}

GtkWidget*
gle_file_dialog_new_open (const gchar *pattern)
{
  GtkWidget *dialog;

  dialog =
    gtk_widget_new (GLE_TYPE_FILE_DIALOG,
		    "title", "GLE Open File",
		    NULL);
  gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (dialog)->ok_button),
			     "clicked",
			     GTK_SIGNAL_FUNC (gle_file_dialog_open),
			     GTK_OBJECT (dialog));
  gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (dialog)->cancel_button),
			     "clicked",
			     GTK_SIGNAL_FUNC (gtk_widget_destroy),
			     GTK_OBJECT (dialog));
  if (pattern)
    {
      gtk_file_selection_complete (GTK_FILE_SELECTION (dialog), pattern);
      gtk_file_selection_set_filename (GTK_FILE_SELECTION (dialog), "");
    }

  return dialog;
}

GtkWidget*
gle_file_dialog_new_save (const gchar *pattern)
{
  GtkWidget *dialog;
  GtkWidget *frame;
  GtkWidget *vbox;
  GtkWidget *radio1;
  GtkWidget *radio2;

  dialog = gtk_widget_new (GLE_TYPE_FILE_DIALOG,
			   "title", "GLE Save File",
			   NULL);
  
  frame = gtk_widget_new (GTK_TYPE_FRAME,
			  "label", "Contents",
			  "visible", TRUE,
			  "parent", GTK_FILE_SELECTION (dialog)->action_area,
			  NULL);
  vbox = gtk_widget_new (GTK_TYPE_VBOX,
			 "visible", TRUE,
			 "parent", frame,
			 NULL);
  radio1 = gtk_widget_new (GTK_TYPE_RADIO_BUTTON,
			   "label", "Save all toplevels",
			   "visible", TRUE,
			   "parent", vbox,
			   "can_focus", FALSE,
			   NULL);
  gtk_misc_set_alignment (GTK_MISC (GTK_BIN (radio1)->child), 0, .5);
  gtk_object_set_data (GTK_OBJECT (dialog), "radio-all", radio1);
  radio2 = gtk_widget_new (GTK_TYPE_RADIO_BUTTON,
			   "label", "Save selected toplevels",
			   "visible", TRUE,
			   "parent", vbox,
			   "group", radio1,
			   "can_focus", FALSE,
			   NULL);
  gtk_misc_set_alignment (GTK_MISC (GTK_BIN (radio2)->child), 0, .5);
  
  gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (dialog)->ok_button),
			     "clicked",
			     GTK_SIGNAL_FUNC (gle_file_dialog_save),
			     GTK_OBJECT (dialog));
  gtk_signal_connect_object (GTK_OBJECT (GTK_FILE_SELECTION (dialog)->cancel_button),
			     "clicked",
			     GTK_SIGNAL_FUNC (gtk_widget_destroy),
			     GTK_OBJECT (dialog));
  if (pattern)
    gtk_file_selection_set_filename (GTK_FILE_SELECTION (dialog), pattern);

  return dialog;
}
