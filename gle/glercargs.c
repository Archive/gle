/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#include	"glercargs.h"
#include	<gtk/gtk.h>
#include	<stdarg.h>
#include	<fcntl.h>
#include	<stdio.h>
#include	<string.h>
#include	<sys/stat.h>
#include	<unistd.h>

/* --- prototypes --- */
static guint	ifactory_parser	(GScanner		*scanner,
				 GleRcCallback		*cb_arg);
static void	ifactory_dumper	(void			*stream,
				 GleRcCallback		*cb_arg,
				 gchar			*indent);


/* --- variables --- */
static GQuark rc_id_shell = 0;
static GQuark rc_id_wtree = 0;
static GQuark rc_id_ptree = 0;
static GQuark rc_id_plist = 0;
static GQuark rc_id_editor = 0;
static GQuark rc_id_wpalette = 0;
static GQuark rc_id_ted = 0;
static GQuark rc_id_global = 0;
static GleRcBool rc_args_bool[] = {
  { _GLE_RCARG_SHELL_TRACK_MOVEMENTS,	GLE_RC_BOOL,
    &rc_id_shell,	GLE_RC_FORCE_DUMP | GLE_RC_PREFERENCES,
    "Track movement of the GleShell window?",
    TRUE },
  { _GLE_RCARG_SHELL_USE_POSITION,	GLE_RC_BOOL,
    &rc_id_shell,	GLE_RC_FORCE_DUMP | GLE_RC_PREFERENCES,
    "Track movement of the GleShell window, and place it\n"
    "at the saved position upon popup?",
    FALSE },
  { _GLE_RCARG_AUTO_SAVE_GLERC,		GLE_RC_BOOL,
    NULL,		GLE_RC_FORCE_DUMP | GLE_RC_PREFERENCES,
    "Automatically save ~/.glerc upon exit of GleShell?",
    TRUE },
  { _GLE_RCARG_WTREE_RESET_COLUMNS,	GLE_RC_BOOL,
    &rc_id_wtree,	GLE_RC_FORCE_DUMP | GLE_RC_PREFERENCES,
    "Automatically reset GleWTree column widths on rebuilds?",
    TRUE },
  { _GLE_RCARG_PTREE_RESET_COLUMNS,	GLE_RC_BOOL,
    &rc_id_ptree,	GLE_RC_FORCE_DUMP | GLE_RC_PREFERENCES,
    "Automatically reset GlePTree column widths on rebuilds?",
    TRUE },
  { _GLE_RCARG_EDITOR_CHILD_ARGS_FIRST,	GLE_RC_BOOL,
    &rc_id_editor,	GLE_RC_FORCE_DUMP | GLE_RC_PREFERENCES,
    "Should the `Child Args' page be the first one in the widget editor notebook?",
    TRUE },
  { _GLE_RCARG_WPALETTE_FEATURE_WTREES,	GLE_RC_BOOL,
    &rc_id_wpalette,	GLE_RC_FORCE_DUMP | GLE_RC_PREFERENCES,
    "Should the currently candidating container be shown in associated widget trees?",
    TRUE },
  { _GLE_RCARG_WPALETTE_REBUILD_WTREES,	GLE_RC_BOOL,
    &rc_id_wpalette,	GLE_RC_FORCE_DUMP | GLE_RC_PREFERENCES,
    "Should all associated widget trees get rebuild automatically on widget addition?",
    TRUE },
};
static GleRcLong rc_args_long[] = {
  { _GLE_RCARG_SHELL_INIT_X,		GLE_RC_LONG,
    &rc_id_shell,	GLE_RC_FORCE_DUMP,
    "Initial X-position for the GleShell window",
    -1, -1, 4096 },
  { _GLE_RCARG_SHELL_INIT_Y,		GLE_RC_LONG,
    &rc_id_shell,	GLE_RC_FORCE_DUMP,
    "Initial Y-position for the GleShell window",
    -1, -1, 4096 },
  { _GLE_RCARG_SHELL_WIDTH,		GLE_RC_LONG,
    &rc_id_shell,	GLE_RC_FORCE_DUMP,
    "Initial width for the GleShell window",
    250, 0, 4096 },
  { _GLE_RCARG_SHELL_HEIGHT,		GLE_RC_LONG,
    &rc_id_shell,	GLE_RC_FORCE_DUMP,
    "Initial height for the GleShell window",
    300, 0, 4096 },
  { _GLE_RCARG_WTREE_HEIGHT,		GLE_RC_LONG,
    &rc_id_wtree,	GLE_RC_FORCE_DUMP,
    "Initial height for GleWTree",
    300, 10, 4096 },
  { _GLE_RCARG_WTREE_WIDTH,		GLE_RC_LONG,
    &rc_id_wtree,	GLE_RC_FORCE_DUMP,
    "Initial width for GleWTree",
    200, 10, 4096 },
  { _GLE_RCARG_WTREE_EXPAND_DEPTH,	GLE_RC_LONG,
    &rc_id_wtree,	GLE_RC_FORCE_DUMP | GLE_RC_PREFERENCES,
    "Initial expand depth of GleWTree",
    3, 0, 999999 },
  { _GLE_RCARG_PTREE_HEIGHT,		GLE_RC_LONG,
    &rc_id_ptree,	GLE_RC_FORCE_DUMP,
    "Initial height for GlePTree",
    300, 10, 4096 },
  { _GLE_RCARG_PTREE_WIDTH,		GLE_RC_LONG,
    &rc_id_ptree,	GLE_RC_FORCE_DUMP,
    "Initial width for GlePTree",
    250, 10, 4096 },
  { _GLE_RCARG_PTREE_EXPAND_DEPTH,	GLE_RC_LONG,
    &rc_id_ptree,	GLE_RC_FORCE_DUMP | GLE_RC_PREFERENCES,
    "Initial expand depth of GlePTree",
    3, 0, 999999 },
  { _GLE_RCARG_WPALETTE_N_COLUMNS,	GLE_RC_LONG,
    &rc_id_wpalette,	GLE_RC_FORCE_DUMP | GLE_RC_PREFERENCES,
    "Number of columns of the widget palette",
    7, 2, 30 },
  { _GLE_RCARG_TOOLTIPS_DELAY,	GLE_RC_LONG,
    &rc_id_global,	GLE_RC_FORCE_DUMP | GLE_RC_PREFERENCES,
    "Tooltips delay for tooltips used throughout GLE",
    500, 0, 10000 },
  { _GLE_RCARG_PLIST_HEIGHT,		GLE_RC_LONG,
    &rc_id_plist,	GLE_RC_FORCE_DUMP,
    "Initial height for GlePList",
    200, 10, 4096 },
  { _GLE_RCARG_PLIST_WIDTH,		GLE_RC_LONG,
    &rc_id_wtree,	GLE_RC_FORCE_DUMP,
    "Initial width for GlePList",
    200, 10, 4096 },
};
static GleRcString rc_args_string[] = {
  { _GLE_RCARG_SHELL_SELECTOR_NAME,	GLE_RC_STRING,
    &rc_id_shell,	GLE_RC_FORCE_DUMP,
    "Button name to start the window selector",
    "Selector", NULL },
};
static GleRcCallback	menu_callback_arg =
{
  "menu-accelerators",			GLE_RC_CALLBACK,
  NULL,			GLE_RC_ZERO,
  "List of menu paths and their corresponding accelerator bindings",
  NULL,
  ifactory_parser,
  ifactory_dumper,
  NULL,
};
GleRcHandle     *gle_rc = NULL;


/* --- functions --- */
void
gle_rc_args_init (void)
{
  if (!gle_rc)
    {
      guint i;

      gle_rc = gle_rc_handle_new ("GLE");

      rc_id_shell = g_quark_from_static_string ("Shell");
      rc_id_wtree = g_quark_from_static_string ("Widget Tree");
      rc_id_plist = g_quark_from_static_string ("Proxy List");
      rc_id_editor = g_quark_from_static_string ("Editor");
      rc_id_ted = g_quark_from_static_string ("Ted");
      rc_id_wpalette = g_quark_from_static_string ("Widget Palette");
      rc_id_global = g_quark_from_static_string ("Global");
      
      for (i = 0; i < sizeof (rc_args_bool) / sizeof (rc_args_bool[0]); i++)
	gle_rc_declare_arg (gle_rc, (GleRcArg*) (rc_args_bool + i));
      for (i = 0; i < sizeof (rc_args_long) / sizeof (rc_args_long[0]); i++)
	gle_rc_declare_arg (gle_rc, (GleRcArg*) (rc_args_long + i));
      for (i = 0; i < sizeof (rc_args_string) / sizeof (rc_args_string[0]); i++)
	gle_rc_declare_arg (gle_rc, (GleRcArg*) (rc_args_string + i));

      gle_rc_declare_arg (gle_rc, (GleRcArg*) &menu_callback_arg);
    }
}

static guint
ifactory_parser (GScanner		*scanner,
		 GleRcCallback		*cb_arg)
{
  gtk_item_factory_parse_rc_scanner (scanner);

  g_scanner_get_next_token (scanner);

  return scanner->token == ')' ? G_TOKEN_NONE : ')';
}

static void
arg_print_func (gpointer  user_data,
		gchar	 *str)
{
  gpointer *d = user_data;
  FILE *f_out = d[0];
  gchar *indent = d[1];
  gchar *gle_key = "(menu-path \"<GLE";
  gchar *p;

  p = str;
  while (*p && *p != '(')
    p++;

  if (*p == '(' && strncmp (p, gle_key, strlen (gle_key)) == 0)
    {
      fputs (indent, f_out);
      fputs (str, f_out);
      fputc ('\n', f_out);
    }
}

static void
ifactory_dumper (void			*stream,
		 GleRcCallback		*cb_arg,
		 gchar			*indent)
{
  gpointer d[2];

  d[0] = stream;
  d[1] = indent;
  fputc ('\n', stream);
  gtk_item_factory_dump_items (NULL, FALSE, arg_print_func, d);
}
