static struct { gchar *name; gchar *text; } gle_docus[] = {
  { "../docs/introduction.txt",
"Introduction
------------
Eventhough this is the second time I'm implementing a GUI builder interface,
I still find it occasionally hard to present the user with a decent layout
while providing (preserving ;) all of the underlying functionality.
Since Gle is not an application in itself, but merely a shared library or
gtk-module, which presents it's own dialogs in coexistance with the ones
provided by an actuall application, it needs to organize its dialogs in a
special manner, so they are easily distinguishable from the application
provided ones, and needs to also provide facilities to reinvoke its GUI
after it has been intentionally shut down.
So in order to get the occasional or novice user an easy start, I've tried
to briefly summarize Gle's most important basic concepts in the following
lines. (Commentaries on this are more than welcome).


Gle's GUI
---------
Gle implements the concept of one main window, the GleShell, and from there
on, different and context specific assistant dialogs can be launched. If the
GleShell is shutdown, all existing Gle dialogs will also immediatedly get
closed, just as if Gle was an application and would have been quitted.
Assistant dialogs are e.g. widget editors or tree views, here Gle will
associate several distinct dialogs with specific widgets, rather than
using on global window to serve multiple widgets.
The lists and trees that Gle provides, usually feature a right-click popup
menu, offering available operations for the list/tree item in question,
eventually leading to further widget specific dialogs.
For lists and trees that feature multiple selections, group operations are
supported through the accelerators shown in the popup menus.


Gle Proxy
---------
Gle doesn't directly deal with GtkWidgets for a variety of reasons, one of
which is that Gle needs to store widget specific informations across a
widgets lifetime. To hold this information persistant and to provide a means
of \"templates\" that widgets can be instantiated from, Gle introduces a \"Proxy\",
a ``container''-like structure, that holds specific bits of information about
certain Gtk+ widgets. Proxies and their facilities vary in dependance of the
GtkObject type they actually represent. The GtkWidget related proxies link
together in a child-parent relationship tree, similar to the widget hierachies
in Gtk+.
Any proxy is either associated with an already existing Gtk+ widget - we say:
the proxy is instantiated - or is sitting around in order to be used as a
template that's yet to be instantiated.
Using an existing and not instantiated proxy tree, whole Gtk+ dialogs can be
build, with the proxies acting as templates for the widgets to be created.
Since proxies can be used either from within an existing program, or from the
user operating Gle's GUI, they introduce a specific concept of ownership, which
indicates whether a proxy is owned by the GUI user or some program internals.
The GleShell marks internal proxies with a tools icon in the second column
of the proxy list, unmarked proxies are owned by the GUI user and are not
flagged initially. By clicking into the flag column on an unmarked proxies,
an auto-destruction flag for the shell can be toggled, indicated by a small
bomb icon. Destruction of the proxies or their associated widgets will
immediatedly shutdown the GleShell as well.
The leftmost icon in the proxy list indicates the type of widget contained
by the proxy, not yet instantiated proxies are shown as a crossed box.


Tour through the dialogs
------------------------
GleShell -
This is Gle's main window, it contains a list of existing toplevel widget
proxies which features a right-button popup menu to perform proxy specific
operations, like popping up a tree or widget editor, or instantiation of
a proxy tree.
The shell also contains a menubar to open up several other assistent
dialogs and provides facilities to load or save widget proxy trees.
Grouped operations are supported for selected toplevel proxies through the
popup menu's accelerators.

GleEditor -
A GleEditor is a dialog that offers the user possibilities to edit the various
widget or object specific arguments (i.e. properties).
the editor does in fact operate mostly on the object's associated Gle proxy
structure, and thus it is only capable to provide editing facilities for
objects and widgets that have a Gle proxy assigned.

GleTed -
GleTed is the widget tree editor for Gle. It displays the inheritance tree
starting at a certain container and offers a right-click popup menu to
operate on certain widgets. Group operations are facilitated through drag-
selection with the left mouse button and accelerator invokation.
Widgets can be dragged from the tree with the middle mouse button, and can
then be dropped onto a different parent within the same Ted or onto container
widgets displayed in other Ted dialogs.

General notes
-------------
Gle's GUI is still rough on some edges and needs further improvements, it's
also a little unstable, mainly due to lack of constructor argument support
in Gtk. Instantiation of some Gtk widgets may produce warnings or even crash
the whole application due to this, known problems in this regard exist with
GtkCList, GtkCTree, GtkScrolledWindow, GtkViewport and GtkPixmap. Others
may or may not work currently.
Comments, suggestions, feature requests and patches are thus greatly
apprechiated.


	- Tim Janik <timj@gtk.org>
	  1998/10/20" },
}; static guint gle_n_docus = sizeof (gle_docus) / sizeof (gle_docus[0]);
