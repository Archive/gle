/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#include        <gle/gleconfig.h>

#include        "gleprivate.h"
#include        "gleshell.h"


/* -- global variables --- */
const gchar	*gle_key_gle_tag = GLE_PRIVATE_KEY (gle-internal-object-tag);


/* --- functions --- */
void
gle_control_object_life (GtkObject      *object)
{
  gle_shell_manage_object (object);
}

gboolean
gle_is_gle_widget (GtkWidget *widget)
{
  g_return_val_if_fail (widget != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_WIDGET (widget), FALSE);

  while (widget->parent)
    {
      if (GLE_HAS_TAG (widget))
	return TRUE;
      widget = widget->parent;
    }

  return GLE_HAS_TAG (widget);
}
