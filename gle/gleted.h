/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001-2002 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GLE_TED_H__
#define __GLE_TED_H__


#include	<gle/gleproxy.h>
#include	<gle/gleptree.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* --- type macros --- */
#define GLE_TYPE_TED                  (gle_ted_get_type ())
#define GLE_TED(obj)                  (GTK_CHECK_CAST ((obj), GLE_TYPE_TED, GleTed))
#define GLE_TED_CLASS(klass)          (GTK_CHECK_CLASS_CAST ((klass), GLE_TYPE_TED, GleTedClass))
#define GLE_IS_TED(obj)               (GTK_CHECK_TYPE ((obj), GLE_TYPE_TED))
#define GLE_IS_TED_CLASS(klass)       (GTK_CHECK_CLASS_TYPE ((klass), GLE_TYPE_TED))
#define GLE_TED_GET_CLASS(obj)        (GTK_CHECK_GET_CLASS ((obj), GLE_TYPE_TED, GleTedClass))


/* --- typedefs --- */
typedef	struct	_GleTed		GleTed;
typedef	struct	_GleTedClass	GleTedClass;


/* --- structures --- */
struct	_GleTed
{
  GtkWindow	  window;
  
  GleProxy	 *proxy;
  GtkItemFactory *menubar_factory;
  GtkItemFactory *popup_factory;
  GtkWidget	 *window_label;
  GlePTree	 *ptree;
  GtkWidget	 *rebuild_button;
  GtkWidget	 *find_button;
};
typedef struct {
  GtkItemFactory *factory;
  GleForeign     *foreign;
} GleTedFactory;
struct	_GleTedClass
{
  GtkWindowClass  parent_class;
  gchar		 *factories_path;
  guint		  n_factories;
  GleTedFactory  *factories;
};


/* --- operations --- */
typedef enum
{
  GLE_TED_OP_NONE,
  GLE_TED_OP_TREE_UPDATE,
  GLE_TED_OP_TREE_REBUILD,
  GLE_TED_OP_FIND_WIDGET,
  GLE_TED_OP_DELETE,
  GLE_TED_OP_LAST
} GleTedOps;


/* --- prototypes --- */
GtkType		gle_ted_get_type		(void);
GtkWidget*	gle_ted_new			(GleProxy	*proxy);
void		gle_ted_operation		(GleTed		*ted,
						 GleTedOps	 ted_op);
void		gle_ted_mark_proxy		(GleTed		*ted,
						 GleProxy	*proxy);
GleTed*		gle_ted_from_proxy		(GleProxy	*proxy);





#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif	/* __GLE_TED_H__ */
