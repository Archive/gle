/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GLE_PLIST_H__
#define __GLE_PLIST_H__


#include	<gle/gleproxyset.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* --- type macros --- */
#define GLE_TYPE_PLIST                  (gle_plist_get_type ())
#define GLE_PLIST(obj)                  (GTK_CHECK_CAST ((obj), GLE_TYPE_PLIST, GlePList))
#define GLE_PLIST_CLASS(klass)          (GTK_CHECK_CLASS_CAST ((klass), GLE_TYPE_PLIST, GlePListClass))
#define GLE_IS_PLIST(obj)               (GTK_CHECK_TYPE ((obj), GLE_TYPE_PLIST))
#define GLE_IS_PLIST_CLASS(klass)       (GTK_CHECK_CLASS_TYPE ((klass), GLE_TYPE_PLIST))
#define GLE_PLIST_GET_CLASS(obj)        (GTK_CHECK_GET_CLASS ((obj), GLE_TYPE_PLIST, GlePListClass))

#define	GLE_RCVAL_PLIST_WIDTH	(300)
#define	GLE_RCVAL_PLIST_HEIGHT	(300)


/* --- PList columns --- */
typedef enum
{
  GLE_PLIST_COL_ICON,
  GLE_PLIST_COL_TAG,
  GLE_PLIST_COL_PSET_NAME,
  GLE_PLIST_COL_OTYPE,
  GLE_PLIST_COL_PSET_SOURCE,
  GLE_PLIST_N_COLS
} GlePListColumnType;


/* --- typedefs --- */
typedef struct	_GlePList		GlePList;
typedef	struct	_GlePListClass		GlePListClass;
typedef	struct	_GlePListRowData	GlePListRowData;
typedef struct  _GlePListPix		GlePListPix;


/* --- structures --- */
struct	_GlePList
{
  GtkCList	   clist;
  
  GlePListRowData *shown_row_data;
  GSList	  *row_datas;
  GlePListPix	  *pix_cache;
  gchar		 **unknown_xpm;
};
struct	_GlePListClass
{
  GtkCListClass		parent_class;
  
  gboolean		(*button_click)	(GlePList    *plist,
					 GleProxySet *pset,
					 guint	      root_x,
					 guint	      root_y,
					 guint	      button,
					 guint	      time);
  GQuark		(*check_tag)	(GlePList    *plist,
					 GleProxySet *pset);
};
struct _GlePListRowData
{
  GlePList	*plist;
  GleProxySet	*pset;
};
struct _GlePListPix
{
  GQuark id;
  GdkPixmap *pixmap;
  GdkBitmap *bitmap;
  GlePListPix *next;
};


/* --- prototypes --- */
GtkType		gle_plist_get_type		(void);
GtkWidget*	gle_plist_new			(void);
void		gle_plist_append		(GlePList	*plist,
						 GleProxySet	*pset);
void		gle_plist_prepend		(GlePList	*plist,
						 GleProxySet	*pset);
void		gle_plist_update		(GlePList	*plist,
						 GleProxySet	*pset);
void		gle_plist_update_all		(GlePList	*plist);
void		gle_plist_remove		(GlePList	*plist,
						 GleProxySet	*pset);
gboolean	gle_plist_mark			(GlePList	*plist,
						 GleProxySet	*pset);
void		gle_plist_clear			(GlePList	*plist);
void		gle_plist_freeze		(GlePList	*plist);
void		gle_plist_thaw			(GlePList	*plist);
GleProxySet*	gle_plist_get_proxy_set		(GlePList	*plist,
						 gint		 row);
GList*		gle_plist_list_selected		(GlePList	*plist);
GList*		gle_plist_list_all		(GlePList	*plist);






#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif	/* __GLE_PLIST_H__ */
