/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001-2002 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GLE_SHELL_H__
#define __GLE_SHELL_H__


#include	<gle/gleproxy.h>
#include	<gle/gleplist.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* --- type macros --- */
#define GLE_TYPE_SHELL                  (gle_shell_get_type ())
#define GLE_SHELL(obj)                  (GTK_CHECK_CAST ((obj), GLE_TYPE_SHELL, GleShell))
#define GLE_SHELL_CLASS(klass)          (GTK_CHECK_CLASS_CAST ((klass), GLE_TYPE_SHELL, GleShellClass))
#define GLE_IS_SHELL(obj)               (GTK_CHECK_TYPE ((obj), GLE_TYPE_SHELL))
#define GLE_IS_SHELL_CLASS(klass)       (GTK_CHECK_CLASS_TYPE ((klass), GLE_TYPE_SHELL))
#define GLE_SHELL_GET_CLASS(obj)        (GTK_CHECK_GET_CLASS ((obj), GLE_TYPE_SHELL, GleShellClass))

#define	GLE_RCVAL_SHELL_WIDTH		(500)
#define	GLE_RCVAL_SHELL_HEIGHT		(350)
#define	GLE_RCVAL_SHELL_USE_POSITION	(FALSE)
#define	GLE_RCVAL_SHELL_INIT_X		(0)
#define	GLE_RCVAL_SHELL_INIT_Y		(0)


/* --- typedefs & structures --- */
typedef	struct	_GleShell	GleShell;
typedef	struct	_GleShellClass	GleShellClass;
struct	_GleShell
{
  GtkWindow	  window;
  
  GlePList	 *plist;
  GtkWidget	 *update_button;
  GtkWidget	 *selector_button;
  
  GtkWidget	 *selector;
  
  GtkItemFactory *menubar_factory;
  GtkItemFactory *popup_factory;
  
  gpointer	 popup_data;
  
  /* unique dialogs */
  GtkWidget	 *rc_pref;
  GtkWidget	 *wpalette;
  GtkWidget	 *file_open_dialog;
  GtkWidget	 *file_save_dialog;
  GtkWidget	 *help_intro;
  GtkWidget	 *help_about;
};
struct	_GleShellClass
{
  GtkWindowClass	parent_class;
  
  gchar			*factories_path;
};


/* --- operations --- */
typedef enum
{
  GLE_SHELL_OP_NONE,
  GLE_SHELL_OP_DELETE,
  GLE_SHELL_OP_OPEN,
  GLE_SHELL_OP_SAVE,
  GLE_SHELL_OP_SELECTOR,
  GLE_SHELL_OP_UPDATE,
  
  /* proxy ops */
  GLE_SHELL_OP_NEW_WINDOW,
  
  /* dialogs */
  GLE_SHELL_OP_RC_PREF,
  GLE_SHELL_OP_WPALETTE,
  GLE_SHELL_OP_HELP_INTRO,
  GLE_SHELL_OP_HELP_ABOUT,
  GLE_SHELL_OP_LAST
} GleShellOps;


/* --- prototypes --- */
GtkType		gle_shell_get_type		(void);
GleShell*	gle_shell_popup			(void);
gboolean	gle_shell_is_popped_up		(void);
void		gle_shell_operation		(GleShell	*shell,
						 GleShellOps	 shell_op);
void		gle_shell_manage_object		(GtkObject	*object);
GList*		gle_shell_list_selected		(void);
GList*		gle_shell_list_all		(void);
void		gle_shell_rip_gtk_window	(GtkWindow	*window);


/* --- internal --- */
void		gle_shell_addremove_proxy_set	(GleProxySet	*pset,
						 gboolean	 is_add);
     





#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif	/* __GLE_SHELL_H__ */
