/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef __GLE_RC_H__
#define __GLE_RC_H__

#include	<gle/glemisc.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


typedef enum
{
  GLE_RC_ZERO			=  0x00,
  GLE_RC_FORCE_DUMP		=  1 << 0,
  GLE_RC_AUTOMATIC		=  1 << 1,
  GLE_RC_PREFERENCES		=  1 << 2,
  GLE_RC_MASK			=  0x07
} GleRcFlags;

typedef enum
{
  GLE_RC_NONE,
  GLE_RC_BOOL,
  GLE_RC_LONG,
  GLE_RC_DOUBLE,
  GLE_RC_STRING,
  GLE_RC_CALLBACK,
  GLE_RC_LAST
} GleRcType;

typedef struct _GleRcHandle	GleRcHandle;
typedef struct _GleRcAny	GleRcAny;
typedef struct _GleRcBool	GleRcBool;
typedef struct _GleRcLong	GleRcLong;
typedef struct _GleRcDouble	GleRcDouble;
typedef struct _GleRcString	GleRcString;
typedef	struct _GleRcCallback	GleRcCallback;
typedef union  _GleRcArg	GleRcArg;
typedef union  _GleRcValue	GleRcValue;

struct _GleRcHandle
{
  gpointer user_data;
};

struct _GleRcAny
{
  gchar		*name;
  GleRcType	 type;
  GQuark	*group_id;
  GleRcFlags	 flags;
  gchar		*description;
};

struct _GleRcBool
{
  gchar         *name;
  GleRcType      type;
  GQuark	*group_id;
  GleRcFlags     flags;
  gchar         *description;
  gboolean	 default_value;
  gboolean	 value;
};

struct _GleRcLong
{
  gchar         *name;
  GleRcType      type;
  GQuark	*group_id;
  GleRcFlags     flags;
  gchar         *description;
  glong		 default_value;
  glong		 minimum;
  glong		 maximum;
  glong		 value;
};

struct _GleRcDouble
{
  gchar         *name;
  GleRcType      type;
  GQuark	*group_id;
  GleRcFlags     flags;
  gchar         *description;
  gdouble	 default_value;
  gdouble	 minimum;
  gdouble	 maximum;
  gdouble	 value;
};

struct _GleRcString
{
  gchar         *name;
  GleRcType      type;
  GQuark	*group_id;
  GleRcFlags     flags;
  gchar         *description;
  gchar		*default_value;
  gchar		*value;
};

struct _GleRcCallback
{
  gchar         *name;
  GleRcType      type;
  GQuark	*group_id;
  GleRcFlags     flags;
  gchar         *description;
  gpointer	 default_value;
  guint		(*parse_arg)	(GScanner      *,
				 GleRcCallback *);
  void		(*dump_arg)	(void          *stream,
				 GleRcCallback *,
				 gchar         *indent);
  gboolean	(*has_default)	(GleRcCallback *);
  gpointer	 data1	/* parse_arg data */;
  gpointer	 data2	/* dump_arg data */;
  gpointer	 data3	/* has_default data */;
  gpointer	 value	/* value */;
};

union _GleRcArg
{
  gchar			*name;
  GleRcAny		 any;
  GleRcBool		 abool;
  GleRcLong		 along;
  GleRcDouble		 adouble;
  GleRcString		 astring;
  GleRcCallback		 acallback;
};

union _GleRcValue
{
  gboolean	 v_bool;
  glong		 v_long;
  gdouble	 v_double;
  gchar		*v_string;
  gpointer	 v_callback;
};

typedef	void	(*GleRcHookFunc)	(gpointer	 data,
					 GleRcArg	*arg);

GleRcHandle*	gle_rc_handle_new	(const gchar	*prg_name);
const gchar*	gle_rc_handle_prg_name	(GleRcHandle	*rc);
void		gle_rc_declare_arg	(GleRcHandle	*rc,
					 GleRcArg	*arg);
void		gle_rc_parse		(GleRcHandle	*rc,
					 const gchar	*file_name);
void		gle_rc_dump		(GleRcHandle	*rc,
					 const gchar	*file_name);
GList*		gle_rc_get_arg_list	(GleRcHandle	*rc);
guint		gle_rc_add_hook		(GleRcHandle	*rc,
					 const gchar    *name,
					 GleRcHookFunc	 changed_hook,
					 gpointer	 data);
guint		gle_rc_add_hook_full	(GleRcHandle	*rc,
					 const gchar    *name,
					 GleRcHookFunc	 changed_hook,
					 gpointer	 data,
					 GDestroyNotify	 destroy);
void		gle_rc_remove_hook	(GleRcHandle	*rc,
					 guint		 hook_id);

/* get rc argument values */
gboolean	gle_rc_get_bool		(GleRcHandle	*rc,
					 const gchar	*name);
glong		gle_rc_get_long		(GleRcHandle	*rc,
					 const gchar	*name);
gdouble		gle_rc_get_double	(GleRcHandle	*rc,
					 const gchar	*name);
const gchar*	gle_rc_get_string	(GleRcHandle	*rc,
					 const gchar	*name);
GleRcArg*	gle_rc_get_arg		(GleRcHandle	*rc,
					 const gchar	*name);

/* set rc argument values */
void		gle_rc_set_bool		(GleRcHandle	*rc,
					 const gchar	*name,
					 gboolean	 value);
void		gle_rc_set_long		(GleRcHandle	*rc,
					 const gchar	*name,
					 glong		 value);
void		gle_rc_set_double	(GleRcHandle	*rc,
					 const gchar	*name,
					 gdouble         value);
void		gle_rc_set_string	(GleRcHandle	*rc,
					 const gchar	*name,
					 const gchar    *value);


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GLE_RC_H__ */
