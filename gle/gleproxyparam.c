/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include "gleproxyparam.h"


/* --- functions --- */
GleProxyParam*
gle_proxy_param_check_create (GleProxy   *proxy,
			      GParamSpec *pspec)
{
  GleProxyParam *param = NULL;
  
  /* g_return_val_if_fail (GLE_IS_PROXY (proxy), NULL); */
  g_return_val_if_fail (G_IS_PARAM_SPEC (pspec), NULL);

  if (pspec->flags & G_PARAM_READABLE)
    {
      param = g_new0 (GleProxyParam, 1);
      param->pspec = g_param_spec_ref (pspec);
    }
  return param;
}

void
gle_proxy_param_set (GleProxyParam *param,
		     const GValue  *value)
{
  g_return_if_fail (param != NULL);
  g_return_if_fail (G_IS_PARAM_SPEC (param->pspec));
  g_return_if_fail (G_IS_VALUE (value));

  if (!g_param_value_convert (param->pspec, value, &param->value, FALSE))
    g_warning ("%s: unable to convert param value \"%s\" from `%s' to `%s'",
	       G_STRLOC, param->pspec->name,
	       G_VALUE_TYPE_NAME (value),
	       G_VALUE_TYPE_NAME (&param->value));
}

gboolean
gle_proxy_param_get (GleProxyParam *param,
		     GValue        *value)
{
  g_return_val_if_fail (param != NULL, FALSE);
  g_return_val_if_fail (G_IS_PARAM_SPEC (param->pspec), FALSE);
  g_return_val_if_fail (G_IS_VALUE (value), FALSE);

  if (!g_value_transform (&param->value, value))
    {
      g_warning ("%s: unable to transform param value \"%s\" from `%s' to `%s'",
		 G_STRLOC, param->pspec->name,
		 G_VALUE_TYPE_NAME (&param->value),
		 G_VALUE_TYPE_NAME (value));
      return FALSE;
    }
  else
    return TRUE;
}

void
gle_proxy_param_update (GleProxyParam     *param,
			GleForeignGetParam get_func,
			GObject           *object)
{
  GValue value = { 0, };

  g_return_if_fail (param != NULL);
  g_return_if_fail (G_IS_PARAM_SPEC (param->pspec));
  g_return_if_fail (G_IS_OBJECT (object));
  g_return_if_fail (get_func != NULL);

  g_value_init (&value, G_PARAM_SPEC_VALUE_TYPE (param->pspec));
  get_func (object, param->pspec, &value);
  gle_proxy_param_set (param, &value);
  g_value_unset (&value);
}

void
gle_proxy_param_apply (GleProxyParam     *param,
		       GleForeignSetParam set_func,
		       GObject           *object)
{
  GValue value = { 0, };

  g_return_if_fail (param != NULL);
  g_return_if_fail (G_IS_PARAM_SPEC (param->pspec));
  g_return_if_fail (G_IS_OBJECT (object));
  g_return_if_fail (set_func != NULL);

  g_value_init (&value, G_PARAM_SPEC_VALUE_TYPE (param->pspec));
  if (gle_proxy_param_get (param, &value))
    set_func (object, param->pspec, &value);
  g_value_unset (&value);
}

void
gle_proxy_param_set_default (GleProxyParam *param)
{
  g_return_if_fail (param != NULL);
  g_return_if_fail (G_IS_PARAM_SPEC (param->pspec));

  g_param_value_set_default (param->pspec, &param->value);
}

void
gle_proxy_param_free (GleProxyParam *param)
{
  g_return_if_fail (param != NULL);
  g_return_if_fail (G_IS_PARAM_SPEC (param->pspec));

  if (G_IS_VALUE (&param->value))
    g_value_unset (&param->value);
  g_param_spec_unref (param->pspec);
  g_free (param);
}
