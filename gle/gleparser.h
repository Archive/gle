/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef __GLE_PARSER_H__
#define __GLE_PARSER_H__


#include        <gle/glegtoplevel.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */




/* --- typedefs --- */
typedef struct _GleParserData	GleParserData;


/* --- structures --- */
struct _GleParserData
{
  GSList *gtoplevels;
  gpointer gnames;
};

GleParserData*	gle_parser_data_from_file	(const gchar	*file_name);
GleGToplevel*	gle_parser_data_get_gtoplevel	(GleParserData	*pdata,
						 const gchar	*gname);
void		gle_parser_data_destroy		(GleParserData	*pdata);




#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif  /* __GLE_PARSER_H__ */
