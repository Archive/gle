/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GLE_STOCK_H__
#define __GLE_STOCK_H__


#include	<gle/gleutils.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* --- type macros --- */
#define	GLE_XPM_UNKNOWN		("unknown")
#define	GLE_XPM_QUESTION_MARK	("question-mark")
#define	GLE_XPM_PLACEHOLDER	("placeholder")
#define	GLE_XPM_PROXY		("proxy")
#define	GLE_XPM_POPUP		("popup")
#define	GLE_XPM_POPUP_MENU	("popup-menu")
#define	GLE_XPM_SELECTOR	("selector")
#define	GLE_XPM_BOMB		("bomb")
#define	GLE_XPM_CODE		("code")
#define	GLE_XPM_SHELL_OWNED	("shell-owned")


/* --- prototypes --- */
gchar**		gle_stock_get_xpm		(const gchar	*xpm_name);
gchar**		gle_stock_id_get_xpm		(GQuark		 id);
gchar**		gle_stock_get_xpm_unknown	(void);




#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif	/* __GLE_STOCK_H__ */
