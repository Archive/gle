/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef __GLE_RCARGS_H__
#define __GLE_RCARGS_H__

#include	<gle/glerc.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */






/* argument definitions
 */
#define	_GLE_RCARG_SHELL_TRACK_MOVEMENTS	("shell-track-movement")
#define	_GLE_RCARG_SHELL_USE_POSITION		("shell-use-position")
#define	_GLE_RCARG_SHELL_INIT_X			("shell-init-x")
#define	_GLE_RCARG_SHELL_INIT_Y			("shell-init-y")
#define	_GLE_RCARG_SHELL_WIDTH			("shell-width")
#define	_GLE_RCARG_SHELL_HEIGHT			("shell-height")
#define	_GLE_RCARG_SHELL_SELECTOR_NAME		("shell-selector-name")
#define	_GLE_RCARG_AUTO_PLACE_SHELL		("auto-place-shell")
#define	_GLE_RCARG_AUTO_SAVE_GLERC		("auto-save-glerc")
#define	_GLE_RCARG_WTREE_WIDTH			("wtree-width")
#define	_GLE_RCARG_WTREE_HEIGHT			("wtree-height")
#define	_GLE_RCARG_WTREE_RESET_COLUMNS		("wtree-reset-columns")
#define	_GLE_RCARG_WTREE_EXPAND_DEPTH		("wtree-expand-depth")
#define	_GLE_RCARG_PTREE_WIDTH			("ptree-width")
#define	_GLE_RCARG_PTREE_HEIGHT			("ptree-height")
#define	_GLE_RCARG_PTREE_RESET_COLUMNS		("ptree-reset-columns")
#define	_GLE_RCARG_PTREE_EXPAND_DEPTH		("ptree-expand-depth")
#define	_GLE_RCARG_EDITOR_CHILD_ARGS_FIRST	("editor-child-args-first")
#define	_GLE_RCARG_WPALETTE_REBUILD_WTREES	("wpalette-rebuild-wtrees")
#define	_GLE_RCARG_WPALETTE_FEATURE_WTREES	("wpalette-feature-wtrees")
#define	_GLE_RCARG_WPALETTE_N_COLUMNS		("wpalette-n-columns")
#define	_GLE_RCARG_TOOLTIPS_DELAY		("tooltips-delay")
#define	_GLE_RCARG_PLIST_WIDTH			("plist-width")
#define	_GLE_RCARG_PLIST_HEIGHT			("plist-height")

/* value access definitions for the arguments
 */
#define	GLE_RCVAL_(type, name)		  (gle_rc_get_ ## type (gle_rc, _GLE_RCARG_ ## name))
#define	GLE_RCVAL_SHELL_TRACK_MOVEMENTS	  (GLE_RCVAL_ (bool, SHELL_TRACK_MOVEMENTS))
#define	GLE_RCVAL_SHELL_USE_POSITION	  (GLE_RCVAL_ (bool, SHELL_USE_POSITION))
#define	GLE_RCVAL_SHELL_INIT_X		  (GLE_RCVAL_ (long, SHELL_INIT_X))
#define	GLE_RCVAL_SHELL_INIT_Y		  (GLE_RCVAL_ (long, SHELL_INIT_Y))
#define	GLE_RCVAL_SHELL_WIDTH		  (GLE_RCVAL_ (long, SHELL_WIDTH))
#define	GLE_RCVAL_SHELL_HEIGHT		  (GLE_RCVAL_ (long, SHELL_HEIGHT))
#define	GLE_RCVAL_SHELL_SELECTOR_NAME	  (GLE_RCVAL_ (string, SHELL_SELECTOR_NAME))
#define	GLE_RCVAL_AUTO_PLACE_SHELL	  (GLE_RCVAL_ (bool, AUTO_PLACE_SHELL))
#define	GLE_RCVAL_AUTO_SAVE_GLERC	  (GLE_RCVAL_ (bool, AUTO_SAVE_GLERC))
#define	GLE_RCVAL_WTREE_HEIGHT		  (GLE_RCVAL_ (long, WTREE_HEIGHT))
#define	GLE_RCVAL_WTREE_WIDTH		  (GLE_RCVAL_ (long, WTREE_WIDTH))
#define	GLE_RCVAL_WTREE_RESET_COLUMNS	  (GLE_RCVAL_ (bool, WTREE_RESET_COLUMNS))
#define	GLE_RCVAL_WTREE_EXPAND_DEPTH	  (GLE_RCVAL_ (long, WTREE_EXPAND_DEPTH))
#define	GLE_RCVAL_PTREE_HEIGHT		  (GLE_RCVAL_ (long, PTREE_HEIGHT))
#define	GLE_RCVAL_PTREE_WIDTH		  (GLE_RCVAL_ (long, PTREE_WIDTH))
#define	GLE_RCVAL_PTREE_RESET_COLUMNS	  (GLE_RCVAL_ (bool, PTREE_RESET_COLUMNS))
#define	GLE_RCVAL_PTREE_EXPAND_DEPTH	  (GLE_RCVAL_ (long, PTREE_EXPAND_DEPTH))
#define	GLE_RCVAL_EDITOR_CHILD_ARGS_FIRST (GLE_RCVAL_ (bool, EDITOR_CHILD_ARGS_FIRST))
#define	GLE_RCVAL_WPALETTE_REBUILD_WTREES (GLE_RCVAL_ (bool, WPALETTE_REBUILD_WTREES))
#define	GLE_RCVAL_WPALETTE_FEATURE_WTREES (GLE_RCVAL_ (bool, WPALETTE_FEATURE_WTREES))
#define	GLE_RCVAL_WPALETTE_N_COLUMNS	  (GLE_RCVAL_ (long, WPALETTE_N_COLUMNS))
#define	GLE_RCVAL_TOOLTIPS_DELAY	  (GLE_RCVAL_ (long, TOOLTIPS_DELAY))
#define	GLE_RCVAL_PLIST_HEIGHT		  (GLE_RCVAL_ (long, PLIST_HEIGHT))
#define	GLE_RCVAL_PLIST_WIDTH		  (GLE_RCVAL_ (long, PLIST_WIDTH))

/* initialization function for arguments setup
 */
extern	GleRcHandle	*gle_rc;
void	gle_rc_args_init	(void);

  


     

#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GLE_RCARGS_H__ */
