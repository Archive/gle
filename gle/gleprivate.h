/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef __GLEPRIVATE_H__
#define __GLEPRIVATE_H__


#include	<gle/glemisc.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* --- macros --- */
/* macros that are used to tag gtkobjects as internal GLE objects.
 * this mechanism is likely to change...
 */
#define GLE_TAG(widget, name)		G_STMT_START { \
  GtkWidget *__tag_widget = GTK_WIDGET (widget); \
  const gchar *__tag_name = (name); \
  g_assert (GTK_IS_WIDGET (__tag_widget) && __tag_name && __tag_name[0]); \
  if (!GLE_HAS_TAG (__tag_widget)) \
    { \
      GLE_TAG_WEAK (__tag_widget); \
      gtk_widget_set_name (__tag_widget, __tag_name); \
      gle_control_object_life ((GtkObject*) __tag_widget); \
    } \
} G_STMT_END
#define GLE_TAG_WEAK(object)		G_STMT_START { \
  gtk_object_set_data (GTK_OBJECT (object), gle_key_gle_tag, gle_control_object_life); \
} G_STMT_END
#define	GLE_HAS_TAG(object)		( \
  gtk_object_get_data (GTK_OBJECT (object), gle_key_gle_tag) == gle_control_object_life \
)


/* --- keys --- */
extern const gchar	*gle_key_gle_tag;


/* --- prototypes --- */
void		gle_control_object_life		(GtkObject	*object);
gboolean	gle_is_gle_widget		(GtkWidget	*widget);

/* from glepublic.h */
const gchar*    gle_get_prgname                 (void);


     




#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif	/* __GLEPRIVATE_H__ */
