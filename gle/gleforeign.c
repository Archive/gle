/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001-2002 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include "gleforeign.h"

#include "gleproxy.h"
#include "gmodule.h"


/* --- module glue --- */
#include "gleforeign-gtk.c"
#include "gleforeign-canvas.c"


/* --- Gle foreign --- */
GQuark	gle_foreign_quark = 0;
GleForeign*
gle_foreign_from_class (GObjectClass *oclass)
{
  GType canvas_item = g_type_from_name ("GnomeCanvasItem");

  if (GTK_IS_WIDGET_CLASS (oclass))
    return &foreign_gtk_widget;
  else if (g_type_is_a (G_OBJECT_CLASS_TYPE (oclass), canvas_item))
    return &foreign_canvas_item;
  else
    return NULL;
}

void
gle_foreign_reload_link_params (GleProxy *proxy)
{
  gle_proxy_foreign_notify (proxy, GLE_FOREIGN_RELOAD_LINK_PARAMS);
}

void
gle_foreign_reload_parent (GleProxy *proxy)
{
  gle_proxy_foreign_notify (proxy, GLE_FOREIGN_RELOAD_PARENT);
}

void
gle_foreign_update_object_params (GleProxy *proxy)
{
  gle_proxy_foreign_notify (proxy, GLE_FOREIGN_UPDATE_OBJECT_PARAMS);
}

void
gle_foreign_update_link_params (GleProxy *proxy)
{
  gle_proxy_foreign_notify (proxy, GLE_FOREIGN_UPDATE_LINK_PARAMS);
}

void
gle_foreign_detach_object (GleProxy *proxy)
{
  gle_proxy_foreign_notify (proxy, GLE_FOREIGN_DETACH_OBJECT);
}

gboolean
gle_foreign_is_toplevel (GleProxy *proxy)
{
  g_return_val_if_fail (GLE_IS_PROXY (proxy), FALSE);

  if (!proxy->object || !proxy->pset->foreign->check_toplevel)
    return TRUE;
  return proxy->pset->foreign->check_toplevel (proxy->object);
}

static void
hash2slist_foreach (gpointer  key,
		    gpointer  value,
		    gpointer  user_data)
{
  GSList **slist_p = user_data;

  *slist_p = g_slist_prepend (*slist_p, value);
}

static GSList*
g_hash_table_slist_values (GHashTable *hash_table)
{
  GSList *slist = NULL;

  g_return_val_if_fail (hash_table != NULL, NULL);

  g_hash_table_foreach (hash_table, hash2slist_foreach, &slist);

  return slist;
}

void
gle_foreign_sync_rip (GleProxy *proxy,
		      gboolean  recursive)
{
  GSList *client_children, *slist, *kill_slist = NULL, *missing_slist = NULL, *recheck_slist = NULL;
  GList *list;
  GHashTable *hash_map;

  g_return_if_fail (GLE_IS_PROXY (proxy));

  if (!GLE_PROXY_HAS_OBJECT (proxy) ||
      !proxy->pset->foreign->list_ref_children ||
      !proxy->pset->foreign->get_parent)
    return;

  hash_map = g_hash_table_new (NULL, NULL);

  client_children = proxy->pset->foreign->list_ref_children (proxy->object);
  for (slist = client_children; slist; slist = slist->next)
    g_hash_table_insert (hash_map, slist->data, slist->data);

  for (list = proxy->children; list; list = list->next)
    {
      GleProxy *cproxy = list->data;

      if (g_hash_table_lookup (hash_map, cproxy->object))
	{
	  recheck_slist = g_slist_prepend (recheck_slist, cproxy);
	  g_hash_table_remove (hash_map, cproxy->object);
	}
      else
	kill_slist = g_slist_prepend (kill_slist, cproxy);
    }

  for (slist = kill_slist; slist; slist = slist->next)
    gle_proxy_kill (slist->data);
  g_slist_free (kill_slist);
  
  missing_slist = g_hash_table_slist_values (hash_map);
  for (slist = missing_slist; slist; slist = slist->next)
    {
      GObject *object = G_OBJECT (slist->data);
      GObject *parent = proxy->pset->foreign->get_parent (object);

      if (G_IS_OBJECT (parent) && parent == proxy->object)
	{
	  GleProxy *nproxy;

	  nproxy = gle_proxy_new_from_object (proxy->pset, object);
	  if (GLE_IS_PROXY (nproxy) && GLE_PROXY_HAS_OBJECT (nproxy) && recursive)
	    gle_foreign_sync_rip (nproxy, TRUE);
	}
    }
  g_slist_free (missing_slist);

  g_hash_table_destroy (hash_map);

  for (slist = client_children; slist; slist = slist->next)
    g_object_unref (slist->data);
  g_slist_free (client_children);

  if (recursive)
    for (slist = recheck_slist; slist; slist = slist->next)
      gle_foreign_sync_rip (slist->data, TRUE);
  g_slist_free (recheck_slist);
}
