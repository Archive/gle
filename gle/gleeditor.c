/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include	"gleeditor.h"

#include	"gtkcluehunter.h"
#include	<string.h>
#include	<stdio.h>
#include	<stdlib.h>


/* --- signal list entries --- */
enum {
  SFIELD_OBJECT,
  SFIELD_SIGNAL,
  SFIELD_FUNC,
  SFIELD_DATA,
  SFIELD_CTYPE,
  SFIELD_N_COLUMNS
};


/* --- signals --- */
enum {
  SIGNAL_REFRESH_VALUES,
  SIGNAL_RELOAD_VALUES,
  SIGNAL_APPLY_VALUES,
  SIGNAL_RESET_VALUES,
  SIGNAL_RESTORE_VALUES,
  SIGNAL_REFRESH_GARG,
  SIGNAL_LAST
};


/* --- prototypes --- */
static void	gle_editor_destroy		(GtkObject		*object);
static void	gle_editor_class_init		(GleEditorClass		*class);
static void	gle_editor_init			(GleEditor		*editor);
static void	gle_editor_real_refresh_values	(GleEditor		*editor);
static void	gle_editor_real_reload_values	(GleEditor		*editor);
static void	gle_editor_real_reset_values	(GleEditor		*editor);
static void	gle_editor_tree_select_row	(GleEditor		*editor,
						 GtkCTreeNode		*node,
						 gint			 column,
						 gpointer		 user_data);
static void	gle_editor_switch_page		(GleEditor		*editor,
						 GtkNotebookPage	*page,
						 guint			 page_num,
						 GtkNotebook		*notebook);


/* -- variables --- */
static GtkWindowClass	*parent_class = NULL;
static guint		 editor_signals[SIGNAL_LAST] = { 0 };
static GleEditorClass	*gle_editor_class = NULL;
static GQuark		 quark_enum_value = 0;
static GQuark		 quark_flags_value = 0;


/* --- functions --- */
GtkType
gle_editor_get_type (void)
{
  static GtkType editor_type = 0;
  
  if (!editor_type)
    {
      GtkTypeInfo editor_info =
      {
	"GleEditor",
	sizeof (GleEditor),
	sizeof (GleEditorClass),
	(GtkClassInitFunc) gle_editor_class_init,
	(GtkObjectInitFunc) gle_editor_init,
	/* reserved_1 */ NULL,
	/* reserved_2 */ NULL,
	(GtkClassInitFunc) NULL,
      };
      
      editor_type = gtk_type_unique (GTK_TYPE_WINDOW, &editor_info);
    }
  
  return editor_type;
}

static void
gle_editor_class_init (GleEditorClass *class)
{
  GtkObjectClass *object_class = GTK_OBJECT_CLASS (class);

  parent_class = g_type_class_peek_parent (class);
  gle_editor_class = class;

  quark_enum_value = g_quark_from_static_string ("Gle-enum-value");
  quark_flags_value = g_quark_from_static_string ("Gle-flags-value");
  
  object_class->destroy = gle_editor_destroy;
  
  class->refresh_values = gle_editor_real_refresh_values;
  class->reload_values = gle_editor_real_reload_values;
  class->reset_values = gle_editor_real_reset_values;

  editor_signals[SIGNAL_REFRESH_VALUES] =
    gtk_signal_new ("refresh_values",
		    GTK_RUN_LAST,
		    GTK_CLASS_TYPE (object_class),
		    GTK_SIGNAL_OFFSET (GleEditorClass, refresh_values),
		    gtk_signal_default_marshaller,
		    GTK_TYPE_NONE, 0);
  editor_signals[SIGNAL_RELOAD_VALUES] =
    gtk_signal_new ("reload_values",
		    GTK_RUN_LAST,
		    GTK_CLASS_TYPE (object_class),
		    GTK_SIGNAL_OFFSET (GleEditorClass, reload_values),
		    gtk_signal_default_marshaller,
		    GTK_TYPE_NONE, 0);
  editor_signals[SIGNAL_RESET_VALUES] =
    gtk_signal_new ("reset_values",
		    GTK_RUN_LAST,
		    GTK_CLASS_TYPE (object_class),
		    GTK_SIGNAL_OFFSET (GleEditorClass, reset_values),
		    gtk_signal_default_marshaller,
		    GTK_TYPE_NONE, 0);
}

static void
gle_editor_init (GleEditor *editor)
{
  GtkWidget *main_vbox;
  GtkWidget *hbox;
  GtkWidget *any;
  GtkWidget *button;
  static gchar *ctree_titles[] = { "", "Category" };

  editor->ignore_changes = 0;
  editor->proxy = NULL;
  editor->fields = NULL;

  GLE_TAG (editor);
  
  /* setup the main window and its container and tooltips
   */
  gtk_widget_set (GTK_WIDGET (editor),
		  "type", GTK_WINDOW_TOPLEVEL,
		  "title", "GLE-Editor",
		  "allow_shrink", FALSE,
		  "allow_grow", TRUE,
		  NULL);
  main_vbox =
    gtk_widget_new (GTK_TYPE_VBOX,
		    "homogeneous", FALSE,
		    "spacing", 0,
		    "border_width", 0,
		    "parent", editor,
		    "visible", TRUE,
		    NULL);
  /* gle name
   */
  hbox =
    gtk_widget_new (GTK_TYPE_HBOX,
		    "homogeneous", FALSE,
		    "spacing", 5,
		    "border_width", 5,
		    "visible", TRUE,
		    NULL);
  gtk_box_pack_start (GTK_BOX (main_vbox), hbox, FALSE, TRUE, 0);
  editor->proxy_entry =
    g_object_connect (g_object_new (GTK_TYPE_ENTRY,
				    "parent", hbox,
				    "visible", TRUE,
				    NULL),
		      "signal::destroy", gtk_widget_destroyed, &editor->proxy_entry,
		      NULL);
  any =
    g_object_new (GTK_TYPE_HSEPARATOR,
		  "visible", TRUE,
		  NULL);
  gtk_box_pack_start (GTK_BOX (main_vbox), any, FALSE, TRUE, 0);
  
  /* proxy parameter ctree/notebook
   */
  hbox =
    g_object_new (GTK_TYPE_HBOX,
		  "homogeneous", FALSE,
		  "spacing", 5,
		  "border_width", 5,
		  "visible", TRUE,
		  NULL);
  gtk_box_pack_start (GTK_BOX (main_vbox), hbox, TRUE, TRUE, 0);
  any = g_object_new (GTK_TYPE_SCROLLED_WINDOW,
		      "visible", TRUE,
		      "hscrollbar_policy", GTK_POLICY_AUTOMATIC,
		      "vscrollbar_policy", GTK_POLICY_AUTOMATIC,
		      "parent", hbox,
		      NULL);
  editor->ctree = GTK_CTREE (gtk_ctree_new_with_titles (2, 0, ctree_titles));
  gtk_container_add (GTK_CONTAINER (any), GTK_WIDGET (editor->ctree));
  editor->clist = GTK_CLIST (editor->ctree);
  g_object_set (GTK_WIDGET (editor->ctree),
		"visible", TRUE,
		"width_request", 120,
		"selection_mode", GTK_SELECTION_BROWSE,
		"line_style", GTK_CTREE_LINES_NONE,
		NULL);
  g_object_connect (editor->ctree,
		    "signal::destroy", gtk_widget_destroyed, &editor->ctree,
		    "swapped_signal::tree_select_row", gle_editor_tree_select_row, editor,
		    NULL);
  gtk_clist_column_titles_passive (editor->clist);
  gtk_clist_set_column_width (editor->clist, 0, 12);
  editor->notebook =
    g_object_new (GTK_TYPE_NOTEBOOK,
		  "parent", hbox,
		  "visible", TRUE,
		  "scrollable", TRUE,
		  /* "tab_border", 0, */
		  "show_border", FALSE,
		  "enable_popup", TRUE,
		  "show_tabs", FALSE,
		  NULL);
  g_object_connect (editor->notebook,
		    "signal::destroy", gtk_widget_destroyed, &editor->notebook,
		    "swapped_signal_after::switch_page", gle_editor_switch_page, editor,
		    NULL);
  gtk_widget_set_name (GTK_NOTEBOOK (editor->notebook)->menu, "GLE-Notebook-Popup");
  any =
    g_object_new (GTK_TYPE_HSEPARATOR,
		  "visible", TRUE,
		  NULL);
  gtk_box_pack_start (GTK_BOX (main_vbox), any, FALSE, TRUE, 0);
  
  /* arg buttons, they map to the following actions */
  hbox = g_object_new (GTK_TYPE_HBOX,
		       "homogeneous", TRUE,
		       "spacing", 5,
		       "border_width", 5,
		       "visible", TRUE,
		       NULL);
  gtk_box_pack_start (GTK_BOX (main_vbox), hbox, FALSE, TRUE, 0);
  button = g_object_connect (g_object_new (GTK_TYPE_BUTTON,
					   "label", "Reload",
					   "visible", TRUE,
					   NULL),
			     "swapped_signal::clicked", gle_editor_reload_values, editor,
			     NULL);
  gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, TRUE, 0);
  gtk_tooltips_set_tip (GLE_TOOLTIPS, button,
			"Reload proxy properties values from GObject, "
			"use this if the object changed its properties behind the editors back.",
			NULL);
  button = g_object_connect (g_object_new (GTK_TYPE_BUTTON,
					   "label", "Reset",
					   "visible", TRUE,
					   NULL),
			     "swapped_signal::clicked", gle_editor_reset_values, editor,
			     NULL);
  gtk_box_pack_start (GTK_BOX (hbox), button, FALSE, TRUE, 0);
  gtk_tooltips_set_tip (GLE_TOOLTIPS, button,
			"Reset object properties to factory defaults.",
			NULL);
  any = g_object_new (GTK_TYPE_HSEPARATOR,
		      "visible", TRUE,
		      NULL);
  gtk_box_pack_start (GTK_BOX (main_vbox), any, FALSE, TRUE, 0);

  /* Close button
   */
  hbox = g_object_new (GTK_TYPE_HBOX,
		       "homogeneous", TRUE,
		       "spacing", 5,
		       "border_width", 5,
		       "visible", TRUE,
		       NULL);
  gtk_box_pack_end (GTK_BOX (main_vbox), hbox, FALSE, TRUE, 0);
  button = g_object_connect (g_object_new (GTK_TYPE_BUTTON,
					   "label", "Close",
					   "parent", hbox,
					   "visible", TRUE,
					   "can_default", TRUE,
					   NULL),
			     "swapped_signal::clicked", gtk_widget_destroy, editor,
			     NULL);
  gtk_tooltips_set_tip (GLE_TOOLTIPS, button,
			"Close the editor",
			NULL);
}

static void
gle_editor_destroy_pages (GleEditor *editor)
{
  GSList *slist;

  if (editor->notebook)
    gtk_container_foreach (GTK_CONTAINER (editor->notebook),
			   (GtkCallback) gtk_widget_destroy,
			   NULL);
  if (editor->ctree)
    gtk_ctree_remove_node (editor->ctree, NULL); /* clear tree */
  for (slist = editor->fields; slist; slist = slist->next)
    {
      GleEditorField *field = slist->data;
      
      g_assert (field->widget == NULL);
      g_free (field);
    }
  g_slist_free (editor->fields);
  editor->fields = NULL;
}

static void
gle_editor_destroy (GtkObject *object)
{
  GleEditor *editor = GLE_EDITOR (object);
  
  if (editor->proxy)
    {
      GLE_PROXY_REMOVE_NOTIFIES (editor->proxy, gtk_widget_destroy, editor);
      editor->proxy = NULL;
    }
  gle_editor_destroy_pages (editor);
  
  GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

GtkWidget*
gle_editor_new (GleProxy *proxy)
{
  GleEditor *editor;
  
  g_return_val_if_fail (GLE_IS_PROXY (proxy), NULL);
  
  editor = g_object_new (GLE_TYPE_EDITOR, NULL);
  editor->proxy = proxy;
  GLE_PROXY_NOTIFY_DESTROY (proxy, gtk_widget_destroy, editor);
  gle_editor_rebuild (editor);
  
  return GTK_WIDGET (editor);
}

void
gle_editor_refresh_values (GleEditor *editor)
{
  g_return_if_fail (GLE_IS_EDITOR (editor));
  
  gtk_signal_emit (GTK_OBJECT (editor), editor_signals[SIGNAL_REFRESH_VALUES]);
}

void
gle_editor_reload_values (GleEditor *editor)
{
  g_return_if_fail (GLE_IS_EDITOR (editor));
  
  gtk_signal_emit (GTK_OBJECT (editor), editor_signals[SIGNAL_REFRESH_VALUES]);
}

void
gle_editor_reset_values (GleEditor *editor)
{
  g_return_if_fail (GLE_IS_EDITOR (editor));
  
  gtk_signal_emit (GTK_OBJECT (editor), editor_signals[SIGNAL_REFRESH_VALUES]);
}

static void
gle_editor_real_refresh_values (GleEditor *editor)
{
  GSList *slist;

  for (slist = editor->fields; slist; slist = slist->next)
    {
      GleEditorField *field = slist->data;

      gle_editor_field_refresh (field);
    }
}

static void
gle_editor_real_reload_values (GleEditor *editor)
{
  GSList *slist;

  gle_proxy_capture_object_params (editor->proxy);
  gle_proxy_capture_link_params (editor->proxy);
  for (slist = editor->fields; slist; slist = slist->next)
    {
      GleEditorField *field = slist->data;

      gle_editor_field_refresh (field);
    }
}

static void
gle_editor_real_reset_values (GleEditor *editor)
{
  GSList *slist;

  for (slist = editor->fields; slist; slist = slist->next)
    {
      GleEditorField *field = slist->data;

      gle_proxy_param_set_default (field->param);
      gle_editor_field_refresh (field);
    }
}

static void
gle_editor_tree_select_row (GleEditor	 *editor,
			    GtkCTreeNode *node,
			    gint	  column,
			    gpointer	  user_data)
{
  GtkNotebook *notebook = GTK_NOTEBOOK (editor->notebook);
  GtkWidget *vbox = gtk_ctree_node_get_row_data (editor->ctree, node);

  if (vbox)
    {
      gint num = gtk_notebook_page_num (notebook, vbox);

      if (gtk_notebook_get_current_page (notebook) != num)
	{
	  GtkWidget *focus = gtk_object_get_data (GTK_OBJECT (vbox), "focus");

	  gtk_notebook_set_current_page (notebook, num);
	  if (focus)
	    {
	      gtk_widget_grab_focus (focus);
	      gtk_widget_grab_default (focus);
	    }
	  else
	    {
	      // gtk_widget_grab_default (editor->default_default);
	      gtk_widget_grab_focus (GTK_WIDGET (editor->ctree));
	    }
	}
    }
}

static void
gle_editor_switch_page (GleEditor	*editor,
			GtkNotebookPage *page,
			guint		 page_num,
			GtkNotebook     *notebook)
{
  GtkCTreeNode *node = gtk_object_get_user_data (GTK_OBJECT (gtk_notebook_get_nth_page (notebook, page_num)));
  
  if (editor->ctree)
    gtk_ctree_select (editor->ctree, node);
}

static void
gle_editor_notebook_child_destroy (GtkWidget *child,
				   GleEditor *editor)
{
  GtkCTreeNode *node;
  
  if (editor->ctree)
    {
      node = gtk_object_get_user_data (GTK_OBJECT (child));
      gtk_ctree_remove_node (editor->ctree, node);
    }
}

static GtkWidget*
gle_editor_new_page (GleEditor	 *editor,
		     const gchar *group,
		     gboolean	  with_children)
{
  GtkCTreeNode *node;
  GtkWidget *vbox;
  
  node = gtk_ctree_insert_node (editor->ctree, NULL, NULL, NULL, 0,
				NULL, NULL, NULL, NULL,
				TRUE, TRUE);
  gtk_ctree_node_set_text (editor->ctree, node, 1, group);
  
  if (!with_children)
    {
      /* we need the correct ctree style before referencing it's fields,
       * or we'll operate on colors from some gtk default style
       */
      gtk_widget_ensure_style (GTK_WIDGET (editor->ctree));
      gtk_ctree_node_set_foreground (editor->ctree,
				     node,
				     &GTK_WIDGET (editor->ctree)->style->fg[GTK_STATE_INSENSITIVE]);
      gtk_ctree_node_set_background (editor->ctree,
				     node,
				     &GTK_WIDGET (editor->ctree)->style->bg[GTK_STATE_INSENSITIVE]);
      gtk_ctree_node_set_selectable (editor->ctree, node, FALSE);
      vbox = NULL;
    }
  else
    {
      gchar *pattern;
      GtkWidget *title;
      
      pattern = g_strnfill (strlen (group), '_');
      title = g_object_new (GTK_TYPE_ALIGNMENT,
			    "xalign", 0.5,
			    "yalign", 0.5,
			    "xscale", 1.0,
			    "yscale", 1.0,
			    "visible", TRUE,
			    "child", g_object_new (GTK_TYPE_LABEL,
						   "label", group,
						   "pattern", pattern,
						   "visible", TRUE,
						   NULL),
			    NULL);
      g_free (pattern);
      vbox = g_object_new (GTK_TYPE_VBOX,
			   "homogeneous", FALSE,
			   "spacing", 0,
			   "border_width", 5,
			   "visible", TRUE,
			   "user_data", node,
			   NULL);
      g_object_connect (vbox,
			"signal::destroy", gle_editor_notebook_child_destroy, editor,
			NULL);
      gtk_box_pack_start (GTK_BOX (vbox), title, FALSE, FALSE, 5);
      gtk_ctree_node_set_row_data (editor->ctree, node, vbox);
      
      /* we already rely on GtkNotebook::switch_page here */
      gtk_notebook_append_page (GTK_NOTEBOOK (editor->notebook),
				vbox,
				gtk_label_new (group));
    }
  
  return vbox;
}

void
gle_editor_rebuild (GleEditor *editor)
{
  struct { gpointer next; gchar *group; GtkWidget *widget; } *page_list = NULL, *page;
  GSList *slist;
  GtkWidget *box = NULL;
  GleProxy *proxy;
  
  g_return_if_fail (GLE_IS_EDITOR (editor));
  g_return_if_fail (editor->proxy != NULL);

  proxy = editor->proxy;
  gtk_widget_hide (editor->notebook->parent);
  if (!GTK_WIDGET_REALIZED (editor))
    gtk_widget_realize (GTK_WIDGET (editor));
  gle_editor_destroy_pages (editor);
  
  /* create object property fields
   */
  for (slist = proxy->object_params; slist; slist = slist->next)
    {
      GleProxyParam *param = slist->data;
      GleEditorField *field = g_new0 (GleEditorField, 1);
      gchar *group = g_type_name (param->pspec->owner_type);
      
      for (page = page_list; page; page = page->next)
	if (strcmp (group, page->group) == 0)
	  break;
      if (!page)
	{
	  page = g_malloc (sizeof (*page));
	  page->group = group;
	  page->widget = gle_editor_new_page (editor, group, TRUE);
	  page->next = page_list;
	  page_list = page;
	}
      field = gle_editor_field_create (editor, GTK_BOX (page->widget), param);
      if ((param->pspec->flags & G_PARAM_CONSTRUCT_ONLY) ||
	  !(param->pspec->flags & G_PARAM_WRITABLE))
	gtk_widget_set_sensitive (field->widget, FALSE);
    }
  while (page_list)
    {
      page = page_list->next;
      g_free (page_list);
      page_list = page;
    }

  /* create link property fields
   */
  if (proxy->link_params)
    box = gle_editor_new_page (editor, "Link properties", TRUE);
  for (slist = proxy->link_params; slist; slist = slist->next)
    {
      GleProxyParam *param = slist->data;
      GleEditorField *field = g_new0 (GleEditorField, 1);

      field = gle_editor_field_create (editor, GTK_BOX (box), param);
      if ((param->pspec->flags & G_PARAM_CONSTRUCT_ONLY) ||
	  !(param->pspec->flags & G_PARAM_WRITABLE))
	gtk_widget_set_sensitive (field->widget, FALSE);
    }

  /* update GUI contents */
  gtk_notebook_set_current_page (GTK_NOTEBOOK (editor->notebook), 0);
  gle_editor_refresh_values (editor);

  gtk_widget_show (editor->notebook->parent);
}

static gboolean
idle_apply (gpointer data)
{
  GleEditor *editor = data;
  GSList *slist;

  GDK_THREADS_ENTER ();

  editor->idle_apply = 0;
  if (GTK_WIDGET_VISIBLE (editor))
    for (slist = editor->fields; slist; slist = slist->next)
      {
	GleEditorField *field = slist->data;

	if (field->has_changed)
	  {
	    gle_editor_field_update_param (field);
	    if (0)
	      g_print ("field \"%s\" changed: %s\n",
		       field->param->pspec->name,
		       g_strdup_value_contents (&field->param->value));
	    gle_proxy_set_param (editor->proxy, field->param->pspec, &field->param->value);
	    field->has_changed = FALSE;
	  }
      }
  g_object_unref (editor);

  GDK_THREADS_LEAVE ();

  return FALSE;
}

static gboolean	/* might be connected to an event handler */
editor_field_queue_changed (GleEditorField *field)
{
  g_return_val_if_fail (field != NULL, FALSE);
  g_return_val_if_fail (GLE_IS_EDITOR (field->editor), FALSE);

  if (!field->editor->ignore_changes)
    {
      field->has_changed = TRUE;
      if (!field->editor->idle_apply)
	field->editor->idle_apply = g_idle_add (idle_apply, g_object_ref (field->editor));
    }
  
  return FALSE;	/* continue processing event */
}

static void
toggle_button_mksensitive (GtkToggleButton *tb,
			   GtkWidget	   *widget)
{
  gtk_widget_set_sensitive (widget, tb->active);
}

static void
watch_field (GleEditorField *field,
	     gpointer        object,
	     const gchar    *signal)
{
  g_signal_connect_swapped (object, signal, G_CALLBACK (editor_field_queue_changed), field);
}

static void
watch_field_entry (GleEditorField *field,
		   GtkWidget      *entry)
{
  watch_field (field, entry, "activate");
  watch_field (field, entry, "focus_out_event");
}

GleEditorField*
gle_editor_field_create (GleEditor     *editor,
			 GtkBox        *parent_box,
			 GleProxyParam *param)
{
  GleEditorField *field = g_new0 (GleEditorField, 1);
  GParamSpec *pspec = param->pspec;
  gchar *tooltip = g_param_spec_get_blurb (param->pspec);
  gchar *name = g_param_spec_get_nick (param->pspec);
  GType vtype = G_PARAM_SPEC_VALUE_TYPE (pspec);
  GtkObject *adjustment = NULL;
  GtkWidget *widget;
  
  editor->fields = g_slist_prepend (editor->fields, field);
  field->editor = editor;
  field->param = param;
  field->has_changed = FALSE;

  editor->ignore_changes++;
  switch (G_TYPE_FUNDAMENTAL (vtype))
    {
      GtkWidget *box;
      GtkWidget *bar;
      GtkWidget *label;
      GtkWidget *entry;
      GtkWidget *button;
      GEnumValue *ev;
      GFlagsValue *fv;
      gchar *string;
    case G_TYPE_BOOLEAN:
      widget = g_object_new (GTK_TYPE_CHECK_BUTTON,
			     "label", name,
			     "visible", TRUE,
			     NULL);
      gtk_misc_set_alignment (GTK_MISC (GTK_BIN (widget)->child), 0, 0.5);
      gtk_tooltips_set_tip (GLE_TOOLTIPS, widget, tooltip, NULL);
      gtk_box_pack_start (parent_box, widget, FALSE, FALSE, 0);
      watch_field (field, widget, "clicked");
      break;
    case G_TYPE_INT:
    case G_TYPE_UINT:
    case G_TYPE_LONG:
    case G_TYPE_ULONG:
      if (G_IS_PARAM_SPEC_ULONG (pspec))
	adjustment = gtk_adjustment_new (G_PARAM_SPEC_ULONG (pspec)->default_value,
					 G_PARAM_SPEC_ULONG (pspec)->minimum,
					 G_PARAM_SPEC_ULONG (pspec)->maximum,
					 1, 5, 0);
      else if (G_IS_PARAM_SPEC_LONG (pspec))
	adjustment = gtk_adjustment_new (G_PARAM_SPEC_LONG (pspec)->default_value,
					 G_PARAM_SPEC_LONG (pspec)->minimum,
					 G_PARAM_SPEC_LONG (pspec)->maximum,
					 1, 5, 0);
      else if (G_IS_PARAM_SPEC_INT (pspec))
	adjustment = gtk_adjustment_new (G_PARAM_SPEC_INT (pspec)->default_value,
					 G_PARAM_SPEC_INT (pspec)->minimum,
					 G_PARAM_SPEC_INT (pspec)->maximum,
					 1, 5, 0);
      else if (G_IS_PARAM_SPEC_UINT (pspec))
	adjustment = gtk_adjustment_new (G_PARAM_SPEC_UINT (pspec)->default_value,
					 G_PARAM_SPEC_UINT (pspec)->minimum,
					 G_PARAM_SPEC_UINT (pspec)->maximum,
					 1, 5, 0);
      else if (G_IS_PARAM_SPEC_UNICHAR (pspec))
	adjustment = gtk_adjustment_new (G_PARAM_SPEC_UNICHAR (pspec)->default_value,
					 -G_MAXINT, G_MAXINT,
					 1, 5, 0);
      else
	adjustment = gtk_adjustment_new (0, -G_MAXINT, G_MAXINT, 1, 5, 0);
      watch_field (field, adjustment, "value_changed");
      box = g_object_new (GTK_TYPE_HBOX,
			  "homogeneous", FALSE,
			  "spacing", 10,
			  "border_width", 0,
			  "visible", TRUE,
			  NULL);
      gtk_box_pack_start (parent_box, box, FALSE, TRUE, 0);
      label = g_object_new (GTK_TYPE_LABEL,
			    "label", name,
			    "justify", GTK_JUSTIFY_LEFT,
			    "visible", TRUE,
			    NULL);
      gtk_box_pack_start (GTK_BOX (box), label, FALSE, TRUE, 0);
      entry = g_object_new (GTK_TYPE_SPIN_BUTTON,
			    "adjustment", adjustment,
			    "digits", 0,
			    "width_request", 140,
			    "visible", TRUE,
			    NULL);
      gtk_tooltips_set_tip (GLE_TOOLTIPS, entry, tooltip, NULL);
      gtk_box_pack_end (GTK_BOX (box), entry, FALSE, TRUE, 0);
      watch_field_entry (field, entry);
      widget = entry;
      break;
    case G_TYPE_FLOAT:
      if (!adjustment)
	adjustment = gtk_adjustment_new (G_PARAM_SPEC_FLOAT (pspec)->default_value,
					 G_PARAM_SPEC_FLOAT (pspec)->minimum,
					 G_PARAM_SPEC_FLOAT (pspec)->maximum,
					 0.1, 1.0, 0);
    case G_TYPE_DOUBLE:
      if (!adjustment)
	adjustment = gtk_adjustment_new (G_PARAM_SPEC_DOUBLE (pspec)->default_value,
					 G_PARAM_SPEC_DOUBLE (pspec)->minimum,
					 G_PARAM_SPEC_DOUBLE (pspec)->maximum,
					 0.1, 1.0, 0);
      watch_field (field, adjustment, "value_changed");
      box = g_object_new (GTK_TYPE_HBOX,
			  "homogeneous", FALSE,
			  "spacing", 10,
			  "border_width", 0,
			  "visible", TRUE,
			  NULL);
      gtk_box_pack_start (parent_box, box, FALSE, TRUE, 0);
      label = g_object_new (GTK_TYPE_LABEL,
			    "label", name,
			    "justify", GTK_JUSTIFY_LEFT,
			    "visible", TRUE,
			    NULL);
      gtk_box_pack_start (GTK_BOX (box), label, FALSE, TRUE, 0);
      entry = g_object_new (GTK_TYPE_SPIN_BUTTON,
			    "adjustment", adjustment,
			    "digits", 7,
			    "width_request", 150,
			    "visible", TRUE,
			    NULL);
      gtk_tooltips_set_tip (GLE_TOOLTIPS, entry, tooltip, NULL);
      gtk_box_pack_end (GTK_BOX (box), entry, FALSE, TRUE, 0);
      watch_field_entry (field, entry);
      widget = entry;
      break;
    case G_TYPE_STRING:
      box = g_object_new (GTK_TYPE_HBOX,
			  "homogeneous", FALSE,
			  "spacing", 10,
			  "border_width", 0,
			  "visible", TRUE,
			  NULL);
      gtk_box_pack_start (parent_box, box, FALSE, TRUE, 0);
      label = g_object_new (GTK_TYPE_LABEL,
			    "label", name,
			    "justify", GTK_JUSTIFY_LEFT,
			    "visible", TRUE,
			    NULL);
      gtk_box_pack_start (GTK_BOX (box), label, FALSE, TRUE, 0);
      button = g_object_new (GTK_TYPE_CHECK_BUTTON,
			     "visible", TRUE,
			     "can_focus", FALSE,
			     NULL);
      string = g_strdup_printf ("Set %s to NULL", name);
      gtk_tooltips_set_tip (GLE_TOOLTIPS, button, string, NULL);
      g_free (string);
      watch_field (field, button, "clicked");
      entry = g_object_new (GTK_TYPE_ENTRY,
			    "width_request", 140,
			    "visible", TRUE,
			    "user_data", button,
			    NULL);
      gtk_tooltips_set_tip (GLE_TOOLTIPS, entry, tooltip, NULL);
      gtk_signal_connect (GTK_OBJECT (button),
			  "clicked",
			  GTK_SIGNAL_FUNC (toggle_button_mksensitive),
			  entry);
      toggle_button_mksensitive (GTK_TOGGLE_BUTTON (button), entry);
      gtk_box_pack_end (GTK_BOX (box), entry, FALSE, TRUE, 0);
      gtk_box_pack_end (GTK_BOX (box), button, FALSE, FALSE, 0);
      watch_field_entry (field, entry);
      widget = entry;
      break;
    case G_TYPE_ENUM:
      box = g_object_new (GTK_TYPE_HBOX,
			  "homogeneous", FALSE,
			  "spacing", 10,
			  "border_width", 0,
			  "visible", TRUE,
			  NULL);
      gtk_box_pack_start (parent_box, box, FALSE, TRUE, 0);
      label = g_object_new (GTK_TYPE_LABEL,
			    "label", name,
			    "justify", GTK_JUSTIFY_LEFT,
			    "visible", TRUE,
			    NULL);
      gtk_box_pack_start (GTK_BOX (box), label, FALSE, TRUE, 0);
      widget = gtk_option_menu_new ();
      gtk_tooltips_set_tip (GLE_TOOLTIPS, widget, tooltip, NULL);
      gtk_widget_show (widget);
      gtk_box_pack_end (GTK_BOX (box), widget, FALSE, TRUE, 0);
      ev = G_PARAM_SPEC_ENUM (pspec)->enum_class->values;
      if (ev)
	{
	  GtkWidget *menu = gtk_menu_new ();

	  GLE_TAG (menu);
	  while (ev->value_nick)
	    {
	      GtkWidget *item = gtk_menu_item_new_with_label (ev->value_nick);

	      gtk_widget_show (item);
	      g_object_set_qdata (G_OBJECT (item), quark_enum_value, ev);
	      gtk_container_add (GTK_CONTAINER (menu), item);
	      watch_field (field, item, "activate");
	      ev++;
	    }
	  gtk_option_menu_set_menu (GTK_OPTION_MENU (widget), menu);
	}
      else
	gtk_widget_set_sensitive (GTK_WIDGET (box), FALSE);
      break;
    case G_TYPE_FLAGS:
      box = g_object_new (GTK_TYPE_HBOX,
			  "homogeneous", FALSE,
			  "spacing", 10,
			  "border_width", 0,
			  "visible", TRUE,
			  NULL);
      gtk_box_pack_start (parent_box, box, FALSE, TRUE, 0);
      label = g_object_new (GTK_TYPE_LABEL,
			    "label", name,
			    "justify", GTK_JUSTIFY_LEFT,
			    "visible", TRUE,
			    NULL);
      gtk_box_pack_start (GTK_BOX (box), label, FALSE, TRUE, 0);
      bar = gtk_menu_bar_new ();
      gtk_tooltips_set_tip (GLE_TOOLTIPS, bar, tooltip, NULL);
      gtk_widget_show (bar);
      gtk_box_pack_end (GTK_BOX (box), bar, FALSE, TRUE, 0);
      widget = gtk_menu_item_new_with_label ("Flags");
      gtk_widget_show (widget);
      gtk_menu_bar_append (GTK_MENU_BAR (bar), widget);
      fv = G_PARAM_SPEC_FLAGS (pspec)->flags_class->values;
      if (fv)
	{
	  GtkWidget *menu = gtk_menu_new ();

	  GLE_TAG (menu);
	  while (fv->value_nick)
	    {
	      GtkWidget *item = gtk_check_menu_item_new_with_label (fv->value_nick);

	      gtk_check_menu_item_set_show_toggle (GTK_CHECK_MENU_ITEM (item), TRUE);
	      gtk_widget_show (item);
	      g_object_set_qdata (G_OBJECT (item), quark_flags_value, fv);
	      gtk_container_add (GTK_CONTAINER (menu), item);
              watch_field (field, item, "activate");
	      fv++;
	    }
	  gtk_menu_item_set_submenu (GTK_MENU_ITEM (widget), menu);
	}
      else
	gtk_widget_set_sensitive (GTK_WIDGET (box), FALSE);
      break;
    default:
      widget = g_object_new (GTK_TYPE_LABEL,
			     "label", name,
			     "sensitive", FALSE,
			     "visible", TRUE,
			     NULL);
      gtk_misc_set_alignment (GTK_MISC (widget), 0, 0.5);
      gtk_box_pack_start (parent_box, widget, FALSE, FALSE, 0);
      break;
    }
  field->widget = widget;
  gtk_signal_connect (GTK_OBJECT (widget),
		      "destroy",
		      GTK_SIGNAL_FUNC (gtk_widget_destroyed),
		      &field->widget);
  editor->ignore_changes--;

  return field;
}

static void
entry_setup (gpointer entry)
{
  gtk_entry_set_position (GTK_ENTRY (entry), 0);
}

void
gle_editor_field_refresh (GleEditorField *field)
{
  GParamSpec *pspec = field->param->pspec;
  const GValue *value = &field->param->value;
  GType vtype = G_PARAM_SPEC_VALUE_TYPE (pspec);
  GtkWidget *widget = field->widget;

  field->editor->ignore_changes++;
  switch (G_TYPE_FUNDAMENTAL (vtype))
    {
      gchar *string;
      gchar buffer[64];
      GtkWidget *menu;
      GtkWidget *button;
    case G_TYPE_BOOLEAN:
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (widget), g_value_get_boolean (value));
      break;
    case G_TYPE_INT:
      sprintf (buffer, "%d", g_value_get_int (value));
      gtk_entry_set_text (GTK_ENTRY (widget), buffer);
      entry_setup (GTK_ENTRY (widget));
      break;
    case G_TYPE_LONG:
      sprintf (buffer, "%ld", g_value_get_long (value));
      gtk_entry_set_text (GTK_ENTRY (widget), buffer);
      entry_setup (GTK_ENTRY (widget));
      break;
    case G_TYPE_UINT:
      sprintf (buffer, "%u", g_value_get_uint (value));
      gtk_entry_set_text (GTK_ENTRY (widget), buffer);
      entry_setup (GTK_ENTRY (widget));
      break;
    case G_TYPE_ULONG:
      sprintf (buffer, "%lu", g_value_get_ulong (value));
      gtk_entry_set_text (GTK_ENTRY (widget), buffer);
      entry_setup (GTK_ENTRY (widget));
      break;
    case G_TYPE_FLOAT:
      sprintf (buffer, "%f", g_value_get_float (value));
      gtk_entry_set_text (GTK_ENTRY (widget), buffer);
      entry_setup (GTK_ENTRY (widget));
      break;
    case G_TYPE_DOUBLE:
      sprintf (buffer, "%f", g_value_get_double (value));
      gtk_entry_set_text (GTK_ENTRY (widget), buffer);
      entry_setup (GTK_ENTRY (widget));
      break;
    case GTK_TYPE_STRING:
      string = g_value_get_string (value);
      gtk_entry_set_text (GTK_ENTRY (widget), string ? string : "");
      entry_setup (GTK_ENTRY (widget));
      button = gtk_object_get_user_data (GTK_OBJECT (widget));
      gtk_toggle_button_set_active (GTK_TOGGLE_BUTTON (button), string != NULL);
      break;
    case GTK_TYPE_ENUM:
      menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (widget));
      if (menu)
	{
	  guint n;
	  GList *list;
	  
	  n = 0;
	  for (list = GTK_MENU_SHELL (menu)->children; list; list = list->next)
	    {
	      GtkWidget *item;
	      GEnumValue *ev;
	      
	      item = list->data;
	      ev = g_object_get_qdata (G_OBJECT (item), quark_enum_value);
	      if (ev->value == g_value_get_enum (value))
		{
		  gtk_option_menu_set_history (GTK_OPTION_MENU (widget), n);
		  break;
		}
	      n++;
	    }
	}
      break;
    case GTK_TYPE_FLAGS:
      menu = GTK_MENU_ITEM (widget)->submenu;
      if (menu)
	{
	  GList *list;
	  
	  for (list = GTK_MENU_SHELL (menu)->children; list; list = list->next)
	    {
	      GtkWidget *item;
	      GFlagsValue *fv;
	      
	      item = list->data;
	      fv = g_object_get_qdata (G_OBJECT (item), quark_flags_value);
	      gtk_check_menu_item_set_active (GTK_CHECK_MENU_ITEM (item), g_value_get_flags (value) & fv->value);
	    }
	}
      break;
    default:
      break;
    }
  field->editor->ignore_changes--;
}

void
gle_editor_field_update_param (GleEditorField *field)
{
  GParamSpec *pspec = field->param->pspec;
  GValue *value = &field->param->value;
  GType vtype = G_PARAM_SPEC_VALUE_TYPE (pspec);
  GtkWidget *widget = field->widget;

  field->editor->ignore_changes++;
  switch (G_TYPE_FUNDAMENTAL (vtype))
    {
      gchar *string;
      GtkWidget *item;
      GtkWidget *button;
      GtkWidget *menu;
    case G_TYPE_BOOLEAN:
      g_value_set_boolean (value, GTK_TOGGLE_BUTTON (widget)->active);
      break;
    case G_TYPE_INT:
      g_value_set_int (value, gle_util_parse_long (gtk_entry_get_text (GTK_ENTRY (widget))));
      break;
    case G_TYPE_LONG:
      g_value_set_long (value, gle_util_parse_long (gtk_entry_get_text (GTK_ENTRY (widget))));
      break;
    case G_TYPE_UINT:
      g_value_set_uint (value, gle_util_parse_long (gtk_entry_get_text (GTK_ENTRY (widget))));
      break;
    case G_TYPE_ULONG:
      g_value_set_ulong (value, gle_util_parse_long (gtk_entry_get_text (GTK_ENTRY (widget))));
      break;
    case G_TYPE_FLOAT:
      g_value_set_float (value, gle_util_parse_double (gtk_entry_get_text (GTK_ENTRY (widget))));
      break;
    case G_TYPE_DOUBLE:
      g_value_set_double (value, gle_util_parse_double (gtk_entry_get_text (GTK_ENTRY (widget))));
      break;
    case G_TYPE_STRING:
      button = gtk_object_get_user_data (GTK_OBJECT (widget));
      if (GTK_TOGGLE_BUTTON (button)->active)
	string = gtk_entry_get_text (GTK_ENTRY (widget));
      else
	string = NULL;
      g_value_set_string (value, string);
      break;
    case G_TYPE_ENUM:
      item = GTK_OPTION_MENU (widget)->menu_item;
      if (item)
	{
	  GEnumValue *ev = g_object_get_qdata (G_OBJECT (item), quark_enum_value);

	  g_value_set_enum (value, ev->value);
	}
      break;
    case GTK_TYPE_FLAGS:
      menu = GTK_MENU_ITEM (widget)->submenu;
      if (menu)
	{
	  GList *list;
	  guint v;
	  
	  v = 0;
	  for (list = GTK_MENU_SHELL (menu)->children; list; list = list->next)
	    {
	      GFlagsValue *fv;

	      item = list->data;
	      fv = g_object_get_qdata (G_OBJECT (item), quark_flags_value);
	      if (GTK_CHECK_MENU_ITEM (item)->active)
		v |= fv->value;
	    }
	  g_value_set_flags (value, v);
	}
      break;
    default:
      break;
    }
  field->editor->ignore_changes--;
}



#if 0
static gchar *sfield_names[SFIELD_N_COLUMNS] = {
  "Object",
  "Signal",
  "Function",
  "Data",
  "Type",
};
static gchar *sfield_tips[SFIELD_N_COLUMNS] = {
  NULL,
  ("Signal name entry, hit TAB multiple times "
   "to get a list of possible completions (features wildcards)."),
  ("Function name entry, hit TAB multiple times "
   "to get a list of possible completions (features wildcards)."),
  NULL,
  NULL,
};
static void	gle_editor_create_signal	(GleEditor		*editor);
static void	gle_editor_set_signal		(GleEditor		*editor);
static void	gle_editor_delete_signal	(GleEditor		*editor);
static void	gle_editor_signal_selected	(GleEditor		*editor,
						 gint       		 row);
static void	gle_editor_apply_signals	(GleEditor		*editor);

static void
clue_hunter_add_signals (GtkClueHunter *clue_hunter,
			 GtkType	type)
{
  GtkObjectClass *class;
  GtkWidget *clist;

  clist = gtk_widget_new (GTK_TYPE_CLIST,
			  "n_columns", 2,
			  "titles_active", FALSE,
			  NULL);
  gtk_clist_set_auto_sort (GTK_CLIST (clist), FALSE);
  gtk_clist_column_titles_hide (GTK_CLIST (clist));

  class = gtk_type_class (type);
  while (class)
    {
      guint *object_signals;
      gint i;

      object_signals = class->signals;

      for (i = class->nsignals - 1; i >= 0; i--)
	{
	  GtkSignalQuery *query;

	  query = object_signals[i] ? gtk_signal_query (object_signals[i]) : NULL;
	  if (query)
	    {
	      gchar *text[2];

	      text[0] = gtk_type_name (query->object_type);
	      text[1] = (gchar*) query->signal_name;

	      gtk_clist_insert (GTK_CLIST (clist), -1, text);
	      g_free (query);
	    }
	}

      class = gtk_type_parent_class (class->type);
    }

  gtk_clue_hunter_set_clist (clue_hunter, clist, 1);
  gtk_clue_hunter_set_align_width (clue_hunter, FALSE);
  gtk_scrolled_window_set_policy (GTK_SCROLLED_WINDOW (clue_hunter->scw),
				  GTK_POLICY_NEVER,
				  GTK_POLICY_AUTOMATIC);
}

static gboolean
clue_hunter_add_handler (GleHandler *handler,
			 gpointer    user_data)
{
  GtkClueHunter *clue_hunter = user_data;

  gtk_clue_hunter_add_string (clue_hunter, g_quark_to_string (handler->id));
  
  return TRUE;
}

static GtkWidget*
create_signal_field (GleEditor *editor,
		     guint	sfield,
		     GtkBox    *parent_box)
{
  GtkWidget *box;
  GtkWidget *label;
  GtkWidget *entry;

  box = gtk_widget_new (GTK_TYPE_HBOX,
			"visible", TRUE,
			"homogeneous", FALSE,
			"spacing", 0,
			"border_width", 0,
			NULL);
  gtk_box_pack_start (parent_box, box, FALSE, TRUE, 0);
  label = gtk_widget_new (GTK_TYPE_LABEL,
			  "label", sfield_names[sfield],
			  "justify", GTK_JUSTIFY_LEFT,
			  "visible", TRUE,
			  NULL);
  gtk_box_pack_start (GTK_BOX (box), label, FALSE, TRUE, 0);
  entry = gtk_widget_new (GTK_TYPE_ENTRY,
			  "width_request", 200,
			  "visible", TRUE,
			  "user_data", GUINT_TO_POINTER (sfield),
			  NULL);
  if (sfield_tips[sfield])
    gtk_tooltips_set_tip (GLE_TOOLTIPS, entry, sfield_tips[sfield], NULL);

  if (sfield == SFIELD_SIGNAL || sfield == SFIELD_FUNC)
    {
      GtkWidget *any, *arrow;

      any = gtk_widget_new (GTK_TYPE_CLUE_HUNTER,
			    "keep_history", FALSE,
			    "entry", entry,
			    "user_data", editor->gobject,
			    NULL);
      GLE_TAG (any, "GLE-Editor-clue-hunter-popup");
      if (sfield == SFIELD_FUNC)
	gle_handler_foreach (clue_hunter_add_handler, any);
      else
	clue_hunter_add_signals (GTK_CLUE_HUNTER (any),
				 GLE_GOBJECT_OTYPE (editor->gobject));
      arrow = gtk_clue_hunter_create_arrow (GTK_CLUE_HUNTER (any));
      gtk_box_pack_end (GTK_BOX (box), arrow, FALSE, TRUE, 0);
    }
  gtk_signal_connect_object (GTK_OBJECT (entry),
			     "activate",
			     GTK_SIGNAL_FUNC (gtk_window_activate_default),
			     GTK_OBJECT (editor));
  gtk_box_pack_end (GTK_BOX (box), entry, FALSE, TRUE, 0);

  return entry;
}

static GtkWidget*
gle_editor_signal_page (GleEditor *editor)
{
  GtkCTreeNode *node;
  GtkWidget *vbox;
  GtkWidget *box;
  GleGObject *gobject;
  gchar *pattern;
  GtkWidget *title;
  gchar *page_name = "Signals";
  GtkWidget *any;

  g_return_val_if_fail (editor->sfield_list == NULL, NULL);

  gobject = editor->gobject;
  
  node = gtk_ctree_insert_node (editor->ctree, NULL, NULL, NULL, 0,
				NULL, NULL, NULL, NULL,
				TRUE, TRUE);
  gtk_ctree_node_set_text (editor->ctree, node, 1, page_name);
  
  pattern = g_strnfill (strlen (page_name), '_');
  title =
    gtk_widget_new (GTK_TYPE_ALIGNMENT,
		    "xalign", 0.5,
		    "yalign", 0.5,
		    "xscale", 1.0,
		    "yscale", 1.0,
		    "visible", TRUE,
		    "child", gtk_widget_new (GTK_TYPE_LABEL,
					     "label", page_name,
					     "pattern", pattern,
					     "visible", TRUE,
					     NULL),
		    NULL);
  g_free (pattern);
  vbox =
    gtk_widget_new (GTK_TYPE_VBOX,
		    "GtkBox::homogeneous", FALSE,
		    "GtkBox::spacing", 0,
		    "GtkContainer::border_width", 5,
		    "GtkWidget::visible", TRUE,
		    "user_data", node,
		    "signal::destroy", gle_editor_notebook_child_destroy, editor,
		    NULL);
  gtk_box_pack_start (GTK_BOX (vbox), title, FALSE, FALSE, 5);

  /* Signal:
   */
  editor->sfield_signal = create_signal_field (editor, SFIELD_SIGNAL, GTK_BOX (vbox));
  gtk_widget_set (editor->sfield_signal,
		  "signal::destroy", gtk_widget_destroyed, &editor->sfield_signal,
		  NULL);
  /* Function:
   */
  editor->sfield_func =  create_signal_field (editor, SFIELD_FUNC, GTK_BOX (vbox));
  gtk_widget_set (editor->sfield_func,
		  "signal::destroy", gtk_widget_destroyed, &editor->sfield_func,
		  NULL);
  /* Data:
   */
  editor->sfield_data =  create_signal_field (editor, SFIELD_DATA, GTK_BOX (vbox));
  gtk_widget_set (editor->sfield_data,
		  "signal::destroy", gtk_widget_destroyed, &editor->sfield_data,
		  NULL);
  /* Signal Type
   */
  {
    struct {
      gchar *name;
      guint  ctype;
    } ctypes[] = {
      { "normal",	GLE_CONNECTION_NORMAL, },
      { "object",	GLE_CONNECTION_OBJECT, },
      { "after",	GLE_CONNECTION_AFTER, },
      { "object after",	GLE_CONNECTION_OBJECT_AFTER, },
      { NULL,		0, },
    }, *ct = ctypes;
    GtkWidget *menu;
    GtkWidget *label;

    box = gtk_widget_new (GTK_TYPE_HBOX,
			  "homogeneous", FALSE,
			  "spacing", 10,
			  "border_width", 0,
			  "visible", TRUE,
			  NULL);
    gtk_box_pack_start (GTK_BOX (vbox), box, FALSE, TRUE, 0);
    label = gtk_widget_new (GTK_TYPE_LABEL,
			    "label", "Connect",
			    "justify", GTK_JUSTIFY_LEFT,
			    "visible", TRUE,
			    NULL);
    gtk_box_pack_start (GTK_BOX (box), label, FALSE, TRUE, 0);
    editor->sfield_ctype = gtk_option_menu_new ();
    gtk_widget_set (editor->sfield_ctype,
		    "signal::destroy", gtk_widget_destroyed, &editor->sfield_ctype,
		    NULL);
    gtk_widget_show (editor->sfield_ctype);
    gtk_box_pack_end (GTK_BOX (box), editor->sfield_ctype, FALSE, TRUE, 0);
    
    menu = gtk_menu_new ();
    GLE_TAG (menu, "GLE-Editor-Signal-Popup");
    
    while (ct->name)
      {
	GtkWidget *item;
	
	item = gtk_menu_item_new_with_label (ct->name);
	gtk_widget_show (item);
	gtk_object_set_user_data (GTK_OBJECT (item), GUINT_TO_POINTER (ct->ctype));
	gtk_container_add (GTK_CONTAINER (menu), item);

	ct++;
      }

    gtk_option_menu_set_menu (GTK_OPTION_MENU (editor->sfield_ctype), menu);
  }
  
  box = gtk_widget_new (GTK_TYPE_HBOX,
			"homogeneous", FALSE,
			"spacing", 5,
			"border_width", 0,
			"visible", TRUE,
			NULL);
  gtk_box_pack_start (GTK_BOX (vbox), box, FALSE, TRUE, 0);
  any =
    gtk_widget_new (GTK_TYPE_BUTTON,
		    "visible", TRUE,
		    "label", "Create",
		    "can_default", TRUE,
		    "object_signal::clicked", gle_editor_create_signal, editor,
		    NULL);
  gtk_box_pack_start (GTK_BOX (box), any, FALSE, TRUE, 0);
  any =
    gtk_widget_new (GTK_TYPE_BUTTON,
		    "visible", TRUE,
		    "label", "Change",
		    "can_default", TRUE,
		    "object_signal::clicked", gle_editor_set_signal, editor,
		    NULL);
  gtk_box_pack_start (GTK_BOX (box), any, FALSE, TRUE, 0);
  gtk_object_set_data (GTK_OBJECT (vbox), "focus", any);
  any =
    gtk_widget_new (GTK_TYPE_BUTTON,
		    "visible", TRUE,
		    "label", "Delete",
		    "can_default", TRUE,
		    "object_signal::clicked", gle_editor_delete_signal, editor,
		    NULL);
  gtk_box_pack_start (GTK_BOX (box), any, FALSE, TRUE, 0);
  any =
    gtk_widget_new (GTK_TYPE_SCROLLED_WINDOW,
		    "visible", TRUE,
		    "hscrollbar_policy", GTK_POLICY_AUTOMATIC,
		    "vscrollbar_policy", GTK_POLICY_AUTOMATIC,
		    "parent", vbox,
		    NULL);
  editor->sfield_list =
    gtk_widget_new (GTK_TYPE_CLIST,
		    "n_columns", SFIELD_N_COLUMNS,
		    "visible", TRUE,
		    "parent", any,
		    "selection_mode", GTK_SELECTION_BROWSE,
		    "signal::destroy", gtk_widget_destroyed, &editor->sfield_list,
		    "object_signal::select_row", gle_editor_signal_selected, editor,
		    NULL);
  gtk_clist_set_auto_sort (GTK_CLIST (editor->sfield_list), TRUE);
  gtk_clist_set_column_title (GTK_CLIST (editor->sfield_list), SFIELD_OBJECT, sfield_names[SFIELD_OBJECT]);
  gtk_clist_set_column_title (GTK_CLIST (editor->sfield_list), SFIELD_SIGNAL, sfield_names[SFIELD_SIGNAL]);
  gtk_clist_set_column_title (GTK_CLIST (editor->sfield_list), SFIELD_FUNC, sfield_names[SFIELD_FUNC]);
  gtk_clist_set_column_title (GTK_CLIST (editor->sfield_list), SFIELD_DATA, sfield_names[SFIELD_DATA]);
  gtk_clist_set_column_title (GTK_CLIST (editor->sfield_list), SFIELD_CTYPE, sfield_names[SFIELD_CTYPE]);
  gtk_clist_column_titles_show (GTK_CLIST (editor->sfield_list));
  gtk_clist_column_titles_passive (GTK_CLIST (editor->sfield_list));
  gtk_ctree_node_set_row_data (editor->ctree, node, vbox);
      
  /* we already rely on GtkNotebook::switch_page here */
  gtk_notebook_append_page (GTK_NOTEBOOK (editor->notebook),
			    vbox,
			    gtk_label_new (page_name));
  
  return vbox;
}

static void
gle_editor_garg_widget_cb (GleGArg		  *garg,
			   GleGArgCbType	   cb_type)
{
  GleEditor *editor;
  
  editor = gtk_object_get_data_by_id (GTK_OBJECT (garg->widget), quark_editor);
  switch (cb_type)
    {
    case GLE_GARG_CB_ARG_RESET:
    case GLE_GARG_CB_ARG_RESTORE:
    case GLE_GARG_CB_ARG_GET:
      gle_editor_refresh_garg (editor, garg);
      break;
    case GLE_GARG_CB_ARG_SET:
    case GLE_GARG_CB_ARG_SAVE:
    default:
      break;
    }
}

static void
gle_editor_signal_selected (GleEditor *editor,
			    gint       row)
{
  GtkCListRow *clist_row;
  GtkCList *clist;
  gchar *text;
  GtkWidget *menu;
  GleConnectionType ctype;

  g_return_if_fail (editor != NULL);
  g_return_if_fail (GLE_IS_EDITOR (editor));

  clist = GTK_CLIST (editor->sfield_list);

  if (row < 0 || row >= clist->rows)
    return ;

  clist_row = g_list_nth (clist->row_list, row)->data;

  text = clist_row->cell[SFIELD_SIGNAL].u.text;
  gtk_entry_set_text (GTK_ENTRY (editor->sfield_signal), text ? text : "");
  text = clist_row->cell[SFIELD_FUNC].u.text;
  gtk_entry_set_text (GTK_ENTRY (editor->sfield_func), text ? text : "");
  text = clist_row->cell[SFIELD_DATA].u.text;
  gtk_entry_set_text (GTK_ENTRY (editor->sfield_data), text ? text : "");
  text = clist_row->cell[SFIELD_CTYPE].u.text;
  ctype = GLE_CONNECTION_NORMAL;
  if (text && (text[0] == 'O' || text[1] == 'O'))
    ctype |= GLE_CONNECTION_OBJECT;
  if (text && (text[0] == 'A' || text[1] == 'A'))
    ctype |= GLE_CONNECTION_AFTER;

    menu = gtk_option_menu_get_menu (GTK_OPTION_MENU (editor->sfield_ctype));
  if (menu)
    {
      guint n;
      GList *list;

      n = 0;
      for (list = GTK_MENU_SHELL (menu)->children; list; list = list->next)
	{
	  GtkWidget *item;
	  gpointer data;

	  item = list->data;
	  data = gtk_object_get_user_data (GTK_OBJECT (item));
	  if (GPOINTER_TO_UINT (data) == ctype)
	    {
	      gtk_option_menu_set_history (GTK_OPTION_MENU (editor->sfield_ctype), n);
	      break;
	    }
	  n++;
	}
    }
  
}

void
gle_editor_update_signals (GleEditor *editor)
{
  GtkCList *clist;
  GleConnection *connection;
  gboolean need_resize;

  g_return_if_fail (editor != NULL);
  g_return_if_fail (GLE_IS_EDITOR (editor));

  clist = GTK_CLIST (editor->sfield_list);

  gtk_clist_freeze (clist);

  need_resize = clist->rows < 1;

  gtk_clist_clear (clist);

  for (connection = editor->gobject->connections; connection; connection = connection->next)
    {
      GtkSignalQuery *query;
      gchar buffer[32], *text[SFIELD_N_COLUMNS] = { NULL, };
      gchar ctype_string[3] = { 0, 0, 0, }, *ctype_p = ctype_string;
      gint row;
      
      query = gtk_signal_query (connection->signal_id);

      text[SFIELD_OBJECT] = gtk_type_name (query->object_type);
      text[SFIELD_SIGNAL] = (gchar*) query->signal_name;
      text[SFIELD_FUNC] = connection->handler;
      sprintf (buffer, "%p", connection->data);
      text[SFIELD_DATA] = buffer;
      if (connection->ctype & GLE_CONNECTION_OBJECT)
	*(ctype_p++) = 'O';
      if (connection->ctype & GLE_CONNECTION_AFTER)
	*(ctype_p++) = 'A';
      text[SFIELD_CTYPE] = ctype_string;

      row = gtk_clist_insert (clist, -1, text);
      gtk_clist_set_row_data (clist, row, connection);
      
      g_free (query);
    }

  if (need_resize)
    gtk_clist_columns_autosize (clist);

  gtk_clist_thaw (clist);
}

static void
gle_editor_change_signal (GleEditor        *editor,
			  gint              row,
			  gchar            *signal,
			  gchar            *func,
			  gchar            *data,
			  GleConnectionType ctype)
{
  GtkCList *clist;
  GtkSignalQuery *query;
  gchar buffer[32];
  gchar ctype_string[3] = { 0, 0, 0, }, *ctype_p = ctype_string;
  guint signal_id;
  guint base;

  clist = GTK_CLIST (editor->sfield_list);

  gtk_clist_freeze (clist);
  
  signal_id = gtk_signal_lookup (signal, GLE_GOBJECT_OTYPE (editor->gobject));
  if (!signal_id)
    signal_id = 1;
  query = gtk_signal_query (signal_id);
  if (!query)
    gtk_signal_query (1);

  gtk_clist_set_text (clist, row, SFIELD_OBJECT, gtk_type_name (query->object_type));
  gtk_clist_set_text (clist, row, SFIELD_SIGNAL, query->signal_name);
  gtk_clist_set_text (clist, row, SFIELD_FUNC, func);
  if (data && data[0] == '0')
    {
      base = 8;
      data++;
      if (data[0] == 'x' || data[0] == 'X')
	{
	  base = 16;
	  data++;
	}
    }
  else
    base = 10;
  sprintf (buffer, "%p", GINT_TO_POINTER (strtol (data, NULL, base)));
  gtk_clist_set_text (clist, row, SFIELD_DATA, buffer);
  if (ctype & GLE_CONNECTION_OBJECT)
    *(ctype_p++) = 'O';
  if (ctype & GLE_CONNECTION_AFTER)
    *(ctype_p++) = 'A';
  gtk_clist_set_text (clist, row, SFIELD_CTYPE, ctype_string);

  g_free (query);

  gtk_clist_thaw (clist);

  gle_editor_signal_selected (editor, row);
}

static void
gle_editor_create_signal (GleEditor *editor)
{
  static gchar *text[SFIELD_N_COLUMNS] = { NULL, };
  GtkCList *clist;
  gboolean need_resize;
  gint row;
  gchar *signal;
  gchar *func;
  gchar *data;
  gpointer ctype;

  g_return_if_fail (editor != NULL);
  g_return_if_fail (GLE_IS_EDITOR (editor));

  clist = GTK_CLIST (editor->sfield_list);

  signal = g_strdup (gtk_entry_get_text (GTK_ENTRY (editor->sfield_signal)));
  func = g_strdup (gtk_entry_get_text (GTK_ENTRY (editor->sfield_func)));
  data = g_strdup (gtk_entry_get_text (GTK_ENTRY (editor->sfield_data)));
  ctype = gtk_object_get_user_data (GTK_OBJECT (GTK_OPTION_MENU (editor->sfield_ctype)->menu_item));

  gtk_clist_freeze (clist);
  
  need_resize = clist->rows < 1;
      
  row = gtk_clist_insert (GTK_CLIST (clist), 0, text);

  gle_editor_change_signal (editor, row, signal, func, data, GPOINTER_TO_UINT (ctype));

  g_free (signal);
  g_free (func);
  g_free (data);

  if (need_resize)
    gtk_clist_columns_autosize (clist);

  gtk_clist_thaw (clist);
}

static void
gle_editor_set_signal (GleEditor *editor)
{
  GtkCList *clist;
  gint row;
  gchar *signal;
  gchar *func;
  gchar *data;
  gpointer ctype;
  
  g_return_if_fail (editor != NULL);
  g_return_if_fail (GLE_IS_EDITOR (editor));

  clist = GTK_CLIST (editor->sfield_list);

  row = clist->selection ? GPOINTER_TO_INT (clist->selection->data) : -1;

  if (row < 0 || row >= clist->rows)
    return;
  
  signal = g_strdup (gtk_entry_get_text (GTK_ENTRY (editor->sfield_signal)));
  func = g_strdup (gtk_entry_get_text (GTK_ENTRY (editor->sfield_func)));
  data = g_strdup (gtk_entry_get_text (GTK_ENTRY (editor->sfield_data)));
  ctype = gtk_object_get_user_data (GTK_OBJECT (GTK_OPTION_MENU (editor->sfield_ctype)->menu_item));

  gle_editor_change_signal (editor, row, signal, func, data, GPOINTER_TO_UINT (ctype));

  g_free (signal);
  g_free (func);
  g_free (data);

  gle_editor_signal_selected (editor, row);
}

static void
gle_editor_apply_signals (GleEditor *editor)
{
  GtkCList *clist;
  GSList *delete_connections = NULL;
  GleConnection *cwalk;
  GSList *slist;
  GList *list;
  
  g_return_if_fail (editor != NULL);
  g_return_if_fail (GLE_IS_EDITOR (editor));

  clist = GTK_CLIST (editor->sfield_list);

  for (cwalk = editor->gobject->connections; cwalk; cwalk = cwalk->next)
    delete_connections = g_slist_prepend (delete_connections, cwalk);

  for (list = clist->row_list; list; list = list->next)
    {
      GtkCListRow *clist_row = list->data;
      gchar *text;
      guint signal_id;
      gchar *handler;
      gpointer data;
      GleConnectionType ctype;
      guint base;

      if (clist_row->data && !g_slist_find (delete_connections, clist_row->data))
	{
	  clist_row->data = NULL;
	  continue;
	}

      text = clist_row->cell[SFIELD_SIGNAL].u.text;
      signal_id = gtk_signal_lookup (text ? text : "", GLE_GOBJECT_OTYPE (editor->gobject));
      if (!signal_id)
	signal_id = 1;
      
      handler = clist_row->cell[SFIELD_FUNC].u.text;
      if (!handler)
	handler = "gtk_false";
      
      text = clist_row->cell[SFIELD_DATA].u.text;
      if (text && text[0] == '0')
	{
	  base = 8;
	  text++;
	  if (text[0] == 'x' || text[0] == 'X')
	    {
	      base = 16;
	      text++;
	    }
	}
      else
	base = 10;
      data = (gpointer) strtol (text ? text : "", NULL, base);

      text = clist_row->cell[SFIELD_CTYPE].u.text;
      ctype = GLE_CONNECTION_NORMAL;
      if (text && (text[0] == 'O' || text[1] == 'O'))
	ctype |= GLE_CONNECTION_OBJECT;
      if (text && (text[0] == 'A' || text[1] == 'A'))
	ctype |= GLE_CONNECTION_AFTER;

      if (!clist_row->data)
	clist_row->data = gle_connection_new (editor->gobject,
					      signal_id,
					      handler,
					      data,
					      ctype);
      else
	{
	  delete_connections = g_slist_remove (delete_connections, clist_row->data);
	  gle_connection_change (clist_row->data,
				 signal_id,
				 handler,
				 data,
				 ctype);
	}
    }
  
  for (slist = delete_connections; slist; slist = slist->next)
    gle_connection_destroy (slist->data);

  g_slist_free (delete_connections);

  gle_gobject_connect (editor->gobject);
}

static void
gle_editor_delete_signal (GleEditor *editor)
{
  GtkCList *clist;
  gint row;

  g_return_if_fail (editor != NULL);
  g_return_if_fail (GLE_IS_EDITOR (editor));

  clist = GTK_CLIST (editor->sfield_list);

  row = clist->selection ? GPOINTER_TO_INT (clist->selection->data) : -1;

  if (row < 0 || row >= clist->rows)
    return;
  
  gtk_clist_remove (GTK_CLIST (clist), row);
}

void
gle_editor_update_names (GleEditor *editor)
{
  static gchar *generic_prompt = "Generic GLE Name:";
  static const guint prompt_offs = 8;
  gchar *string;
  
  g_return_if_fail (editor != NULL);
  g_return_if_fail (GLE_IS_EDITOR (editor));
  
  if (editor->is_widget)
    {
      GtkWidget *widget;
      
      widget = GLE_GWIDGET_WIDGET (editor->gobject);
      string = g_strconcat (gtk_type_name (GLE_GOBJECT_OTYPE (editor->gobject)),
			    "::",
			    "\"",
			    widget && widget->name ? widget->name : "",
			    "\"",
			    NULL);
    }
  else
    string = g_strconcat (gtk_type_name (GLE_GOBJECT_OTYPE (editor->gobject)), NULL);
  
  if (!g_str_equal (GTK_LABEL (editor->object_name_label)->label, string))
    gtk_label_set_text (GTK_LABEL (editor->object_name_label), string);
  g_free (string);
  
  if (GLE_GOBJECT_GENERIC_GNAME (editor->gobject))
    {
      if (!g_str_equal (GTK_LABEL (editor->gname_label)->label, generic_prompt))
	gtk_label_set_text (GTK_LABEL (editor->gname_label), generic_prompt);
    }
  else
    {
      if (!g_str_equal (GTK_LABEL (editor->gname_label)->label, generic_prompt + prompt_offs))
	gtk_label_set_text (GTK_LABEL (editor->gname_label), generic_prompt + prompt_offs);
    }
  
  if (!g_str_equal (gtk_entry_get_text (GTK_ENTRY (editor->gname_entry)), editor->gobject->gname))
    {
      gtk_entry_set_text (GTK_ENTRY (editor->gname_entry),
			  editor->gobject->gname ? editor->gobject->gname : "");
      gtk_entry_set_position (GTK_ENTRY (editor->gname_entry), 0);
    }
}

static void
gle_editor_real_refresh_values (GleEditor *editor)
{
  GleGArgGroup *group;
  GleGObject *gobject;
  
  gobject = editor->gobject;
  
  for (group = gobject->garg_groups;
       group < gobject->garg_groups + gobject->n_garg_groups;
       group++)
    {
      GSList *slist;
      
      for (slist = group->gargs; slist; slist = slist->next)
	{
	  GleGArg *garg;
	  
	  garg = slist->data;
	  
	  if (garg->widget)
	    gle_editor_refresh_garg (editor, garg);
	}
    }
}

static gint
gle_editor_reload_handler (GleEditor *editor)
{
  GDK_THREADS_ENTER ();
  
  g_return_val_if_fail (editor != NULL, FALSE);
  g_return_val_if_fail (GLE_IS_EDITOR (editor), FALSE);
  
  editor->reload_queued = FALSE;
  
  if (!GTK_OBJECT_DESTROYED (editor))
    gtk_signal_emit (GTK_OBJECT (editor), editor_signals[SIGNAL_RELOAD_VALUES]);
  
  gtk_object_unref (GTK_OBJECT (editor));

  GDK_THREADS_LEAVE ();
  
  return FALSE;
}

void
gle_editor_queue_reload_values (GleEditor *editor)
{
  g_return_if_fail (editor != NULL);
  g_return_if_fail (GLE_IS_EDITOR (editor));
  
  if (!editor->reload_queued)
    {
      editor->reload_queued = TRUE;
      
      gtk_object_ref (GTK_OBJECT (editor));
      
      gtk_idle_add ((GtkFunction) gle_editor_reload_handler, editor);
    }
}

static void
gle_editor_real_reload_values (GleEditor *editor)
{
  g_return_if_fail (editor != NULL);
  g_return_if_fail (GLE_IS_EDITOR (editor));
  
  gle_gobject_update_all_args (editor->gobject);
  gle_editor_update_names (editor);
  gle_editor_update_signals (editor);
}

void
gle_editor_apply_values (GleEditor *editor)
{
  g_return_if_fail (editor != NULL);
  g_return_if_fail (GLE_IS_EDITOR (editor));
  
  gtk_signal_emit (GTK_OBJECT (editor), editor_signals[SIGNAL_APPLY_VALUES]);
}

static void
gle_editor_real_apply_values (GleEditor *editor)
{
  GleGObject *gobject;
  GleGArgGroup *group;
  gchar *gname;
  
  g_return_if_fail (editor != NULL);
  g_return_if_fail (GLE_IS_EDITOR (editor));
  
  gobject = editor->gobject;
  
  gname = g_strdup (gtk_entry_get_text (GTK_ENTRY (editor->gname_entry)));
  gle_gname_canonicalize (gname);
  if (gname[0] &&
      !g_str_equal ((gchar*) editor->gobject->gname, gname) &&
      !gle_gname_lookup (gname))
    gle_gobject_set_gname (editor->gobject, gname);
  g_free (gname);
  
  for (group = gobject->garg_groups;
       group < gobject->garg_groups + gobject->n_garg_groups;
       group++)
    {
      GSList *slist;
      
      for (slist = group->gargs; slist; slist = slist->next)
	{
	  GleGArg *garg;
	  
	  garg = slist->data;
	  
	  if (GLE_GARG_IS_READWRITE (garg) && garg->widget)
	    garg->needs_set |= gle_arg_set_from_field (&garg->object_arg, garg->widget);
	}
    }
  if (GLE_GOBJECT_IS_INSTANTIATED (editor->gobject))
    gle_gobject_apply_all_args (editor->gobject);
  else
    gle_gobject_save_all_args (editor->gobject);
  
  gle_editor_apply_signals (editor);

  gle_editor_queue_reload_values (editor);
}

void
gle_editor_restore_values (GleEditor *editor)
{
  g_return_if_fail (editor != NULL);
  g_return_if_fail (GLE_IS_EDITOR (editor));
  
  gtk_signal_emit (GTK_OBJECT (editor), editor_signals[SIGNAL_RESTORE_VALUES]);
}

static void
gle_editor_real_restore_values (GleEditor *editor)
{
  gle_gobject_restore_all_args (editor->gobject);
  gle_editor_refresh_values (editor);
}

void
gle_editor_refresh_garg (GleEditor	*editor,
			 GleGArg	*garg)
{
  g_return_if_fail (editor != NULL);
  g_return_if_fail (GLE_IS_EDITOR (editor));
  g_return_if_fail (garg != NULL);
  if (!garg->widget)
    return;
  g_return_if_fail (gtk_widget_get_toplevel (garg->widget) == GTK_WIDGET (editor));
  
  gtk_signal_emit (GTK_OBJECT (editor), editor_signals[SIGNAL_REFRESH_GARG], garg);
}

static void
gle_editor_real_refresh_garg (GleEditor	     *editor,
			      GleGArg	     *garg)
{
  g_return_if_fail (garg != NULL);
  g_return_if_fail (garg->widget != NULL);
  
  gle_arg_refresh_field (&garg->object_arg, garg->widget);
}

GleEditor*
gle_editor_from_gobject (GleGObject	*gobject)
{
  g_return_val_if_fail (gobject != NULL, NULL);
  g_return_val_if_fail (GLE_IS_GOBJECT (gobject), NULL);
  
  return gle_gobject_get_qdata (gobject, quark_editor);
}

static const gchar *key_arg_ev = "gle-arg-item-enum-value";



#endif
