/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#include	"glerc.h"
#include	<stdarg.h>
#include        <fcntl.h>
#include        <sys/stat.h>
#include	<unistd.h>
#include	<string.h>
#include	<stdio.h>


/* --- structures --- */
typedef struct _GleRcData GleRcData;
typedef struct _GleRcHook GleRcHook;
struct _GleRcData
{
  gchar		*prg_name;
  GList		*arg_list;
  GHookList	 rc_hooks;
};
struct _GleRcHook
{
  GHook		 hook;
  const gchar	*name;
};


/* --- variables --- */
static	GScannerConfig	scanner_config =
{
  (
   " \t\n"
   )			/* cset_skip_characters */,
  (
   G_CSET_a_2_z
   "_#"
   G_CSET_A_2_Z
   )			/* cset_identifier_first */,
  (
   G_CSET_a_2_z
   "-+_0123456789"
   G_CSET_A_2_Z
   G_CSET_LATINS
   G_CSET_LATINC
   )			/* cset_identifier_nth */,
  ( ";\n" )		/* cpair_comment_single */,
  
  FALSE			/* case_sensitive */,
  
  TRUE			/* skip_comment_multi */,
  TRUE			/* skip_comment_single */,
  FALSE			/* scan_comment_multi */,
  TRUE			/* scan_identifier */,
  FALSE			/* scan_identifier_1char */,
  FALSE			/* scan_identifier_NULL */,
  TRUE			/* scan_symbols */,
  TRUE			/* scan_binary */,
  TRUE			/* scan_octal */,
  TRUE			/* scan_float */,
  TRUE			/* scan_hex */,
  FALSE			/* scan_hex_dollar */,
  TRUE			/* scan_string_sq */,
  TRUE			/* scan_string_dq */,
  TRUE			/* numbers_2_int */,
  TRUE			/* int_2_float */,
  FALSE			/* identifier_2_string */,
  TRUE			/* char_2_token */,
  FALSE			/* symbol_2_token */,
  FALSE                 /* scope_0_fallback */,
};


/* --- functions --- */
static void
gle_rc_init (void)
{
  if (!0)
    {
      /*    gle_rc_scanner = g_scanner_new (&scanner_config);
            key_id_misc = g_dataset_force_id (key_misc);
            gle_rc_group (key_id_misc); */
    }
}

GleRcHandle*
gle_rc_handle_new (const gchar    *prg_name)
{
  GScanner *rc_scanner;
  GleRcHandle *rc;
  GleRcData *rc_data;

  g_return_val_if_fail (prg_name != NULL, NULL);

  gle_rc_init ();

  rc_scanner = g_scanner_new (&scanner_config);
  rc = (GleRcHandle*) rc_scanner;
  rc_data = g_new0 (GleRcData, 1);
  rc_scanner->derived_data = rc_data;

  rc_data->prg_name = g_strdup (prg_name);
  rc_data->arg_list = NULL;
  g_hook_list_init (&rc_data->rc_hooks,
		    sizeof (GleRcHook));

  return rc;
}

void
gle_rc_declare_arg (GleRcHandle *rc,
		    GleRcArg  *arg)
{
  GScanner *rc_scanner = (GScanner*) rc;
  GleRcData *rc_data;

  g_return_if_fail (rc != NULL);
  g_return_if_fail (arg != NULL);
  g_return_if_fail (arg->any.type < GLE_RC_LAST);
  g_return_if_fail (g_scanner_lookup_symbol (rc_scanner, arg->any.name) == NULL);
  rc_data = rc_scanner->derived_data;
  g_return_if_fail (rc_data != NULL);

  if (arg->any.type > GLE_RC_NONE)
    {
      switch (arg->any.type)
	{
	case GLE_RC_BOOL:
	  arg->abool.value = arg->abool.default_value;
	  break;
	case GLE_RC_LONG:
	  arg->along.value = arg->along.default_value;
	  break;
	case GLE_RC_DOUBLE:
	  arg->adouble.value = arg->adouble.default_value;
	  break;
	case GLE_RC_STRING:
	  arg->astring.value = g_strdup (arg->astring.default_value);
	  if (!arg->astring.value)
	    arg->astring.value = g_strdup ("");
	  break;
	case GLE_RC_CALLBACK:
	  g_return_if_fail (arg->acallback.parse_arg != NULL);
	  arg->acallback.value = arg->acallback.default_value;
	  break;
	default:
	  g_assert_not_reached ();
	  break;
	}
      arg->any.flags &= GLE_RC_MASK;

      rc_data->arg_list = g_list_prepend (rc_data->arg_list, arg);

      g_scanner_add_symbol (rc_scanner, arg->any.name, arg);
    }
  else
    g_scanner_add_symbol (rc_scanner, arg->any.name, NULL);
}

GList*
gle_rc_get_arg_list (GleRcHandle *rc)
{
  GScanner *rc_scanner = (GScanner*) rc;
  GleRcData *rc_data;

  g_return_val_if_fail (rc != NULL, NULL);
  rc_data = rc_scanner->derived_data;
  g_return_val_if_fail (rc_data != NULL, NULL);

  return rc_data->arg_list;
}

static gboolean
arg_has_default (GleRcArg *arg)
{
  if (arg->any.type == GLE_RC_CALLBACK &&
      arg->acallback.dump_arg == NULL)
    return TRUE;

  switch (arg->any.type)
    {
    case GLE_RC_BOOL:
      return arg->abool.value == arg->abool.default_value;
    case GLE_RC_LONG:
      return arg->along.value == arg->along.default_value;
    case GLE_RC_DOUBLE:
      return arg->adouble.value == arg->adouble.default_value;
    case GLE_RC_STRING:
      return g_str_equal (arg->astring.value, arg->astring.default_value);
    case GLE_RC_CALLBACK:
      return (arg->acallback.has_default && arg->acallback.has_default (&arg->acallback));
    default:
      g_assert_not_reached ();
      return TRUE;
    }
}

static guint
parse_arg (GScanner *scanner,
	   GleRcArg *arg)
{
  gboolean negate = FALSE;

  switch (arg->any.type)
    {
      gdouble v;
      gchar *s;

    case GLE_RC_BOOL:
      scanner->config->scan_symbols = FALSE;
      g_scanner_get_next_token (scanner);
      scanner->config->scan_symbols = TRUE;
      if (scanner->token == G_TOKEN_IDENTIFIER)
	{
	  s = scanner->value.v_identifier;
	  if (*s == '#')
	    s++;
	  arg->abool.value = (*s == 't' || *s == 'T' ||
			      *s == 'j' || *s == 'J' ||
			      *s == 'y' || *s == 'Y');
	}
      else if (scanner->token == G_TOKEN_FLOAT)
	arg->abool.value = scanner->value.v_float >= 0.5 || scanner->value.v_float <= -0.5;
      else
	return G_TOKEN_IDENTIFIER;
      break;

    case GLE_RC_LONG:
      g_scanner_get_next_token (scanner);
      while (scanner->token == '-')
	{
	  negate = !negate;
	  g_scanner_get_next_token (scanner);
	}
      if (scanner->token != G_TOKEN_FLOAT)
	return G_TOKEN_INT;
      v = negate ? - scanner->value.v_float : scanner->value.v_float;
      arg->along.value = CLAMP (v, arg->along.minimum, arg->along.maximum);
      break;

    case GLE_RC_DOUBLE:
      g_scanner_get_next_token (scanner);
      while (scanner->token == '-')
	{
	  negate = !negate;
	  g_scanner_get_next_token (scanner);
	}
      if (scanner->token != G_TOKEN_FLOAT)
	return G_TOKEN_FLOAT;
      v = negate ? - scanner->value.v_float : scanner->value.v_float;
      arg->adouble.value = CLAMP (v, arg->adouble.minimum, arg->adouble.maximum);
      break;

    case GLE_RC_STRING:
      g_scanner_peek_next_token (scanner);
      if (scanner->next_token != G_TOKEN_STRING)
	{
	  g_scanner_get_next_token (scanner);
	  return G_TOKEN_STRING;
	}
      s = g_strdup ("");
      while (scanner->next_token == G_TOKEN_STRING)
	{
	  guint l;
	  gchar *o;

	  g_scanner_get_next_token (scanner);

	  l = strlen (s);
	  o = s;
	  s = g_strconcat (o, l > 0 ? " " : "", scanner->value.v_string, NULL);
	  g_free (o);

	  g_scanner_peek_next_token (scanner);
	}
      g_free (arg->astring.value);
      arg->astring.value = s;
      break;

    default:
      g_assert_not_reached ();
      break;
    }

  g_scanner_get_next_token (scanner);

  return scanner->token == ')' ? G_TOKEN_NONE : ')';
}

static void
parse_statement (GScanner            *scanner)
{
  guint expected_token;
  gboolean is_error = FALSE;
  guint parse_level = 0;
  
  g_scanner_get_next_token (scanner);
  if (scanner->token == '(')
    {
      parse_level = 1;
      g_scanner_get_next_token (scanner);
      
      if (scanner->token == G_TOKEN_SYMBOL)
	{
	  if (scanner->value.v_symbol)
	    {
	      GleRcArg *arg;

	      arg = scanner->value.v_symbol;
	      if (arg->any.type == GLE_RC_CALLBACK)
		expected_token = arg->acallback.parse_arg (scanner, &arg->acallback);
	      else
		expected_token = parse_arg (scanner, arg);
	    }
	  else
	    expected_token = G_TOKEN_NONE;
	}
      else
	expected_token = G_TOKEN_SYMBOL;
    }
  else
    {
      expected_token = '(';
      is_error = TRUE;
    }

  if (expected_token != G_TOKEN_NONE)
    {
      g_scanner_unexp_token (scanner, expected_token, NULL, "argument", NULL, NULL, is_error);
      
      /* skip rest of statement on errrors
       */
      if (parse_level)
	{
	  register guint level;

	  level = parse_level;
	  if (scanner->token == ')')
	    level--;
	  if (scanner->token == '(')
	    level++;
	  
	  while (!g_scanner_eof (scanner) && level > 0)
	    {
	      g_scanner_get_next_token (scanner);
	      
	      if (scanner->token == '(')
		level++;
	      else if (scanner->token == ')')
		level--;
	    }
	}
    }
}

void
gle_rc_parse (GleRcHandle	*rc,
	      const gchar	*file_name)
{
  GScanner *rc_scanner = (GScanner*) rc;
  gint fd;

  g_return_if_fail (rc != NULL);
  g_return_if_fail (file_name != NULL);

  fd = open (file_name, O_RDONLY);
  if (fd < 0)
    return;

  g_scanner_input_file (rc_scanner, fd);
  rc_scanner->parse_errors = 0;
  rc_scanner->input_name = file_name;

  g_scanner_peek_next_token (rc_scanner);

  while (rc_scanner->next_token != G_TOKEN_EOF &&
	 rc_scanner->parse_errors == 0)
    {
      parse_statement (rc_scanner);

      g_scanner_peek_next_token (rc_scanner);
    }

  rc_scanner->input_name = NULL;
  g_scanner_input_text (rc_scanner, "", 0);

  close (fd);
}

static void
dump_arg (FILE		*f_out,
	  GleRcArg	*arg)
{
  gboolean dfl;

  dfl = arg_has_default (arg);
  
  if (dfl && !(arg->any.flags & GLE_RC_FORCE_DUMP))
    return;

  fputc ('\n', f_out);

  if (arg->any.description)
    {
      gchar *s;

      s = arg->any.description;
      while (s[0] != 0)
	{
	  gchar *p;

	  fputs ("; ", f_out);
	  p = strchr (s, '\n');
	  if (!p)
	    p = s + strlen (s);
	  fwrite (s, 1, p - s, f_out);
	  fputc ('\n', f_out);
	  
	  s = p + (*p == '\n' ? 1 : 0);
	}
    }

  if (dfl && ((arg->any.type == GLE_RC_BOOL) ||
	      (arg->any.type == GLE_RC_LONG) ||
	      (arg->any.type == GLE_RC_DOUBLE) ||
	      (arg->any.type == GLE_RC_STRING)))
    fputc (';', f_out);
  
  fputc ('(', f_out);
  fputs (arg->any.name, f_out);
  switch (arg->any.type)
    {
    case GLE_RC_BOOL:
      fprintf (f_out, " #%c", arg->abool.value ? 't' : 'f');
      break;
    case GLE_RC_LONG:
      fprintf (f_out, " %ld", arg->along.value);
      break;
    case GLE_RC_DOUBLE:
      fprintf (f_out, " %f", arg->adouble.value);
      break;
    case GLE_RC_STRING:
      fprintf (f_out, " \"%s\"", arg->astring.value ? arg->astring.value : "");
      break;
    case GLE_RC_CALLBACK:
      fputc (' ', f_out);
      arg->acallback.dump_arg (f_out, &arg->acallback, "  ");
      break;
    default:
      g_assert_not_reached ();
      break;
    }
  fputs (")\n", f_out);
}

void
gle_rc_dump (GleRcHandle	*rc,
	     const gchar	*file_name)
{
  GScanner *rc_scanner = (GScanner*) rc;
  GleRcData *rc_data;
  FILE *f_out;
  GList *list;
  gboolean have_auto;

  g_return_if_fail (rc != NULL);
  rc_data = rc_scanner->derived_data;
  g_return_if_fail (rc_data != NULL);
  g_return_if_fail (file_name != NULL);

  f_out = fopen (file_name, "w");
  if (!f_out)
    return;

  fputs ("; ", f_out);
  fputs (rc_data->prg_name, f_out);
  fputs (" rc-file         -*- scheme -*-\n", f_out);
  fputs ("; this file will automatically be rewritten at program shutdown\n", f_out);
  fputs (";\n", f_out);

  have_auto = FALSE;
  for (list = g_list_last (rc_data->arg_list); list; list = list->prev)
    {
      GleRcArg *arg;

      arg = list->data;
      if (!(arg->any.flags & GLE_RC_AUTOMATIC))
	dump_arg (f_out, arg);
      else
	have_auto = TRUE;
    }

  if (have_auto)
    {
      fputs ("\n\n; automatic data\n", f_out);
      fputs (";\n", f_out);
    }
  for (list = g_list_last (rc_data->arg_list); list; list = list->prev)
    {
      GleRcArg *arg;

      arg = list->data;
      if (arg->any.flags & GLE_RC_AUTOMATIC)
	dump_arg (f_out, arg);
    }

  fclose (f_out);
}

const gchar*
gle_rc_handle_prg_name (GleRcHandle    *rc)
{
  GScanner *rc_scanner = (GScanner*) rc;
  GleRcData *rc_data;
  
  g_return_val_if_fail (rc != NULL, NULL);
  rc_data = rc_scanner->derived_data;
  g_return_val_if_fail (rc_data != NULL, NULL);

  return rc_data->prg_name;
}

GleRcArg*
gle_rc_get_arg (GleRcHandle	*rc,
		const gchar     *name)
{
  GScanner *rc_scanner = (GScanner*) rc;

  g_return_val_if_fail (rc != NULL, NULL);
  g_return_val_if_fail (name != NULL, NULL);

  return g_scanner_lookup_symbol (rc_scanner, name);
}

gboolean
gle_rc_get_bool (GleRcHandle    *rc,
		 const gchar    *name)
{
  GleRcArg *arg;

  arg = gle_rc_get_arg (rc, name);
  if (!arg || arg->any.type != GLE_RC_BOOL)
    {
      g_warning ("%s: no boolean argument \"%s\"",
		 gle_rc_handle_prg_name (rc),
		 name);
      return 0;
    }

  return arg->abool.value;
}

glong
gle_rc_get_long (GleRcHandle    *rc,
		 const gchar    *name)
{
  GleRcArg *arg;

  arg = gle_rc_get_arg (rc, name);
  if (!arg || arg->any.type != GLE_RC_LONG)
    {
      g_warning ("%s: no long number argument \"%s\"",
		 gle_rc_handle_prg_name (rc),
		 name);
      return 0;
    }

  return arg->along.value;
}

gdouble
gle_rc_get_double (GleRcHandle    *rc,
		   const gchar    *name)
{
  GleRcArg *arg;

  arg = gle_rc_get_arg (rc, name);
  if (!arg || arg->any.type != GLE_RC_DOUBLE)
    {
      g_warning ("%s: no floating number argument \"%s\"",
		 gle_rc_handle_prg_name (rc),
		 name);
      return 0;
    }

  return arg->adouble.value;
}

const gchar*
gle_rc_get_string (GleRcHandle    *rc,
		   const gchar    *name)
{
  GleRcArg *arg;

  arg = gle_rc_get_arg (rc, name);
  if (!arg || arg->any.type != GLE_RC_STRING)
    {
      g_warning ("%s: no string argument \"%s\"",
		 gle_rc_handle_prg_name (rc),
		 name);
      return NULL;
    }

  return arg->astring.value;
}

guint
gle_rc_add_hook (GleRcHandle    *rc,
		 const gchar    *name,
		 GleRcHookFunc   changed_hook,
		 gpointer        data)
{
  return gle_rc_add_hook_full (rc, name, changed_hook, data, NULL);
}

guint
gle_rc_add_hook_full (GleRcHandle    *rc,
		      const gchar    *name,
		      GleRcHookFunc   changed_hook,
		      gpointer        data,
		      GDestroyNotify  destroy)
{
  GScanner *rc_scanner = (GScanner*) rc;
  GleRcData *rc_data;
  GleRcArg *arg;
  GHook *hook;
  GleRcHook *rc_hook;

  g_return_val_if_fail (rc != NULL, 0);
  rc_data = rc_scanner->derived_data;
  g_return_val_if_fail (rc_data != NULL, 0);
  g_return_val_if_fail (name != NULL, 0);
  g_return_val_if_fail (changed_hook != NULL, 0);

  arg = gle_rc_get_arg (rc, name);
  if (!arg)
    {
      g_warning ("%s: no such argument \"%s\"",
		 gle_rc_handle_prg_name (rc),
		 name);
      return 0;
    }

  hook = g_hook_alloc (&rc_data->rc_hooks);
  rc_hook = (GleRcHook*) hook;
  hook->func = changed_hook;
  hook->data = data;
  hook->destroy = destroy;
  rc_hook->name = arg->name;

  g_hook_prepend (&rc_data->rc_hooks, hook);

  return hook->hook_id;
}

void
gle_rc_remove_hook (GleRcHandle    *rc,
		    guint           hook_id)
{
  GScanner *rc_scanner = (GScanner*) rc;
  GleRcData *rc_data;

  g_return_if_fail (rc != NULL);
  rc_data = rc_scanner->derived_data;
  g_return_if_fail (rc_data != NULL);
  g_return_if_fail (hook_id > 0);

  if (!g_hook_destroy (&rc_data->rc_hooks, hook_id))
    g_warning ("%s: no such hook id `%u'",
	       gle_rc_handle_prg_name (rc),
	       hook_id);
}

static void
gle_rc_hook_marshal (GHook          *hook,
		     gpointer        data)
{
  GleRcHook *rc_hook = (GleRcHook*) hook;
  GleRcArg *arg = data;

  if (rc_hook->name == arg->name)
    {
      GleRcHookFunc func;

      func = (GleRcHookFunc) hook->func;
      func (hook->data, arg);
    }
}

static void
gle_rc_hook (GleRcHandle    *rc,
	     GleRcArg	    *arg)
{
  GScanner *rc_scanner = (GScanner*) rc;
  GleRcData *rc_data;

  g_return_if_fail (rc != NULL);
  rc_data = rc_scanner->derived_data;
  g_return_if_fail (rc_data != NULL);
  g_return_if_fail (arg != NULL);

  g_hook_list_marshal (&rc_data->rc_hooks, FALSE, gle_rc_hook_marshal, arg);
}

void
gle_rc_set_bool (GleRcHandle    *rc,
		 const gchar    *name,
		 gboolean        value)
{
  GleRcArg *arg;

  arg = gle_rc_get_arg (rc, name);
  if (!arg || arg->any.type != GLE_RC_BOOL)
    {
      g_warning ("%s: no boolean argument \"%s\"",
		 gle_rc_handle_prg_name (rc),
		 name);
      return;
    }

  arg->abool.value = value;
  gle_rc_hook (rc, arg);
}

void
gle_rc_set_long (GleRcHandle    *rc,
		 const gchar    *name,
		 glong           value)
{
  GleRcArg *arg;

  arg = gle_rc_get_arg (rc, name);
  if (!arg || arg->any.type != GLE_RC_LONG)
    {
      g_warning ("%s: no long number argument \"%s\"",
		 gle_rc_handle_prg_name (rc),
		 name);
      return;
    }

  arg->along.value = value;
  gle_rc_hook (rc, arg);
}

void
gle_rc_set_double (GleRcHandle    *rc,
		   const gchar    *name,
		   gdouble         value)
{
  GleRcArg *arg;

  arg = gle_rc_get_arg (rc, name);
  if (!arg || arg->any.type != GLE_RC_DOUBLE)
    {
      g_warning ("%s: no floating number argument \"%s\"",
		 gle_rc_handle_prg_name (rc),
		 name);
      return;
    }

  arg->adouble.value = value;
  gle_rc_hook (rc, arg);
}

void
gle_rc_set_string (GleRcHandle    *rc,
		   const gchar    *name,
		   const gchar    *value)
{
  GleRcArg *arg;
  gchar *string;

  arg = gle_rc_get_arg (rc, name);
  if (!arg || arg->any.type != GLE_RC_STRING)
    {
      g_warning ("%s: no string argument \"%s\"",
		 gle_rc_handle_prg_name (rc),
		 name);
      return;
    }

  string = g_strdup (value);
  g_free (arg->astring.value);
  arg->astring.value = string;
  gle_rc_hook (rc, arg);
}
