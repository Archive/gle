/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001-2002 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GLE_FOREIGN_H__
#define __GLE_FOREIGN_H__


#include        <gle/gleutils.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* --- convenience API for foreign => proxy notifies --- */
typedef enum
{
  GLE_FOREIGN_RELOAD_LINK_PARAMS        = (1 << 0),
  GLE_FOREIGN_RELOAD_PARENT             = (1 << 1),
  GLE_FOREIGN_UPDATE_OBJECT_PARAMS      = (1 << 2),
  GLE_FOREIGN_UPDATE_LINK_PARAMS        = (1 << 3),
  GLE_FOREIGN_DETACH_OBJECT             = (1 << 4)
} GleForeignNotify;
void	gle_foreign_reload_link_params		(GleProxy 	*proxy);
void	gle_foreign_reload_parent		(GleProxy 	*proxy);
void	gle_foreign_update_object_params	(GleProxy 	*proxy);
void	gle_foreign_update_link_params		(GleProxy 	*proxy);
void	gle_foreign_detach_object		(GleProxy 	*proxy);


/* --- function defs --- */
typedef GParamSpec** (*GleForeignOParamSpecs)	(GObjectClass	*oclass,
						 guint		*n_pspecs);
typedef GParamSpec** (*GleForeignLParamSpecs)	(GObjectClass	*oclass,
						 GObjectClass	*parent_class,
						 guint		*n_pspecs);
typedef GleProxy*    (*GleForeignGetProxy)	(GObject	*object);
typedef void	     (*GleForeignSetProxy)	(GObject	*object,
						 GleProxy	*proxy);
typedef GObject*     (*GleForeignConstruct)	(GObjectClass	*oclass);
typedef void	     (*GleForeignOFunc)		(GObject	*object);
typedef void	     (*GleForeignGetParam)	(GObject	*object,
						 GParamSpec	*pspec,
						 GValue		*value);
typedef void	     (*GleForeignSetParam)	(GObject	*object,
						 GParamSpec	*pspec,
						 const GValue	*value);
typedef GObject*     (*GleForeignGetParent)	(GObject	*object);
typedef void	     (*GleForeignSetParent)	(GObject	*object,
						 GObject	*parent);
typedef gboolean     (*GleForeignCheckToplevel)	(GObject	*object);
typedef GSList*	     (*GleForeignListRefChildren) (GObject	*object);
typedef gchar*	     (*GleForeignStateHint)	(GObject	*object);
typedef enum {
  GLE_FOREIGN_ACTION_DISABLED,
  GLE_FOREIGN_ACTION_ENABLED,
  GLE_FOREIGN_ACTION_HIDE
} GleForeignActionState;
typedef struct {
  guint  action_type;
  gchar *action_name;
} GleForeignAction;
typedef GleForeignActionState (*GleForeignCheckAction) (GObject	         *object,
							GleForeignAction *action,
							GleProxy         *proxy);
typedef void                  (*GleForeignExecAction)  (GObject	         *object,
							GleForeignAction *action,
							GleProxy         *proxy);


/* --- glue table --- */
typedef struct _GleForeign GleForeign;
struct _GleForeign
{
  GleForeignOParamSpecs	    list_object_pspecs;
  GleForeignLParamSpecs	    list_link_pspecs;
  GleForeignGetProxy	    get_proxy;
  GleForeignSetProxy	    set_proxy;
  GleForeignConstruct	    construct_object;
  GleForeignOFunc	    attach_object;
  GleForeignOFunc	    detach_object;
  GleForeignOFunc	    destruct_object;
  GleForeignGetParam	    get_object_param;
  GleForeignSetParam	    set_object_param;
  GleForeignGetParam	    get_link_param;
  GleForeignSetParam	    set_link_param;
  GleForeignGetParent	    get_parent;
  GleForeignSetParent	    set_parent;
  GleForeignCheckToplevel   check_toplevel;
  GleForeignListRefChildren list_ref_children;
  GleForeignStateHint	    state_hint;
  guint                     n_actions;
  GleForeignAction         *actions;
  GleForeignCheckAction     check_action;
  GleForeignExecAction      exec_action;
};


/* --- functions --- */
GleForeign*	gle_foreign_from_class		(GObjectClass	*oclass);
void		gle_foreign_sync_rip		(GleProxy	*proxy,
						 gboolean        recursive);
gboolean	gle_foreign_is_toplevel		(GleProxy	*proxy);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif  /* __GLE_FOREIGN_H__ */
