/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GLE_EDITOR_H__
#define __GLE_EDITOR_H__


#include	<gle/gleutils.h>
#include	<gle/gleproxy.h>
#include	<gle/gleproxyparam.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* --- type macros --- */
#define GLE_TYPE_EDITOR                  (gle_editor_get_type ())
#define GLE_EDITOR(obj)                  (GTK_CHECK_CAST ((obj), GLE_TYPE_EDITOR, GleEditor))
#define GLE_EDITOR_CLASS(klass)          (GTK_CHECK_CLASS_CAST ((klass), GLE_TYPE_EDITOR, GleEditorClass))
#define GLE_IS_EDITOR(obj)               (GTK_CHECK_TYPE ((obj), GLE_TYPE_EDITOR))
#define GLE_IS_EDITOR_CLASS(klass)       (GTK_CHECK_CLASS_TYPE ((klass), GLE_TYPE_EDITOR))
#define GLE_EDITOR_GET_CLASS(obj)        (GTK_CHECK_GET_CLASS ((obj), GLE_TYPE_EDITOR, GleEditorClass))


/* --- typedefs --- */
typedef	struct _GleEditor	GleEditor;
typedef	struct _GleEditorClass	GleEditorClass;
typedef struct _GleEditorField	GleEditorField;


/* --- structures --- */
struct _GleEditorField
{
  GleEditor	*editor;
  GleProxyParam *param;
  GtkWidget     *widget;
  guint		 has_changed : 1;
};
struct	_GleEditor
{
  GtkWindow	 window;

  guint		 ignore_changes;
  GleProxy	*proxy;
  GSList	*fields;
  GtkWidget	*proxy_entry;
  GtkWidget	*notebook;
  GtkCTree	*ctree;
  GtkCList	*clist;
  guint          idle_apply;
};
struct	_GleEditorClass
{
  GtkWindowClass	 parent_class;
  
  void	(*refresh_values)	(GleEditor *editor);
  void	(*reload_values)	(GleEditor *editor);
  void	(*reset_values)		(GleEditor *editor);
};


/* --- prototypes --- */
GtkType		gle_editor_get_type		(void);
GtkWidget*	gle_editor_new			(GleProxy	*proxy);
void		gle_editor_rebuild		(GleEditor	*editor);
void		gle_editor_update_names		(GleEditor	*editor);

/* refresh_values - update screen contents from proxy contents
 * reload_values  - recapture proxy contents, then refresh
 * reset_values	  - reset parameters to default values of a newly created object
 */
void		gle_editor_refresh_values	(GleEditor	*editor);
void		gle_editor_reload_values	(GleEditor	*editor);
void		gle_editor_reset_values		(GleEditor	*editor);


/* --- internal --- */
GleEditorField*	gle_editor_field_create		(GleEditor	*editor,
						 GtkBox		*parent,
						 GleProxyParam	*param);
void		gle_editor_field_refresh	(GleEditorField	*field);
void		gle_editor_field_update_param	(GleEditorField	*field);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif	/* __GLE_EDITOR_H__ */
