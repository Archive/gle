/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#include	<gle/gleconfig.h>

#include	"glemisc.h"
#include	<gdk/gdk.h>
#include	<string.h>
#include	<ctype.h>
#include	<gtk/gtkprivate.h>




/* --- variables --- */
static GtkWidget *gle_flash_widget = NULL;


/* --- functions --- */
GtkWidget*
gle_info_window (const gchar    *title,
		 const gchar    *info_text)
{
  GtkWidget *window;
  GtkWidget *main_vbox;
  GtkWidget *any;
  GtkWidget *sb;
  GtkText *text;

  window =
    gtk_widget_new (GTK_TYPE_WINDOW,
		    "title", title,
		    "allow_shrink", FALSE,
		    "allow_grow", TRUE,
		    NULL);
  main_vbox =
    gtk_widget_new (GTK_TYPE_VBOX,
		    "homogeneous", FALSE,
		    "spacing", 0,
		    "border_width", 0,
		    "parent", window,
		    "visible", TRUE,
		    NULL);
  any =
    gtk_widget_new (GTK_TYPE_HBOX,
		    "homogeneous", FALSE,
		    "spacing", 0,
		    "border_width", 5,
		    "parent", main_vbox,
		    "visible", TRUE,
		    NULL);
  sb = gtk_vscrollbar_new (NULL);
  gtk_widget_show (sb);
  gtk_box_pack_end (GTK_BOX (any), sb, FALSE, TRUE, 0);
  text = (GtkText*) gtk_widget_new (GTK_TYPE_TEXT,
				    "vadjustment", GTK_RANGE (sb)->adjustment,
				    "editable", FALSE,
				    "word_wrap", TRUE,
				    "line_wrap", TRUE,
				    /* "object_signal::activate", gtk_window_activate_default, window, */
				    NULL);
  gtk_text_insert (text, NULL, NULL, NULL, info_text, strlen (info_text));
  gtk_widget_set_usize (GTK_WIDGET (text), 250, 350);
  gtk_widget_show (GTK_WIDGET (text));
  gtk_container_add (GTK_CONTAINER (any), GTK_WIDGET (text));

  any =
    gtk_widget_new (GTK_TYPE_HSEPARATOR,
		    "visible", TRUE,
		    NULL);
  gtk_box_pack_start (GTK_BOX (main_vbox), any, FALSE, TRUE, 0);
  any =
    gtk_widget_new (GTK_TYPE_BUTTON,
		    "label", "Close",
		    "object_signal::clicked", gtk_widget_destroy, window,
		    "border_width", 5,
		    "visible", TRUE,
		    "can_default", TRUE,
		    NULL);
  gtk_box_pack_start (GTK_BOX (main_vbox), any, FALSE, TRUE, 0);
  gtk_widget_grab_default (any);

  return window;
}

void
gle_widget_make_sensitive (GtkWidget	*widget)
{
  if (!GTK_WIDGET_SENSITIVE (widget))
    gtk_widget_set_sensitive (widget, TRUE);
}

void
gle_widget_make_insensitive (GtkWidget	*widget)
{
  if (GTK_WIDGET_SENSITIVE (widget))
    gtk_widget_set_sensitive (widget, FALSE);
}

static gboolean	gtk_types_hard_initialized = FALSE;

static void
gle_hard_initialize_gtk_types (void)
{
  gtk_types_hard_initialized = TRUE;
  gtk_type_init ();
#define GLE_TYPE_CALL(type_func)	type_func ();
#  include "gletypecalls.h"
#undef	GLE_TYPE_CALL
}

GtkType
gle_type_from_name (const gchar *type_name)
{
  GtkType type;

  g_return_val_if_fail (type_name != NULL, 0);

  type = gtk_type_from_name (type_name);
  
  if (!type && !gtk_types_hard_initialized)
    {
      gle_hard_initialize_gtk_types ();
      type = gtk_type_from_name (type_name);
    }

  return type;
}

GtkType
gtk_type_from_arg_name (const gchar      *arg_name,
			const gchar	**class_name_p)
{
  static gchar	*buffer = NULL;
  static guint	buffer_len = 0;
  GtkType	object_type;
  const gchar	*p;
  guint		class_length;
  
  g_return_val_if_fail (arg_name != NULL, 0);
  
  p = strchr (arg_name, ':');
  if (!p)
    p = arg_name + strlen (arg_name);
  
  class_length = p - arg_name;
  
  g_return_val_if_fail (class_length > 0, 0);
  
  if (buffer_len <= class_length)
    {
      g_free (buffer);
      buffer_len = class_length + 1;
      buffer = g_new (gchar, buffer_len);
    }
  g_memmove (buffer, arg_name, class_length);
  buffer[class_length] = 0;
  
  object_type = gtk_type_from_name (buffer);
  if (!object_type && !gtk_types_hard_initialized)
    {
      gle_hard_initialize_gtk_types ();
      object_type = gtk_type_from_name (buffer);
    }
  
  if (object_type && class_name_p)
    *class_name_p = buffer;
  
  return object_type;
}

static gint
gle_flash_timer (gpointer data)
{
  static gboolean toggle = FALSE;
  GtkWidget *widget;
  GdkGC *gc;

  GDK_THREADS_ENTER ();
  
  toggle = !toggle;
  
  widget = gle_flash_widget;
  if (!widget || !widget->window)
    {
      GDK_THREADS_LEAVE ();
      
      return TRUE;
    }
  
  if (toggle)
    gc = gtk_widget_get_default_style ()->white_gc;
  else
    gc = gtk_widget_get_default_style ()->black_gc;
  
  gdk_draw_rectangle	  (widget->window,
			   gc,
			   FALSE,
			   widget->allocation.x,
			   widget->allocation.y,
			   widget->allocation.width-1,
			   widget->allocation.height-1);
  gdk_draw_rectangle	  (widget->window,
			   gc,
			   FALSE,
			   widget->allocation.x,
			   widget->allocation.y,
			   widget->allocation.width,
			   widget->allocation.height);
  gdk_draw_line	  (widget->window,
		   gc,
		   widget->allocation.x,
		   widget->allocation.y,
		   widget->allocation.x +
		   widget->allocation.width,
		   widget->allocation.y +
		   widget->allocation.height);
  gdk_draw_line	  (widget->window,
		   gc,
		   widget->allocation.x +
		   widget->allocation.width,
		   widget->allocation.y,
		   widget->allocation.x,
		   widget->allocation.y +
		   widget->allocation.height);
  
  GDK_THREADS_LEAVE ();
  
  return TRUE;
}

static void
gle_expose_widget (GtkWidget *widget)
{
  if (widget->window)
    gdk_window_clear_area_e (widget->window, 0, 0, 0, 0);
}

void
gle_set_flash_widget (GtkWidget *widget)
{
  static gint flash_timer = 0;
  static gint flash_widget_handler1;
  static gint flash_widget_handler2;
  
  if (widget)
    g_return_if_fail (GTK_IS_WIDGET (widget));
  
  if (!flash_timer && widget)
    flash_timer = gtk_timeout_add (GLE_FLASH_MSEC, gle_flash_timer, NULL);
  
  if (flash_timer && !widget)
    {
      gtk_timeout_remove (flash_timer);
      flash_timer = 0;
    }
  
  if (gle_flash_widget)
    {
      gtk_signal_disconnect (GTK_OBJECT (gle_flash_widget), flash_widget_handler1);
      gtk_signal_disconnect (GTK_OBJECT (gle_flash_widget), flash_widget_handler2);
      gle_expose_widget (gle_flash_widget);
    }
  
  gle_flash_widget = widget;
  if (gle_flash_widget)
    {
      flash_widget_handler1 = gtk_signal_connect_object (GTK_OBJECT (gle_flash_widget),
							 "destroy",
							 GTK_SIGNAL_FUNC (gle_set_flash_widget),
							 NULL);
      flash_widget_handler2 = gtk_signal_connect_object (GTK_OBJECT (gle_flash_widget),
							 "size_allocate",
							 GTK_SIGNAL_FUNC (gle_expose_widget),
							 GTK_OBJECT (gle_flash_widget));
    }
}

GtkWidget*
gle_get_flash_widget (void)
{
  return gle_flash_widget;
}

gint
gtk_clist_find_row (GtkCList *clist,
		    gpointer  data)
{
  GList *list;
  gint n;
  
  g_return_val_if_fail (clist != NULL, -1);
  g_return_val_if_fail (GTK_IS_CLIST (clist), -1);
  
  if (clist->rows < 1)
    return -1;
  
  n = 0;
  list = clist->row_list;
  while (list)
    {
      GtkCListRow *clist_row;
      
      clist_row = list->data;
      if (clist_row->data == data)
	break;
      n++;
      list = list->next;
    }
  
  if (list)
    return n;
  
  return -1;
}

static	gint	destroy_handler = 0;
static GList	*destroy_queue = NULL;

static gint
gtk_delayed_destroy_handler (gpointer data)
{
  GList *list;

  GDK_THREADS_ENTER ();
  
  list = destroy_queue;
  while (list)
    {
      GtkWidget *widget;
      
      widget = list->data;
      list = list->next;
      
      gtk_widget_destroy (widget);
      destroy_queue = g_list_remove (destroy_queue, widget);
    }
  
  destroy_handler = 0;

  GDK_THREADS_LEAVE ();
  
  return FALSE;
}

void
gtk_clist_foreach_row (GtkCList               *clist,
		       GtkCListRowFunc        func,
		       gpointer               func_data)
{
  GList *list;
  guint n;
  
  g_return_if_fail (clist != NULL);
  g_return_if_fail (GTK_IS_CLIST (clist));
  g_return_if_fail (func != NULL);
  
  n = 0;
  list = clist->row_list;
  while (list)
    {
      GtkCListRow *clist_row;
      
      clist_row = list->data;
      list = list->next;
      (*func) (clist, n++, &clist_row->data, func_data);
    }
}

void
gtk_widget_queue_destroy (GtkWidget      *widget)
{
  if (!g_list_find (destroy_queue, widget))
    {
      if (!destroy_handler)
	destroy_handler = gtk_idle_add (gtk_delayed_destroy_handler, NULL);
      destroy_queue = g_list_prepend (destroy_queue, widget);
    }
}

GList*
gtk_widget_query_arg_list (GtkWidget *widget,
			   GtkType    class_type)
{
  GtkArg* args;
  gint nargs;
  GList *list;
  guint i;
  
  nargs = 0;
  args = gtk_object_query_args	 (class_type, NULL, &nargs);
  gtk_widget_set (widget, "GtkObject::signal::destroy", g_free, args, NULL);
  
  list = NULL;
  for (i = nargs; i > 0; i--)
    {
      list = g_list_prepend (list, &args[i - 1]);
    }
  
  return list;
}

void
gle_item_factory_adjust (GtkItemFactory *ifactory,
			 guint           action,
			 gboolean        sensitive,
			 gpointer        user_data)
{
  GtkWidget *widget;

  g_return_if_fail (ifactory != NULL);
  g_return_if_fail (GTK_IS_ITEM_FACTORY (ifactory));
  
  widget = gtk_item_factory_get_widget_by_action (ifactory, action);
  if (widget)
    {
      gtk_widget_set_sensitive (widget, sensitive);
      gtk_object_set_user_data (GTK_OBJECT (widget), user_data);
    }
}

gchar*
g_str_canonicalize (gchar  *string,
		    gchar   subsitutor,
		    gchar  *charsets,
		    ...)
{
  va_list args;
  GSList *charset_slist;
  register gchar *c;

  g_return_val_if_fail (string != NULL, NULL);
  g_return_val_if_fail (subsitutor > 0, string);
  g_return_val_if_fail (charsets != NULL, string);

  charset_slist = g_slist_append (NULL, charsets);
  va_start (args, charsets);
  c = va_arg (args, gchar*);
  while (c)
    {
      charset_slist = g_slist_append (charset_slist, c);
      c = va_arg (args, gchar*);
    }
  va_end (args);

  for (c = string; *c; c++)
    {
      register GSList *slist;
      register gboolean miss = TRUE;

      for (slist = charset_slist; slist && miss; slist = slist->next)
	miss = !strchr (slist->data, *c);
      if (miss)
	*c = subsitutor;
    }
  g_slist_free (charset_slist);

  return string;
}

gint
gle_clist_selection_info (GtkCList       *clist,
			  GdkWindow	 *window,
			  gint            x,
			  gint            y,
			  gint           *row,
			  gint           *column)
{
  g_return_val_if_fail (clist != NULL, FALSE);
  g_return_val_if_fail (GTK_IS_CLIST (clist), FALSE);

  if (window && window == clist->clist_window)
    return gtk_clist_get_selection_info (clist, x, y, row, column);

  return FALSE;
}

void
gle_file_selection_heal (GtkFileSelection *fs)
{
  GtkWidget *main_vbox;
  GtkWidget *hbox;
  GtkWidget *any;

  g_return_if_fail (fs != NULL);
  g_return_if_fail (GTK_IS_FILE_SELECTION (fs));

  /* button placement
   */
  gtk_container_set_border_width (GTK_CONTAINER (fs), 0);
  gtk_file_selection_hide_fileop_buttons (fs);
  gtk_widget_ref (fs->main_vbox);
  gtk_container_remove (GTK_CONTAINER (fs), fs->main_vbox);
  gtk_box_set_spacing (GTK_BOX (fs->main_vbox), 0);
  gtk_container_set_border_width (GTK_CONTAINER (fs->main_vbox), 5);
  main_vbox =
    gtk_widget_new (GTK_TYPE_VBOX,
		    "homogeneous", FALSE,
		    "spacing", 0,
		    "border_width", 0,
		    "parent", fs,
		    "visible", TRUE,
		    NULL);
  gtk_box_pack_start (GTK_BOX (main_vbox), fs->main_vbox, TRUE, TRUE, 0);
  gtk_widget_unref (fs->main_vbox);
  gtk_widget_hide (fs->ok_button->parent);
  hbox =
    gtk_widget_new (GTK_TYPE_HBOX,
		    "homogeneous", TRUE,
		    "spacing", 0,
		    "border_width", 5,
		    "visible", TRUE,
		    NULL);
  gtk_box_pack_end (GTK_BOX (main_vbox), hbox, FALSE, TRUE, 0);
  gtk_widget_reparent (fs->ok_button, hbox);
  gtk_widget_reparent (fs->cancel_button, hbox);
  gtk_widget_grab_default (fs->ok_button);
  gtk_label_set_text (GTK_LABEL (GTK_BIN (fs->ok_button)->child), "Ok");
  gtk_label_set_text (GTK_LABEL (GTK_BIN (fs->cancel_button)->child), "Cancel");

  /* heal the action_area packing so we can customize children
   */
  gtk_box_set_child_packing (GTK_BOX (fs->action_area->parent),
			     fs->action_area,
			     FALSE, TRUE,
			     5, GTK_PACK_START);

  any =
    gtk_widget_new (gtk_hseparator_get_type (),
		    "GtkWidget::visible", TRUE,
		    NULL);
  gtk_box_pack_end (GTK_BOX (main_vbox), any, FALSE, TRUE, 0);
  gtk_widget_grab_focus (fs->selection_entry);
}

static gint
idle_shower (GtkWidget **widget_p)
{
  GDK_THREADS_ENTER ();
  
  if (GTK_IS_WIDGET (*widget_p) && !GTK_OBJECT_DESTROYED (*widget_p))
    {
      gtk_signal_disconnect_by_func (GTK_OBJECT (*widget_p),
				     GTK_SIGNAL_FUNC (gtk_widget_destroyed),
				     widget_p);
      gtk_widget_show (*widget_p);
    }

  g_free (widget_p);

  GDK_THREADS_LEAVE ();

  return FALSE;
}

void
gtk_idle_show_widget (GtkWidget *widget)
{
  GtkWidget **widget_p;

  g_return_if_fail (GTK_IS_WIDGET (widget));
  if (GTK_OBJECT_DESTROYED (widget))
    return;

  widget_p = g_new (GtkWidget*, 1);
  *widget_p = widget;
  gtk_signal_connect (GTK_OBJECT (widget),
		      "destroy",
		      GTK_SIGNAL_FUNC (gtk_widget_destroyed),
		      widget_p);
  gtk_idle_add_priority (G_PRIORITY_LOW, (GtkFunction) idle_shower, widget_p);
}
