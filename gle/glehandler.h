/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef __GLE_HANDLER_H__
#define __GLE_HANDLER_H__




#include	<gle/glegobject.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* --- typedefs & structs --- */
typedef	struct  _GleHandler		GleHandler;
typedef gboolean (*GleHandlerForeach)	(GleHandler	*handler,
					 gpointer	 user_data);
typedef enum
{
  GLE_CONNECTION_NORMAL		= 0,
  GLE_CONNECTION_OBJECT		= 1 << 0,
  GLE_CONNECTION_AFTER		= 1 << 1,
  GLE_CONNECTION_OBJECT_AFTER	= (GLE_CONNECTION_OBJECT | GLE_CONNECTION_AFTER)
} GleConnectionType;
struct _GleHandler
{
  GQuark	id;
  gpointer	handler;
  gpointer	data;
};
struct _GleConnection
{
  GleConnection    *next;
  GleGObject	   *gobject;
  guint             signal_id;
  gchar		   *handler;
  gpointer          data;
  GleConnectionType ctype;
  guint             gtk_id;
};


/* --- prototypes --- */
void		gle_handler_init		(void);
void		gle_handler_register_default	(const gchar		*name,
						 gpointer		 func,
						 gpointer		 data);
void		gle_handler_register		(const gchar		*name,
						 gpointer		 func,
						 gpointer		 data,
						 GtkType		 return_type,
						 guint			 n_params,
						 ...);
GleHandler*	gle_handler_get			(const gchar		*name);
void		gle_handler_foreach		(GleHandlerForeach	 func,
						 gpointer		 user_data);
GleConnection*	gle_connection_new		(GleGObject     	*gobject,
						 guint			 signal_id,
						 const gchar		*handler,
						 gpointer       	 data,
						 GleConnectionType	 ctype);
void            gle_connection_connect		(GleConnection  	*connection);
void            gle_connection_disconnect	(GleConnection  	*connection);
void            gle_connection_destroy		(GleConnection  	*connection);
void            gle_connection_change		(GleConnection		*connection,
						 guint                   signal_id,
						 const gchar		*handler,
						 gpointer       	 data,
						 GleConnectionType	 ctype);
void            gle_connections_destroy		(GleGObject		*gobject);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif  /* __GLE_HANDLER_H__ */
