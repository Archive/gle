/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GLE_SELECTOR_H__
#define __GLE_SELECTOR_H__

#include	<gle/gleutils.h>

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* --- type macros --- */
#define GLE_TYPE_SELECTOR		(gle_selector_get_type ())
#define GLE_SELECTOR(obj)		(GTK_CHECK_CAST ((obj), GLE_TYPE_SELECTOR, GleSelector))
#define GLE_SELECTOR_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GLE_TYPE_SELECTOR, GleSelectorClass))
#define GLE_IS_SELECTOR(obj)		(GTK_CHECK_TYPE ((obj), GLE_TYPE_SELECTOR))
#define GLE_IS_SELECTOR_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), GLE_TYPE_SELECTOR))
#define GLE_SELECTOR_GET_CLASS(obj)	(GTK_CHECK_GET_CLASS ((obj), GLE_TYPE_SELECTOR, GleSelectorClass))


/* --- typedefs & structures --- */
typedef	struct	_GleSelector		GleSelector;
typedef	struct	_GleSelectorClass	GleSelectorClass;
struct	_GleSelector
{
  GtkWindow	window;
  
  guint		warning_visible : 1;
  guint		is_drag : 1;
  
  GtkWidget	*candidate;
  GdkWindow	*current;
  GdkCursor	*query_cursor;
  GtkWidget	*drag_window;
  
  GtkWidget	*message_label;
  GtkWidget	*candidate_label;
  guint		 warning_timer;
  GtkWidget	*warning_window;
};
struct	_GleSelectorClass
{
  GtkWindowClass parent_class;
  
  GleSelector	*active_selector;
  void	       (*candidate_check)	(GleSelector	 *selector,
					 GtkWidget	**new_candidate,
					 gboolean	 *candidate_ok);
  void	       (*candidate_selected)	(GleSelector	 *selector,
					 GtkWidget	 *candidate);
  void	       (*abort)			(GleSelector	 *selector);
};


/* --- prototypes --- */
GtkType		gle_selector_get_type		(void);
GtkWidget*	gle_selector_new		(const gchar	*title,
						 const gchar	*message);
GtkWidget*	gle_selector_make_selection	(GleSelector	*selector);
GtkWidget*	gle_selector_extended_selection (GleSelector	*selector,
						 GtkWindow	*drag_window,
						 gboolean	 is_drag);
void		gle_selector_reset		(GleSelector	*selector);
void		gle_selector_abort		(GleSelector	*selector);
void		gle_selector_popup_warning	(GleSelector	*selector);
gboolean	gle_selector_in_selection	(GleSelector	*selector);
void		gle_selector_set_cursor		(GleSelector	*selector,
						 GdkCursor	*query_cursor);



#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif	/* __GLE_SELECTOR_H__ */
