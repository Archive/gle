/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001-2002 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GLE_PROXY_H__
#define __GLE_PROXY_H__


#include        <gle/gleutils.h>
#include        <gle/gleproxyset.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */



/* --- macros --- */
#define	GLE_TYPE_PROXY			(G_TYPE_POINTER)
#define	GLE_IS_PROXY(proxy)		((proxy) != NULL && G_IS_OBJECT_CLASS ((proxy)->oclass))
#define	GLE_PROXY_HAS_OBJECT(proxy)	((proxy)->object != NULL)
#define	GLE_PROXY_OTYPE(proxy)		(G_OBJECT_CLASS_TYPE ((proxy)->oclass))
#define	GLE_PROXY_NAME(proxy)		("ProxyName")


/* --- structures --- */
struct _GleProxy
{
  /* proxy set belongings */
  GleProxySet *pset;
  GleProxy    *prev, *next;
  guint        ref_count;

  /* type specifics */
  GObjectClass *oclass;

  /* tree structure */
  GleProxy    *parent;
  GList	      *children;

  /* proxy specifics */
  gchar	       *pname;
  GHookList	hook_list;
  guint		update_mask;
  guint         update_handler;

  /* client object and values thereof */
  GObject      *object;
  gchar	       *instance_flag;	/* for widgets: "internal"/"composite" */
  gchar	       *state_hint;	/* for widgets: "active"/"prelight"... */
  GSList       *object_params;
  GSList       *link_params;
};


/* --- prototypes --- */
GleProxy*	gle_proxy_from_type		(GleProxySet	*pset,
						 GType		 type);
GleProxy*	gle_proxy_new_from_object	(GleProxySet	*pset,
						 GObject	*object);
GleProxy*	gle_proxy_get_from_object	(GObject	*object,
						 GleForeign	*foreign);
GleProxy*	gle_proxy_ref			(GleProxy	*proxy);
void		gle_proxy_unref			(GleProxy	*proxy);
/* capturing values from foreign object */
void		gle_proxy_capture_parent	(GleProxy	*proxy);
void		gle_proxy_capture_object_params	(GleProxy	*proxy);
void		gle_proxy_capture_link_params	(GleProxy	*proxy);
/* modifying foreign object */
void		gle_proxy_apply_object_params	(GleProxy	*proxy);
void		gle_proxy_apply_parent		(GleProxy	*proxy);
void		gle_proxy_apply_link_params	(GleProxy	*proxy);
/* set/get params from proxy (and foreign object) */
void		gle_proxy_set_param		(GleProxy	*proxy,
						 GParamSpec	*pspec,
						 const GValue	*value);
void		gle_proxy_get_param		(GleProxy	*proxy,
						 GParamSpec	*pspec,
						 GValue		*value);
void		gle_proxy_kill			(GleProxy	*proxy);


/* --- proxy notification --- */
typedef enum
{
  GLE_PROXY_DESTROY,
  GLE_PROXY_DETACH,
  GLE_PROXY_ATTACH,
  GLE_PROXY_CHANGED,	/* param values changed */
  GLE_PROXY_RELOAD,	/* parent/pspecs changed */
  GLE_PROXY_ADD_CHILD
} GleProxyNotify;
/* proxy notification: attach/detach/changed/destroy */
#define	GLE_PROXY_NOTIFY_DESTROY(proxy, func, data) \
    gle_proxy_add_hook ((proxy), GLE_PROXY_DESTROY, (func), (data))
#define	GLE_PROXY_NOTIFY_ATTACH(proxy, func, data) \
    gle_proxy_add_hook ((proxy), GLE_PROXY_ATTACH, (func), (data))
#define	GLE_PROXY_NOTIFY_DETACH(proxy, func, data) \
    gle_proxy_add_hook ((proxy), GLE_PROXY_DETACH, (func), (data))
#define	GLE_PROXY_NOTIFY_CHANGED(proxy, func, data) \
    gle_proxy_add_hook ((proxy), GLE_PROXY_CHANGED, (func), (data))
#define	GLE_PROXY_NOTIFY_RELOAD(proxy, func, data) \
    gle_proxy_add_hook ((proxy), GLE_PROXY_RELOAD, (func), (data))
#define	GLE_PROXY_NOTIFY_ADD_CHILD(proxy, func, data) \
    gle_proxy_add_hook ((proxy), GLE_PROXY_ADD_CHILD, (func), (data))
#define	GLE_PROXY_REMOVE_NOTIFIES(proxy, func, data) \
    gle_proxy_remove_hooks ((proxy), (func), (data))


/* --- internal --- */
void		gle_proxy_create_foreign	(GleProxy	*proxy);
void		gle_proxy_detach_foreign	(GleProxy	*proxy);
void		gle_proxy_dispose_foreign	(GleProxy	*proxy);
void		gle_proxy_add_hook		(GleProxy	*proxy,
						 guint		 hook_id,
						 gpointer	 func,
						 gpointer	 data);
void		gle_proxy_remove_hooks		(GleProxy	*proxy,
						 gpointer	 func,
						 gpointer	 data);
void		gle_proxy_invoke_hooks		(GleProxy	*proxy,
						 guint           hook_id);
void		gle_proxy_foreign_notify	(GleProxy	*proxy,
						 GleForeignNotify nofiy_mask);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif  /* __GLE_PROXY_H__ */
