/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001-2002 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include	"gleted.h"
#include	"gleeditor.h"
#include	"gleselector.h"
#include	"gleutils.h"
#include	<string.h>



#define PROXY_SET_TED(p,t)

typedef struct
{
  GleTed	*ted;
  GleProxy	*proxy;
} GleTedPopupData;


/* --- prototypes --- */
static void		gle_ted_class_init		(GleTedClass	*class);
static void		gle_ted_init			(GleTed		*ted);
static void		gle_ted_construct		(GleTed		*ted,
							 GleProxy	*proxy);
static void		gle_ted_destroy			(GtkObject	*object);
static gboolean		gle_ted_ptree_button		(GleTed		*ted,
							 GleProxy	*proxy,
							 guint		 root_x,
							 guint		 root_y,
							 guint		 button,
							 guint32	 time);
static void		gle_ted_find_widget		(GleTed		*ted);
static void		gle_ted_popup_operation		(gpointer	 callback_data,
							 guint		 popup_op,
							 GtkWidget	*item);
static GleTedPopupData*	gle_ted_popup_start		(GleTed		*ted,
							 GtkItemFactory	*popup_factory,
							 GleProxy	*proxy);
static void		gle_ted_popup_done		(gpointer	 data);
static GtkItemFactory*  gle_ted_item_factory_from_foreign (GleTedClass *class,
							   GleForeign  *foreign);


/* --- menu operations --- */
enum
{
  POPUP_OP_NONE = 0xffffff00,
  POPUP_OP_P_TED,
  POPUP_OP_P_EDITOR,
  POPUP_OP_P_DESTROY,
  POPUP_OP_LAST
};
static GtkItemFactoryEntry head_menu_entries[] =
{
#define POP(popup_op)	(gle_ted_popup_operation), (POPUP_OP_ ## popup_op)
  { "/Proxy Operations", "",		POP (NONE),			"<Title>" },
  { "/---",		  "",		NULL, 0,			"<Separator>" },
  { "/Object Editor...", "<ctrl>E",	POP (P_EDITOR),			NULL },
  { "/Tree Editor...",	"<ctrl>T",	POP (P_TED),			NULL },
  { "/---",		  "",		NULL, 0,			"<Separator>" },
#undef	POP
};
static guint n_head_menu_entries = (sizeof (head_menu_entries) / sizeof (head_menu_entries[0]));
static GtkItemFactoryEntry tail_menu_entries[] =
{
#define POP(popup_op)	(gle_ted_popup_operation), (POPUP_OP_ ## popup_op)
  { "/---",		  "",		NULL, 0,			"<Separator>" },
  { "/Destroy",		NULL,		POP (P_DESTROY),		NULL },
#undef	POP
};
static guint n_tail_menu_entries = (sizeof (tail_menu_entries) / sizeof (tail_menu_entries[0]));


/* --- variables --- */
static GtkWindowClass	*parent_class = NULL;
static GleTedClass	*gle_ted_class = NULL;


/* --- menus --- */
static gchar	*gle_ted_factories_path = "<GLE-GleTed>";
static GtkItemFactoryEntry menubar_entries[] =
{
#define TED_OP(ted_op)	(gle_ted_operation), (GLE_TED_OP_ ## ted_op)
  { "/File/Test",	"",		TED_OP (NONE),	"<Item>" },
  { "/File/-----",	"",		NULL, 0,		"<Separator>" },
  { "/File/-----",	"",		NULL, 0,		"<Separator>" },
  { "/File/Close",	"<ctrl>W",	TED_OP (DELETE),	"<Item>" },
#undef	TED_OP
};
static guint n_menubar_entries = sizeof (menubar_entries) / sizeof (menubar_entries[0]);


/* --- functions --- */
GtkType
gle_ted_get_type (void)
{
  static GtkType ted_type = 0;
  
  if (!ted_type)
    {
      GtkTypeInfo ted_info =
      {
	"GleTed",
	sizeof (GleTed),
	sizeof (GleTedClass),
	(GtkClassInitFunc) gle_ted_class_init,
	(GtkObjectInitFunc) gle_ted_init,
	/* reserved_1 */ NULL,
	/* reserved_2 */ NULL,
	(GtkClassInitFunc) NULL,
      };
      
      ted_type = gtk_type_unique (GTK_TYPE_WINDOW, &ted_info);
    }
  
  return ted_type;
}

static gint
gle_ted_delete_event (GtkWidget		*widget,
		      GdkEventAny	*event)
{
  gle_ted_operation (GLE_TED (widget), GLE_TED_OP_DELETE);
  
  return TRUE;
}

static void
gle_ted_class_init (GleTedClass *class)
{
  GtkObjectClass *object_class = GTK_OBJECT_CLASS (class);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);
  
  parent_class = g_type_class_peek_parent (class);
  gle_ted_class = class;
  
  object_class->destroy = gle_ted_destroy;
  
  widget_class->delete_event = gle_ted_delete_event;
  
  class->factories_path = gle_ted_factories_path;
  class->n_factories = 0;
  class->factories = NULL;
}

static void
rebuild_proxy_tree (GleTed *ted)
{
  if (ted->ptree->proxy && ted->ptree->proxy->object)
    gle_foreign_sync_rip (ted->ptree->proxy, TRUE);
  if (ted->ptree && 0) // FIXME
    gle_ptree_rebuild (ted->ptree);
}

static void
gle_ted_init (GleTed *ted)
{
  GtkWidget *ted_vbox;
  GtkWidget *main_vbox;
  GtkWidget *status_hbox;
  GtkWidget *hbox;
  GtkWidget *any;
  GtkWidget *button;
  
  GLE_TAG (ted);
  
  ted->menubar_factory = NULL;
  ted->window_label = NULL;
  ted->ptree = NULL;
  ted->rebuild_button = NULL;
  ted->find_button = NULL;
  
  /* create GUI
   */
  g_object_set (GTK_WIDGET (ted),
		"title", "GLE-Ted",
		"type", GTK_WINDOW_TOPLEVEL,
		"allow_shrink", TRUE,
		"allow_grow", TRUE,
		"default_width", 550,
		NULL);
  ted_vbox = g_object_new (GTK_TYPE_VBOX,
			   "homogeneous", FALSE,
			   "spacing", 0,
			   "border_width", 0,
			   "parent", ted,
			   "visible", TRUE,
			   NULL);
  main_vbox = g_object_new (GTK_TYPE_VBOX,
			    "homogeneous", FALSE,
			    "spacing", 5,
			    "border_width", 5,
			    "visible", TRUE,
			    NULL);
  gtk_container_add_with_properties (GTK_CONTAINER (ted_vbox), main_vbox,
				     "expand", TRUE,
				     "fill", TRUE,
				     NULL);
  status_hbox = g_object_new (GTK_TYPE_HBOX,
			      "homogeneous", FALSE,
			      "spacing", 5,
			      "border_width", 5,
			      "visible", TRUE,
			      NULL);
  gtk_container_add_with_properties (GTK_CONTAINER (main_vbox), status_hbox,
				     "expand", FALSE,
				     "fill", TRUE,
				     NULL);
  ted->window_label = g_object_new (GTK_TYPE_LABEL,
				    "label", "<None>",
				    "visible", TRUE,
				    NULL);
  gtk_container_add_with_properties (GTK_CONTAINER (main_vbox), ted->window_label,
				     "expand", FALSE,
				     "fill", TRUE,
				     NULL);
  
  /* crete the widget tree
   */
  ted->ptree = g_object_connect (g_object_new (GLE_TYPE_PTREE,
					       "visible", TRUE,
					       NULL),
				 "swapped_object_signal::button_click", gle_ted_ptree_button, ted,
				 "swapped_object_signal::destroy", gtk_widget_destroy, ted,
				 NULL);
  any = g_object_new (GTK_TYPE_SCROLLED_WINDOW,
		      "visible", TRUE,
		      "hscrollbar_policy", GTK_POLICY_AUTOMATIC,
		      "vscrollbar_policy", GTK_POLICY_AUTOMATIC,
		      "parent", main_vbox,
		      NULL);
  gtk_container_add (GTK_CONTAINER (any), GTK_WIDGET (ted->ptree));
  
  /* create tree buttons
   */
  hbox = g_object_new (GTK_TYPE_HBOX,
		       "homogeneous", TRUE,
		       "spacing", 5,
		       "border_width", 0,
		       "visible", TRUE,
		       NULL);
  gtk_container_add_with_properties (GTK_CONTAINER (main_vbox), hbox,
				     "expand", FALSE,
				     "fill", TRUE,
				     NULL);
  ted->rebuild_button = g_object_connect (g_object_new (GTK_TYPE_BUTTON,
							"label", "Rebuild Tree",
							"visible", TRUE,
							NULL),
					  "swapped_object_signal::clicked", rebuild_proxy_tree, ted,
					  NULL);
  gtk_container_add_with_properties (GTK_CONTAINER (hbox), ted->rebuild_button,
				     "expand", FALSE,
				     "fill", TRUE,
				     NULL);
  ted->find_button = g_object_connect (g_object_new (GTK_TYPE_BUTTON,
						     "label", "Find Widget",
						     "visible", TRUE,
						     NULL),
				       "swapped_object_signal::clicked", gle_ted_find_widget, ted,
				       NULL);
  gtk_container_add_with_properties (GTK_CONTAINER (hbox), ted->find_button,
				     "expand", FALSE,
				     "fill", TRUE,
				     NULL);
  any = g_object_new (GTK_TYPE_HSEPARATOR,
		      "visible", TRUE,
		      NULL);
  gtk_box_pack_start (GTK_BOX (ted_vbox), any, FALSE, TRUE, 0);
  button = g_object_connect (g_object_new (GTK_TYPE_BUTTON,
					   "visible", TRUE,
					   "label", "Close",
					   "border_width", 5,
					   "parent", ted_vbox,
					   "can_default", TRUE,
					   "has_default", TRUE,
					   NULL),
			     "swapped_object_signal::clicked", gtk_widget_destroy, ted,
			     NULL);
  gtk_box_set_child_packing (GTK_BOX (ted_vbox), button, FALSE, TRUE, 0, GTK_PACK_START);
  
  /* create the menu bar
   */
  ted->menubar_factory = gtk_item_factory_new (GTK_TYPE_MENU_BAR,
					       gle_ted_factories_path,
					       NULL);
  GLE_TAG (ted->menubar_factory);
  gtk_window_add_accel_group (GTK_WINDOW (ted),
			      ted->menubar_factory->accel_group);
  gtk_item_factory_create_items (ted->menubar_factory,
				 n_menubar_entries,
				 menubar_entries,
				 ted);
  gtk_container_add_with_properties (GTK_CONTAINER (ted_vbox), ted->menubar_factory->widget,
				     "expand", FALSE,
				     "fill", TRUE,
				     "position", 0,
				     NULL);
  gtk_widget_show (ted->menubar_factory->widget);
}

GtkWidget*
gle_ted_new (GleProxy *proxy)
{
  GleTed *ted;
  
  g_return_val_if_fail (GLE_IS_PROXY (proxy), NULL);
  
  ted = g_object_new (GLE_TYPE_TED, NULL);
  
  gle_ted_construct (ted, proxy);
  
  return GTK_WIDGET (ted);
}

static void
gle_ted_construct (GleTed   *ted,
		   GleProxy *proxy)
{
  g_return_if_fail (GLE_IS_TED (ted));
  g_return_if_fail (GLE_IS_PROXY (proxy));
  g_return_if_fail (ted->proxy == NULL);
  g_return_if_fail (gle_ted_from_proxy (proxy) == NULL);
  
  ted->proxy = proxy;
  PROXY_SET_TED (proxy, ted);
  
  GLE_PROXY_NOTIFY_DESTROY (proxy, gtk_widget_destroy, ted);
  gle_ptree_set_proxy (ted->ptree, proxy);

  if (ted->popup_factory)
    gtk_window_remove_accel_group (GTK_WINDOW (ted), ted->popup_factory->accel_group);
  ted->popup_factory = gle_ted_item_factory_from_foreign (GLE_TED_GET_CLASS (ted),
							  proxy->pset->foreign);
  gtk_window_add_accel_group (GTK_WINDOW (ted), ted->popup_factory->accel_group);
  if (GTK_IS_WIDGET (ted->proxy->object))
    gtk_widget_show (ted->find_button);
  else
    gtk_widget_hide (ted->find_button);
}

static void
gle_ted_destroy (GtkObject *object)
{
  GleTed *ted = GLE_TED (object);
  
  if (ted->proxy)
    {
      GLE_PROXY_REMOVE_NOTIFIES (ted->proxy, gtk_widget_destroy, ted);
      PROXY_SET_TED (ted->proxy, NULL);
      ted->proxy = NULL;
    }
  
  if (ted->menubar_factory)
    {
      g_object_unref (ted->menubar_factory);
      ted->menubar_factory = NULL;
    }
  
  GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static gboolean
gle_ted_ptree_button (GleTed   *ted,
		      GleProxy *proxy,
		      guint	root_x,
		      guint	root_y,
		      guint	button,
		      guint32	time)
{
  gboolean handled = FALSE;
  
  g_return_val_if_fail (ted != NULL, FALSE);
  g_return_val_if_fail (GLE_IS_TED (ted), FALSE);
  
  if (proxy)
    {
      if (button == 3)
	{
	  GleTedPopupData *popup_data;
	  
	  popup_data = gle_ted_popup_start (ted, ted->popup_factory, proxy);
	  if (popup_data)
	    {
	      gtk_item_factory_popup_with_data (ted->popup_factory,
						popup_data,
						gle_ted_popup_done,
						root_x,
						root_y,
						button,
						time);
	      handled = TRUE;
	    }
	}
    }
  
  return handled;
}

void
gle_ted_mark_proxy (GleTed   *ted,
		    GleProxy *proxy)
{
  g_return_if_fail (ted != NULL);
  g_return_if_fail (GLE_IS_TED (ted));
  
  gle_ptree_mark_proxy (ted->ptree, proxy);
}

GleTed*
gle_ted_from_proxy (GleProxy *proxy)
{
#if 0
  g_return_val_if_fail (GLE_IS_PROXY (proxy), NULL);
  
  return gle_gobject_get_qdata (GLE_GOBJECT (proxy), quark_ted);
#endif
  return NULL; // FIXME
}

static void
gle_ted_candidate_check (GleTed	    *ted,
			 GtkWidget **new_candidate,
			 gint	    *candidate_ok)
{
  /* selector */
  
  g_return_if_fail (GLE_IS_TED (ted));

  if (*candidate_ok)
    {
      GleProxy *proxy = gle_proxy_get_from_object (G_OBJECT (*new_candidate), ted->proxy->pset->foreign);

      if (!gle_ptree_mark_proxy (ted->ptree, proxy))
	*candidate_ok = FALSE;
    }
  if (!*candidate_ok && *new_candidate)
    *new_candidate = gtk_widget_get_toplevel (*new_candidate);
}

static void
gle_ted_find_widget (GleTed *ted)
{
  GtkWidget *selector;
  
  g_return_if_fail (GLE_IS_TED (ted));
  
  gle_ptree_mark_proxy (ted->ptree, NULL);
  if (gle_selector_in_selection (NULL) ||
      gdk_pointer_is_grabbed ())
    return;
  
  selector = gle_selector_new ("GLE Find Widget", "Select a widget");
  GLE_TAG (selector);
  GLE_TAG (GLE_SELECTOR (selector)->warning_window);
  gle_selector_set_cursor (GLE_SELECTOR (selector), gdk_cursor_new (GDK_HAND2));
  g_object_connect (GTK_OBJECT (selector),
		    "swapped_object_signal::candidate_check", gle_ted_candidate_check, ted,
		    NULL);
  
  gtk_widget_show (GTK_WIDGET (ted));
  gdk_window_raise (GTK_WIDGET (ted)->window);
  
  gtk_object_ref (GTK_OBJECT (ted));
  gtk_object_ref (GTK_OBJECT (selector));
  gle_selector_make_selection (GLE_SELECTOR (selector));
  gtk_widget_destroy (GTK_WIDGET (selector));
  gtk_object_unref (GTK_OBJECT (selector));
  gtk_object_unref (GTK_OBJECT (ted));
}

void
gle_ted_operation (GleTed	*ted,
		   GleTedOps	 ted_op)
{
  g_return_if_fail (ted != NULL);
  g_return_if_fail (GLE_IS_TED (ted));
  g_return_if_fail (ted_op < GLE_TED_OP_LAST);
  
  gtk_widget_ref (GTK_WIDGET (ted));
  
  switch (ted_op)
    {
    case GLE_TED_OP_NONE:
      break;
    case GLE_TED_OP_DELETE:
      gle_util_set_flash_widget (NULL);
      gtk_widget_destroy (GTK_WIDGET (ted));
      break;
    case GLE_TED_OP_TREE_UPDATE:
      gle_ptree_update (ted->ptree);
      break;
    case GLE_TED_OP_TREE_REBUILD:
      rebuild_proxy_tree (ted);
      break;
    default:
      g_printerr ("GleTedOps: invlid op-code %u\n", ted_op);
      break;
    }
  
  gtk_widget_unref (GTK_WIDGET (ted));
}

static GleTed*
gle_ted_get_current (void)
{
  GdkEvent *event;
  GtkWidget *widget;
  
  event = gtk_get_current_event ();
  if (!event)
    return NULL;
  widget = gtk_get_event_widget (event);
  gdk_event_free (event);
  if (!widget)
    return NULL;
  widget = gtk_widget_get_toplevel (widget);
  if (widget && GLE_IS_TED (widget))
    return GLE_TED (widget);
  
  return NULL;
}

static void
gle_ted_popup_operation (gpointer   callback_data,
			 guint	    popup_op,
			 GtkWidget *item)
{
  GleTedPopupData *popup_data;
  GtkItemFactory *factory;
  GleTed *ted;
  GSList *proxy_list, *slist;
  
  g_return_if_fail (item != NULL);
  g_return_if_fail (GTK_IS_MENU_ITEM (item));
  g_return_if_fail (popup_op < POPUP_OP_LAST);

  factory = gtk_item_factory_from_widget (item);
  popup_data = gtk_item_factory_popup_data_from_widget (item);
  if (popup_data)
    {
      /* we are invoked from a popup */
      ted = popup_data->ted;
      gle_proxy_ref (popup_data->proxy);
      proxy_list = g_slist_prepend (NULL, popup_data->proxy);
    }
  else
    {
      /* we are invoked via accelerator, in this case
       * we try to get the Ted from the current event and
       * then group-operate on all selected proxys
       */
      
      ted = gle_ted_get_current ();
      if (!ted)
	return;
      
      proxy_list = gle_ptree_selected_proxies (ted->ptree);
      for (slist = proxy_list; slist; slist = slist->next)
	gle_proxy_ref (slist->data);
    }
  
  g_object_ref (ted);
  for (slist = proxy_list; slist; slist = slist->next)
    {
      GleProxy *proxy = slist->data;
      
      switch (popup_op)
	{
	  GtkWidget *dialog;
	  GleForeignAction *action;
	case POPUP_OP_NONE:
	  break;
	case POPUP_OP_P_TED:
	  if (proxy->children)
	    {
	      dialog = (GtkWidget*) gle_ted_from_proxy (proxy);
	      if (!dialog)
		dialog = gle_ted_new (proxy);
	      gtk_widget_show (dialog);
	      gdk_window_raise (dialog->window);
	    }
	  break;
	case POPUP_OP_P_EDITOR:
	  dialog = NULL; // FIXME: (GtkWidget*) gle_editor_from_proxy (proxy);
	  if (!dialog)
	    dialog = gle_editor_new (proxy);
	  gtk_widget_show (dialog);
	  gdk_window_raise (dialog->window);
	  break;
	case POPUP_OP_P_DESTROY:
	  /* gle_proxy_destroy (proxy); */
	  break;
	default:
	  action = gtk_object_get_user_data (GTK_OBJECT (item));
	  if (action && proxy->object)
	    {
	      GleForeign *foreign;

	      g_assert (popup_op == action->action_type);

	      foreign = g_object_get_data (G_OBJECT (factory), "GleForeign");
	      foreign->exec_action (proxy->object, action, proxy);
	    }
	  else
	    g_printerr ("GleTed: invalid popup operation: %u\n", popup_op);
	  break;
	}
      gle_proxy_unref (proxy);
    }
  g_slist_free (proxy_list);
  g_object_unref (ted);
}

static GleTedPopupData*
gle_ted_popup_start (GleTed	    *ted,
		     GtkItemFactory *popup_factory,
		     GleProxy	    *proxy)
{
  GleTedPopupData *popup_data;
  GleForeign *foreign = g_object_get_data (G_OBJECT (popup_factory), "GleForeign");
  guint i;

  popup_data = g_new (GleTedPopupData, 1);
  popup_data->ted = ted;
  popup_data->proxy = proxy;
  gle_proxy_ref (popup_data->proxy);
  
  gle_item_factory_adjust (popup_factory, POPUP_OP_P_EDITOR,	TRUE,			NULL);
  gle_item_factory_adjust (popup_factory, POPUP_OP_P_TED,	!!proxy->children,	NULL);
  gle_item_factory_adjust (popup_factory, POPUP_OP_P_DESTROY,	TRUE,			NULL);

  for (i = 0; i < foreign->n_actions; i++)
    {
      GleForeignAction *action = foreign->actions + i;
      GleForeignActionState state = (proxy->object ? foreign->check_action (proxy->object, action, proxy) :
				     GLE_FOREIGN_ACTION_DISABLED);

      gle_item_factory_adjust (popup_factory, action->action_type,
			       state == GLE_FOREIGN_ACTION_ENABLED ? GLE_ITEM_FACTORY_ITEM_SENSITIVE :
			       state == GLE_FOREIGN_ACTION_DISABLED ? GLE_ITEM_FACTORY_ITEM_INSENSITIVE :
			       GLE_FOREIGN_ACTION_HIDE,
			       action);
    }

  return popup_data;
}

static void
gle_ted_popup_done (gpointer data)
{
  GleTedPopupData *popup_data = data;
  
  g_return_if_fail (popup_data != NULL);
  
  gle_proxy_unref (popup_data->proxy);
  g_free (popup_data);
}

static GtkItemFactory*
gle_ted_item_factory_from_foreign (GleTedClass *class,
				   GleForeign  *foreign)
{
  GtkItemFactory *factory;
  guint i;

  for (i = 0; i < class->n_factories; i++)
    if (class->factories[i].foreign == foreign)
      return GTK_ITEM_FACTORY (class->factories[i].factory);

  factory = gtk_item_factory_new (GTK_TYPE_MENU,
				  gle_ted_factories_path,
				  NULL);
  g_object_ref (factory);
  gtk_object_sink (GTK_OBJECT (factory));
  GLE_TAG (factory);
  g_object_set_data (G_OBJECT (factory), "GleForeign", foreign);

  i = class->n_factories++;
  class->factories = g_renew (GleTedFactory, class->factories, class->n_factories);
  class->factories[i].factory = factory;
  class->factories[i].foreign = foreign;

  gtk_item_factory_create_items (factory,
				 n_head_menu_entries,
				 head_menu_entries,
				 class);

  for (i = 0; i < foreign->n_actions; i++)
    {
      GleForeignAction *action = foreign->actions + i;
      GtkItemFactoryEntry entry = { NULL, NULL, };

      entry.path = g_strdup_printf ("/%s", action->action_name);
      entry.accelerator = NULL;
      entry.callback = gle_ted_popup_operation;
      entry.callback_action = action->action_type;
      entry.item_type = "<Item>";
      entry.extra_data = NULL;

      gtk_item_factory_create_items (factory, 1, &entry, class);
      g_free (entry.path);
    }

  gtk_item_factory_create_items (factory,
				 n_tail_menu_entries,
				 tail_menu_entries,
				 class);
  
  
  return factory;
}
