/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include	"gleselector.h"


#include	"glemarshal.h"


/* --- debug stuff --- */
#define	GLE_DEBUG_SELECTOR	0
#if (GLE_DEBUG_SELECTOR > 0)
#  define DEBUG	g_print
#else	/* !GLE_DEBUG_SELECTOR */
#  ifdef __OPTIMIZE__
#    define DEBUG	while (0) (* ((int (*) (char*, ...)) 0))
#  else	 /* !__OPTIMIZE__ */
static int __gle_debug_dummy = 0;
#    define DEBUG	__gle_debug_dummy += 0 && (* ((int (*) (char*, ...)) 0))
#  endif /* !__OPTIMIZE__ */
#endif	/* !GLE_DEBUG_SELECTOR */


/* --- defines ---*/
#define	GLE_SELECTOR_WARNING_TIMEOUT	(3000)


/* --- signals --- */
enum
{
  SIGNAL_CANDIDATE_CHECK,
  SIGNAL_CANDIDATE_SELECTED,
  SIGNAL_ABORT,
  SIGNAL_LAST
};


/* --- prototypes --- */
static void		gle_selector_class_init		(GleSelectorClass	*class);
static void		gle_selector_init		(GleSelector		*selector);
static void		gle_selector_destroy		(GtkObject		*object);
static gint		gle_selector_event		(GtkWidget		*widget,
							 GdkEvent		*event);
static void		gle_selector_cleanup		(GleSelector		*selector);
static void		gle_selector_candidate_label_set(GleSelector		*selector,
							 GtkWidget		*candidate);
static void		gle_selector_warning_draw	(GleSelector		*selector);
static gint		gle_selector_child_event	(GtkWidget		*widget,
							 GdkEvent		*event,
							 GleSelector		*selector);
static void		gle_selector_do_abort		(GleSelector		*selector);


/* --- variables --- */
static GtkWindowClass	*parent_class = NULL;
static GleSelectorClass	*gle_selector_class = NULL;
static guint		 selector_signals[SIGNAL_LAST] = { 0 };
static GMainLoop	*gle_selector_loop = NULL;
static const gchar	*warning_rc_string =
( "style'GleSelectorWarning'"
  "{"
  "font='-adobe-helvetica-medium-r-normal--*-120-*-*-*-*-*-*'"
  "font_name='Helvetica 20'"
  "bg[NORMAL]={0.5,0.5,0.5}"
  "base[NORMAL]={1.,0,0}"
  "fg[NORMAL]={1.,1.,0}"
  "}"
  "widget'GleSelectorWarning*'style'GleSelectorWarning'" );


/* --- functions --- */
GtkType
gle_selector_get_type (void)
{
  static GtkType selector_type = 0;
  
  if (!selector_type)
    {
      GtkTypeInfo selector_info =
      {
	"GleSelector",
	sizeof (GleSelector),
	sizeof (GleSelectorClass),
	(GtkClassInitFunc) gle_selector_class_init,
	(GtkObjectInitFunc) gle_selector_init,
	/* reserved_1 */ NULL,
	/* reserved_2 */ NULL,
	(GtkClassInitFunc) NULL,
      };
      
      selector_type = gtk_type_unique (GTK_TYPE_WINDOW, &selector_info);
    }
  
  return selector_type;
}

static void
gle_selector_class_init (GleSelectorClass *class)
{
  GtkObjectClass *object_class = GTK_OBJECT_CLASS (class);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);
  
  parent_class = g_type_class_peek_parent (class);
  gle_selector_class = class;
  
  object_class->destroy = gle_selector_destroy;

  widget_class->event = gle_selector_event;
  
  class->active_selector = NULL;
  class->candidate_check = NULL;
  class->candidate_selected = NULL;
  class->abort = gle_selector_do_abort;

  selector_signals[SIGNAL_CANDIDATE_CHECK] =
    gtk_signal_new ("candidate-check",
		    GTK_RUN_LAST,
		    G_OBJECT_CLASS_TYPE (class),
		    GTK_SIGNAL_OFFSET (GleSelectorClass, candidate_check),
		    gle_marshal_VOID__POINTER_POINTER,
		    GTK_TYPE_NONE, 2,
		    GTK_TYPE_POINTER,
		    GTK_TYPE_POINTER);
  selector_signals[SIGNAL_CANDIDATE_SELECTED] =
    gtk_signal_new ("candidate-selected",
		    GTK_RUN_LAST,
		    G_OBJECT_CLASS_TYPE (class),
		    GTK_SIGNAL_OFFSET (GleSelectorClass, candidate_selected),
		    gle_marshal_NONE__OBJECT,
		    GTK_TYPE_NONE, 1,
		    GTK_TYPE_WIDGET);
  selector_signals[SIGNAL_ABORT] =
    gtk_signal_new ("abort",
		    GTK_RUN_FIRST,
                    G_OBJECT_CLASS_TYPE (class),
		    GTK_SIGNAL_OFFSET (GleSelectorClass, abort),
		    gtk_signal_default_marshaller,
		    GTK_TYPE_NONE, 0);
}

static void
gle_selector_init (GleSelector *selector)
{
  GtkWidget *main_vbox;
  GtkWidget *label;
  GtkWidget *any;
  GtkWidget *button;
  
  selector->warning_visible = FALSE;
  selector->is_drag = FALSE;
  selector->candidate = NULL;
  selector->current = NULL;
  selector->warning_timer = 0;
  
  selector->query_cursor = gdk_cursor_new (GDK_DOT);
  selector->drag_window = NULL;
  
  g_object_set (GTK_WIDGET (selector),
		"type", GTK_WINDOW_TOPLEVEL,
		"window_position", GTK_WIN_POS_CENTER_ALWAYS,
		"allow_shrink", FALSE,
		"allow_grow", FALSE,
		"resize_mode", GTK_RESIZE_IMMEDIATE,
		NULL);
  main_vbox = g_object_new (GTK_TYPE_VBOX,
			    "homogeneous", FALSE,
			    "spacing", 5,
			    "border_width", 10,
			    "parent", selector,
			    "visible", TRUE,
			    NULL);
  selector->message_label = g_object_new (GTK_TYPE_LABEL,
					  "label", "Make a selection",
					  "visible", TRUE,
					  NULL);
  gtk_box_pack_start (GTK_BOX (main_vbox), selector->message_label, FALSE, FALSE, 10);
  any = g_object_new (GTK_TYPE_FRAME,
		      "shadow", GTK_SHADOW_ETCHED_OUT,
		      "label_xalign", 0.5,
		      "label", "Candidate",
		      "parent", main_vbox,
		      "visible", TRUE,
		      NULL);
  selector->candidate_label = g_object_new (GTK_TYPE_LABEL,
					    "label", "<None>",
					    "sensitive", FALSE,
					    "parent", any,
					    "visible", TRUE,
					    NULL);
  any = g_object_new (GTK_TYPE_HSEPARATOR,
		      "parent", main_vbox,
		      "visible", TRUE,
		      NULL);
  button = g_object_new (GTK_TYPE_BUTTON,
			 "label", "Abort",
			 "border_width", 5,
			 "can_default", TRUE,
			 "parent", main_vbox,
			 "visible", TRUE,
			 "has_default", TRUE,
			 NULL);
  g_object_connect (button,
		    /* "object_signal::clicked", gle_selector_abort, selector, */
		    "signal::event", gle_selector_child_event, selector,
		    NULL);
  
  selector->warning_window = g_object_new (GTK_TYPE_WINDOW,
					   "name", "GleSelectorWarningWindow",
					   "type", GTK_WINDOW_POPUP,
					   "window_position", GTK_WIN_POS_CENTER,
					   "allow_shrink", FALSE,
					   "allow_grow", FALSE,
					   "border_width", 10,
					   NULL);
  g_object_connect (selector->warning_window,
		    "swapped_signal::expose_event", gle_selector_warning_draw, selector,
		    "signal::delete_event", gtk_true, NULL,
		    NULL);
  label	= g_object_new (GTK_TYPE_LABEL,
			"visible", TRUE,
			"parent", selector->warning_window,
			"label", ("GLE-Selector Warning:\n"
				  "There already is a server grab in effect.\n"
				  "Overriding this might lead to strange behaviour\n"
				  "or worst case, program instability.\n"
				  "Reinvoke to override.\n"),
			NULL);
}

void
gle_selector_set_cursor (GleSelector *selector,
			 GdkCursor   *query_cursor)
{
  g_return_if_fail (GLE_IS_SELECTOR (selector));
  g_return_if_fail (query_cursor != NULL);
  
  if (query_cursor != selector->query_cursor)
    {
      gdk_cursor_destroy (selector->query_cursor);
      selector->query_cursor = query_cursor;
    }
  if (gle_selector_class->active_selector == selector)
    {
      gdk_pointer_ungrab (GDK_CURRENT_TIME);
      while (gdk_pointer_grab (GTK_WIDGET (selector)->window,
			       TRUE,
			       GDK_BUTTON_PRESS_MASK | GDK_BUTTON_RELEASE_MASK |
			       GDK_ENTER_NOTIFY_MASK | GDK_LEAVE_NOTIFY_MASK |
			       GDK_POINTER_MOTION_MASK | GDK_POINTER_MOTION_HINT_MASK,
			       NULL,
			       selector->query_cursor,
			       GDK_CURRENT_TIME));
    }
}

static void
gle_selector_destroy (GtkObject	*object)
{
  GleSelector *selector = GLE_SELECTOR (object);

  if (gle_selector_class->active_selector == selector)
    gle_selector_cleanup (selector);
  gle_selector_reset (selector);
  
  if (selector->query_cursor)
    {
      gdk_cursor_destroy (selector->query_cursor);
      selector->query_cursor = NULL;
    }
  if (selector->warning_window)
    {
      gtk_widget_destroy (selector->warning_window);
      selector->warning_window = NULL;
    }
  GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

GtkWidget*
gle_selector_new (const gchar *title,
		  const gchar *message)
{
  GtkWidget *selector;
  
  selector = g_object_new (GLE_TYPE_SELECTOR,
			   "title", title ? title : "GleSelector",
			   NULL);
  if (message)
    gtk_label_set_text (GTK_LABEL (GLE_SELECTOR (selector)->message_label), message);

  return selector;
}

void
gle_selector_abort (GleSelector	*selector)
{
  g_return_if_fail (GLE_IS_SELECTOR (selector));
  
  if (gle_selector_class->active_selector == selector)
    gtk_signal_emit (GTK_OBJECT (selector), selector_signals[SIGNAL_ABORT]);
}

static void
gle_selector_do_abort (GleSelector *selector)
{
  gle_selector_cleanup (selector);
  gle_selector_reset (selector);
}

static void
gle_selector_warning_draw (GleSelector *selector)
{
  GdkGC *frame_gc;
  GdkDrawable *drawable;
  guint max_width;
  guint max_height;
  
  g_return_if_fail (GLE_IS_SELECTOR (selector));
  
  if (!selector->warning_visible)
    return;
  
  drawable = selector->warning_window->window;
  frame_gc = selector->warning_window->style->base_gc[GTK_STATE_NORMAL];
  max_width = selector->warning_window->allocation.width;
  max_height = selector->warning_window->allocation.height;
  
  gdk_draw_rectangle (drawable, frame_gc, FALSE,
		      0, 0, 
		      max_width, max_height);
  gdk_draw_rectangle (drawable, frame_gc, FALSE,
		      1, 1,
		      max_width - 2, max_height - 2);
  gdk_draw_rectangle (drawable, frame_gc, FALSE,
		      2, 2,
		      max_width - 4, max_height - 4);
}

static void
gle_selector_warning_timeout_unlocked (GleSelector *selector)
{
  g_return_if_fail (GLE_IS_SELECTOR (selector));
  
  selector->warning_timer = 0;
  selector->warning_visible = FALSE;
  gtk_widget_hide (selector->warning_window);
}

void
gle_selector_reset (GleSelector	*selector)
{
  g_return_if_fail (GLE_IS_SELECTOR (selector));
  g_return_if_fail (gle_selector_class->active_selector == NULL);
  
  if (selector->warning_visible)
    {
      gtk_timeout_remove (selector->warning_timer);
      gle_selector_warning_timeout_unlocked (selector);
    }
  
  if (selector->candidate)
    {
      gtk_widget_unref (selector->candidate);
      selector->candidate = NULL;
      gle_selector_candidate_label_set (selector, NULL);
    }
  
  selector->current = NULL;
  selector->drag_window = NULL;
  selector->is_drag = FALSE;
}

gboolean
gle_selector_in_selection (GleSelector *selector)
{
  if (selector)
    {
      g_return_val_if_fail (GLE_IS_SELECTOR (selector), FALSE);
      
      return gle_selector_class->active_selector == selector;
    }
  else
    return gle_selector_class->active_selector != NULL;
}

static gboolean
warning_timeout (gpointer selector)
{
  GDK_THREADS_ENTER ();

  gle_selector_warning_timeout_unlocked (selector);
  
  GDK_THREADS_LEAVE ();

  return FALSE;
}

void
gle_selector_popup_warning (GleSelector	*selector)
{
  g_return_if_fail (GLE_IS_SELECTOR (selector));
  g_return_if_fail (gle_selector_class->active_selector == NULL);
  g_return_if_fail (selector->warning_visible == FALSE);
  g_return_if_fail (gdk_pointer_is_grabbed () == TRUE);
  
  selector->warning_visible = TRUE;
  if (warning_rc_string)
    {
      gtk_rc_parse_string (warning_rc_string);
      gtk_widget_set_rc_style (selector->warning_window);
      warning_rc_string = NULL;
    }
  gtk_widget_show (selector->warning_window);
  selector->warning_timer = gtk_timeout_add (GLE_SELECTOR_WARNING_TIMEOUT,
					     warning_timeout,
					     selector);
}

GtkWidget*
gle_selector_make_selection (GleSelector *selector)
{
  g_return_val_if_fail (GLE_IS_SELECTOR (selector), NULL);
  g_return_val_if_fail (gle_selector_class->active_selector == NULL, NULL);
  
  return gle_selector_extended_selection (selector, NULL, FALSE);
}

GtkWidget*
gle_selector_extended_selection (GleSelector *selector,
				 GtkWindow   *drag_window,
				 gboolean     is_drag)
{
  GtkWidget *result;
  
  g_return_val_if_fail (GLE_IS_SELECTOR (selector), NULL);
  g_return_val_if_fail (gle_selector_class->active_selector == NULL, NULL);

  g_assert (gle_selector_loop == NULL);
  
  if (drag_window)
    {
      g_return_val_if_fail (GTK_IS_WINDOW (drag_window), NULL);

      gtk_container_set_resize_mode (GTK_CONTAINER (drag_window), GTK_RESIZE_IMMEDIATE);
    }
  
  gtk_widget_ref (GTK_WIDGET (selector));
  
  gle_selector_reset (selector);
  
  selector->is_drag = is_drag;
  selector->drag_window = (GtkWidget*) drag_window;
  
  gtk_container_check_resize (GTK_CONTAINER (selector));
  gtk_widget_show (GTK_WIDGET (selector));
  gdk_window_raise (GTK_WIDGET (selector)->window);
  
  if (selector->drag_window)
    {
      gint px, py;
      
      gtk_widget_realize (selector->drag_window);
      gdk_window_get_pointer (NULL, &px, &py, NULL);
      gtk_widget_set_uposition (selector->drag_window, px + 1, py + 1);
      gtk_widget_show (selector->drag_window);
      gdk_window_raise (selector->drag_window->window);
    }
  
  gtk_grab_add (GTK_WIDGET (selector));
  gle_selector_class->active_selector = selector;
  
  result = NULL;
  gle_selector_set_cursor (selector, selector->query_cursor);

  gle_selector_loop = g_main_new (TRUE);

  GDK_THREADS_LEAVE ();
  g_main_run (gle_selector_loop);
  GDK_THREADS_ENTER ();
  
  gtk_widget_hide (GTK_WIDGET (selector));

  g_main_destroy (gle_selector_loop);
  gle_selector_loop = NULL;
  
  if (gle_selector_class->active_selector == selector)
    {
      g_warning ("GleSelector: Gle-selectorloop quitted for unknown reason");
      gle_selector_abort (selector);

      g_error ("GleSelector: can't recover from premature GMainLoop aborts");
    }
  else
    {
      gtk_widget_hide (GTK_WIDGET (selector));
      gdk_flush ();
    }
  
  selector->is_drag = FALSE;
  selector->drag_window = NULL;
  
  result = selector->candidate;
  
  gtk_widget_unref (GTK_WIDGET (selector));
  
  return result;
}

static void
gle_selector_cleanup (GleSelector *selector)
{
  g_return_if_fail (GLE_IS_SELECTOR (selector));
  g_return_if_fail (gle_selector_class->active_selector == selector);
  
  gtk_grab_remove (GTK_WIDGET (selector));
  gdk_pointer_ungrab (GDK_CURRENT_TIME);
  gle_selector_class->active_selector = NULL;
  
  if (selector->drag_window)
    gtk_widget_hide (selector->drag_window);
  
  selector->current = NULL;
  
  /* make the ungrab take effect *immediatedly* */
  gdk_flush ();
  
  g_main_quit (gle_selector_loop);
}

static void
gle_selector_candidate_label_set (GleSelector *selector,
				  GtkWidget   *candidate)
{
  GtkLabel *label;
  gchar *string;
  
  g_return_if_fail (GLE_IS_SELECTOR (selector));
  
  label = GTK_LABEL (selector->candidate_label);
  
  if (!candidate)
    string = g_strdup ("<None>");
  else
    {
      if (GTK_CHECK_TYPE (candidate, GTK_TYPE_WINDOW) &&
	  GTK_WINDOW (candidate)->title)
	string = g_strconcat (gtk_type_name (GTK_OBJECT_TYPE (candidate)),
			      "::",
			      GTK_WINDOW (candidate)->title,
			      NULL);
      else
	{
	  gchar *type_name;
	  
	  type_name = gtk_type_name (GTK_OBJECT_TYPE (candidate));
	  string = gtk_widget_get_name (candidate);
	  if (!g_str_equal (string, type_name))
	    string = g_strconcat (type_name, "::", string, NULL);
	  else
	    string = g_strdup (string);
	}
    }
  if (!g_str_equal (label->label, string))
    gtk_label_set_text (label, string);
  g_free (string);
  gtk_widget_set_sensitive (GTK_WIDGET (label), candidate && candidate == selector->candidate);
}

static gboolean
gle_selector_check_candidate (GleSelector *selector,
			      GtkWidget	 **new_candidate)
{
  gboolean candidate_ok;
  
  candidate_ok = *new_candidate != NULL;
  
  if (*new_candidate)
    {
      GtkWidget *toplevel;
      
      toplevel = gtk_widget_get_toplevel (*new_candidate);
      if (toplevel == selector->drag_window)
	{
	  selector->current = NULL;
	  return FALSE;
	}
      if (toplevel == GTK_WIDGET (selector))
	{
	  selector->current = NULL;
	  *new_candidate = toplevel;
	  candidate_ok = FALSE;
	}
    }
  
  gtk_signal_emit (GTK_OBJECT (selector),
		   selector_signals[SIGNAL_CANDIDATE_CHECK],
		   new_candidate,
		   &candidate_ok);
  candidate_ok &= *new_candidate != NULL;
  
  if (candidate_ok)
    {
      GtkWidget *toplevel;
      
      toplevel = gtk_widget_get_toplevel (*new_candidate);
      candidate_ok &= (toplevel != GTK_WIDGET (selector) &&
		       toplevel != selector->drag_window);
    }
  
  if (selector->candidate)
    gtk_widget_unref (selector->candidate);
  if (candidate_ok)
    {
      selector->candidate = *new_candidate;
      gtk_widget_ref (selector->candidate);
    }
  else
    selector->candidate = NULL;
  
  gle_selector_candidate_label_set (selector, *new_candidate);
  
  return candidate_ok;
}

typedef struct
{
  GtkWidget *child;
  GdkWindow *window;
  gint x;
  gint y;
} GleChildLocation;

static void
gle_selector_child_location_forech (GtkWidget	 *child,
				    gpointer	  data)
{
  GleChildLocation *cloc = data;
  
  if (!cloc->child &&
      GTK_WIDGET_NO_WINDOW (child) &&
      GTK_WIDGET_DRAWABLE (child))
    {
      if (child->window != cloc->window)
	{
	  cloc->window = child->window;
	  cloc->x = 0;
	  cloc->y = 0;
	  gdk_window_get_pointer (cloc->window, &cloc->x, &cloc->y, NULL);
	}
      
      DEBUG ("%s (%u,%u:%u,%u): (%u,%u) in: %s, ",
	     gtk_type_name (GTK_OBJECT_TYPE (child)),
	     child->allocation.x,
	     child->allocation.y,
	     child->allocation.width,
	     child->allocation.height,
	     cloc->x,
	     cloc->y,
	     cloc->child ? gtk_type_name (GTK_OBJECT_TYPE (cloc->child)) : "None");
      
      if (cloc->x >= child->allocation.x &&
	  cloc->y >= child->allocation.y &&
	  cloc->x < child->allocation.x + child->allocation.width &&
	  cloc->y < child->allocation.y + child->allocation.height)
	cloc->child = child;
      DEBUG ("out: %s\n",
	     cloc->child ? gtk_type_name (GTK_OBJECT_TYPE (cloc->child)) : "None");
    }
}

static gint
gle_selector_child_event (GtkWidget	       *widget,
			  GdkEvent	       *event,
			  GleSelector	       *selector)
{
  gboolean event_handled;
  
  g_return_val_if_fail (selector != NULL, FALSE);
  g_return_val_if_fail (GLE_IS_SELECTOR (selector), FALSE);
  
  event_handled = FALSE;
  
  if (gle_selector_class->active_selector != selector)
    return event_handled;
  
  if (event->type == GDK_BUTTON_RELEASE ||
      /* selector->is_drag || */
      event->type == GDK_BUTTON_PRESS)
    {
      event_handled = TRUE;
      gle_selector_abort (selector);
    }
  
  return event_handled;
}

static gint
gle_selector_event (GtkWidget *widget,
		    GdkEvent  *event)
{
  GtkWidget *event_widget;
  GleSelector *selector = GLE_SELECTOR (widget);
  gboolean event_handled;
  
  if (gle_selector_class->active_selector != selector)
    {
      if (GTK_WIDGET_CLASS (parent_class)->event)
	return GTK_WIDGET_CLASS (parent_class)->event (widget, event);
      else
	return FALSE;
    }
  
  event_widget = gtk_get_event_widget (event);
  
  event_handled = FALSE;
  switch (event->type)
    {
    case GDK_MOTION_NOTIFY:
      if (selector->drag_window)
	{
	  gint px, py;
	  
	  gdk_window_get_pointer (NULL, &px, &py, NULL);
	  gtk_widget_set_uposition (selector->drag_window, px + 1, py + 1);
	  gdk_window_raise (selector->drag_window->window);
	}
      if (event_widget && selector->current /* && event->motion.is_hint */)
	{
	  event_widget = NULL;
	  gdk_window_get_user_data (selector->current, (gpointer*) &event_widget);
	  
	  if (event_widget && GTK_IS_CONTAINER (event_widget))
	    {
	      GleChildLocation cloc = { NULL, NULL, 0, 0 };
	      
	      cloc.child = event_widget;
	      cloc.window = cloc.child->window;
	      gdk_window_get_pointer (cloc.window, &cloc.x, &cloc.y, NULL);
	      
	      do
		{
		  event_widget = cloc.child;
		  cloc.child = NULL;
		  gtk_container_foreach (GTK_CONTAINER (event_widget),
					 gle_selector_child_location_forech,
					 &cloc);
		}
	      while (cloc.child && GTK_IS_CONTAINER (cloc.child));
	      
	      if (cloc.child)
		event_widget = cloc.child;
	      
	      if (selector->candidate != event_widget)
		gle_selector_check_candidate (selector, &event_widget);
	    }
	}
      event_handled = TRUE;
      break;
      
    case GDK_LEAVE_NOTIFY:
    case GDK_ENTER_NOTIFY:
      selector->current = gdk_window_at_pointer (NULL, NULL);
      if (selector->current)
	gdk_window_get_user_data (selector->current, (gpointer*) &event_widget);
      else
	event_widget = NULL;
      gle_selector_check_candidate (selector, &event_widget);
      event_handled = TRUE;
      break;
      
    case GDK_BUTTON_RELEASE:
      if (selector->is_drag &&
	  !GTK_WIDGET_IS_SENSITIVE (selector->candidate_label))
	{
	  event_handled = TRUE;
	  gle_selector_abort (selector);
	  break;
	}
      /* fall through */
    case GDK_BUTTON_PRESS:
      event_widget = selector->candidate;
      if (GTK_WIDGET_IS_SENSITIVE (selector->candidate_label))
	{
	  gle_selector_cleanup (selector);
	  gtk_signal_emit (GTK_OBJECT (selector),
			   selector_signals[SIGNAL_CANDIDATE_SELECTED],
			   selector->candidate);
	}
      event_handled = TRUE;
      break;
      
    case GDK_DELETE:
      gle_selector_abort (selector);
      event_handled = TRUE;
      break;
      
    default:
      break;
    }
  
  return event_handled;
}
