/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#include        <gle/gleconfig.h>

#include	"glehandler.h"
#include	"glegobject.h"


/* --- prototypes --- */


/* --- variables -- */
static GMemChunk	*handler_mem_chunk = NULL;
static GHashTable	*handler_ht = NULL;
static GMemChunk	*connection_mem_chunk = NULL;


/* --- functions --- */
static inline GleHandler*
gle_handler_lookup (GQuark quark)
{
  return handler_ht ? g_hash_table_lookup (handler_ht, GUINT_TO_POINTER (quark)) : NULL;
}

void
gle_handler_register_default (const gchar *name,
			      gpointer     func,
			      gpointer     data)
{
  GleHandler *handler;
  GQuark quark;

  g_return_if_fail (name != NULL);
  g_return_if_fail (func != NULL);

  quark = g_quark_from_string (name);
  handler = gle_handler_lookup (quark);
  if (!handler)
    {
      handler = g_chunk_new (GleHandler, handler_mem_chunk);
      handler->id = quark;
      g_hash_table_insert (handler_ht, GUINT_TO_POINTER (quark), handler);
    }
  handler->handler = func;
  handler->data = data;
}

void
gle_handler_register (const gchar            *name,
		      gpointer                func,
		      gpointer                data,
		      GtkType                 return_type,
		      guint                   n_params,
		      ...)
{
  g_return_if_fail (name != NULL);
  g_return_if_fail (func != NULL);
  g_return_if_fail (return_type == GTK_TYPE_NONE);
  g_return_if_fail (n_params == 0);

  gle_handler_register_default (name, func, data);
}

GleHandler*
gle_handler_get (const gchar *name)
{
  GQuark quark;

  g_return_val_if_fail (name != NULL, NULL);

  quark = g_quark_try_string (name);

  return quark ? gle_handler_lookup (quark) : NULL;
}

static void
gle_handler_foreach_func (gpointer key,
			  gpointer value,
			  gpointer user_data)
{
  gpointer *data = user_data;
  GleHandlerForeach func;

  func = data[0];
  if (func && !func (value, data[1]))
    data[0] = NULL;
}

void
gle_handler_foreach (GleHandlerForeach func,
		     gpointer          user_data)

{
  g_return_if_fail (func != NULL);

  if (handler_ht)
    {
      gpointer data[2];

      data[0] = func;
      data[1] = user_data;

      g_hash_table_foreach (handler_ht, gle_handler_foreach_func, &data);
    }
}

void
gle_handler_init (void)
{
  static const struct {
    const gchar	*name;
    gpointer	 func;
    gpointer	 data;
  } default_handlers[] = {
    { "gtk_false",			gtk_false,			NULL, },
    { "gtk_true",			gtk_true,			NULL, },
    { "gtk_main_quit",			gtk_main_quit,			NULL, },
    { "gtk_widget_destroy",		gtk_widget_destroy,		NULL, },
    { "gtk_widget_show",		gtk_widget_show,		NULL, },
    { "gtk_widget_hide",		gtk_widget_hide,		NULL, },
    { "gtk_widget_activate",		gtk_widget_activate,		NULL, },
    { "gtk_widget_grab_focus",		gtk_widget_grab_focus,		NULL, },
    { "gtk_widget_grab_default",	gtk_widget_grab_default,	NULL, },
    { "gtk_widget_queue_draw",		gtk_widget_queue_draw,		NULL, },
    { "gtk_widget_queue_clear",		gtk_widget_queue_clear,		NULL, },
    { "gtk_window_activate_focus",	gtk_window_activate_focus,	NULL, },
    { "gtk_window_activate_default",	gtk_window_activate_default,	NULL, },
  };
  static const guint n_default_handlers = sizeof (default_handlers) / sizeof (default_handlers[0]);

  if (!handler_mem_chunk)
    {
      guint i;

      handler_mem_chunk = g_mem_chunk_create (GleHandler, 10, G_ALLOC_AND_FREE);
      handler_ht = g_hash_table_new (NULL, g_direct_equal);
      connection_mem_chunk = g_mem_chunk_create (GleConnection, 16, G_ALLOC_AND_FREE);

      for (i = 0; i < n_default_handlers; i++)
	gle_handler_register_default (default_handlers[i].name,
				      default_handlers[i].func,
				      default_handlers[i].data);
    }
}

GleConnection*
gle_connection_new (GleGObject       *gobject,
		    guint             signal_id,
		    const gchar      *handler,
		    gpointer          data,
		    GleConnectionType ctype)
{
  GleConnection *connection;

  g_return_val_if_fail (GLE_IS_GOBJECT (gobject), NULL);
  g_return_val_if_fail (!GLE_GOBJECT_DESTROYED (gobject), NULL);
  g_return_val_if_fail (signal_id > 0, NULL);
  g_return_val_if_fail (handler != NULL, NULL);

  connection = g_chunk_new0 (GleConnection, connection_mem_chunk);
  connection->next = gobject->connections;
  gobject->connections = connection;
  connection->gobject = gobject;
  connection->signal_id = signal_id;
  connection->handler = g_strdup (handler);
  connection->data = data;
  connection->ctype = ctype & GLE_CONNECTION_OBJECT_AFTER;

  return connection;
}

void
gle_connection_connect (GleConnection *connection)
{
  GtkObject *object;

  g_return_if_fail (connection != NULL);
  g_return_if_fail (GLE_GOBJECT_IS_INSTANTIATED (connection->gobject));

  object = connection->gobject->object;

  if (!connection->gtk_id && !GTK_OBJECT_DESTROYED (object))
    {
      GleHandler *handler;

      handler = gle_handler_get (connection->handler);
      if (handler)
	connection->gtk_id = gtk_signal_connect_full (object,
						      gtk_signal_name (connection->signal_id),
						      handler->handler,
						      NULL,
						      handler,
						      NULL,
						      (connection->ctype & GLE_CONNECTION_OBJECT) != FALSE,
						      (connection->ctype & GLE_CONNECTION_AFTER) != FALSE);
      else
	g_warning ("unable to find handler \"%s\" for ::%s connection of `%s' proxy %s \"%s\"",
		   connection->handler,
		   gtk_signal_name (connection->signal_id),
		   gtk_type_name (GLE_GOBJECT_OTYPE (connection->gobject)),
		   gtk_type_name (GLE_GOBJECT_PROXY_TYPE (connection->gobject)),
		   connection->gobject->gname);
    }
}

void
gle_connection_disconnect (GleConnection *connection)
{
  GtkObject *object;

  g_return_if_fail (connection != NULL);

  object = connection->gobject->object;

  if (connection->gtk_id && object &&
      gtk_signal_handler_pending_by_id (object, connection->gtk_id, TRUE))
    gtk_signal_disconnect (object, connection->gtk_id);

  connection->gtk_id = 0;
}

void
gle_connection_change (GleConnection    *connection,
		       guint             signal_id,
		       const gchar      *handler,
		       gpointer          data,
		       GleConnectionType ctype)
{
  g_return_if_fail (connection != NULL);
  g_return_if_fail (!GLE_GOBJECT_DESTROYED (connection->gobject));
  g_return_if_fail (signal_id > 0);
  g_return_if_fail (handler != NULL);

  ctype &= GLE_CONNECTION_OBJECT_AFTER;

  if (connection->signal_id != signal_id ||
      connection->data != data ||
      connection->ctype != ctype ||
      strcmp (connection->handler, handler))
    {
      gle_connection_disconnect (connection);
      connection->handler = g_strdup (handler);
      connection->signal_id = signal_id;
      connection->data = data;
      connection->ctype = ctype;
    }
}

void
gle_connection_destroy (GleConnection *connection)
{
  GleConnection *last, *node;
  
  g_return_if_fail (connection != NULL);

  gle_connection_disconnect (connection);

  last = NULL;
  for (node = connection->gobject->connections; node; last = node, node = last->next)
    if (node == connection)
      {
	if (last)
	  last->next = node->next;
	else
	  connection->gobject->connections = node->next;
	break;
      }
  connection->next = NULL;

  g_chunk_free (connection, connection_mem_chunk);
}

void
gle_connections_destroy (GleGObject *gobject)
{
  g_return_if_fail (GLE_IS_GOBJECT (gobject));

  while (gobject->connections)
    gle_connection_destroy (gobject->connections);
}
