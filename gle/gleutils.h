/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001-2002 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GLE_UTILS_H__
#define __GLE_UTILS_H__


#include	<gtk/gtk.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* --- macros --- */
#define	GLE_TOOLTIPS		(gle_util_get_tooltips ())
#define	GLE_TAG(gobject)	(gle_util_tag_object (G_OBJECT (gobject), #gobject))
#define	GLE_HAS_TAG(gobject)	(gle_util_check_tag (G_OBJECT (gobject)))


/* --- typedefs (forward) --- */
typedef struct _GleProxy     GleProxy;


/* --- prototypes --- */
GtkTooltips*	gle_util_get_tooltips		(void);
glong		gle_util_parse_long		(const gchar		*string);
gdouble		gle_util_parse_double		(const gchar		*string);
gint		gle_clist_selection_info	(GtkCList		*clist,
						 GdkWindow		*window,
						 gint			 x,
						 gint			 y,
						 gint			*row,
						 gint			*column);
typedef enum {
  GLE_ITEM_FACTORY_ITEM_INSENSITIVE = FALSE,
  GLE_ITEM_FACTORY_ITEM_SENSITIVE = TRUE,
  GLE_ITEM_FACTORY_ITEM_HIDDEN = 2
} GleItemFactoryItemState;
void		gle_item_factory_adjust		(GtkItemFactory		*ifactory,
						 guint			 action,
						 GleItemFactoryItemState sensitive,
						 gpointer		 user_data);
void		gle_util_set_flash_widget	(GtkWidget		*widget);
GtkWidget*	gle_info_window			(const gchar		*title,
						 const gchar		*info_text);
void		gle_util_tag_object		(GObject		*object,
						 const gchar		*string);
gboolean	gle_util_check_tag		(GObject		*object);
void		gle_file_selection_heal		(GtkFileSelection	*fs);
       


#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif  /* __GLE_UTILS_H__ */
