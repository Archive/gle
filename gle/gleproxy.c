/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001-2002 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include "gleproxy.h"


#include "gleproxyparam.h"


/* --- structures --- */
typedef struct {
  GHook ghook;
  guint hook_id;
} ProxyHook;


/* --- functions --- */
static void
proxy_set_parent (GleProxy *proxy,
		  GleProxy *parent)
{
  g_return_if_fail (GLE_IS_PROXY (proxy));
  if (parent)
    {
      g_return_if_fail (GLE_IS_PROXY (parent));
      g_return_if_fail (proxy->pset == parent->pset);
      g_return_if_fail (proxy->parent == NULL);
    }
  g_return_if_fail (proxy != parent);

  if (proxy->parent != parent)
    {
      if (parent)
	{
	  proxy->parent = parent;
	  parent->children = g_list_prepend (parent->children, proxy);
	}
      else
	{
	  proxy->parent->children = g_list_remove (proxy->parent->children, proxy);
	  proxy->parent = NULL;
	}
    }
}

static GleProxyParam*
proxy_find_object_param (GleProxy   *proxy,
			 GParamSpec *pspec)
{
  GSList *slist;

  g_return_val_if_fail (GLE_IS_PROXY (proxy), NULL);
  g_return_val_if_fail (G_IS_PARAM_SPEC (pspec), NULL);

  for (slist = proxy->object_params; slist; slist = slist->next)
    {
      GleProxyParam *param = slist->data;

      if (param->pspec == pspec)
	return param;
    }
  return NULL;
}

static GleProxyParam*
proxy_find_link_param (GleProxy   *proxy,
		       GParamSpec *pspec)
{
  GSList *slist;

  g_return_val_if_fail (GLE_IS_PROXY (proxy), NULL);
  g_return_val_if_fail (G_IS_PARAM_SPEC (pspec), NULL);

  for (slist = proxy->link_params; slist; slist = slist->next)
    {
      GleProxyParam *param = slist->data;

      if (param->pspec == pspec)
	return param;
    }
  return NULL;
}

static void
gle_proxy_reload_link_params (GleProxy *proxy)
{
  GSList *slist;
  
  /* get rid of old link params */
  for (slist = proxy->link_params; slist; slist = slist->next)
    gle_proxy_param_free (slist->data);
  g_slist_free (proxy->link_params);
  proxy->link_params = NULL;
  
  /* fetch new link params */
  if (proxy->parent)
    {
      GParamSpec **pspecs = NULL;
      guint i, n = 0;

      if (proxy->pset->foreign->list_link_pspecs)
	pspecs = proxy->pset->foreign->list_link_pspecs (proxy->oclass, proxy->parent->oclass, &n);
      for (i = 0; i < n; i++)
	{
	  GleProxyParam *param = pspecs[i] ? gle_proxy_param_check_create (proxy, pspecs[i]) : NULL;

	  if (param)
	    proxy->link_params = g_slist_prepend (proxy->link_params, param);
	}
      g_free (pspecs);
      gle_proxy_foreign_notify (proxy, GLE_FOREIGN_UPDATE_LINK_PARAMS);
    }

  gle_proxy_invoke_hooks (proxy, GLE_PROXY_RELOAD);
}

GleProxy*
gle_proxy_from_type (GleProxySet *pset,
		     GType        type)
{
  GleProxy *proxy;
  GParamSpec **pspecs = NULL;
  guint i, n = 0;

  g_return_val_if_fail (GLE_IS_PROXY_SET (pset), NULL);
  g_return_val_if_fail (G_TYPE_IS_OBJECT (type), NULL);

  proxy = g_new0 (GleProxy, 1);
  proxy->oclass = g_type_class_ref (type);
  g_hook_list_init (&proxy->hook_list, sizeof (ProxyHook));
  
  /* add to proxy set */
  proxy->ref_count = 1;
  gle_proxy_set_append_proxy (pset, proxy);

  /* fetch object pspecs */
  if (proxy->pset->foreign->list_object_pspecs)
    pspecs = proxy->pset->foreign->list_object_pspecs (proxy->oclass, &n);
  for (i = 0; i < n; i++)
    {
      GleProxyParam *param = pspecs[i] ? gle_proxy_param_check_create (proxy, pspecs[i]) : NULL;
      if (param)
	proxy->object_params = g_slist_prepend (proxy->object_params, param);
    }
  g_free (pspecs);

  gle_proxy_reload_link_params (proxy);
  gle_proxy_unref (proxy);
  
  return proxy;
}

static void
gle_proxy_finalize (GleProxy *proxy)
{
  GSList *slist;

  g_return_if_fail (proxy->object == NULL);
  g_return_if_fail (proxy->parent == NULL);
  g_return_if_fail (proxy->children == NULL);

  if (proxy->update_handler)
    g_source_remove (proxy->update_handler);
  for (slist = proxy->link_params; slist; slist = slist->next)
    gle_proxy_param_free (slist->data);
  g_slist_free (proxy->link_params);
  for (slist = proxy->object_params; slist; slist = slist->next)
    gle_proxy_param_free (slist->data);
  g_slist_free (proxy->object_params);
  g_free (proxy->state_hint);
  g_free (proxy->instance_flag);
  g_hook_list_clear (&proxy->hook_list);
  g_free (proxy->pname);
  g_type_class_unref (proxy->oclass);
}

GleProxy*
gle_proxy_ref (GleProxy *proxy)
{
  g_return_val_if_fail (GLE_IS_PROXY (proxy), NULL);
  g_return_val_if_fail (proxy->ref_count > 0, NULL);

  proxy->ref_count += 1;

  return proxy;
}

void
gle_proxy_unref (GleProxy *proxy)
{
  g_return_if_fail (GLE_IS_PROXY (proxy));
  g_return_if_fail (proxy->ref_count > 0);

  proxy->ref_count -= 1;
  if (!proxy->ref_count)
    {
      g_return_if_fail (proxy->pset == NULL);
      gle_proxy_finalize (proxy);
      g_free (proxy);
    }
}

void
gle_proxy_kill (GleProxy *proxy)
{
  g_return_if_fail (GLE_IS_PROXY (proxy));
  g_return_if_fail (proxy->pset != NULL);

  gle_proxy_ref (proxy);
  while (proxy->children)
    gle_proxy_kill (proxy->children->data);
  gle_proxy_invoke_hooks (proxy, GLE_PROXY_DESTROY);
  proxy_set_parent (proxy, NULL);
  if (proxy->object)
    gle_proxy_detach_foreign (proxy);
  gle_proxy_set_remove_proxy (proxy->pset, proxy);
  gle_proxy_unref (proxy);
}

GleProxy*
gle_proxy_new_from_object (GleProxySet *pset,
			   GObject     *object)
{
  GleProxy *proxy;

  g_return_val_if_fail (GLE_IS_PROXY_SET (pset), NULL);
  g_return_val_if_fail (G_IS_OBJECT (object), NULL);
  g_return_val_if_fail (!GLE_HAS_TAG (object), NULL);
  g_return_val_if_fail (gle_proxy_get_from_object (object, pset->foreign) == NULL, NULL);

  proxy = gle_proxy_from_type (pset, G_OBJECT_TYPE (object));
  proxy->object = object;
  proxy->pset->foreign->attach_object (object);
  proxy->pset->foreign->set_proxy (object, proxy);
  gle_proxy_capture_object_params (proxy);
  gle_proxy_capture_parent (proxy);
  gle_proxy_reload_link_params (proxy);
  gle_proxy_capture_link_params (proxy);

  return proxy;
}

GleProxy*
gle_proxy_get_from_object (GObject    *object,
			   GleForeign *foreign)
{
  g_return_val_if_fail (G_IS_OBJECT (object), NULL);
  g_return_val_if_fail (foreign != NULL, NULL);

  return foreign->get_proxy (object);
}

void
gle_proxy_capture_parent (GleProxy *proxy)
{
  GObject *oparent;
  gboolean new_child = FALSE;

  g_return_if_fail (GLE_IS_PROXY (proxy));
  g_return_if_fail (GLE_PROXY_HAS_OBJECT (proxy));
  g_return_if_fail (proxy->parent == NULL);

  oparent = proxy->pset->foreign->get_parent (proxy->object);
  if (oparent)
    {
      GleProxy *parent = gle_proxy_get_from_object (oparent, proxy->pset->foreign);

      if (parent && parent->pset == proxy->pset)
	{
	  new_child = TRUE;
	  proxy_set_parent (proxy, parent);
	}
    }
  gle_proxy_invoke_hooks (proxy, GLE_PROXY_RELOAD);
  if (new_child)
    gle_proxy_invoke_hooks (proxy->parent, GLE_PROXY_ADD_CHILD);
}

void
gle_proxy_apply_parent (GleProxy *proxy)
{
  g_return_if_fail (GLE_IS_PROXY (proxy));
  g_return_if_fail (GLE_PROXY_HAS_OBJECT (proxy));

  if (proxy->parent && proxy->pset->foreign->set_parent)
    proxy->pset->foreign->set_parent (proxy->object, proxy->parent->object);
}

void
gle_proxy_capture_object_params (GleProxy *proxy)
{
  GSList *slist;

  g_return_if_fail (GLE_IS_PROXY (proxy));
  g_return_if_fail (GLE_PROXY_HAS_OBJECT (proxy));

  for (slist = proxy->object_params; slist; slist = slist->next)
    {
      GleProxyParam *param = slist->data;

      if (G_IS_VALUE (&param->value))
	g_value_unset (&param->value);
      g_value_init (&param->value, G_PARAM_SPEC_VALUE_TYPE (param->pspec));
      if (proxy->pset->foreign->get_object_param)
	gle_proxy_param_update (slist->data, proxy->pset->foreign->get_object_param, proxy->object);
    }
  g_free (proxy->state_hint);
  if (proxy->pset->foreign->state_hint)
    proxy->state_hint = proxy->pset->foreign->state_hint (proxy->object);
  else
    proxy->state_hint = NULL;
  gle_proxy_invoke_hooks (proxy, GLE_PROXY_CHANGED);
}

void
gle_proxy_capture_link_params (GleProxy *proxy)
{
  GSList *slist;

  g_return_if_fail (GLE_IS_PROXY (proxy));
  g_return_if_fail (GLE_PROXY_HAS_OBJECT (proxy));

  for (slist = proxy->link_params; slist; slist = slist->next)
    {
      GleProxyParam *param = slist->data;

      if (G_IS_VALUE (&param->value))
	g_value_unset (&param->value);
      g_value_init (&param->value, G_PARAM_SPEC_VALUE_TYPE (param->pspec));
      if (proxy->pset->foreign->get_link_param)
	gle_proxy_param_update (slist->data, proxy->pset->foreign->get_link_param, proxy->object);
    }
  gle_proxy_invoke_hooks (proxy, GLE_PROXY_CHANGED);
}

void
gle_proxy_apply_object_params (GleProxy *proxy)
{
  GSList *slist;

  g_return_if_fail (GLE_IS_PROXY (proxy));
  g_return_if_fail (GLE_PROXY_HAS_OBJECT (proxy));

  for (slist = proxy->object_params; slist; slist = slist->next)
    {
      GleProxyParam *param = slist->data;

      if (G_IS_VALUE (&param->value) && proxy->pset->foreign->set_object_param)
	gle_proxy_param_apply (slist->data, proxy->pset->foreign->set_object_param, proxy->object);
    }
}

void
gle_proxy_apply_link_params (GleProxy *proxy)
{
  GSList *slist;

  g_return_if_fail (GLE_IS_PROXY (proxy));
  g_return_if_fail (GLE_PROXY_HAS_OBJECT (proxy));

  for (slist = proxy->link_params; slist; slist = slist->next)
    {
      GleProxyParam *param = slist->data;

      if (G_IS_VALUE (&param->value) && proxy->pset->foreign->set_link_param)
	gle_proxy_param_apply (slist->data, proxy->pset->foreign->set_link_param, proxy->object);
    }
}

void
gle_proxy_set_param (GleProxy     *proxy,
		     GParamSpec   *pspec,
		     const GValue *value)
{
  GleProxyParam *param;
  GleForeignSetParam set_func;
  
  g_return_if_fail (GLE_IS_PROXY (proxy));
  g_return_if_fail (G_IS_PARAM_SPEC (pspec));
  g_return_if_fail (G_IS_VALUE (value));

  set_func = proxy->pset->foreign->set_object_param;
  param = proxy_find_object_param (proxy, pspec);
  if (!param)
    {
      set_func = proxy->pset->foreign->set_link_param;
      param = proxy_find_link_param (proxy, pspec);
    }
  if (param)
    {
      gle_proxy_param_set (param, value);
      if (GLE_PROXY_HAS_OBJECT (proxy) && set_func)
	gle_proxy_param_apply (param, set_func, proxy->object);
    }
  else
    g_warning ("%s: param spec \"%s\" unknown",
	       G_STRLOC, pspec->name);
}

void
gle_proxy_get_param (GleProxy   *proxy,
		     GParamSpec *pspec,
		     GValue     *value)
{
  GleProxyParam *param;
  
  g_return_if_fail (GLE_IS_PROXY (proxy));
  g_return_if_fail (G_IS_PARAM_SPEC (pspec));
  g_return_if_fail (G_IS_VALUE (value));

  param = proxy_find_object_param (proxy, pspec);
  if (!param)
    param = proxy_find_link_param (proxy, pspec);
  if (param)
    gle_proxy_param_get (param, value);
  else
    g_warning ("%s: param spec \"%s\" unknown",
	       G_STRLOC, pspec->name);
}

void
gle_proxy_create_foreign (GleProxy *proxy)
{
  g_return_if_fail (GLE_IS_PROXY (proxy));
  g_return_if_fail (!GLE_PROXY_HAS_OBJECT (proxy));

  if (!proxy->pset->foreign->construct_object)
    return;

  proxy->object = proxy->pset->foreign->construct_object (proxy->oclass);
  proxy->pset->foreign->set_proxy (proxy->object, proxy);
  gle_proxy_invoke_hooks (proxy, GLE_PROXY_ATTACH);
}

void
gle_proxy_detach_foreign (GleProxy *proxy)
{
  g_return_if_fail (GLE_IS_PROXY (proxy));
  g_return_if_fail (GLE_PROXY_HAS_OBJECT (proxy));

  gle_proxy_invoke_hooks (proxy, GLE_PROXY_DETACH);
  proxy->pset->foreign->set_proxy (proxy->object, NULL);
  proxy->pset->foreign->detach_object (proxy->object);
  proxy->object = NULL;
}

void
gle_proxy_dispose_foreign (GleProxy *proxy)
{
  g_return_if_fail (GLE_IS_PROXY (proxy));
  g_return_if_fail (GLE_PROXY_HAS_OBJECT (proxy));

  gle_proxy_invoke_hooks (proxy, GLE_PROXY_DETACH);
  proxy->pset->foreign->set_proxy (proxy->object, NULL);
  if (proxy->pset->foreign->destruct_object)
    proxy->pset->foreign->destruct_object (proxy->object);
  proxy->object = NULL;
}

void
gle_proxy_add_hook (GleProxy *proxy,
		    guint     hook_id,
		    gpointer  func,
		    gpointer  data)
{
  ProxyHook *phook;

  g_return_if_fail (GLE_IS_PROXY (proxy));
  g_return_if_fail (proxy->ref_count > 0);
  g_return_if_fail (func != NULL);

  phook = (ProxyHook*) g_hook_alloc (&proxy->hook_list);
  phook->ghook.func = func;
  phook->ghook.data = data;
  phook->hook_id = hook_id;
  g_hook_append (&proxy->hook_list, &phook->ghook);
}

void
gle_proxy_remove_hooks (GleProxy *proxy,
			gpointer  func,
			gpointer  data)
{
  GHook *hook;

  g_return_if_fail (GLE_IS_PROXY (proxy));
  g_return_if_fail (proxy->ref_count > 0);
  g_return_if_fail (func != NULL);

  hook = g_hook_find_func_data (&proxy->hook_list, FALSE, func, data);
  if (!hook)
    g_warning (G_STRLOC ": couldn't find hook %p(%p) to disconnect", func, data);
  else
    while (hook)
      {
	g_hook_destroy_link (&proxy->hook_list, hook);
	hook = g_hook_find_func_data (&proxy->hook_list, FALSE, func, data);
      }
}

void
gle_proxy_invoke_hooks (GleProxy *proxy,
			guint     hook_id)
{
  gboolean may_recurse = FALSE;
  GHook *hook;
  
  g_return_if_fail (GLE_IS_PROXY (proxy));
  g_return_if_fail (proxy->ref_count > 0);

  /* g_printerr ("NOTIFY(%p): %u\n", proxy, hook_id); */

  gle_proxy_ref (proxy);
  hook = g_hook_first_valid (&proxy->hook_list, may_recurse);
  while (hook)
    {
      ProxyHook *phook = (ProxyHook*) hook;

      if (phook->hook_id == hook_id)
	{
	  void (*func) (gpointer data, GleProxy *proxy) = hook->func;
	  gboolean was_in_call = G_HOOK_IN_CALL (hook);

	  hook->flags |= G_HOOK_FLAG_IN_CALL;
	  func (hook->data, proxy);
	  if (!was_in_call)
	    hook->flags &= ~G_HOOK_FLAG_IN_CALL;
	}
      hook = g_hook_next_valid (&proxy->hook_list, hook, may_recurse);
    }
  gle_proxy_unref (proxy);
}

static gboolean
proxy_update_handler (gpointer data)
{
  GleProxy *proxy = data;

  GDK_THREADS_ENTER ();
  proxy->update_handler = 0;
  if (proxy->pset)
    {
      guint i;

      for (i = 0; i < 32; i++)
	{
	  switch (proxy->update_mask & (1 << i))
	    {
	    case GLE_FOREIGN_RELOAD_LINK_PARAMS:
	      gle_proxy_reload_link_params (proxy);
	      break;
	    case GLE_FOREIGN_RELOAD_PARENT:
	      proxy_set_parent (proxy, NULL);
	      gle_proxy_capture_parent (proxy);
	      /*	      if (!gle_foreign_is_toplevel (proxy))
			      gle_proxy_kill (proxy);
	      */
	      break;
	    case GLE_FOREIGN_UPDATE_OBJECT_PARAMS:
	      gle_proxy_capture_object_params (proxy);
	      break;
	    case GLE_FOREIGN_UPDATE_LINK_PARAMS:
	      gle_proxy_capture_link_params (proxy);
	      break;
	    case GLE_FOREIGN_DETACH_OBJECT:
	      if (GLE_PROXY_HAS_OBJECT (proxy))
		gle_proxy_detach_foreign (proxy);
	      break;
	    }
	  proxy->update_mask &= ~(1 << i);
	}
    }
  GDK_THREADS_LEAVE ();

  return FALSE;
}

void
gle_proxy_foreign_notify (GleProxy        *proxy,
			  GleForeignNotify notify_mask)
{
  g_return_if_fail (GLE_IS_PROXY (proxy));

  if (notify_mask)
    {
      proxy->update_mask |= notify_mask;
      if (!proxy->update_handler)
	proxy->update_handler = g_idle_add (proxy_update_handler, proxy);
    }
}
