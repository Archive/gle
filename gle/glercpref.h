/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef __GLE_RC_PREF_H__
#define __GLE_RC_PREF_H__

#include	<gle/glerc.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* --- type macros --- */
#define GLE_TYPE_RC_PREF	    (gle_rc_pref_get_type ())
#define GLE_RC_PREF(object)	    (GTK_CHECK_CAST ((object), GLE_TYPE_RC_PREF, GleRcPref))
#define GLE_RC_PREF_CLASS(klass)    (GTK_CHECK_CLASS_CAST ((klass), GLE_TYPE_RC_PREF, GleRcPrefClass))
#define GLE_IS_RC_PREF(object)	    (GTK_CHECK_TYPE ((object), GLE_TYPE_RC_PREF))
#define GLE_IS_RC_PREF_CLASS(klass) (GTK_CHECK_CLASS_TYPE ((klass), GLE_TYPE_RC_PREF))


/* --- typedefs --- */
typedef struct	_GleRcPref	GleRcPref;
typedef struct	_GleRcPrefClass	GleRcPrefClass;
typedef struct	_GleRcGroupW	GleRcGroupW;
typedef struct	_GleRcArgW	GleRcArgW;


/* --- structures --- */
struct	_GleRcPref
{
  GtkAlignment		alignment;
  
  GleRcArgW		*args;
  GtkTooltips		*tooltips;
  GtkWidget		*notebook;
};

struct	_GleRcPrefClass
{
  GtkAlignmentClass	parent_class;
};

struct _GleRcArgW
{
  GleRcArgW		*next;
  GleRcArg		*rc_arg;
  GtkWidget		*widget;
};



GtkType		gle_rc_pref_get_type		(void);
void		gle_rc_pref_construct		(GleRcPref	*rc_pref,
						 GleRcHandle	*rc_handle);
void		gle_rc_pref_apply_args		(GleRcPref	*rc_pref);
void		gle_rc_pref_refresh_args	(GleRcPref	*rc_pref);
void		gle_rc_pref_reset_args		(GleRcPref	*rc_pref);

GtkWidget*	gle_rc_pref_dialog_new		(GleRcHandle	*rc_handle,
						 const gchar	*window_title);





#ifdef __cplusplus
}
#endif /* __cplusplus */

#endif /* __GLE_RC_PREF_H__ */

