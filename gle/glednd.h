/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef __GLE_DND_H__
#define __GLE_DND_H__


#include	<gle/glegobject.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


typedef	GleGObject*	(*GleDndStart)	(GtkWidget	*widget,
					 gint		 root_x,
					 gint		 root_y);

typedef	GleGObject*	(*GleDndCheck)	(GtkWidget	*widget,
					 GtkType	 drag_type,
					 gint		 root_x,
					 gint		 root_y);
typedef	void		(*GleDndNotify)	(GtkWidget	*widget,
					 GleGObject	*candidate);
typedef	void		(*GleDndStop)	(GtkWidget	*widget,
					 GleGObject	*gobject,
					 gboolean	 drop_success);


void	gle_dnd_add_source	(GtkWidget	*pwidget,
				 guint		 button_mask,
				 GleDndStart	 dnd_start,
				 GleDndStop	 dnd_stop);
void	gle_dnd_add_target	(GtkWidget	*pwidget,
				 GleDndCheck	 dnd_check);
void	gle_dnd_add_notifier	(GtkWidget	*pwidget,
				 GleDndNotify	 dnd_notify);
void	gle_dnd_remove_notifier	(GtkWidget	*pwidget);





#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif  /* __GLE_DND_H__ */
