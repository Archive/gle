/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include	"gleplist.h"

#include	<gle/gleconfig.h>
#include	"gleproxyset.h"
#include	"glemarshal.h"
#include	"glestock.h"
#include	<gdk/gdk.h>
#include	<string.h>
#include	<stdio.h>


/* --- signals --- */
enum {
  SIGNAL_BUTTON_CLICK,
  SIGNAL_CHECK_TAG,
  SIGNAL_LAST
};


/* --- prototypes --- */
static void		gle_plist_class_init		(GlePListClass	 *class);
static void		gle_plist_init			(GlePList	 *plist);
static GObject*		gle_plist_constructor		(GType		        type,
							 guint                  n_construct_properties,
							 GObjectConstructParam *construct_properties);
static void		gle_plist_destroy		(GtkObject	 *object);
static GlePListPix*	gle_plist_get_pix		(GlePList	 *plist,
							 GQuark		  pix_id);
static void		gle_plist_realize		(GtkWidget	 *widget);
static void		gle_plist_unrealize		(GtkWidget	 *widget);
static gint		gle_plist_button_press_event	(GtkWidget	 *widget,
							 GdkEventButton	 *event);
static void		gle_plist_row_data_new		(GlePList	 *plist,
							 GleProxySet	 *pset,
							 gint		  row);
static void		gle_plist_row_data_update	(gpointer	  data,
							 GleProxySet	 *pset,
							 GleProxySetNotifyID id);
static void		gle_plist_row_data_destroyed	(GlePListRowData *row_data);


/* --- variables --- */
static GtkCListClass	*parent_class = NULL;
static GlePListClass	*gle_plist_class = NULL;
static guint		 plist_signals[SIGNAL_LAST] = { 0 };


/* --- functions --- */
GtkType
gle_plist_get_type (void)
{
  static GtkType plist_type = 0;
  
  if (!plist_type)
    {
      GtkTypeInfo plist_info =
      {
	"GlePList",
	sizeof (GlePList),
	sizeof (GlePListClass),
	(GtkClassInitFunc) gle_plist_class_init,
	(GtkObjectInitFunc) gle_plist_init,
	NULL,
	NULL,
      };
      
      plist_type = gtk_type_unique (GTK_TYPE_CLIST, &plist_info);
    }
  
  return plist_type;
}

static void
gle_plist_class_init (GlePListClass *class)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (class);
  GtkObjectClass *object_class = GTK_OBJECT_CLASS (class);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);
  
  parent_class = g_type_class_peek_parent (class);
  gle_plist_class = class;
  
  gobject_class->constructor = gle_plist_constructor;

  object_class->destroy = gle_plist_destroy;
  
  widget_class->realize = gle_plist_realize;
  widget_class->unrealize = gle_plist_unrealize;
  widget_class->button_press_event = gle_plist_button_press_event;
  
  class->button_click = NULL;
  class->check_tag = NULL;

  plist_signals[SIGNAL_BUTTON_CLICK] =
    gtk_signal_new ("button-click",
		    GTK_RUN_LAST,
		    GTK_CLASS_TYPE (class),
		    GTK_SIGNAL_OFFSET (GlePListClass, button_click),
		    gle_marshal_BOOLEAN__OBJECT_UINT_UINT_UINT_UINT,
		    G_TYPE_BOOLEAN, 5,
		    GLE_TYPE_PROXY_SET,
		    G_TYPE_UINT,
		    G_TYPE_UINT,
		    G_TYPE_UINT,
		    G_TYPE_UINT);
  plist_signals[SIGNAL_CHECK_TAG] =
    gtk_signal_new ("check-tag",
		    GTK_RUN_LAST,
                    GTK_CLASS_TYPE (class),
		    GTK_SIGNAL_OFFSET (GlePListClass, check_tag),
		    gle_marshal_UINT__OBJECT,
		    G_TYPE_UINT, 1,
		    GLE_TYPE_PROXY_SET);

  /* fix number of columns */
  g_object_class_install_property (gobject_class, 1,
				   g_param_spec_uint ("n_columns", NULL, NULL,
						      GLE_PLIST_N_COLS, GLE_PLIST_N_COLS, GLE_PLIST_N_COLS,
						      0));
}

static void
gle_plist_init (GlePList *plist)
{
  GtkCList *clist = GTK_CLIST (plist);
  
  plist->shown_row_data = NULL;
  plist->row_datas = NULL;
  plist->pix_cache = NULL;
  plist->unknown_xpm = gle_stock_get_xpm_unknown ();

  /* setup widget
   */
  g_object_set (clist,
		"GtkCList::n_columns", GLE_PLIST_N_COLS,
		NULL);
  gtk_widget_set_usize (GTK_WIDGET (plist),
			GLE_RCVAL_PLIST_WIDTH > 0 ? GLE_RCVAL_PLIST_WIDTH : -1,
			GLE_RCVAL_PLIST_HEIGHT > 0 ? GLE_RCVAL_PLIST_HEIGHT : -1);
}

static GObject*
gle_plist_constructor (GType                  type,
		       guint                  n_construct_properties,
		       GObjectConstructParam *construct_properties)
{
  static const struct {
    guint justify, width;
    gchar *title;
  } titles[GLE_PLIST_N_COLS] = {
    /* GLE_PLIST_COL_ICON */		{ GTK_JUSTIFY_LEFT,	30,	"Type", },
    /* GLE_PLIST_COL_TAG */		{ GTK_JUSTIFY_CENTER,	20,	"Tag?", },
    /* GLE_PLIST_COL_PSET_NAME */	{ GTK_JUSTIFY_LEFT,	120,	"Name", },
    /* GLE_PLIST_COL_OTYPE */		{ GTK_JUSTIFY_LEFT,	90,	"Objects", },
    /* GLE_PLIST_COL_PSET_SOURCE */	{ GTK_JUSTIFY_LEFT,	200,	"Source", },
  };
  GObject *object = G_OBJECT_CLASS (parent_class)->constructor (type, n_construct_properties, construct_properties);
  GtkCList *clist = GTK_CLIST (object);
  guint i;

  /* post-construction setup
   */
  gtk_clist_column_titles_hide (clist);
  gtk_clist_set_selection_mode (clist, GTK_SELECTION_EXTENDED);
  gtk_clist_column_titles_passive (clist);
  for (i = 0; i < GLE_PLIST_N_COLS; i++)
    {
      gtk_clist_set_column_justification (clist, i, titles[i].justify);
      gtk_clist_set_column_width (clist, i, titles[i].width);
      gtk_clist_set_column_title (clist, i, titles[i].title);
    }
  gtk_clist_column_titles_show (clist);
  
  return object;
}

static void
gle_plist_realize (GtkWidget *widget)
{
  GlePList *plist = GLE_PLIST (widget);

  GTK_WIDGET_CLASS (parent_class)->realize (widget);

  gle_plist_update_all (plist);
}

static void
gle_plist_unrealize (GtkWidget *widget)
{
  GlePList *plist;
  GlePListPix *pix;
  
  plist = GLE_PLIST (widget);
  
  GTK_WIDGET_CLASS (parent_class)->unrealize (widget);
  
  pix = plist->pix_cache;
  plist->pix_cache = NULL;
  while (pix)
    {
      GlePListPix *prev;
      
      gdk_pixmap_unref (pix->pixmap);
      gdk_bitmap_unref (pix->bitmap);
      
      prev = pix;
      pix = prev->next;
      g_free (prev);
    }
}

static GlePListPix*
gle_plist_get_pix (GlePList *plist,
		   GQuark    quark)
{
  gchar **xpm = NULL;
  GlePListPix *pix;
  GtkWidget *widget;
  
  if (!GTK_WIDGET_REALIZED (plist) || quark == 0)
    return NULL;
  
  for (pix = plist->pix_cache; pix; pix = pix->next)
    if (pix->id == quark)
      return pix;
  
  xpm = gle_stock_id_get_xpm (quark);
  
  if (!xpm)
    xpm = plist->unknown_xpm;
  
  widget = GTK_WIDGET (plist);
  
  pix = g_new0 (GlePListPix, 1);
  pix->next = plist->pix_cache;
  plist->pix_cache = pix;
  pix->id = quark;
  pix->pixmap = gdk_pixmap_create_from_xpm_d (widget->window,
					      &pix->bitmap,
					      &widget->style->bg[GTK_STATE_PRELIGHT],
					      xpm);
  return pix;
}

GtkWidget*
gle_plist_new (void)
{
  GlePList *plist;
  
  plist = g_object_new (GLE_TYPE_PLIST, NULL);
  
  return GTK_WIDGET (plist);
}

static void
gle_plist_destroy (GtkObject *object)
{
  GlePList *plist;
  
  plist = GLE_PLIST (object);
  
  GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

void
gle_plist_clear (GlePList *plist)
{
  g_return_if_fail (GLE_IS_PLIST (plist));
  
  gtk_clist_clear (GTK_CLIST (plist));
}

void
gle_plist_freeze (GlePList *plist)
{
  g_return_if_fail (GLE_IS_PLIST (plist));
  
  gtk_clist_freeze (GTK_CLIST (plist));
}

void
gle_plist_thaw (GlePList *plist)
{
  g_return_if_fail (GLE_IS_PLIST (plist));
  
  gtk_clist_thaw (GTK_CLIST (plist));
}

GleProxySet*
gle_plist_get_proxy_set (GlePList *plist,
			 gint      row)
{
  GlePListRowData *row_data;
  
  g_return_val_if_fail (GLE_IS_PLIST (plist), NULL);
  
  row_data = gtk_clist_get_row_data (GTK_CLIST (plist), row);
  
  return row_data ? row_data->pset : NULL;
}

GList*
gle_plist_list_selected (GlePList *plist)
{
  GtkCList *clist;
  GList *list;
  GList *selection = NULL;
  
  g_return_val_if_fail (GLE_IS_PLIST (plist), NULL);
  
  clist = GTK_CLIST (plist);
  for (list = g_list_last (clist->selection); list; list = list->prev)
    selection = g_list_prepend (selection,
				gle_plist_get_proxy_set (plist,
							 GPOINTER_TO_INT (list->data)));
  
  return selection;
}

GList*
gle_plist_list_all (GlePList *plist)
{
  GtkCList *clist;
  guint i;
  GList *selection = NULL;
  
  g_return_val_if_fail (GLE_IS_PLIST (plist), NULL);
  
  clist = GTK_CLIST (plist);
  for (i = 0; i < clist->rows; i++)
    selection = g_list_prepend (selection, gle_plist_get_proxy_set (plist, i));
  
  return selection;
}

gboolean
gle_plist_mark (GlePList    *plist,
		GleProxySet *pset)
{
  GtkCList *clist;
  GlePListRowData *row_data;
  
  g_return_val_if_fail (GLE_IS_PLIST (plist), FALSE);
  
  if ((plist->shown_row_data == NULL && pset == NULL) ||
      (plist->shown_row_data && pset == plist->shown_row_data->pset))
    return pset != NULL;
  
  clist = GTK_CLIST (plist);
  
  gtk_clist_freeze (clist);
  
  if (plist->shown_row_data)
    {
      row_data = plist->shown_row_data;
      plist->shown_row_data = NULL;
      gle_plist_row_data_update (row_data, row_data->pset, 0);
    }
  
  if (pset)
    {
      GSList *slist;
      
      row_data = NULL;
      for (slist = plist->row_datas; slist; slist = slist->next)
	{
	  row_data = slist->data;
	  
	  if (row_data->pset == pset)
	    break;
	}
      if (row_data && row_data->pset != pset)
	row_data = NULL;
      
      if (row_data)
	{
	  gint row;
	  
	  plist->shown_row_data = row_data;
	  gle_plist_row_data_update (row_data, row_data->pset, 0);
	  
	  row = gtk_clist_find_row_from_data (clist, row_data);
	  if (gtk_clist_row_is_visible (GTK_CLIST (plist), row) != GTK_VISIBILITY_FULL)
	    gtk_clist_moveto (GTK_CLIST (plist),
			      row, -1,
			      0.5, 0);
	}
      else
	pset = NULL;
    }
  
  gtk_clist_thaw (clist);
  
  return pset != NULL;
}

static void
gle_plist_row_data_new (GlePList    *plist,
			GleProxySet *pset,
			gint	     row)
{
  gchar *text[GLE_PLIST_N_COLS];
  GlePListRowData *row_data;
  guint i;
  
  for (i = 0; i < GLE_PLIST_N_COLS; i++)
    text[i] = NULL;
  
  text[GLE_PLIST_COL_ICON] = NULL;
  text[GLE_PLIST_COL_TAG] = NULL;
  text[GLE_PLIST_COL_PSET_NAME] = "";
  text[GLE_PLIST_COL_OTYPE] = "some proxy set";
  switch (pset->source)
    {
    case GLE_PROXY_SET_SOURCE_RIPPED:	text[GLE_PLIST_COL_PSET_SOURCE] = "Ripped";	break;
    case GLE_PROXY_SET_SOURCE_FILE:	text[GLE_PLIST_COL_PSET_SOURCE] = "Resource";	break;
    case GLE_PROXY_SET_SOURCE_USER:	text[GLE_PLIST_COL_PSET_SOURCE] = "User";	break;
    default:				text[GLE_PLIST_COL_PSET_SOURCE] = "Unknown";	break;
    }
  row_data = g_new0 (GlePListRowData, 1);
  row_data->plist = plist;
  plist->row_datas = g_slist_prepend (plist->row_datas, row_data);
  row_data->pset = pset;
  row = gtk_clist_insert (GTK_CLIST (plist), row, text);
  gtk_clist_set_row_data_full (GTK_CLIST (plist),
			       row,
			       row_data,
			       (GtkDestroyNotify) gle_plist_row_data_destroyed);
  
  gle_proxy_set_add_notify (pset,
			    gle_plist_row_data_update,
			    row_data);
  gle_plist_row_data_update (row_data, row_data->pset, 0);
}

static void
gle_plist_insert (GlePList    *plist,
		  GleProxySet *pset,
		  gint	       row)
{
  GlePListRowData *row_data = NULL;
  GSList *slist;
  
  for (slist = plist->row_datas; slist; slist = slist->next)
    {
      row_data = slist->data;
      
      if (row_data->pset == pset)
	break;
    }
  if (row_data && row_data->pset != pset)
    row_data = NULL;
  
  if (row_data)
    {
      g_warning ("GlePList: `%s' proxy set \"%s\" already contained",
		 "FIXME",
		 "FIXME");
      return;
    }
  
  gle_plist_row_data_new (plist, pset, row);
}

void
gle_plist_append (GlePList    *plist,
		  GleProxySet *pset)
{
  g_return_if_fail (GLE_IS_PLIST (plist));
  g_return_if_fail (GLE_IS_PROXY_SET (pset));
  
  gle_plist_insert (plist, pset, -1);
}

void
gle_plist_prepend (GlePList    *plist,
		   GleProxySet *pset)
{
  g_return_if_fail (GLE_IS_PLIST (plist));
  g_return_if_fail (GLE_IS_PROXY_SET (pset));
  
  gle_plist_insert (plist, pset, 0);
}

static void
gle_plist_row_data_update (gpointer	       data,
			   GleProxySet	      *pset,
			   GleProxySetNotifyID id)
{
  GlePListRowData *row_data = data;

  if (id == GLE_PROXY_SET_DESTROY)
    {
      GtkCList *clist = GTK_CLIST (row_data->plist);
      gint row = gtk_clist_find_row_from_data (clist, row_data);

      gtk_clist_remove (clist, row);
    }
  else if (row_data)
    {
      GlePList *plist = row_data->plist;
      GtkCList *clist = GTK_CLIST (plist);
      GleProxySet *pset = row_data->pset;
      GlePListPix* pix;
      gint row;
      gint row_height;
      GQuark tag_id;
      
      row = gtk_clist_find_row_from_data (clist, row_data);
      row_height = clist->row_height;
      
      if (plist->shown_row_data == row_data)
	gtk_clist_set_foreground (clist, row, &GTK_WIDGET (clist)->style->base[GTK_STATE_PRELIGHT]);
      else
	gtk_clist_set_foreground (clist, row, NULL);
      
      gtk_clist_set_text (clist, row, GLE_PLIST_COL_PSET_NAME, pset->name);
      
      if (0 /* FIXME */)
	; // pix = gle_plist_get_pix (plist, g_quark_try_string (gtk_type_name (GLE_PROXY_OTYPE (proxy))));
      else
	pix = gle_plist_get_pix (plist, g_quark_try_string (GLE_XPM_PROXY));
      if (!pix)
	gtk_clist_set_text (clist, row, GLE_PLIST_COL_ICON, NULL);
      else
	{
	  gint h;
	  
	  gtk_clist_set_pixmap (clist, row, GLE_PLIST_COL_ICON, pix->pixmap, pix->bitmap);
	  gdk_window_get_size (pix->pixmap, NULL, &h);
	  row_height = MAX (row_height, h);
	}
      
      tag_id = 0;
      gtk_signal_emit (GTK_OBJECT (plist),
		       plist_signals[SIGNAL_CHECK_TAG],
		       pset,
		       &tag_id);
      pix = gle_plist_get_pix (plist, tag_id);
      if (!pix)
	gtk_clist_set_text (clist, row, GLE_PLIST_COL_TAG, NULL);
      else
	{
	  gint h;
	  
	  gtk_clist_set_pixmap (clist, row, GLE_PLIST_COL_TAG, pix->pixmap, pix->bitmap);
	  gdk_window_get_size (pix->pixmap, NULL, &h);
	  row_height = MAX (row_height, h);
	}
      
      if (row_height > clist->row_height)
	gtk_clist_set_row_height (clist, row_height);
    }
}

void
gle_plist_update (GlePList    *plist,
		  GleProxySet *pset)
{
  GlePListRowData *row_data = NULL;
  GSList *slist;
  
  g_return_if_fail (GLE_IS_PLIST (plist));
  g_return_if_fail (GLE_IS_PROXY_SET (pset));
  
  for (slist = plist->row_datas; slist; slist = slist->next)
    {
      row_data = slist->data;
      
      if (row_data->pset == pset)
	break;
    }
  if (row_data && row_data->pset != pset)
    row_data = NULL;
  
  if (!row_data)
    {
      g_warning ("GlePList: no such `%s' proxy \"%s\"",
		 "FIXME",
		 "FIXME");
      return;
    }
  
  gle_plist_row_data_update (row_data, row_data->pset, 0);
  
  /*
    gdk_window_get_size (pixmap, NULL, &height);
    row_height = MAX (row_height, height);
    gtk_clist_set_row_height (shell->clist, row_height);
  */
  
}

static void
gle_plist_row_data_destroyed (GlePListRowData *row_data)
{
  GlePList *plist = row_data->plist;
  GtkCList *clist = GTK_CLIST (plist);
  GleProxySet *pset = row_data->pset;
  gint row;
  
  if (plist->shown_row_data == row_data)
    plist->shown_row_data = NULL;
  
  gtk_clist_freeze (clist);
  
  row = gtk_clist_find_row_from_data (clist, row_data);
  gtk_clist_remove (clist, row);
  
  gle_proxy_set_remove_notify (pset, gle_plist_row_data_update, row_data);
  
  gtk_clist_thaw (clist);
  
  row_data->plist = NULL;
  plist->row_datas = g_slist_remove (plist->row_datas, row_data);
  g_free (row_data);
}

void
gle_plist_remove (GlePList    *plist,
		  GleProxySet *pset)
{
  GlePListRowData *row_data = NULL;
  GtkCList *clist;
  GSList *slist;
  gint row;
  
  g_return_if_fail (GLE_IS_PLIST (plist));
  
  clist = GTK_CLIST (plist);
  for (slist = plist->row_datas; slist; slist = slist->next)
    {
      row_data = slist->data;
      
      if (row_data->pset == pset)
	break;
    }
  if (row_data && row_data->pset != pset)
    row_data = NULL;
  
  if (!row_data)
    {
      g_warning ("GlePList: no such `%s' proxy \"%s\"",
		 "FIXME",
		 "FIXME");
      return;
    }
  
  row = gtk_clist_find_row_from_data (clist, row_data);
  gtk_clist_remove (clist, row);
}

void
gle_plist_update_all (GlePList	     *plist)
{
  GtkCList *clist;
  GSList *slist;
  
  g_return_if_fail (plist != NULL);
  g_return_if_fail (GLE_IS_PLIST (plist));
  
  clist = GTK_CLIST (plist);
  
  gtk_clist_freeze (clist);
  
  for (slist = plist->row_datas; slist; slist = slist->next)
    {
      GlePListRowData *row_data = slist->data;

      gle_plist_row_data_update (row_data, row_data->pset, 0);
    }
  gtk_clist_thaw (clist);
}

static gint
gle_plist_button_press_event (GtkWidget		*widget,
			      GdkEventButton	*event)
{
  GlePList *plist;
  gboolean handled;
  
  plist = GLE_PLIST (widget);
  
  handled = FALSE;
  if (event->type == GDK_BUTTON_PRESS && event->button)
    {
      gint row;
      
      if (gle_clist_selection_info (GTK_CLIST (plist),
				    event->window,
				    event->x, event->y,
				    &row, NULL))
	{
	  GlePListRowData *row_data;
	  
	  row_data = gtk_clist_get_row_data (GTK_CLIST (plist), row);
	  if (row_data)
	    {
	      gint return_val = FALSE;
	      
	      gtk_signal_emit (GTK_OBJECT (plist),
			       plist_signals[SIGNAL_BUTTON_CLICK],
			       row_data->pset,
			       (guint) event->x_root,
			       (guint) event->y_root,
			       (guint) event->button,
			       (guint) event->time,
			       &return_val);
	      handled |= return_val != FALSE;
	    }
	}
    }
  if (!handled && GTK_WIDGET_CLASS (parent_class)->button_press_event)
    handled = GTK_WIDGET_CLASS (parent_class)->button_press_event (widget, event);
  
  return handled;
}
