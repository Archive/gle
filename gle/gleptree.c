/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001-2002 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include	<gle/gleconfig.h>

#include	"gleptree.h"
// #include	"glednd.h"
#include	"glemarshal.h"
#include	<gdk/gdk.h>
#include	<string.h>
#include	<stdio.h>


typedef struct
{
  GlePTree	*ptree;
  GtkCTreeNode	*parent;
  GtkCTreeNode	*sibling;
  guint		 depth;
} GlePTreeAppendData;


/* --- signals --- */
enum {
  SIGNAL_BUTTON_CLICK,
  SIGNAL_LAST
};


/* --- prototypes --- */
static void		gle_ptree_class_init		(GlePTreeClass		*class);
static void		gle_ptree_init			(GlePTree		*ptree);
static GObject*		gle_ptree_constructor		(GType                   type,
							 guint                   n_construct_properties,
							 GObjectConstructParam	*construct_properties);
static void		gle_ptree_destroy		(GtkObject		*object);
static void		gle_ptree_finalize		(GObject		*object);
static void		gle_ptree_append_proxy		(GleProxy		*proxy,
							 GlePTreeAppendData	*data);
static void		gle_ptree_row_data_update	(GlePTreeRowData	*row_data);
static void		gle_ptree_row_data_add_child	(GlePTreeRowData	*row_data);
static void		gle_ptree_row_data_destroyed	(GlePTreeRowData	*row_data);
static void		gle_ptree_row_data_free		(GlePTreeRowData	*row_data);
static gint		gle_ptree_button_press_event	(GtkWidget		*widget,
							 GdkEventButton		*event);
#if 0
static GleGObject*	gle_ptree_dnd_start		(GtkWidget		*widget,
							 gint			 root_x,
							 gint			 root_y);
static GleGObject*	gle_ptree_dnd_check		(GtkWidget		*widget,
							 GtkType		 drag_type,
							 gint			 root_x,
							 gint			 root_y);
#endif


/* --- variables --- */
static GtkCTreeClass	*parent_class = NULL;
static GlePTreeClass	*gle_ptree_class = NULL;
static gint		 gle_row_data_counter = 0;
static guint		 ptree_signals[SIGNAL_LAST] = { 0 };


/* --- functions --- */
GtkType
gle_ptree_get_type (void)
{
  static GtkType ptree_type = 0;
  
  if (!ptree_type)
    {
      GtkTypeInfo ptree_info =
      {
	"GlePTree",
	sizeof (GlePTree),
	sizeof (GlePTreeClass),
	(GtkClassInitFunc) gle_ptree_class_init,
	(GtkObjectInitFunc) gle_ptree_init,
	NULL,
	NULL,
      };
      
      ptree_type = gtk_type_unique (GTK_TYPE_CTREE, &ptree_info);
    }
  
  return ptree_type;
}

static void
gle_ptree_class_init (GlePTreeClass  *class)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (class);
  GtkObjectClass *object_class = GTK_OBJECT_CLASS (class);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);
  
  parent_class = g_type_class_peek_parent (class);
  gle_ptree_class = class;
  
  gobject_class->constructor = gle_ptree_constructor;

  gobject_class->finalize = gle_ptree_finalize;

  object_class->destroy = gle_ptree_destroy;
  
  widget_class->button_press_event = gle_ptree_button_press_event;
  
  gle_row_data_counter = 0;
  class->button_click = NULL;
  
  ptree_signals[SIGNAL_BUTTON_CLICK] =
    gtk_signal_new ("button-click",
		    GTK_RUN_LAST,
		    GTK_CLASS_TYPE (class),
		    GTK_SIGNAL_OFFSET (GlePTreeClass, button_click),
		    gle_marshal_BOOLEAN__POINTER_UINT_UINT_UINT_UINT,
		    GTK_TYPE_BOOL, 5,
		    GLE_TYPE_PROXY,
		    GTK_TYPE_UINT,
		    GTK_TYPE_UINT,
		    GTK_TYPE_UINT,
		    GTK_TYPE_UINT);

  /* fix number of columns */
  g_object_class_install_property (gobject_class, 1,
				   g_param_spec_uint ("n_columns", NULL, NULL,
						      GLE_PTREE_N_COLS, GLE_PTREE_N_COLS, GLE_PTREE_N_COLS,
						      0));
}

static void
gle_ptree_init (GlePTree *ptree)
{
  ptree->proxy = NULL;
  ptree->shown_row_data = NULL;
  
  /* setup widget
   */
  g_object_set (ptree,
		"GtkCTree::n_columns", GLE_PTREE_N_COLS,
		NULL);
  gtk_widget_set_usize (GTK_WIDGET (ptree),
			GLE_RCVAL_PTREE_WIDTH > 0 ? GLE_RCVAL_PTREE_WIDTH : -1,
			GLE_RCVAL_PTREE_HEIGHT > 0 ? GLE_RCVAL_PTREE_HEIGHT : -1);

  /* implement dnd facilities
   */
#if 0
  gle_dnd_add_source (GTK_WIDGET (ptree), 2, gle_ptree_dnd_start, NULL);
  gle_dnd_add_target (GTK_WIDGET (ptree), gle_ptree_dnd_check);
  gle_dnd_add_notifier (GTK_WIDGET (ptree), (GleDndNotify) gle_ptree_mark_proxy);
#endif
}

static GObject*
gle_ptree_constructor (GType                  type,
		       guint                  n_construct_properties,
		       GObjectConstructParam *construct_properties)
{
  static const struct {
    guint justify, width;
    gchar *title;
  } titles[GLE_PTREE_N_COLS] = {
    /* GLE_PTREE_COL_OTYPE */	{ GTK_JUSTIFY_LEFT,	440,	"Type", },
    /* GLE_PTREE_COL_STATE */	{ GTK_JUSTIFY_RIGHT,	50,	"State", },
    /* GLE_PTREE_COL_PNAME */	{ GTK_JUSTIFY_RIGHT,	50,	"Name", },
  };
  GObject *object = G_OBJECT_CLASS (parent_class)->constructor (type, n_construct_properties, construct_properties);
  GtkCList *clist = GTK_CLIST (object);
  guint i;

  /* post-construction setup
   */
  gtk_clist_column_titles_hide (clist);
  gtk_clist_set_selection_mode (clist, GTK_SELECTION_EXTENDED);
  gtk_clist_column_titles_passive (clist);
  for (i = 0; i < GLE_PTREE_N_COLS; i++)
    {
      gtk_clist_set_column_justification (clist, i, titles[i].justify);
      gtk_clist_set_column_width (clist, i, titles[i].width);
      gtk_clist_set_column_title (clist, i, titles[i].title);
    }
  gtk_clist_column_titles_show (clist);

  return object;
}

GtkWidget*
gle_ptree_new (void)
{
  GlePTree *ptree;
  
  ptree = g_object_new (GLE_TYPE_PTREE, NULL);
  
  return GTK_WIDGET (ptree);
}

static void
gle_ptree_destroy (GtkObject *object)
{
  GlePTree *ptree;
  
  ptree = GLE_PTREE (object);
  
  gle_ptree_set_proxy (ptree, NULL);
  
  GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static void
gle_ptree_finalize (GObject *object)
{
  GlePTree *ptree = GLE_PTREE (object);
  
  gle_ptree_set_proxy (ptree, NULL);

  if (ptree->thaw_handler)
    g_source_remove (ptree->thaw_handler);
  
  G_OBJECT_CLASS (parent_class)->finalize (object);
}

static void
gle_ptree_proxy_destroyed (GlePTree *ptree)
{
  ptree->proxy = NULL;
  gle_ptree_rebuild (ptree);
}

void
gle_ptree_set_proxy (GlePTree *ptree,
		     GleProxy *proxy)
{
  g_return_if_fail (GLE_IS_PTREE (ptree));
  
  if (proxy)
    g_return_if_fail (GLE_IS_PROXY (proxy));
  
  if (proxy != ptree->proxy)
    {
      if (ptree->proxy)
	GLE_PROXY_REMOVE_NOTIFIES (ptree->proxy, gtk_widget_destroy, ptree);
      ptree->proxy = proxy;
      if (ptree->proxy)
	{
	  GLE_PROXY_NOTIFY_DESTROY (ptree->proxy, gtk_widget_destroy, ptree);
	  GLE_PROXY_NOTIFY_DETACH (ptree->proxy, gtk_widget_destroy, ptree);
	}
      
      gle_ptree_rebuild (ptree);
    }
}

static void
gle_ptree_foreach_ctree_row_update (GtkCTree	 *ctree,
				    GtkCTreeNode *cnode,
				    gpointer	  data)
{
  GlePTreeRowData *row_data;
  
  row_data = gtk_ctree_node_get_row_data (ctree, cnode);
  gle_ptree_row_data_update (row_data);
}

void
gle_ptree_rebuild (GlePTree *ptree)
{
  g_return_if_fail (ptree != NULL);
  g_return_if_fail (GLE_IS_PTREE (ptree));
  
  gtk_clist_freeze (GTK_CLIST (ptree));
  
  gtk_clist_clear (GTK_CLIST (ptree));
  
  if (ptree->proxy)
    {
      GlePTreeAppendData data;
      
      data.ptree = ptree;
      data.parent = NULL;
      data.sibling = NULL;
      data.depth = 0;
      gle_ptree_append_proxy (ptree->proxy, &data);
    }
  
  gtk_clist_thaw (GTK_CLIST (ptree));
}

void
gle_ptree_update (GlePTree *ptree)
{
  g_return_if_fail (ptree != NULL);
  g_return_if_fail (GLE_IS_PTREE (ptree));
  
  gtk_clist_freeze (GTK_CLIST (ptree));
  
  gtk_ctree_post_recursive (GTK_CTREE (ptree), NULL, gle_ptree_foreach_ctree_row_update, NULL);
  
  gtk_clist_thaw (GTK_CLIST (ptree));
}

static void
gle_ptree_foreach_find (GtkCTree     *ctree,
			GtkCTreeNode *cnode,
			gpointer      data_p)
{
  GlePTreeRowData *row_data;
  gpointer *data = data_p;
  
  row_data = gtk_ctree_node_get_row_data (ctree, cnode);
  if (row_data && row_data->proxy == data[0])
    data[1] = row_data;
}

GlePTreeRowData*
gle_ptree_row_data_from_proxy (GlePTree *ptree,
			       GleProxy *proxy)
{
  gpointer data[2];

  g_return_val_if_fail (GLE_IS_PTREE (ptree), NULL);
  g_return_val_if_fail (GLE_IS_PROXY (proxy), NULL);
  
  data[0] = proxy;
  data[1] = NULL;
  gtk_ctree_post_recursive (GTK_CTREE (ptree), NULL, gle_ptree_foreach_find, data);

  return data[1];
}

void
gle_ptree_insert_proxy (GlePTree *ptree,
			GleProxy *proxy)
{
  GleProxy *parent;
  GlePTreeRowData *row_data;
  GlePTreeAppendData data;

  g_return_if_fail (GLE_IS_PTREE (ptree));
  g_return_if_fail (GLE_IS_PROXY (proxy));

  gtk_clist_freeze (GTK_CLIST (ptree));

  row_data = gle_ptree_row_data_from_proxy (ptree, proxy);
  if (row_data)
    gtk_ctree_remove_node (GTK_CTREE (ptree), row_data->ct_node);

  parent = proxy->parent;
  if (parent)
    row_data = gle_ptree_row_data_from_proxy (ptree, parent);
  else
    row_data = NULL;
  if (!parent || !row_data)
    {
      g_warning ("PTree does not contain parent of `%s' proxy \"%s\"",
		 gtk_type_name (GLE_PROXY_OTYPE (proxy)),
		 proxy->pname);
      return;
    }

  data.ptree = ptree;
  data.depth = 0;
  data.parent = row_data->ct_node;
  if (ptree->proxy)
    {
      data.sibling = NULL;
      gle_ptree_append_proxy (proxy, &data);
    }

  gtk_clist_thaw (GTK_CLIST (ptree));
}

gboolean
gle_ptree_mark_proxy (GlePTree *ptree,
		      GleProxy *proxy)
{
  GlePTreeRowData *row_data;
  
  g_return_val_if_fail (ptree != NULL, FALSE);
  g_return_val_if_fail (GLE_IS_PTREE (ptree), FALSE);
  
  if ((ptree->shown_row_data == NULL && proxy == NULL) ||
      (ptree->shown_row_data && proxy == ptree->shown_row_data->proxy))
    return proxy != NULL;
  
  if (ptree->shown_row_data)
    {
      row_data = ptree->shown_row_data;
      ptree->shown_row_data = NULL;
      gle_ptree_row_data_update (row_data);
    }
  
  if (proxy)
    {
      row_data = gle_ptree_row_data_from_proxy (ptree, proxy);
      
      if (row_data)
	{
	  GtkCTreeNode *cnode;
	  gint row;
	  
	  ptree->shown_row_data = row_data;
	  gle_ptree_row_data_update (row_data);
	  
	  cnode = GTK_CTREE_ROW (row_data->ct_node)->parent;
	  while (cnode)
	    {
	      gtk_ctree_expand (GTK_CTREE (ptree), cnode);
	      cnode = GTK_CTREE_ROW (cnode)->parent;
	    }
	  
	  row = g_list_position (GTK_CLIST (ptree)->row_list, (GList*) row_data->ct_node);
	  if (gtk_clist_row_is_visible (GTK_CLIST (ptree), row) != GTK_VISIBILITY_FULL)
	    gtk_ctree_node_moveto (GTK_CTREE (ptree),
				   row_data->ct_node,
				   -1, 0.5, 0);
	}
      else
	proxy = NULL;
    }
  
  return proxy != NULL;
}

GleProxy*
gle_ptree_proxy_from_node (GlePTree	*ptree,
			   GtkCTreeNode *ct_node)
{
  GlePTreeRowData *row_data;
  
  g_return_val_if_fail (GLE_IS_PTREE (ptree), NULL);
  g_return_val_if_fail (ct_node != NULL, NULL);
  
  row_data = gtk_ctree_node_get_row_data (GTK_CTREE (ptree), ct_node);
  if (row_data && row_data->ptree == ptree)
    return row_data->proxy;
  
  return NULL;
}

GSList*
gle_ptree_selected_proxies (GlePTree *ptree)
{
  GList *list;
  GSList *selection;
  
  g_return_val_if_fail (GLE_IS_PTREE (ptree), NULL);
  
  selection = NULL;
  for (list = GTK_CLIST (ptree)->selection; list; list = list->next)
    {
      GleProxy *proxy = gle_ptree_proxy_from_node (ptree, list->data);

      selection = g_slist_prepend (selection, proxy);
    }
  
  return selection;
}

static void
gle_ptree_append_proxy (GleProxy	   *proxy,
			GlePTreeAppendData *data)
{
  GlePTree *ptree = data->ptree;
  gchar *text[GLE_PTREE_N_COLS] = { 0, };
  GlePTreeRowData *row_data = g_new0 (GlePTreeRowData, 1);
  GtkCTree *ctree = GTK_CTREE (ptree);
  guint i;
  
  data->depth += 1;
  for (i = 0; i < GLE_PTREE_N_COLS; i++)
    text[i] = "x";
  text[GLE_PTREE_COL_OTYPE] = gtk_type_name (GLE_PROXY_OTYPE (proxy));
  row_data->ptree = ptree;
  row_data->proxy = proxy;
  row_data->is_connected = FALSE;
  row_data->ct_node = gtk_ctree_insert_node (ctree,
					     data->parent,
					     NULL /* data->sibling */,
					     text, 0,
					     NULL, NULL, NULL, NULL,
					     !proxy->children,
					     data->depth <= GLE_RCVAL_PTREE_EXPAND_DEPTH);

  if (!row_data->ct_node)	/* GRRRRRRRRRRRRRRRRRR */
    {
      gchar *tmptext = NULL;
      guint8 spacing = 0;
      GdkPixmap *pixmap_closed = NULL, *pixmap_opened = NULL;
      GdkBitmap *mask_closed = NULL, *mask_opened = NULL;
      gboolean is_leaf = FALSE;
      gboolean expanded = FALSE;
      
      gtk_ctree_get_node_info (ctree, data->parent,
			       &tmptext, &spacing,
			       &pixmap_closed, &mask_closed,
			       &pixmap_opened, &mask_opened,
			       &is_leaf,
			       &expanded);
      if (is_leaf)
	gtk_ctree_set_node_info (ctree, data->parent,
				 tmptext, spacing,
				 pixmap_closed, mask_closed,
				 pixmap_opened, mask_opened,
				 FALSE,
				 TRUE);

      row_data->ct_node = gtk_ctree_insert_node (ctree,
						 data->parent,
						 NULL /* data->sibling */,
						 text, 0,
						 NULL, NULL, NULL, NULL,
						 !proxy->children,
						 data->depth <= GLE_RCVAL_PTREE_EXPAND_DEPTH);
    }

  gtk_ctree_node_set_row_data_full (ctree,
				    row_data->ct_node,
				    row_data,
				    (GtkDestroyNotify) gle_ptree_row_data_free);
  GLE_PROXY_NOTIFY_DESTROY (proxy, gle_ptree_row_data_destroyed, row_data);
  GLE_PROXY_NOTIFY_ADD_CHILD (proxy, gle_ptree_row_data_add_child, row_data);
  GLE_PROXY_NOTIFY_ATTACH (proxy, gle_ptree_row_data_update, row_data);
  /* GLE_PROXY_NOTIFY_DETACH (proxy, gle_ptree_row_data_update, row_data); */
  GLE_PROXY_NOTIFY_DETACH (proxy, gle_ptree_row_data_destroyed, row_data);
  if (proxy->children)
    {
      GtkCTreeNode *old_parent = data->parent;
      GList *list;
      
      data->parent = row_data->ct_node;
      data->sibling = NULL;
      for (list = proxy->children; list; list = list->next)
	gle_ptree_append_proxy (list->data, data);
      data->parent = old_parent;
    }
  data->sibling = row_data->ct_node;
  data->depth -= 1;
  gle_ptree_row_data_update (row_data);
}

static void
gle_ptree_row_data_update (GlePTreeRowData *row_data)
{
  if (row_data)
    {
      GlePTree *ptree = row_data->ptree;
      GtkCTree *ctree = GTK_CTREE (ptree);
      GleProxy *proxy = row_data->proxy;
      gchar *text[GLE_PTREE_N_COLS] = { NULL };
      guint i;
      gchar *s;

      if (!GLE_PROXY_HAS_OBJECT (proxy))
	gtk_ctree_node_set_background (ctree, row_data->ct_node,
				       &GTK_WIDGET (ptree)->style->bg[GTK_STATE_INSENSITIVE]);
      gtk_ctree_node_set_foreground (ctree, row_data->ct_node,
				     (ptree->shown_row_data == row_data ?
				      &GTK_WIDGET (ptree)->style->base[GTK_STATE_PRELIGHT] :
				      NULL));

      text[GLE_PTREE_COL_OTYPE] = g_strconcat (proxy->instance_flag ? proxy->instance_flag : "",
					       gtk_type_name (GLE_PROXY_OTYPE (proxy)),
					       NULL);
      s = proxy->pname;
      text[GLE_PTREE_COL_PNAME] = g_strconcat ("\"",
					       s ? s : "",
					       "\"",
					       NULL);
      text[GLE_PTREE_COL_STATE] = g_strdup (proxy->state_hint);
      
      for (i = GLE_PTREE_COL_OTYPE; i < GLE_PTREE_N_COLS; i++)
	{
	  if (GLE_RCVAL_PTREE_RESET_COLUMNS)
	    {
	      gint new_width;
	      
	      new_width = text[i] ? gdk_string_width (gtk_style_get_font (GTK_WIDGET (ptree)->style), text[i]) : 0;
	      if (GTK_CLIST (ptree)->column[i].width < new_width)
		gtk_clist_set_column_width (GTK_CLIST (ptree), i, new_width);
	    }
	  
	  gtk_ctree_node_set_text (GTK_CTREE (ptree), row_data->ct_node, i, text[i]);
	  
	  g_free (text[i]);
	}
    }
}

static gboolean
idle_thaw_handler (gpointer data)
{
  GlePTree *ptree = data;

  GDK_THREADS_ENTER ();
  ptree->thaw_handler = 0;
  gtk_clist_thaw (GTK_CLIST (ptree));
  GDK_THREADS_LEAVE ();

  return FALSE;
}

static void
gle_ptree_row_data_add_child (GlePTreeRowData *row_data)
{
  GlePTree *ptree = row_data->ptree;
  GleProxy *child;

  g_return_if_fail (row_data->proxy != NULL);
  g_return_if_fail (row_data->proxy->children != NULL);

  child = row_data->proxy->children->data;

  if (!ptree->thaw_handler)
    {
      gtk_clist_freeze (GTK_CLIST (ptree));
      ptree->thaw_handler = g_idle_add (idle_thaw_handler, ptree);
    }

  gle_ptree_insert_proxy (ptree, child);
}

static void
gle_ptree_row_data_destroyed (GlePTreeRowData *row_data)
{
  GlePTree *ptree = row_data->ptree;
  GtkCTree *ctree = GTK_CTREE (ptree);
  
  gtk_clist_freeze (GTK_CLIST (ptree));
  gtk_ctree_unselect (ctree, row_data->ct_node);
  gtk_ctree_node_set_text (ctree, row_data->ct_node, GLE_PTREE_COL_STATE, "Gone");
  gtk_ctree_node_set_foreground (ctree, row_data->ct_node, &GTK_WIDGET (ptree)->style->fg[GTK_STATE_INSENSITIVE]);
  gtk_ctree_node_set_background (ctree, row_data->ct_node, &GTK_WIDGET (ptree)->style->bg[GTK_STATE_INSENSITIVE]);
  gtk_ctree_node_set_selectable (ctree, row_data->ct_node, FALSE);
  gtk_clist_thaw (GTK_CLIST (ptree));
  
  gtk_ctree_node_set_row_data (GTK_CTREE (ptree), row_data->ct_node, NULL);
  /* gle_ptree_row_data_free (row_data); */
}

static void
gle_ptree_row_data_free (GlePTreeRowData *row_data)
{
  if (row_data)
    {
      GLE_PROXY_REMOVE_NOTIFIES (row_data->proxy, gle_ptree_row_data_destroyed, row_data);
      GLE_PROXY_REMOVE_NOTIFIES (row_data->proxy, gle_ptree_row_data_add_child, row_data);
      GLE_PROXY_REMOVE_NOTIFIES (row_data->proxy, gle_ptree_row_data_update, row_data);
      if (row_data->ptree->shown_row_data == row_data)
	row_data->ptree->shown_row_data = NULL;
      row_data->ptree = NULL;
      g_free (row_data);
    }
}

static gint
gle_ptree_button_press_event (GtkWidget		*widget,
			      GdkEventButton	*event)
{
  GlePTree *ptree;
  gboolean handled;
  
  ptree = GLE_PTREE (widget);
  
  handled = FALSE;
  if (event->type == GDK_BUTTON_PRESS && event->button)
    {
      gint row;
      
      if (gle_clist_selection_info (GTK_CLIST (ptree),
				    event->window,
				    event->x, event->y,
				    &row, NULL))
	{
	  GlePTreeRowData *row_data;
	  
	  row_data = gtk_clist_get_row_data (GTK_CLIST (ptree), row);
	  if (row_data)
	    {
	      gint return_val = FALSE;
	      
	      gtk_signal_emit (GTK_OBJECT (ptree),
			       ptree_signals[SIGNAL_BUTTON_CLICK],
			       row_data->proxy,
			       (guint) event->x_root,
			       (guint) event->y_root,
			       (guint) event->button,
			       (guint) event->time,
			       &return_val);
	      handled |= return_val != FALSE;
	    }
	}
    }
  if (!handled && GTK_WIDGET_CLASS (parent_class)->button_press_event)
    handled = GTK_WIDGET_CLASS (parent_class)->button_press_event (widget, event);
  
  gle_util_set_flash_widget (NULL);
  
  return handled;
}

#if 0
static GleGObject*
gle_ptree_dnd_start (GtkWidget *widget,
		     gint       root_x,
		     gint       root_y)
{
  GlePTree *ptree;
  GtkCList *clist;
  gint x, y, row;
  
  g_return_val_if_fail (widget != NULL, NULL);
  g_return_val_if_fail (GLE_IS_PTREE (widget), NULL);
  
  ptree = GLE_PTREE (widget);
  clist = GTK_CLIST (ptree);
  
  if (GTK_WIDGET_REALIZED (ptree) &&
      gdk_window_get_origin (clist->clist_window, &x, &y) &&
      gle_clist_selection_info (GTK_CLIST (ptree),
				clist->clist_window,
				root_x - x, root_y - y,
				&row, NULL))
    {
      GlePTreeRowData *row_data;
      
      row_data = gtk_clist_get_row_data (GTK_CLIST (ptree), row);
      if (row_data)
	return row_data->proxy;
    }
  
  return NULL;
}

static GleGObject*
gle_ptree_dnd_check (GtkWidget *widget,
		     GtkType	drag_type,
		     gint       root_x,
		     gint       root_y)
{
  GlePTree *ptree;
  GtkCList *clist;
  gint x, y, row;

  g_return_val_if_fail (widget != NULL, NULL);
  g_return_val_if_fail (GLE_IS_PTREE (widget), NULL);
  
  ptree = GLE_PTREE (widget);
  clist = GTK_CLIST (ptree);

  if (GTK_WIDGET_REALIZED (ptree) &&
      gdk_window_get_origin (clist->clist_window, &x, &y) &&
      gle_clist_selection_info (GTK_CLIST (ptree),
				clist->clist_window,
				root_x - x, root_y - y,
				&row, NULL))
    {
      GlePTreeRowData *row_data;
      
      row_data = gtk_clist_get_row_data (clist, row);
      if (row_data)
	{
	  if (GLE_PROXY_HAS_OBJECT (row_data->proxy))
	    {
	      GtkWidget *container;

	      if (GLE_IS_GCONTAINER (row_data->gwidget))
		container = GLE_GWIDGET_WIDGET (row_data->gwidget);
	      else
		container = GLE_GWIDGET_WIDGET (row_data->gwidget->parent);

	      while (container &&
		     !gtk_type_is_a (drag_type, gtk_container_child_type (GTK_CONTAINER (container))))
		container = container->parent;
	      
	      if (container)
		return gle_object_get_gobject (GTK_OBJECT (container));
	    }
	}
    }
  
  return NULL;
}
#endif
