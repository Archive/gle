/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GLE_PROXY_PARAM_H__
#define __GLE_PROXY_PARAM_H__


#include        <gle/gleutils.h>
#include        <gle/gleforeign.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */



/* --- structures --- */
typedef struct _GleProxyParam GleProxyParam;
struct _GleProxyParam
{
  GParamSpec *pspec;
  GValue      value;
};


/* --- prototypes --- */
GleProxyParam*	gle_proxy_param_check_create	(GleProxy	*proxy,
						 GParamSpec	*pspec);
void		gle_proxy_param_set		(GleProxyParam	*param,
						 const GValue	*value);
gboolean	gle_proxy_param_get		(GleProxyParam	*param,
						 GValue		*value);
void		gle_proxy_param_update		(GleProxyParam	*param,
						 GleForeignGetParam get_func,
						 GObject	*object);
void		gle_proxy_param_apply		(GleProxyParam	*param,
						 GleForeignSetParam set_func,
						 GObject	*object);
void		gle_proxy_param_set_default	(GleProxyParam	*param);
void		gle_proxy_param_free		(GleProxyParam	*param);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif  /* __GLE_PROXY_PARAM_H__ */
