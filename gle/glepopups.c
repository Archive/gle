/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#include	<gle/gleconfig.h>

#include	"glepopups.h"
#include	"gleshell.h"
#include	"glegwidget.h"
#include	"gleprivate.h"
#include	"gleeditor.h"
#include	"glewpalette.h"
#include	"gleted.h"
#include	"glercpref.h"
#include	<stdio.h>
#include	<stdarg.h>



/* --- functions --- */
GtkWidget*
gle_popup_gle_shell (void)
{
  GleShell *shell;
  
  shell = gle_shell_get ();
  
  if (!GTK_WIDGET_VISIBLE (shell))
    gtk_widget_show (GTK_WIDGET (shell));
  
  gdk_window_raise (GTK_WIDGET (shell)->window);
  
  return GTK_WIDGET (shell);
}

GtkWidget*
gle_popup_gle_shell_with_customer (GtkWidget      *customer)
{
  GleShell *shell;
  
  if (customer)
    {
      customer = gtk_widget_get_toplevel (customer);
      if (customer)
	g_return_val_if_fail (GTK_IS_WIDGET (customer), NULL);
    }
  
  shell = gle_shell_get ();
  if (customer && !GLE_HAS_TAG (customer))
    gle_shell_add_toplevel (shell, customer);
  
  return gle_popup_gle_shell ();
}

GtkWidget*
gle_popup_gle_shell_selector (void)
{
  GleShell *shell;
  
  shell = gle_shell_get ();
  gle_shell_operation (GLE_SHELL (shell), GLE_SHELL_OP_SELECTOR);
  
  return GTK_WIDGET (shell);
}

GtkWidget*
gle_popup_editor (GtkObject *object)
{
  GleGObject *gobject;
  GtkWidget *editor;
  
  g_return_val_if_fail (object != NULL, NULL);
  g_return_val_if_fail (GTK_IS_OBJECT (object), NULL);
  g_return_val_if_fail (!GLE_HAS_TAG (object), NULL);
  
  if (GTK_IS_WIDGET (object))
    gobject = GLE_GOBJECT (gle_widget_force_gwidget (GTK_WIDGET (object)));
  else
    gobject = gle_object_force_gobject (object);
  
  editor = gle_gobject_get_editor (gobject);
  if (!editor)
    editor = gle_editor_new (gobject);
  gtk_widget_show (editor);
  gdk_window_raise (editor->window);
  
  return editor;
}

GtkWidget*
gle_popup_ted (GtkWidget *widget)
{
  GleGWidget *gwidget;
  GtkWidget *ted;
  
  g_return_val_if_fail (widget != NULL, NULL);
  g_return_val_if_fail (GTK_IS_WIDGET (widget), NULL);
  g_return_val_if_fail (!GLE_HAS_TAG (widget), NULL);

  gwidget = gle_widget_force_gwidget (widget);

  ted = gle_gwidget_get_ted (gwidget);
  if (!ted)
      ted = gle_ted_new (gwidget);
  gtk_widget_show (ted);
  gdk_window_raise (ted->window);
  
  return ted;
}

static void
gle_shell_popup_wpalette (GleShell        *shell)
{
  g_return_if_fail (shell != NULL);
  g_return_if_fail (GLE_IS_SHELL (shell));

  if (!shell->wpalette)
    {
      GtkWidget *window;

      window =
	gtk_widget_new (GTK_TYPE_WINDOW,
			"name", "GLE-Widget-Palette",
			"title", "GLE-Widget-Palette",
			"type", GTK_WINDOW_TOPLEVEL,
			"allow_shrink", TRUE,
			"allow_grow", TRUE,
			NULL);
      GLE_TAG (window);
      shell->wpalette = gle_wpalette_new ();
      gtk_widget_show (shell->wpalette);
      gtk_container_add (GTK_CONTAINER (window), shell->wpalette);
      gtk_signal_connect (GTK_OBJECT (shell->wpalette),
			  "destroy",
			  GTK_SIGNAL_FUNC (gtk_widget_destroyed),
			  &shell->wpalette);
      gtk_widget_show (gtk_widget_get_toplevel (shell->wpalette));
    }
  else
    {
      gtk_widget_show (gtk_widget_get_toplevel (shell->wpalette));
      gdk_window_raise (gtk_widget_get_toplevel (shell->wpalette)->window);
    }
}

GtkWidget*
gle_create_rc_pref (GleRcHandle    *rc_handle)
{
  GtkWidget *window;
  GtkWidget *main_vbox;
  GtkWidget *any;
  GtkWidget *button;
  GtkWidget *rc_pref;
  GtkWidget *hbox;

  g_return_val_if_fail (rc_handle != NULL, NULL);

  window =
    gtk_widget_new (GTK_TYPE_WINDOW,
		    "name", "GLE-Preferences",
		    "title", "GLE-Preferences",
		    "allow_shrink", FALSE,
		    "allow_grow", TRUE,
		    NULL);
  main_vbox =
    gtk_widget_new (GTK_TYPE_VBOX,
		    "homogeneous", FALSE,
		    "spacing", 0,
		    "border_width", 0,
		    "parent", window,
		    "visible", TRUE,
		    NULL);
  rc_pref =
    gtk_widget_new (GLE_TYPE_RC_PREF,
		    "visible", TRUE,
		    "rc_handle", rc_handle,
		    "border_width", 5,
		    "parent", main_vbox,
		    NULL);
  hbox =
    gtk_widget_new (GTK_TYPE_HBOX,
		    "homogeneous", TRUE,
		    "spacing", 5,
		    "border_width", 5,
		    "visible", TRUE,
		    NULL);
  gtk_box_pack_start (GTK_BOX (main_vbox), hbox, FALSE, TRUE, 0);
  button =
    gtk_widget_new (GTK_TYPE_BUTTON,
		    "visible", TRUE,
		    "label", "Reset", /* "Defaults", */
		    "parent", hbox,
		    "object_signal::clicked", gle_rc_pref_reset_args, rc_pref,
		    NULL);
  button =
    gtk_widget_new (GTK_TYPE_BUTTON,
		    "visible", TRUE,
		    "label", "Refresh",
		    "parent", hbox,
		    "object_signal::clicked", gle_rc_pref_refresh_args, rc_pref,
		    NULL);
  any =
    gtk_widget_new (GTK_TYPE_HSEPARATOR,
		    "GtkWidget::visible", TRUE,
		    NULL);
  gtk_box_pack_start (GTK_BOX (main_vbox), any, FALSE, TRUE, 0);
  hbox =
    gtk_widget_new (GTK_TYPE_HBOX,
		    "homogeneous", TRUE,
		    "spacing", 5,
		    "border_width", 5,
		    "visible", TRUE,
		    NULL);
  gtk_box_pack_start (GTK_BOX (main_vbox), hbox, FALSE, TRUE, 0);
  button =
    gtk_widget_new (GTK_TYPE_BUTTON,
		    "visible", TRUE,
		    "label", "Save",
		    "parent", hbox,
		    "can_default", TRUE,
		    "has_default", TRUE,
		    "object_signal::clicked", gle_rc_pref_apply_args, rc_pref,
		    "object_signal::clicked", gtk_widget_destroy, window,
		    NULL);
  button =
    gtk_widget_new (GTK_TYPE_BUTTON,
		    "visible", TRUE,
		    "label", "Close",
		    "parent", hbox,
		    "can_default", TRUE,
		    "object_signal::clicked", gtk_widget_destroy, window,
		    NULL);
  
  return window;
}
