/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001-2002 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include "gleproxyset.h"


#include "gleproxy.h"
#include "gleshell.h"

#include <string.h>


/* --- prototypes --- */
static void	gle_proxy_set_init		(GleProxySet		*pset);
static void	gle_proxy_set_class_init	(GleProxySetClass	*class);
static void	gle_proxy_set_finalize		(GObject		*object);


/* --- variables --- */
static GList   *proxy_set_list = NULL;
static gpointer parent_class = NULL;


/* --- functions --- */
GType
gle_proxy_set_get_type (void)
{
  static GType proxy_set_type = 0;

  if (!proxy_set_type)
    {
      static const GTypeInfo proxy_set_info =
      {
	sizeof (GleProxySetClass),
	NULL,           /* base_init */
	NULL,           /* base_finalize */
	(GClassInitFunc) gle_proxy_set_class_init,
	NULL,           /* class_finalize */
	NULL,           /* class_data */
	sizeof (GleProxySet),
	0,             /* n_preallocs */
	(GInstanceInitFunc) gle_proxy_set_init,
      };

      proxy_set_type = g_type_register_static (G_TYPE_OBJECT, "GProxySet", &proxy_set_info, 0);
    }

  return proxy_set_type;
}

static void
gle_proxy_set_class_init (GleProxySetClass *class)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (class);

  parent_class = g_type_class_peek_parent (class);

  gobject_class->finalize = gle_proxy_set_finalize;
}

static void
gle_proxy_set_init (GleProxySet *pset)
{
  pset->name = g_strdup ("Unnamed");

  proxy_set_list = g_list_prepend (proxy_set_list, pset);
  gle_shell_addremove_proxy_set (pset, TRUE);
}

static void
gle_proxy_set_finalize (GObject *object)
{
  GleProxySet *pset = GLE_PROXY_SET (object);

  gle_shell_addremove_proxy_set (pset, FALSE);
  g_free (pset->name);
					       
  G_OBJECT_CLASS (parent_class)->finalize (object);
}

GleProxySet*
gle_proxy_set_new (GleProxySetSource source_id,
		   const gchar      *name,
		   GleForeign       *foreign)
{
  GleProxySet *pset;
  gchar *s;
  guint n = 2;

  g_return_val_if_fail (foreign != NULL, NULL);
  if (!name)
    name = "Unnamed";

  s = g_strdup (name);
  while (gle_proxy_set_find (s))
    {
      g_free (s);
      s = g_strdup_printf ("%s-%u", name, n);
    }
  pset = g_object_new (GLE_TYPE_PROXY_SET, NULL);
  pset->foreign = foreign;
  g_free (pset->name);
  pset->name = s;

  return pset;
}

GleProxySet*
gle_proxy_set_find (const gchar *name)
{
  GList *list;

  g_return_val_if_fail (name != NULL, NULL);

  for (list = proxy_set_list; list; list = list->next)
    {
      GleProxySet *pset = list->data;

      if (strcmp (pset->name, name) == 0)
	return pset;
    }
  return NULL;
}

GList*
gle_proxy_sets_list (void)
{
  return g_list_copy (proxy_set_list);
}

void
gle_proxy_set_prepend_proxy (GleProxySet *pset,
			     GleProxy    *proxy)
{
  g_return_if_fail (GLE_IS_PROXY_SET (pset));
  g_return_if_fail (GLE_IS_PROXY (proxy));
  g_return_if_fail (proxy->pset == NULL);

  gle_proxy_ref (proxy);
  if (pset->proxies)
    {
      pset->proxies->prev = proxy;
      proxy->next = pset->proxies;
    }
  else
    {
      pset->last_proxy = proxy;
      proxy->next = NULL;
    }
  pset->proxies = proxy;
  proxy->prev = NULL;
  proxy->pset = pset;
}

void
gle_proxy_set_append_proxy (GleProxySet *pset,
			    GleProxy    *proxy)
{
  g_return_if_fail (GLE_IS_PROXY_SET (pset));
  g_return_if_fail (GLE_IS_PROXY (proxy));
  g_return_if_fail (proxy->pset == NULL);

  gle_proxy_ref (proxy);
  if (pset->last_proxy)
    {
      pset->last_proxy->next = proxy;
      proxy->prev = pset->last_proxy;
    }
  else
    {
      pset->proxies = proxy;
      proxy->prev = NULL;
    }
  pset->last_proxy = proxy;
  proxy->next = NULL;
  proxy->pset = pset;
}

void
gle_proxy_set_remove_proxy (GleProxySet *pset,
			    GleProxy    *proxy)
{
  g_return_if_fail (GLE_IS_PROXY_SET (pset));
  g_return_if_fail (GLE_IS_PROXY (proxy));
  g_return_if_fail (proxy->pset == pset);

  if (proxy->prev)
    proxy->prev->next = proxy->next;
  else
    pset->proxies = proxy->next;
  if (proxy->next)
    proxy->next->prev = proxy->prev;
  else
    pset->last_proxy = proxy->prev;
  proxy->next = NULL;
  proxy->prev = NULL;
  proxy->pset = NULL;
  gle_proxy_unref (proxy);
}

GList*
gle_proxy_set_list_toplevels (GleProxySet *pset)
{
  GleProxy *proxy;
  GList *list = NULL;

  g_return_val_if_fail (GLE_IS_PROXY_SET (pset), NULL);

  for (proxy = pset->proxies; proxy; proxy = proxy->next)
    if (!proxy->parent)
      list = g_list_prepend (list, proxy);
  return list;
}

void
gle_proxy_set_add_notify (GleProxySet      *pset,
			  GleProxySetNotify notify,
			  gpointer          data)
{
}

void
gle_proxy_set_remove_notify (GleProxySet       *pset,
			     GleProxySetNotify notify,
			     gpointer          data)
{
}
