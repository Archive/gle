/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef __GLE_WPALETTE_H__
#define __GLE_WPALETTE_H__


#include	<gle/glegwidget.h>
#include	<gle/gleselector.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* --- type macros --- */
#define GLE_TYPE_WPALETTE		   (gle_wpalette_get_type ())
#define GLE_WPALETTE(obj)		   (GTK_CHECK_CAST ((obj), GLE_TYPE_WPALETTE, GleWPalette))
#define GLE_WPALETTE_CLASS(klass)	   (GTK_CHECK_CLASS_CAST ((klass), GLE_TYPE_WPALETTE, GleWPaletteClass))
#define GLE_IS_WPALETTE(obj)		   (GTK_CHECK_TYPE ((obj), GLE_TYPE_WPALETTE))
#define GLE_IS_WPALETTE_CLASS(klass)	   (GTK_CHECK_CLASS_TYPE ((klass), GLE_TYPE_WPALETTE))


/* --- typedefs --- */
typedef	struct	_GleWPalette		GleWPalette;
typedef	struct	_GleWPaletteClass	GleWPaletteClass;


/* --- structures --- */
struct	_GleWPalette
{
  GtkWindow	vbox;
  
  GtkWidget	*table;
  
  GtkType	 drop_type;
  guint16	 width;
  guint16	 height;
  GtkWidget	*type_entry;
  
  GtkTooltips	*tooltips;
};

struct	_GleWPaletteClass
{
  GtkWindowClass	parent_class;
};


/* --- prototypes --- */
GtkType		gle_wpalette_get_type		(void);
GtkWidget*	gle_wpalette_new		(void);




#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif	/* __GLE_WPALETTE_H__ */
