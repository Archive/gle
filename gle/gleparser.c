/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#include	<gle/gleconfig.h>

#include	"gleparser.h"
#include	"glehandler.h"
#include	<unistd.h>
#include	<fcntl.h>
#include	<sys/stat.h>
#include	<errno.h>


/* --- defines --- */
#define parser_unexp_token(t,e)		g_scanner_unexp_token ((t), (e), "identifier", "keyword", NULL, NULL, 1)
#define	GLE_SCANNER_DATA( scanner )	((GleScannerData*) ((GScanner*) scanner)->user_data)

typedef	guint (*GleParserFunc)		(GScanner	*scanner,
					 gpointer	 data);
typedef struct _GleScannerData	GleScannerData;


/* --- structures --- */
typedef struct _GName GName;
struct _GName
{
  GName *next;
  gchar *gname;
  gpointer data;
};
struct _GleScannerData
{
  GSList *gwidgets;
  GName *gnames;
};


/* --- prototypes --- */
static guint	parse_toplevel			(GScanner	*scanner);
static guint	parse_widget			(GScanner	*scanner,
						 GleGWidget	*gparent);
static guint	parse_arg			(GScanner	*scanner,
						 GleGWidget	*gwidget);
static guint	parse_child_arg			(GScanner	*scanner,
						 GleGWidget	*gwidget);
static guint	parse_connect			(GScanner	*scanner,
						 GleGObject	*gobject);
static guint	parse_connect_object		(GScanner	*scanner,
						 GleGObject	*gobject);
static guint	parse_connect_after		(GScanner	*scanner,
						 GleGObject	*gobject);
static guint	parse_connect_object_after	(GScanner	*scanner,
						 GleGObject	*gobject);
static guint	parse_user_name			(GScanner	*scanner,
						 GleGWidget	*gwidget);


/* --- variables ---- */
static GScannerConfig scanner_config =
{
  (
   " \t\n"
   )			/* cset_skip_characters */,
  (
   ":"
   G_CSET_a_2_z
   G_CSET_A_2_Z
   )			/* cset_identifier_first */,
  (
   ":-0123456789"
   G_CSET_a_2_z
   G_CSET_A_2_Z
   G_CSET_LATINS
   G_CSET_LATINC
   )			/* cset_identifier_nth */,
  ( ";\n" )		/* cpair_comment_single */,

  TRUE			/* case_sensitive */,

  TRUE			/* skip_comment_multi */,
  TRUE			/* skip_comment_single */,
  TRUE			/* scan_comment_multi */,
  TRUE			/* scan_identifier */,
  FALSE			/* scan_identifier_1char */,
  FALSE			/* scan_identifier_NULL */,
  TRUE			/* scan_symbols */,
  TRUE			/* scan_binary */,
  TRUE			/* scan_octal */,
  TRUE			/* scan_float */,
  TRUE			/* scan_hex */,
  FALSE			/* scan_hex_dollar */,
  TRUE			/* scan_string_sq */,
  TRUE			/* scan_string_dq */,
  TRUE			/* numbers_2_int */,
  FALSE			/* int_2_float */,
  FALSE			/* identifier_2_string */,
  TRUE			/* char_2_token */,
  FALSE			/* symbol_2_token */,
  FALSE			/* scope_0_fallback */,
};


/* --- functions --- */
GleParserData*
gle_parser_data_from_file (const gchar *file_name)
{
  gint fd;
  GScanner *scanner;
  GleScannerData *sdata;
  GleParserData *pdata;
  guint expected_token;

  fd = open (file_name, O_RDONLY);
  if (fd < 0)
    {
      /* g_warning ("opening \"%s\" failed: %s", file_name, g_strerror (errno)); */
      return NULL;
    }

  /* FIXME: force initialize all gtk types */
  
  scanner = g_scanner_new (&scanner_config);
  g_scanner_input_file (scanner, fd);
  scanner->input_name = file_name;
  scanner->user_data = g_new0 (GleScannerData, 1);
  sdata = GLE_SCANNER_DATA (scanner);
  sdata->gwidgets = NULL;
  sdata->gnames = NULL;

  g_scanner_add_symbol (scanner, "arg", parse_arg);
  g_scanner_add_symbol (scanner, "child-arg", parse_child_arg);
  g_scanner_add_symbol (scanner, "user-gname", parse_user_name);
  g_scanner_add_symbol (scanner, "connect", parse_connect);
  g_scanner_add_symbol (scanner, "connect-object", parse_connect_object);
  g_scanner_add_symbol (scanner, "connect-after", parse_connect_after);
  g_scanner_add_symbol (scanner, "connect-object-after", parse_connect_object_after);

  do
    {
      expected_token = parse_toplevel (scanner);
      if (expected_token != G_TOKEN_NONE)
	{
	  parser_unexp_token (scanner, expected_token);
	  expected_token = G_TOKEN_EOF;
	}
      else
	expected_token = g_scanner_peek_next_token (scanner);
    }
  while (expected_token != G_TOKEN_EOF);

  g_scanner_destroy (scanner);
  close (fd);

  pdata = g_new0 (GleParserData, 1);
  pdata->gtoplevels = sdata->gwidgets;
  pdata->gnames = sdata->gnames;

  g_free (sdata);

  return pdata;
}

GleGToplevel*
gle_parser_data_get_gtoplevel (GleParserData *pdata,
			       const gchar   *gname)
     
{
  GName *node;

  g_return_val_if_fail (pdata != NULL, NULL);
  g_return_val_if_fail (gname != NULL, NULL);

  for (node = pdata->gnames; node; node = node->next)
    {
      if (strcmp (node->gname, gname))
	return node->data;
    }

  return NULL;
}

void
gle_parser_data_destroy (GleParserData *pdata)
{
  if (pdata)
    {
      GSList *slist;
      GName *gname;

      for (slist = pdata->gtoplevels; slist; slist = slist->next)
	{
	  GleGObject *gobject;
	  
	  gobject = slist->data;
	  gle_gobject_unref (gobject);
	}
      g_slist_free (pdata->gtoplevels);
      
      gname = pdata->gnames;
      while (gname)
	{
	  GName *tmp = gname;

	  gname = gname->next;

	  g_free (tmp->gname);
	  g_free (tmp);
	}

      g_free (pdata);
    }
}

static guint
parse_toplevel (GScanner *scanner)
{
  GtkType type;
  
  g_scanner_get_next_token (scanner);
  if (scanner->token != '(')
    return '(';

  g_scanner_peek_next_token (scanner);
  if (scanner->next_token != G_TOKEN_IDENTIFIER)
    {
      g_scanner_get_next_token (scanner);

      return G_TOKEN_IDENTIFIER;
    }

  type = gle_type_from_name (scanner->next_value.v_identifier);
  if (!gtk_type_is_a (type, GTK_TYPE_WINDOW))
    {
      g_scanner_get_next_token (scanner);

      return G_TOKEN_IDENTIFIER;
    }

  return parse_widget (scanner, NULL);
}

static guint
parse_user_name (GScanner	*scanner,
		 GleGWidget	*gwidget)
{
  GleScannerData *sdata = GLE_SCANNER_DATA (scanner);
  GName *gname;
  gchar *name;
  guint i = 0;
  extern int sprintf (char*, const char*, ...);

  for (gname = sdata->gnames; gname; gname = gname->next)
    if (gname->data == gwidget)
      break;
  
  name = g_new (gchar, strlen (gname->gname) + 64);
  strcpy (name, gname->gname);
  while (gle_gname_lookup (name))
    sprintf (name, "%s-%u", gname->gname, ++i);
  
  gle_gobject_set_gname (GLE_GOBJECT (gwidget), name);

  g_free (name);

  g_scanner_get_next_token (scanner);

  return scanner->token == ')' ? G_TOKEN_NONE : ')';
}

static guint
parse_widget (GScanner   *scanner,
	      GleGWidget *gparent)
{
  GleScannerData *sdata = GLE_SCANNER_DATA (scanner);
  GtkType type;
  GleGWidget *gwidget;
  GName *gname;
  
  g_scanner_get_next_token (scanner);
  if (scanner->token != G_TOKEN_IDENTIFIER)
    return G_TOKEN_IDENTIFIER;

  type = gle_type_from_name (scanner->value.v_identifier);
  if (!gtk_type_is_a (type, GTK_TYPE_WIDGET))
    return G_TOKEN_IDENTIFIER;

  g_scanner_get_next_token (scanner);
  if (scanner->token != G_TOKEN_STRING)
    return G_TOKEN_STRING;

  gwidget = gle_gwidget_new (type);
  gname = g_new (GName, 1);
  gname->next = sdata->gnames;
  sdata->gnames = gname;
  gname->gname = g_strdup (scanner->value.v_string);
  gname->data = gwidget;

  if (gparent)
    gle_gcontainer_add_child (GLE_GCONTAINER (gparent), gwidget);
  else
    sdata->gwidgets = g_slist_prepend (sdata->gwidgets, gwidget);

  g_scanner_get_next_token (scanner);
  while (scanner->token != ')')
    {
      guint expected_token;

      if (scanner->token == '(')
	{
	  g_scanner_peek_next_token (scanner);
	  if (scanner->next_token == G_TOKEN_IDENTIFIER)
	    expected_token = parse_widget (scanner, gwidget);
	  else if (scanner->next_token == G_TOKEN_SYMBOL)
	    {
	      g_scanner_get_next_token (scanner);
	      expected_token = ((GleParserFunc) scanner->value.v_symbol) (scanner, gwidget);
	    }
	  else
	    expected_token = G_TOKEN_SYMBOL;
	}
      else
	expected_token = ')';
      
      if (expected_token != G_TOKEN_NONE)
	return expected_token;
      
      g_scanner_get_next_token (scanner);
    }
  
  return G_TOKEN_NONE;
}

static guint
parse_arg_value (GScanner *scanner,
		 GtkArg   *parser_arg)
{
  guint expected_token;
  GtkArg arg;

  arg.type = GTK_TYPE_INVALID;
  arg.name = parser_arg->name;
  gle_arg_copy_value (parser_arg, &arg);

  expected_token = G_TOKEN_NONE;
  g_scanner_get_next_token (scanner);
  
  switch (GTK_FUNDAMENTAL_TYPE (arg.type))
    {
      gboolean negate;

    case GTK_TYPE_DOUBLE:
      negate = FALSE;
      if (scanner->token == '-')
	{
	  negate = !negate;
	  g_scanner_get_next_token (scanner);
	}
      if (scanner->token == G_TOKEN_FLOAT)
	GTK_VALUE_DOUBLE (arg) = negate ? - scanner->value.v_float : scanner->value.v_float;
      else if (scanner->token == G_TOKEN_INT)
	GTK_VALUE_DOUBLE (arg) = negate ? - scanner->value.v_int : scanner->value.v_int;
      else
	expected_token = G_TOKEN_FLOAT;
      break;
    case GTK_TYPE_FLOAT:
      negate = FALSE;
      if (scanner->token == '-')
	{
	  negate = !negate;
	  g_scanner_get_next_token (scanner);
	}
      if (scanner->token == G_TOKEN_FLOAT)
	GTK_VALUE_FLOAT (arg) = negate ? - scanner->value.v_float : scanner->value.v_float;
      else if (scanner->token == G_TOKEN_INT)
	GTK_VALUE_FLOAT (arg) = negate ? - scanner->value.v_int : scanner->value.v_int;
      else
	expected_token = G_TOKEN_FLOAT;
      break;
    case GTK_TYPE_LONG:
      negate = FALSE;
      if (scanner->token == '-')
	{
	  negate = !negate;
	  g_scanner_get_next_token (scanner);
	}
      if (scanner->token != G_TOKEN_INT)
	expected_token = G_TOKEN_INT;
      else
	GTK_VALUE_LONG (arg) = negate ? - scanner->value.v_int : scanner->value.v_int;
      break;
    case GTK_TYPE_ULONG:
      if (scanner->token != G_TOKEN_INT)
	expected_token = G_TOKEN_INT;
      else
	GTK_VALUE_ULONG (arg) = scanner->value.v_int;
      break;
    case GTK_TYPE_INT:
      negate = FALSE;
      if (scanner->token == '-')
	{
	  negate = !negate;
	  g_scanner_get_next_token (scanner);
	}
      if (scanner->token != G_TOKEN_INT)
	expected_token = G_TOKEN_INT;
      else
	GTK_VALUE_INT (arg) = negate ? - scanner->value.v_int : scanner->value.v_int;
      break;
    case GTK_TYPE_UINT:
      if (scanner->token != G_TOKEN_INT)
	expected_token = G_TOKEN_INT;
      else
	GTK_VALUE_UINT (arg) = scanner->value.v_int;
      break;
    case GTK_TYPE_STRING:
      if (scanner->token != G_TOKEN_STRING)
	expected_token = G_TOKEN_STRING;
      else
	{
	  g_free (GTK_VALUE_STRING (arg));
	  GTK_VALUE_STRING (arg) = g_strdup (scanner->value.v_string);
	}
      break;
    case GTK_TYPE_BOOL:
      if (scanner->token == G_TOKEN_INT)
	GTK_VALUE_BOOL (arg) = scanner->value.v_int != FALSE;
      else if (scanner->token == G_TOKEN_IDENTIFIER)
	{
	  if (g_strcasecmp (scanner->value.v_identifier, "true") == 0)
	    GTK_VALUE_BOOL (arg) = TRUE;
	  else if (g_strcasecmp (scanner->value.v_identifier, "false") == 0)
	    GTK_VALUE_BOOL (arg) = FALSE;
	  else
	    expected_token = G_TOKEN_IDENTIFIER;
	}
      else
	expected_token = G_TOKEN_IDENTIFIER;
      break;
    case GTK_TYPE_ENUM:
      negate = FALSE;
      if (scanner->token == '-')
	{
	  negate = !negate;
	  g_scanner_get_next_token (scanner);
	}
      if (scanner->token == G_TOKEN_INT)
	{
	  GTK_VALUE_ENUM (arg) = negate ? - scanner->value.v_int : scanner->value.v_int;
	}
      else if (negate)
	expected_token = G_TOKEN_INT;
      else if (scanner->token == G_TOKEN_IDENTIFIER)
	{
	  GtkEnumValue *ev;
	  
	  ev = gtk_type_enum_find_value (arg.type, scanner->value.v_identifier);
	  if (ev)
	    GTK_VALUE_ENUM (arg) = ev->value;
	  else
	    expected_token = G_TOKEN_IDENTIFIER;
	}
      else
	expected_token = G_TOKEN_IDENTIFIER;
      break;
    case GTK_TYPE_FLAGS:
      GTK_VALUE_FLAGS (arg) = 0;
      do
	{
	  if (scanner->token == G_TOKEN_INT)
	    GTK_VALUE_FLAGS (arg) |= scanner->value.v_int;
	  else if (scanner->token == G_TOKEN_IDENTIFIER)
	    {
	      GtkFlagValue *fv;
	      
	      fv = gtk_type_flags_find_value (arg.type, scanner->value.v_identifier);
	      if (fv)
		GTK_VALUE_FLAGS (arg) |= fv->value;
	      else
		expected_token = G_TOKEN_IDENTIFIER;
	    }
	  else
	    expected_token = G_TOKEN_IDENTIFIER;
	  
	  g_scanner_peek_next_token (scanner);
	  if (scanner->next_token != ')')
	    g_scanner_get_next_token (scanner);
	}
      while (scanner->next_token != ')' && expected_token == G_TOKEN_NONE);
      break;
    default:
      g_scanner_warn (scanner,
		      "unsupported argument type `%s' for \"%s\"",
		      gtk_type_name (arg.type),
		      arg.name);
      do
	{
	  if (scanner->token == G_TOKEN_ERROR ||
	      scanner->token == G_TOKEN_EOF)
	    expected_token = ')';
	  g_scanner_peek_next_token (scanner);
	  if (scanner->next_token != ')')
	    g_scanner_get_next_token (scanner);
	}
      while (scanner->next_token != ')' && expected_token == G_TOKEN_NONE);
      break;
    }

  g_scanner_peek_next_token (scanner);
  if (expected_token == G_TOKEN_NONE &&
      scanner->next_token != ')')
    {
      g_scanner_get_next_token (scanner);
      expected_token = ')';
    }
  
  if (expected_token != G_TOKEN_NONE)
    {
      gle_arg_reset (&arg);
      
      return expected_token;
    }
  
  /* eat ')' */
  g_scanner_get_next_token (scanner);

  gle_arg_copy_value (&arg, parser_arg);
  gle_arg_reset (&arg);
  
  return G_TOKEN_NONE;
}

static guint
parse_arg (GScanner	*scanner,
	   GleGWidget	*gwidget)
{
  GleGArg *garg;
  guint expected_token;

  g_scanner_get_next_token (scanner);
  if (scanner->token != G_TOKEN_STRING)
    return G_TOKEN_STRING;

  garg = gle_gobject_get_object_garg (GLE_GOBJECT (gwidget), scanner->value.v_string);
  if (!garg)
    return G_TOKEN_STRING;

  expected_token = parse_arg_value (scanner, &garg->object_arg);

  if (expected_token == G_TOKEN_NONE)
    gle_garg_save (garg);

  return expected_token;
}

static guint
parse_child_arg (GScanner	*scanner,
		 GleGWidget	*gwidget)
{
  GleGArg *garg;
  guint expected_token;

  g_scanner_get_next_token (scanner);
  if (scanner->token != G_TOKEN_STRING)
    return G_TOKEN_STRING;

  garg = gle_gwidget_get_child_garg (gwidget, scanner->value.v_string);
  if (!garg)
    return G_TOKEN_STRING;

  expected_token = parse_arg_value (scanner, &garg->object_arg);

  if (expected_token == G_TOKEN_NONE)
    gle_garg_save (garg);

  return expected_token;
}

static guint
parse_connect_any (GScanner         *scanner,
		   GleGObject       *gobject,
		   GleConnectionType ctype)
{
  guint signal_id;
  gchar *handler;
  gulong data;

  g_scanner_get_next_token (scanner);
  if (scanner->token != G_TOKEN_IDENTIFIER ||
      scanner->value.v_string[0] != ':' ||
      scanner->value.v_string[1] != ':')
    return G_TOKEN_IDENTIFIER;

  signal_id = gtk_signal_lookup (scanner->value.v_string + 2, GLE_GOBJECT_OTYPE (gobject));
  
  if (!signal_id)
    {
      guint level;
      
      g_scanner_warn (scanner,
		      "signal lookup for %s failed in the `%s' class branch",
		      scanner->value.v_string,
		      gtk_type_name (GLE_GOBJECT_OTYPE (gobject)));
      
      level = 1;
      while (level > 0)
	{
	  g_scanner_get_next_token (scanner);
	  
	  if (scanner->token == G_TOKEN_EOF || scanner->token == G_TOKEN_ERROR)
	    return ')';
	  else if (scanner->token == '(')
	    level++;
	  else if (scanner->token == ')')
	    level--;
	}

      return G_TOKEN_NONE;
    }
  
  g_scanner_get_next_token (scanner);
  if (scanner->token != G_TOKEN_STRING)
    return G_TOKEN_STRING;

  handler = g_strdup (scanner->value.v_string);

  g_scanner_peek_next_token (scanner);
  if (scanner->token == G_TOKEN_INT)
    {
      g_scanner_get_next_token (scanner);
      data = scanner->value.v_int;
    }
  else
    data = 0;

  g_scanner_get_next_token (scanner);
  if (scanner->token != ')')
    {
      g_free (handler);
      return ')';
    }

  gle_connection_new (gobject, signal_id, handler, GUINT_TO_POINTER (data), ctype);

  g_free (handler);

  return G_TOKEN_NONE;
}

static guint
parse_connect (GScanner   *scanner,
	       GleGObject *gobject)
{
  return parse_connect_any (scanner, gobject, GLE_CONNECTION_NORMAL);
}

static guint
parse_connect_object (GScanner   *scanner,
		      GleGObject *gobject)
{
  return parse_connect_any (scanner, gobject, GLE_CONNECTION_OBJECT);
}

static guint
parse_connect_after (GScanner   *scanner,
		     GleGObject *gobject)
{
  return parse_connect_any (scanner, gobject, GLE_CONNECTION_AFTER);
}

static guint
parse_connect_object_after (GScanner   *scanner,
			    GleGObject *gobject)
{
  return parse_connect_any (scanner, gobject, GLE_CONNECTION_OBJECT_AFTER);
}
