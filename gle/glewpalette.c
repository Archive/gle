/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#include	<gle/gleconfig.h>

#include	"glewpalette.h"
#include	"gleprivate.h"
#include	"glegtoplevel.h"
#include	"gtkcluehunter.h"
#include	"glercargs.h"
#include	"glednd.h"
#include	"glestock.h"
#include	<stdlib.h>
#include	<string.h>
#include	<stdio.h>


/* --- signals --- */
enum
{
  SIGNAL_LAST
};


/* --- prototypes --- */
static void		gle_wpalette_destroy		(GtkObject		*object);
static void		gle_wpalette_class_init		(GleWPaletteClass	*class);
static void		gle_wpalette_init		(GleWPalette		*wpalette);
static void		gle_wpalette_create		(GleWPalette		*wpalette);
static GleGObject*	gle_wpalette_dnd_start		(GtkWidget		*eb,
							 gint			 root_x,
							 gint			 root_y);
static void		gle_wpalette_dnd_stop		(GtkWidget		*eb,
							 GleGObject		*gobject,
							 gboolean		 drop_success);



/* -- variables --- */
static GtkVBoxClass	*parent_class = NULL;


/* --- functions --- */
GtkType
gle_wpalette_get_type (void)
{
  static GtkType wpalette_type = 0;
  
  if (!wpalette_type)
    {
      GtkTypeInfo wpalette_info =
      {
	"GleWPalette",
	sizeof (GleWPalette),
	sizeof (GleWPaletteClass),
	(GtkClassInitFunc) gle_wpalette_class_init,
	(GtkObjectInitFunc) gle_wpalette_init,
	/* reserved_1 */ NULL,
	/* reserved_2 */ NULL,
	(GtkClassInitFunc) NULL,
      };
      
      wpalette_type = gtk_type_unique (GTK_TYPE_WINDOW, &wpalette_info);
    }
  
  return wpalette_type;
}

static void
gle_wpalette_class_init (GleWPaletteClass *class)
{
  GtkObjectClass *object_class;
  GtkWidgetClass *widget_class;
  GtkContainerClass *container_class;
  GtkWindowClass *window_class;
  
  object_class = (GtkObjectClass*) class;
  widget_class = (GtkWidgetClass*) class;
  container_class = (GtkContainerClass*) class;
  window_class = (GtkWindowClass*) class;
  
  parent_class = gtk_type_class (GTK_TYPE_WINDOW);
  
  /* gtk_object_class_add_signals (object_class, wpalette_signals, SIGNAL_LAST); */
  
  object_class->destroy = gle_wpalette_destroy;
}

static void
gle_wpalette_check_type (GtkEntry    *entry,
			 GleWPalette *wpalette)
{
  GtkWidget *eb;
  
  eb = gtk_object_get_user_data (GTK_OBJECT (entry));
  if (eb)
    {
      GtkType type;
      gchar *text;
      
      text = gtk_entry_get_text (GTK_ENTRY (wpalette->type_entry));
      
      type = gle_type_from_name (text);

      gtk_object_set_user_data (GTK_OBJECT (eb), (gpointer) type);
      if (gtk_type_is_a (type, GTK_TYPE_WIDGET) &&
	  !(text[0] == 'G' &&
	    text[1] == 'l' &&
	    text[2] == 'e'))
	gtk_frame_set_shadow_type (GTK_FRAME (GTK_BIN (eb)->child), GTK_SHADOW_OUT);
      else
	gtk_frame_set_shadow_type (GTK_FRAME (GTK_BIN (eb)->child), GTK_SHADOW_ETCHED_IN);
    }
}

static void
gle_wpalette_set_width (GtkEntry    *entry,
			GleWPalette *wpalette)
{
  glong along;
  gchar *dummy;
  
  along = strtol (gtk_entry_get_text (entry), &dummy, 10);
  wpalette->width = CLAMP (along, 0, 1024);
  if (along != wpalette->width)
    {
      gchar buffer[64];
      
      sprintf (buffer, "%d", wpalette->width);
      gtk_entry_set_text (entry, buffer);
    }
}

static void
gle_wpalette_set_height (GtkEntry    *entry,
			 GleWPalette *wpalette)
{
  glong along;
  gchar *dummy;
  
  along = strtol (gtk_entry_get_text (entry), &dummy, 10);
  wpalette->height = CLAMP (along, 0, 1024);
  if (along != wpalette->height)
    {
      gchar buffer[64];
      
      sprintf (buffer, "%d", wpalette->height);
      gtk_entry_set_text (entry, buffer);
    }
}

static void
gle_wpalette_init (GleWPalette *wpalette)
{
  GtkWidget *main_vbox;
  GtkWidget *vbox;
  GtkWidget *label;
  GtkWidget *frame;
  GtkWidget *any;
  GtkWidget *box;
  GtkWidget *entry;
  GtkWidget *button;
  
  GLE_TAG (wpalette, "GLE-WPalette");
  
  wpalette->table = NULL;
  wpalette->width = 10;
  wpalette->height = 10;
  
  wpalette->tooltips = gtk_tooltips_new ();
  
  gtk_signal_connect_after (GTK_OBJECT (wpalette),
			    "realize",
			    GTK_SIGNAL_FUNC (gle_wpalette_create),
			    NULL);
  
  /* setup the GUI
   */
  gtk_widget_set (GTK_WIDGET (wpalette),
		  "title", "GLE Widget Palette",
		  "type", GTK_WINDOW_TOPLEVEL,
		  "allow_shrink", FALSE,
		  "allow_grow", TRUE,
		  NULL);
  main_vbox =
    gtk_widget_new (GTK_TYPE_VBOX,
		    "visible", TRUE,
		    "homogeneous", FALSE,
		    "spacing", 0,
		    "border_width", 0,
		    "parent", wpalette,
		    NULL);
  vbox =
    gtk_widget_new (GTK_TYPE_VBOX,
		    "visible", TRUE,
		    "homogeneous", FALSE,
		    "spacing", 5,
		    "border_width", 5,
		    NULL);
  gtk_box_pack_start (GTK_BOX (main_vbox), vbox, FALSE, TRUE, 0);
  label =
    gtk_widget_new (GTK_TYPE_LABEL,
		    "visible", TRUE,
		    "label", "Widget Types",
		    NULL);
  gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, TRUE, 0);
  
  /* widget table
   */
  wpalette->table =
    gtk_widget_new (GTK_TYPE_TABLE,
		    "visible", TRUE,
		    "homogeneous", TRUE,
		    "border_width", 0,
		    "row_spacing", 0,
		    "column_spacing", 0,
		    NULL);
  gtk_box_pack_start (GTK_BOX (vbox), wpalette->table, FALSE, TRUE, 0);
  any =
    gtk_widget_new (GTK_TYPE_HSEPARATOR,
		    "visible", TRUE,
		    NULL);
  gtk_box_pack_start (GTK_BOX (main_vbox), any, FALSE, TRUE, 0);
  
  /* widget creation args
   */
  frame =
    gtk_widget_new (GTK_TYPE_FRAME,
		    "visible", TRUE,
		    "label", "Create Widget",
		    "label_xalign", 0.5,
		    "border_width", 5,
		    "parent", main_vbox,
		    "shadow", GTK_SHADOW_ETCHED_OUT,
		    NULL);
  vbox =
    gtk_widget_new (GTK_TYPE_VBOX,
		    "visible", TRUE,
		    "homogeneous", FALSE,
		    "spacing", 0,
		    "border_width", 5,
		    "parent", frame,
		    NULL);
  box =
    gtk_widget_new (GTK_TYPE_HBOX,
		    "visible", TRUE,
		    "homogeneous", FALSE,
		    "spacing", 0,
		    "border_width", 0,
		    NULL);
  gtk_box_pack_start (GTK_BOX (vbox), box, FALSE, TRUE, 0);
  label =
    gtk_widget_new (GTK_TYPE_LABEL,
		    "visible", TRUE,
		    "label", "Widget Type:",
		    NULL);
  gtk_box_pack_start (GTK_BOX (box), label, FALSE, TRUE, 0);
  wpalette->type_entry =
    gtk_widget_new (GTK_TYPE_ENTRY,
		    "visible", TRUE,
		    // "width", 100,
		    "signal::changed", gle_wpalette_check_type, wpalette,
		    // "object_signal::activate", gtk_window_activate_default, wpalette,
		    NULL);
  gtk_entry_set_text (GTK_ENTRY (wpalette->type_entry), "None");
  any = gtk_widget_new (GTK_TYPE_CLUE_HUNTER,
			"keep_history", FALSE,
			"entry", wpalette->type_entry,
			NULL);
  gtk_box_pack_end (GTK_BOX (box),
		    gtk_clue_hunter_create_arrow (GTK_CLUE_HUNTER (any)),
		    FALSE, FALSE, 0);
  gtk_box_pack_end (GTK_BOX (box), wpalette->type_entry, TRUE, TRUE, 0);
  box =
    gtk_widget_new (GTK_TYPE_HBOX,
		    "visible", TRUE,
		    "homogeneous", FALSE,
		    "spacing", 10,
		    "border_width", 0,
		    NULL);
  gtk_box_pack_start (GTK_BOX (vbox), box, FALSE, TRUE, 0);
  label =
    gtk_widget_new (GTK_TYPE_LABEL,
		    "visible", TRUE,
		    "label", "Initial Width:",
		    NULL);
  gtk_box_pack_start (GTK_BOX (box), label, FALSE, TRUE, 0);
  entry =
    gtk_widget_new (GTK_TYPE_ENTRY,
		    "visible", TRUE,
		    "max_length", 5,
		    "width", 50,
		    "signal::changed", gle_wpalette_set_width, wpalette,
		    "object_signal::activate", gtk_window_activate_default, wpalette,
		    NULL);
  gle_wpalette_set_width (GTK_ENTRY (entry), wpalette);
  gtk_box_pack_end (GTK_BOX (box), entry, FALSE, TRUE, 0);
  gtk_entry_set_text (GTK_ENTRY (entry), "0");
  box =
    gtk_widget_new (GTK_TYPE_HBOX,
		    "visible", TRUE,
		    "homogeneous", FALSE,
		    "spacing", 10,
		    "border_width", 0,
		    NULL);
  gtk_box_pack_start (GTK_BOX (vbox), box, FALSE, TRUE, 0);
  label =
    gtk_widget_new (GTK_TYPE_LABEL,
		    "visible", TRUE,
		    "label", "Initial Height:",
		    NULL);
  gtk_box_pack_start (GTK_BOX (box), label, FALSE, TRUE, 0);
  entry =
    gtk_widget_new (GTK_TYPE_ENTRY,
		    "visible", TRUE,
		    "max_length", 5,
		    "width", 50,
		    "signal::changed", gle_wpalette_set_height, wpalette,
		    "object_signal::activate", gtk_window_activate_default, wpalette,
		    NULL);
  gle_wpalette_set_height (GTK_ENTRY (entry), wpalette);
  gtk_box_pack_end (GTK_BOX (box), entry, FALSE, TRUE, 0);
  gtk_entry_set_text (GTK_ENTRY (entry), "0");
  
  /* close button
   */
  any =
    gtk_widget_new (GTK_TYPE_HSEPARATOR,
		    "visible", TRUE,
		    NULL);
  gtk_box_pack_start (GTK_BOX (main_vbox), any, FALSE, TRUE, 0);
  button =
    gtk_widget_new (GTK_TYPE_BUTTON,
		    "visible", TRUE,
		    "label", "Close",
		    "border_width", 5,
		    "parent", main_vbox,
		    "can_default", TRUE,
		    "has_default", TRUE,
		    "object_signal::clicked", gtk_widget_destroy, wpalette,
		    NULL);
  gtk_box_set_child_packing (GTK_BOX (main_vbox), button, FALSE, TRUE, 0, GTK_PACK_START);
}

GtkWidget*
gle_wpalette_new (void)
{
  return gtk_widget_new (GLE_TYPE_WPALETTE, NULL);
}

static gint
gle_wpalette_eb_event (GtkWidget   *eb,
		       GdkEvent	   *event,
		       GleWPalette *wpalette)
{
  g_return_val_if_fail (GLE_IS_WPALETTE (wpalette), FALSE);
  
  if (event->type == GDK_BUTTON_PRESS &&
      event->button.button == 1)
    {
      GtkType type;
      
      type = GPOINTER_TO_UINT (gtk_object_get_user_data (GTK_OBJECT (eb)));
      if (type)
	gtk_entry_set_text (GTK_ENTRY (wpalette->type_entry), gtk_type_name (type));
    }
  
  return FALSE;
}

static GleGObject*
gle_wpalette_dnd_start (GtkWidget   *eb,
			gint         root_x,
			gint         root_y)
{
  GtkType type;
  GleGWidget *gwidget;
  GleGArg *garg;

  type = GPOINTER_TO_UINT (gtk_object_get_user_data (GTK_OBJECT (eb)));
  if (!type)
    return NULL;

  gwidget = gle_gwidget_new (type);
  garg = gle_gobject_get_object_garg (GLE_GOBJECT (gwidget), "visible");
  GTK_VALUE_BOOL (garg->object_arg) = TRUE;
  
  return GLE_GOBJECT (gwidget);
}

static void
gle_wpalette_dnd_stop (GtkWidget  *eb,
		       GleGObject *gobject,
		       gboolean    drop_success)
{
  if (!drop_success)
    gle_gobject_destroy (gobject);
}

static GtkWidget*
gle_wpalette_eb_from_xpm (GtkWidget *widget,
			  gpointer   user_data,
			  gchar	   **xpm)
{
  GtkWidget *pix;
  GtkWidget *toplevel;
  GdkPixmap *pixmap = NULL;
  GdkBitmap *bitmap = NULL;
  
  g_return_val_if_fail (GTK_WIDGET_REALIZED (widget), NULL);
  
  toplevel = gtk_widget_get_toplevel (widget);
  
  pixmap = gdk_pixmap_create_from_xpm_d (widget->window,
					 &bitmap,
					 &widget->style->bg[GTK_STATE_NORMAL],
					 xpm);
  pix = gtk_pixmap_new (pixmap, bitmap);
  gdk_pixmap_unref (pixmap);
  gdk_bitmap_unref (bitmap);
  gtk_widget_show (pix);
  widget =
    gtk_widget_new (GTK_TYPE_FRAME,
		    "visible", TRUE,
		    "child", pix,
		    "shadow", GTK_SHADOW_OUT,
		    NULL);
  widget =
    gtk_widget_new (GTK_TYPE_EVENT_BOX,
		    "visible", TRUE,
		    "child", widget,
		    "user_data", user_data,
		    "signal::event", gle_wpalette_eb_event, toplevel,
		    "events", GDK_BUTTON_PRESS_MASK,
		    NULL);
  gle_dnd_add_source (widget,
		      1 | 2,
		      gle_wpalette_dnd_start,
		      gle_wpalette_dnd_stop);

  return widget;
}

static void
gle_wpalette_create (GleWPalette *wpalette)
{
  static GtkType (*type_funcs[]) (void) =
  {
    /* use gtkquery to generate this list, it is far easier than
     * maintaining by hand, and it keeps the hirarchy
     */
    gtk_label_get_type,
    gtk_accel_label_get_type,
    gtk_tips_query_get_type,
    gtk_arrow_get_type,
    gtk_pixmap_get_type,
    gtk_alignment_get_type,
    gtk_frame_get_type,
    gtk_aspect_frame_get_type,
    gtk_list_item_get_type,
    gtk_window_get_type,
    gtk_color_selection_dialog_get_type,
    gtk_dialog_get_type,
    gtk_input_dialog_get_type,
    gtk_file_selection_get_type,
    gtk_event_box_get_type,
    gtk_handle_box_get_type,
    gtk_viewport_get_type,
    gtk_box_get_type,
    gtk_button_box_get_type,
    gtk_hbutton_box_get_type,
    gtk_vbutton_box_get_type,
    gtk_vbox_get_type,
    gtk_color_selection_get_type,
    gtk_gamma_curve_get_type,
    gtk_hbox_get_type,
    gtk_combo_get_type,
    gtk_statusbar_get_type,
    gtk_button_get_type,
    gtk_toggle_button_get_type,
    gtk_check_button_get_type,
    gtk_radio_button_get_type,
    gtk_clist_get_type,
    gtk_ctree_get_type,
    gtk_fixed_get_type,
    gtk_notebook_get_type,
    gtk_font_selection_get_type,
    gtk_hpaned_get_type,
    gtk_vpaned_get_type,
    gtk_list_get_type,
    gtk_packer_get_type,
    gtk_scrolled_window_get_type,
    gtk_table_get_type,
    gtk_toolbar_get_type,
    gtk_drawing_area_get_type,
    gtk_curve_get_type,
    gtk_entry_get_type,
    gtk_spin_button_get_type,
    gtk_text_get_type,
    gtk_hruler_get_type,
    gtk_vruler_get_type,
    gtk_hscrollbar_get_type,
    gtk_vscrollbar_get_type,
    gtk_hseparator_get_type,
    gtk_vseparator_get_type,
    gtk_preview_get_type,
    gtk_progress_bar_get_type,
  };
  const guint n_type_funcs = sizeof (type_funcs) / sizeof (type_funcs[0]);
  guint i;
  GtkWidget *widget;
  GtkClueHunter *ch = NULL;
  guint cols;
  guint n;
  
  widget = GTK_WIDGET (wpalette);
  cols = GLE_RCVAL_WPALETTE_N_COLUMNS;
  
  gtk_signal_disconnect_by_func (GTK_OBJECT (wpalette), GTK_SIGNAL_FUNC (gle_wpalette_create), NULL);
  
  gtk_widget_push_colormap (gtk_widget_get_colormap (widget));
  gtk_widget_push_visual (gtk_widget_get_visual (widget));
  
  if (GTK_IS_BOX (wpalette->type_entry->parent))
    {
      GtkWidget *eb;
      gchar **xpm;
      
      xpm = gle_stock_get_xpm (GLE_XPM_SELECTOR);
      eb = gle_wpalette_eb_from_xpm (widget, (void*) 0, xpm);
      gtk_box_pack_end (GTK_BOX (wpalette->type_entry->parent), eb, FALSE, FALSE, 10);
      gtk_object_set_user_data (GTK_OBJECT (wpalette->type_entry), eb);
      gle_wpalette_check_type (GTK_ENTRY (wpalette->type_entry), wpalette);
      ch = gtk_clue_hunter_from_entry (wpalette->type_entry);
    }
  
  n = 0;
  for (i = 0; i < n_type_funcs; i++)
    {
      GtkType type;
      GtkWidget *eb;
      gchar **xpm;
      
      type = type_funcs[i] ();
      
      xpm = gle_stock_get_xpm (gtk_type_name (type));
      if (!xpm)
	xpm = gle_stock_get_xpm_unknown ();
      
      eb = gle_wpalette_eb_from_xpm (widget, (void*) type, xpm);
      gtk_table_attach (GTK_TABLE (wpalette->table),
			eb,
			n % cols, (n % cols) + 1,
			n / cols, n / cols + 1,
			GTK_FILL | GTK_EXPAND,
			GTK_FILL,
			0, 0);
      
      n++;
      gtk_tooltips_set_tip (wpalette->tooltips, eb, gtk_type_name (type), NULL);
      if (ch)
	gtk_clue_hunter_add_string (ch, gtk_type_name (type));
    }
  
  gtk_tooltips_force_window (GTK_TOOLTIPS (wpalette->tooltips));
  gtk_widget_set_name (GTK_TOOLTIPS (wpalette->tooltips)->tip_window, "GLE-TipWindow");
  
  gtk_widget_pop_colormap ();
  gtk_widget_pop_visual ();
}

static void
gle_wpalette_destroy (GtkObject *object)
{
  GleWPalette *wpalette;
  
  g_return_if_fail (object != NULL);
  g_return_if_fail (GLE_IS_WPALETTE (object));
  
  wpalette = GLE_WPALETTE (object);
  
  gtk_object_unref (GTK_OBJECT (wpalette->tooltips));
  wpalette->tooltips = NULL;
  
  GTK_OBJECT_CLASS (parent_class)->destroy (object);
}
