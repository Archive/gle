/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001-2002 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GLE_PROXY_SET_H__
#define __GLE_PROXY_SET_H__


#include        <gle/gleutils.h>
#include        <gle/gleforeign.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */



/* --- macros --- */
#define GLE_TYPE_PROXY_SET		(gle_proxy_set_get_type ())
#define GLE_PROXY_SET(object)		(G_TYPE_CHECK_INSTANCE_CAST ((object), GLE_TYPE_PROXY_SET, GleProxySet))
#define GLE_PROXY_SET_CLASS(class)	(G_TYPE_CHECK_CLASS_CAST ((class), GLE_TYPE_PROXY_SET, GleProxySetClass))
#define GLE_IS_PROXY_SET(object)	(G_TYPE_CHECK_INSTANCE_TYPE ((object), GLE_TYPE_PROXY_SET))
#define GLE_IS_PROXY_SET_CLASS(class)	(G_TYPE_CHECK_CLASS_TYPE ((class), GLE_TYPE_PROXY_SET))
#define GLE_PROXY_SET_GET_CLASS(object)	(G_TYPE_INSTANCE_GET_CLASS ((object), GLE_TYPE_PROXY_SET, GleProxySetClass))


/* --- structures --- */
typedef struct _GleProxySet  GleProxySet;
typedef struct _GObjectClass GleProxySetClass;
typedef enum
{
  GLE_PROXY_USER_NOTIFY,	/* keep this 0 */
  GLE_PROXY_SET_DESTROY,
  GLE_PROXY_SET_CHANGED
} GleProxySetNotifyID;
typedef void (*GleProxySetNotify)	(gpointer		 data,
					 GleProxySet		*pset,
					 GleProxySetNotifyID	 id);
typedef enum
{
  GLE_PROXY_SET_SOURCE_RIPPED,
  GLE_PROXY_SET_SOURCE_FILE,
  GLE_PROXY_SET_SOURCE_USER
} GleProxySetSource;
struct _GleProxySet
{
  GObject           parent_instance;
  GleProxySetSource source;
  GleForeign	   *foreign;
  gchar            *name;
  GleProxy	   *proxies;
  GleProxy	   *last_proxy;
};


/* --- prototypes --- */
GType		gle_proxy_set_get_type		(void);
GleProxySet*	gle_proxy_set_new		(GleProxySetSource source_id,
						 const gchar	*name,
						 GleForeign	*foreign);
GleProxySet*	gle_proxy_set_find		(const gchar	*name);
GList*		gle_proxy_sets_list		(void);
void		gle_proxy_set_prepend_proxy	(GleProxySet	*pset,
						 GleProxy	*proxy);
void		gle_proxy_set_append_proxy	(GleProxySet	*pset,
						 GleProxy	*proxy);
void		gle_proxy_set_remove_proxy	(GleProxySet	*pset,
						 GleProxy	*proxy);
void		gle_proxy_set_add_notify	(GleProxySet	*pset,
						 GleProxySetNotify notify,
						 gpointer	 data);
void		gle_proxy_set_remove_notify	(GleProxySet	*pset,
						 GleProxySetNotify notify,
						 gpointer	 data);
GList*		gle_proxy_set_list_toplevels	(GleProxySet	*pset);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif  /* __GLE_PROXY_SET_H__ */
