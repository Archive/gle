/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#ifndef __GLE_MODULE_H__
#define __GLE_MODULE_H__


#include	<gtk/gtk.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */



/* --- GLE version variables --- */
extern const guint     gle_version_major;
extern const guint     gle_version_revision;
extern const guint     gle_version_patchlevel;
extern const guint     gle_version_age;
extern const gchar    *gle_version;


/* --- GLE main part --- */
void            gle_init                        (int            *argc,
						 char           ***argv);



#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif	/* __GLE_MODULE_H__ */
