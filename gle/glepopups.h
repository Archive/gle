/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.	 See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef __GLEPOPUPS_H__
#define __GLEPOPUPS_H__


#include	<gle/glerc.h>


#ifdef __cplusplus
extern "C" {
#pragma }
#endif /* __cplusplus */




/* --- prototypes --- */
GtkWidget* gle_popup_gle_shell			(void);
GtkWidget* gle_popup_gle_shell_with_customer	(GtkWidget	*customer);
GtkWidget* gle_popup_gle_shell_selector		(void);
GtkWidget* gle_popup_editor			(GtkObject	*object);
GtkWidget* gle_popup_ted			(GtkWidget	*widget);

GtkWidget* gle_create_rc_pref			(GleRcHandle	*rc_handle);






#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif	/* __GLEPOPUPS_H__ */
