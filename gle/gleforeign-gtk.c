/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001-2002 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include "gleforeign.h"


/* --- GtkWidget --- */
static GParamSpec**
foreign_gtk_widget_list_object_pspecs (GObjectClass *oclass,
				       guint        *n_pspecs)
{
  return g_object_class_list_properties (oclass, n_pspecs);
}

static GParamSpec**
foreign_gtk_widget_list_link_pspecs (GObjectClass *oclass,
				     GObjectClass *parent_class,
				     guint        *n_pspecs)
{
  return gtk_container_class_list_child_properties (parent_class, n_pspecs);
}

static GQuark gle_foreign_gtk_quark = 0;

static GleProxy*
foreign_gtk_widget_get_proxy (GObject *widget)
{
  return g_object_get_qdata (widget, gle_foreign_gtk_quark);
}

static void
foreign_gtk_widget_set_proxy (GObject  *widget,
			      GleProxy *proxy)
{
  if (!gle_foreign_gtk_quark)
    gle_foreign_gtk_quark = g_quark_from_static_string ("GLE-foreign-Gtk-proxy");

  g_object_set_qdata (widget, gle_foreign_gtk_quark, proxy);
  if (proxy)
    {
      g_signal_connect_swapped (widget, "parent_set", G_CALLBACK (gle_foreign_reload_parent), proxy);
      g_signal_connect_swapped (widget, "parent_set", G_CALLBACK (gle_foreign_reload_link_params), proxy);
      g_signal_connect_swapped (widget, "notify", G_CALLBACK (gle_foreign_update_object_params), proxy);
      g_signal_connect_swapped (widget, "child_notify", G_CALLBACK (gle_foreign_update_link_params), proxy);
      g_signal_connect_swapped (widget, "destroy", G_CALLBACK (gle_foreign_detach_object), proxy);
    }
  else
    {
      GSignalMatchType match = G_SIGNAL_MATCH_FUNC | G_SIGNAL_MATCH_DATA;

      g_signal_handlers_disconnect_matched (widget, match, 0, 0, NULL, G_CALLBACK (gle_foreign_reload_parent), proxy);
      g_signal_handlers_disconnect_matched (widget, match, 0, 0, NULL, G_CALLBACK (gle_foreign_reload_link_params), proxy);
      g_signal_handlers_disconnect_matched (widget, match, 0, 0, NULL, G_CALLBACK (gle_foreign_update_object_params), proxy);
      g_signal_handlers_disconnect_matched (widget, match, 0, 0, NULL, G_CALLBACK (gle_foreign_update_link_params), proxy);
      g_signal_handlers_disconnect_matched (widget, match, 0, 0, NULL, G_CALLBACK (gle_foreign_detach_object), proxy);
    }
}

static GObject*
foreign_gtk_widget_construct (GObjectClass *oclass)
{
  GObject *widget;

  widget = g_object_new (G_OBJECT_CLASS_TYPE (oclass), NULL);
  g_object_ref (widget);
  gtk_object_sink (GTK_OBJECT (widget));

  return widget;
}

static void
foreign_gtk_widget_attach (GObject *widget)
{
  g_object_ref (widget);
  gtk_object_sink (GTK_OBJECT (widget));
}

static void
foreign_gtk_widget_detach (GObject *widget)
{
  g_object_unref (widget);
}

static void
foreign_gtk_widget_destruct (GObject *widget)
{
  gtk_object_destroy (GTK_OBJECT (widget));
  g_object_unref (widget);
}

static void
foreign_gtk_widget_get_object_param (GObject    *widget,
				     GParamSpec *pspec,
				     GValue     *value)
{
  g_object_get_property (widget, pspec->name, value);
}

static void
foreign_gtk_widget_set_object_param (GObject      *widget,
				     GParamSpec   *pspec,
				     const GValue *value)
{
  g_object_set_property (widget, pspec->name, value);
}

static void
foreign_gtk_widget_get_link_param (GObject    *object,
				   GParamSpec *pspec,
				   GValue     *value)
{
  GtkWidget *widget = GTK_WIDGET (object);

  if (widget->parent)
    gtk_container_child_get_property (GTK_CONTAINER (widget->parent), widget,
				      pspec->name, value);
}

static void
foreign_gtk_widget_set_link_param (GObject      *object,
				   GParamSpec   *pspec,
				   const GValue *value)
{
  GtkWidget *widget = GTK_WIDGET (object);

  if (widget->parent)
    gtk_container_child_set_property (GTK_CONTAINER (widget->parent), widget,
				      pspec->name, value);
}

static GObject*
foreign_gtk_widget_get_parent (GObject *object)
{
  GtkWidget *widget = GTK_WIDGET (object);
  
  return widget->parent ? G_OBJECT (widget->parent) : NULL;
}

static void
foreign_gtk_widget_set_parent (GObject *object,
			       GObject *parent)
{
  if (GTK_IS_CONTAINER (parent))
    {
      GtkWidget *widget = GTK_WIDGET (widget);

      if (!widget->parent)
	gtk_container_add (GTK_CONTAINER (parent), widget);
    }
}

static gboolean
foreign_gtk_widget_check_toplevel (GObject *object)
{
  return GTK_IS_WINDOW (object);
}

static void
foreign_gtk_container_list_ref (GtkWidget *child,
				gpointer   data)
{
  GSList **slist_p = data;

  *slist_p = g_slist_prepend (*slist_p, g_object_ref (child));
}

static GSList*
foreign_gtk_widget_list_ref_children (GObject *object)
{
  if (GTK_IS_CONTAINER (object))
    {
      GSList *slist = NULL;

      gtk_container_forall (GTK_CONTAINER (object), foreign_gtk_container_list_ref, &slist);

      return slist;
    }
  return NULL;
}

static gchar*
foreign_gtk_widget_state_hint (GObject *object)
{
  gchar *hint;

  switch (GTK_WIDGET_STATE (object))
    {
    case GTK_STATE_ACTIVE:	hint = "(_) Active";		break;
    case GTK_STATE_PRELIGHT:	hint = "(_) Prelight";		break;
    case GTK_STATE_SELECTED:	hint = "(_) Selected";		break;
    case GTK_STATE_INSENSITIVE:	hint = "(_) Insensitive";	break;
    default:			hint = "(_) Normal";		break;
    }
  hint = g_strdup (hint);
  if (GTK_WIDGET_REALIZED (object))
    hint[1] = GTK_WIDGET_VISIBLE (object) ? 'V' : 'H';
  else
    hint[1] = GTK_WIDGET_VISIBLE (object) ? 'v' : 'h';
  return hint;
}

enum {
  PRINT_ALLOCATION,
  QUEUE_REDRAW,
  QUEUE_RESIZE,
  RIP_CANVAS
};

static GleForeignAction gtk_actions[] = {
  { PRINT_ALLOCATION,	"Print Allocation", },
  { QUEUE_REDRAW,	"Queue Redraw", },
  { QUEUE_RESIZE,	"Queue Resize", },
  { RIP_CANVAS,		"Rip Gnome Canvas", },
};

static GleForeignActionState
foreign_gtk_widget_check_action (GObject          *object,
				 GleForeignAction *action,
				 GleProxy         *proxy)
{
  GtkWidget *widget = GTK_WIDGET (object);

  switch (action->action_type)
    {
      GType type;
      gboolean can_action;
    case PRINT_ALLOCATION:
      return GLE_FOREIGN_ACTION_ENABLED;
    case QUEUE_REDRAW:
      return GTK_WIDGET_DRAWABLE (widget) ? GLE_FOREIGN_ACTION_ENABLED : GLE_FOREIGN_ACTION_DISABLED;
    case QUEUE_RESIZE:
      return GTK_WIDGET_REALIZED (widget) ? GLE_FOREIGN_ACTION_ENABLED : GLE_FOREIGN_ACTION_DISABLED;
    case RIP_CANVAS:
      can_action = GLE_FOREIGN_ACTION_HIDE;
      type = g_type_from_name ("GnomeCanvas");
      if (g_type_is_a (G_OBJECT_TYPE (object), type))
	can_action = g_module_supported () ? GLE_FOREIGN_ACTION_ENABLED : GLE_FOREIGN_ACTION_DISABLED;
      return can_action;
    default:
      g_assert_not_reached ();
      return GLE_FOREIGN_ACTION_HIDE;	/* silence compiler */
    }
}

static void
foreign_gtk_widget_exec_action (GObject          *object,
				GleForeignAction *action,
				GleProxy         *proxy)
{
  GtkWidget *widget = GTK_WIDGET (object);

  switch (action->action_type)
    {
      gpointer (*ptr_ptr_func) (gpointer);
      GModule *gmodule;
    case PRINT_ALLOCATION:
      g_message ("Widget \"%s\" allocation: { %d, %d, %d, %u }",
		 proxy->pname,
		 widget->allocation.x,
		 widget->allocation.y,
		 widget->allocation.width,
		 widget->allocation.height);
      break;
    case QUEUE_REDRAW:
      gtk_widget_queue_clear (widget);
      break;
    case QUEUE_RESIZE:
      gtk_widget_queue_resize (widget);
      break;
    case RIP_CANVAS:
      gmodule = g_module_open (NULL, 0);	/* main handle */
      if (g_module_symbol (gmodule, "gnome_canvas_root", (gpointer*) &ptr_ptr_func))
	{
	  GObject *root = ptr_ptr_func (object);

	  if (G_IS_OBJECT (root))
	    {
	      GleForeign *foreign;
	      GleProxySet *pset = NULL;
	      GleProxy *proxy = NULL;

	      foreign = gle_foreign_from_class (G_OBJECT_GET_CLASS (root));
	      if (foreign)
		pset = gle_proxy_set_new (GLE_PROXY_SET_SOURCE_RIPPED, gtk_widget_get_name (GTK_WIDGET (object)), foreign);
	      if (pset)
		proxy = gle_proxy_new_from_object (pset, G_OBJECT (root));
	      if (proxy)
		gle_foreign_sync_rip (proxy, TRUE);
	    }
	}
      else
	gdk_beep ();
      break;
    default:
      g_assert_not_reached ();
    }
}

static GleForeign foreign_gtk_widget = {
  foreign_gtk_widget_list_object_pspecs,
  foreign_gtk_widget_list_link_pspecs,
  foreign_gtk_widget_get_proxy,
  foreign_gtk_widget_set_proxy,
  foreign_gtk_widget_construct,
  foreign_gtk_widget_attach,
  foreign_gtk_widget_detach,
  foreign_gtk_widget_destruct,
  foreign_gtk_widget_get_object_param,
  foreign_gtk_widget_set_object_param,
  foreign_gtk_widget_get_link_param,
  foreign_gtk_widget_set_link_param,
  foreign_gtk_widget_get_parent,
  foreign_gtk_widget_set_parent,
  foreign_gtk_widget_check_toplevel,
  foreign_gtk_widget_list_ref_children,
  foreign_gtk_widget_state_hint,
  G_N_ELEMENTS (gtk_actions),
  gtk_actions,
  foreign_gtk_widget_check_action,
  foreign_gtk_widget_exec_action
};
