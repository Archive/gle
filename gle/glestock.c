/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#define	GTK_ENABLE_BROKEN
#include	"glestock.h"
#include	<gle-xpms/glexpm_inc.c>


/* --- defines --- */
#define	GLE_STOCK_INIT()	while (!stock_initialized) gle_stock_initialize ();


/* --- prototypes --- */
static void	gle_stock_initialize		(void);


/* -- variables --- */
static gboolean	stock_initialized = FALSE;
static struct
{
  GQuark quark;
  gpointer p;
  
  gchar *(*xpm)[];
} gle_xpms[] = {
#include "gle-xpms/glexpm_p.c"
};
static const guint gle_n_xpms = sizeof (gle_xpms) / sizeof (gle_xpms[0]);


/* --- functions --- */
static void
gle_stock_initialize (void)
{
  if (!stock_initialized)
    {
      guint i;
      
      stock_initialized = TRUE;
      
      for (i = 0; i < gle_n_xpms; i++)
	{
	  if (gle_xpms[i].quark == 0)
	    {
	      GtkType (*get_type) (void);
	      
	      get_type = gle_xpms[i].p;
	      gle_xpms[i].quark = g_quark_from_static_string (gtk_type_name (get_type ()));
	    }
	  else if (gle_xpms[i].quark == 1)
	    gle_xpms[i].quark = g_quark_from_static_string (gle_xpms[i].p);
	  else
	    g_assert_not_reached ();
	}
    }
}

gchar**
gle_stock_get_xpm (const gchar *xpm_name)
{
  GQuark quark;
  
  g_return_val_if_fail (xpm_name != NULL, NULL);
  
  GLE_STOCK_INIT ();
  
  quark = g_quark_try_string (xpm_name);
  
  return quark ? gle_stock_id_get_xpm (quark) : NULL;
}

gchar**
gle_stock_id_get_xpm (GQuark id)
{
  guint i;
  
  g_return_val_if_fail (id > 0, NULL);
  
  GLE_STOCK_INIT ();
  
  for (i = 0; i < gle_n_xpms; i++)
    {
      if (gle_xpms[i].quark == id)
	return *gle_xpms[i].xpm;
    }
  
  return NULL;
}

gchar**
gle_stock_get_xpm_unknown (void)
{
  gchar **xpm;
  
  xpm = gle_stock_get_xpm (GLE_XPM_UNKNOWN);
  g_assert (xpm != NULL);
  
  return xpm;
}
