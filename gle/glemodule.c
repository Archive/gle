/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include "glemodule.h"

#include <gdk/gdkkeysyms.h>
#include <gmodule.h>
#include "gleconfig.h"
#include "gleproxyset.h"
#include "gleshell.h"


/* --- global variables --- */
const guint      gle_version_major = GLE_VERSION_MAJOR;
const guint      gle_version_revision = GLE_VERSION_REVISION;
const guint      gle_version_patchlevel = GLE_VERSION_PATCHLEVEL;
const guint      gle_version_age = GLE_VERSION_AGE;
const gchar	*gle_version = GLE_VERSION;


/* --- prototypes --- */
static gint	gle_snooper		(GtkWidget   *grab_widget,
					 GdkEventKey *event,
					 gpointer     func_data);


/* --- static variables --- */
static	guint	snooper_id = 0;


/* --- functions --- */
void
gle_init (int          *argc_p,
	  char       ***argv_p)
{
  static gboolean gle_initialized = FALSE;
  gint argc;
  gchar **argv;
  
  if (gle_initialized)
    return;
  gle_initialized = TRUE;

  gtk_init (argc_p, argv_p);

  {
    /* Gtk+ version check, we refuse to run if the compile time
     * Gtk+ version is not equal with the runtime version.
     */
    static const guint build_gtk_major_version = GTK_MAJOR_VERSION;
    static const guint build_gtk_minor_version = GTK_MINOR_VERSION;
    static const guint build_gtk_micro_version = GTK_MICRO_VERSION;

    if (gtk_major_version != build_gtk_major_version ||
	gtk_minor_version != build_gtk_minor_version ||
	gtk_micro_version < build_gtk_micro_version)
      {
	g_warning ("GLE v%s needs to be rebuilt to work with Gtk+ v%d.%d.%d",
		   GLE_VERSION,
		   gtk_major_version, gtk_minor_version, gtk_micro_version);
	return;
      }
  }
  
  if (!argc_p)
    argc = 0;
  else
    argc = *argc_p;
  
  if (!argv_p)
    argv = NULL;
  else
    argv = *argv_p;
  
  snooper_id = gtk_key_snooper_install (gle_snooper, NULL);
}

static gint
gle_snooper (GtkWidget   *grab_widget,
	     GdkEventKey *event,
	     gpointer     func_data)
{
  if ((event->state & GDK_CONTROL_MASK) &&
      (event->state & GDK_MOD1_MASK) &&
      (event->keyval == '\t' || event->keyval == GDK_Tab ||
       event->keyval == GDK_KP_Tab || event->keyval == GDK_ISO_Left_Tab))
    {
      if (event->type == GDK_KEY_RELEASE)
	return TRUE;
      else if (event->type == GDK_KEY_PRESS)
	{
	  GleShell *shell;
	  
	  /* Ctrl+Alt+Tab is pressed:
	   * - without GLeShell -> popup the shell
	   * - GleShell exists -> register a proxy for the toplevel
	   * - GleShell exists, pointer grab in effect -> popup the shell's selector
	   */
	  if (!gle_shell_is_popped_up ())
	    gle_shell_popup ();
	  else
	    {
	      shell = gle_shell_popup ();
	      
	      if (gdk_pointer_is_grabbed ())
		gle_shell_operation (GLE_SHELL (shell), GLE_SHELL_OP_SELECTOR);
	      else
		{
		  GtkWidget *event_widget = gtk_get_event_widget ((GdkEvent*) event);
		  
		  if (event_widget)
		    {
		      while (event_widget->parent)
			event_widget = event_widget->parent;
		      if (!GLE_HAS_TAG (event_widget))
			gle_shell_rip_gtk_window (GTK_WINDOW (event_widget));
		    }
		}
	    }
	}
      return TRUE;
    }
  return FALSE;
}


/* --- GLE Module functions --- */
G_MODULE_EXPORT const gchar* g_module_check_init (GModule *module);
const gchar*
g_module_check_init (GModule *module)
{
  GModule *main_module;
  gchar *version_check = NULL;
  
  main_module = g_module_open (NULL, 0);
  if (!main_module)
    return "no main handle";
  if (!g_module_symbol (main_module, "gtk_major_version", (gpointer*) &version_check) && version_check)
    return "no gtk library?";

  version_check = gtk_check_version (GTK_MAJOR_VERSION,
				     GTK_MINOR_VERSION,
				     GTK_MICRO_VERSION - GTK_INTERFACE_AGE);
  if (version_check)
    return version_check;
  
  /* make ourselves resident */
  g_module_open (g_module_name (module), G_MODULE_BIND_LAZY);

  return NULL;
}

G_MODULE_EXPORT void gtk_module_init (gint *argc, gchar ***argv);
void
gtk_module_init (int            *argc,
		 char           ***argv)
{
  gle_init (argc, argv);
}
