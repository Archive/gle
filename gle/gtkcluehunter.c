/* GTK - The GIMP Toolkit
 * Copyright (C) 1995-1997 Peter Mattis, Spencer Kimball and Josh MacDonald
 *
 * GtkClueHunter: Completion popup with pattern matching for GtkEntry
 * Copyright (C) 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include	"gtkcluehunter.h"

#include	"glemarshal.h"
#include	<gdk/gdkkeysyms.h>
#include	<string.h>


/* --- signals --- */
enum {
  SIGNAL_ACTIVATE,
  SIGNAL_POPUP,
  SIGNAL_POPDOWN,
  SIGNAL_SELECT_ON,
  SIGNAL_LAST
};


/* --- arguments --- */
enum {
  ARG_0,
  ARG_PATTERN_MATCHING,
  ARG_KEEP_HISTORY,
  ARG_ALIGN_WIDTH,
  ARG_ENTRY
};


/* --- prototypes --- */
static void	gtk_clue_hunter_class_init	(GtkClueHunterClass	*class);
static void	gtk_clue_hunter_init		(GtkClueHunter		*clue_hunter);
static void	gtk_clue_hunter_destroy		(GtkObject		*object);
static void	gtk_clue_hunter_finalize	(GObject		*object);
static void	gtk_clue_hunter_set_arg		(GtkObject      	*object,
						 GtkArg         	*arg,
						 guint          	 arg_id);
static void	gtk_clue_hunter_get_arg		(GtkObject      	*object,
						 GtkArg         	*arg,
						 guint          	 arg_id);
static void	gtk_clue_hunter_entry_changed	(GtkClueHunter		*clue_hunter);
static gint	gtk_clue_hunter_entry_key_press	(GtkClueHunter		*clue_hunter,
						 GdkEventKey		*event,
						 GtkEntry		*entry);
static gint	gtk_clue_hunter_clist_click	(GtkClueHunter		*clue_hunter,
						 GdkEvent		*event,
						 GtkCList		*clist);
static gint	gtk_clue_hunter_event           (GtkWidget		*widget,
						 GdkEvent		*event);
static void	gtk_clue_hunter_do_activate	(GtkClueHunter		*clue_hunter);
static void	gtk_clue_hunter_do_popup	(GtkClueHunter       	*clue_hunter);
static void	gtk_clue_hunter_do_popdown      (GtkClueHunter       	*clue_hunter);
static void	gtk_clue_hunter_add_history	(GtkClueHunter		*clue_hunter,
						 const gchar   		*string);
static void	gtk_clue_hunter_do_select_on	(GtkClueHunter		*clue_hunter,
						 const gchar		*string);
static void	gtk_clue_hunter_popdown		(GtkClueHunter		*clue_hunter);


/* --- variables --- */
static GtkWindowClass	  *parent_class = NULL;
static GtkClueHunterClass *gtk_clue_hunter_class = NULL;
static guint		   clue_hunter_signals[SIGNAL_LAST] = { 0, };


/* --- functions --- */
GtkType
gtk_clue_hunter_get_type (void)
{
  static GtkType clue_hunter_type = 0;
  
  if (!clue_hunter_type)
    {
      GtkTypeInfo clue_hunter_info =
      {
	"GtkClueHunter",
	sizeof (GtkClueHunter),
	sizeof (GtkClueHunterClass),
	(GtkClassInitFunc) gtk_clue_hunter_class_init,
	(GtkObjectInitFunc) gtk_clue_hunter_init,
	/* reserved_1 */ NULL,
	/* reserved_2 */ NULL,
	(GtkClassInitFunc) NULL,
      };
      
      clue_hunter_type = gtk_type_unique (GTK_TYPE_WINDOW, &clue_hunter_info);
    }
  
  return clue_hunter_type;
}

static void
gtk_clue_hunter_class_init (GtkClueHunterClass *class)
{
  GObjectClass *gobject_class = G_OBJECT_CLASS (class);
  GtkObjectClass *object_class = GTK_OBJECT_CLASS (class);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);

  gtk_clue_hunter_class = class;
  parent_class = g_type_class_peek_parent (class);
  
  gobject_class->finalize = gtk_clue_hunter_finalize;

  object_class->set_arg = gtk_clue_hunter_set_arg;
  object_class->get_arg = gtk_clue_hunter_get_arg;
  object_class->destroy = gtk_clue_hunter_destroy;
  
  widget_class->event = gtk_clue_hunter_event;
  
  class->activate = gtk_clue_hunter_do_activate;
  class->popup = gtk_clue_hunter_do_popup;
  class->popdown = gtk_clue_hunter_do_popdown;
  class->select_on = gtk_clue_hunter_do_select_on;

  g_object_class_install_property (gobject_class,
				   1024,
				   g_param_spec_enum ("type", NULL, NULL,
						      GTK_TYPE_WINDOW_TYPE,
						      GTK_WINDOW_POPUP,
						      0));
  gtk_object_add_arg_type ("GtkClueHunter::pattern_matching", GTK_TYPE_BOOL, GTK_ARG_READWRITE, ARG_PATTERN_MATCHING);
  gtk_object_add_arg_type ("GtkClueHunter::keep_history", GTK_TYPE_BOOL, GTK_ARG_READWRITE, ARG_KEEP_HISTORY);
  gtk_object_add_arg_type ("GtkClueHunter::align_width", GTK_TYPE_BOOL, GTK_ARG_READWRITE, ARG_ALIGN_WIDTH);
  gtk_object_add_arg_type ("GtkClueHunter::entry", GTK_TYPE_ENTRY, GTK_ARG_READWRITE, ARG_ENTRY);
  
  clue_hunter_signals[SIGNAL_ACTIVATE] =
    gtk_signal_new ("activate",
		    GTK_RUN_LAST,
		    GTK_CLASS_TYPE (object_class),
		    GTK_SIGNAL_OFFSET (GtkClueHunterClass, activate),
		    gtk_signal_default_marshaller,
		    GTK_TYPE_NONE, 0);
  clue_hunter_signals[SIGNAL_POPUP] =
    gtk_signal_new ("popup",
		    GTK_RUN_LAST,
		    GTK_CLASS_TYPE (object_class),
		    GTK_SIGNAL_OFFSET (GtkClueHunterClass, popup),
		    gtk_signal_default_marshaller,
		    GTK_TYPE_NONE, 0);
  clue_hunter_signals[SIGNAL_POPDOWN] =
    gtk_signal_new ("popdown",
		    GTK_RUN_FIRST,
		    GTK_CLASS_TYPE (object_class),
		    GTK_SIGNAL_OFFSET (GtkClueHunterClass, popdown),
		    gtk_signal_default_marshaller,
		    GTK_TYPE_NONE, 0);
  clue_hunter_signals[SIGNAL_SELECT_ON] =
    gtk_signal_new ("select_on",
		    GTK_RUN_LAST,
		    GTK_CLASS_TYPE (object_class),
		    GTK_SIGNAL_OFFSET (GtkClueHunterClass, select_on),
		    g_cclosure_marshal_VOID__STRING,
		    GTK_TYPE_NONE, 1,
		    GTK_TYPE_STRING);
  widget_class->activate_signal = clue_hunter_signals[SIGNAL_ACTIVATE];
}

static void
gtk_clue_hunter_set_arg (GtkObject *object,
			 GtkArg    *arg,
			 guint      arg_id)
{
  GtkClueHunter *clue_hunter;
  
  clue_hunter = GTK_CLUE_HUNTER (object);
  
  switch (arg_id)
    {
    case ARG_PATTERN_MATCHING:
      gtk_clue_hunter_set_pattern_matching (clue_hunter, GTK_VALUE_BOOL (*arg));
      break;
    case ARG_KEEP_HISTORY:
      gtk_clue_hunter_set_keep_history (clue_hunter, GTK_VALUE_BOOL (*arg));
      break;
    case ARG_ALIGN_WIDTH:
      gtk_clue_hunter_set_align_width (clue_hunter, GTK_VALUE_BOOL (*arg));
      break;
    case ARG_ENTRY:
      gtk_clue_hunter_set_entry (clue_hunter, GTK_VALUE_POINTER (*arg));
      break;
    default:
      break;
    }
}

static void
gtk_clue_hunter_get_arg (GtkObject *object,
			 GtkArg    *arg,
			 guint      arg_id)
{
  GtkClueHunter *clue_hunter;
  
  clue_hunter = GTK_CLUE_HUNTER (object);
  
  switch (arg_id)
    {
    case ARG_PATTERN_MATCHING:
      GTK_VALUE_BOOL (*arg) = clue_hunter->pattern_matching;
      break;
    case ARG_KEEP_HISTORY:
      GTK_VALUE_BOOL (*arg) = clue_hunter->keep_history;
      break;
    case ARG_ALIGN_WIDTH:
      GTK_VALUE_BOOL (*arg) = clue_hunter->align_width;
      break;
    case ARG_ENTRY:
      GTK_VALUE_POINTER (*arg) = clue_hunter->entry;
      break;
    default:
      arg->type = GTK_TYPE_INVALID;
      break;
    }
}

static void
gtk_clue_hunter_init (GtkClueHunter *clue_hunter)
{
  GtkWidget *parent;
  GtkWidget *clist;
  
  clue_hunter->popped_up = FALSE;
  clue_hunter->completion_tag = FALSE;
  clue_hunter->pattern_matching = TRUE;
  clue_hunter->keep_history = TRUE;
  clue_hunter->align_width = TRUE;
  clue_hunter->clist_column = 0;
  clue_hunter->cstring = NULL;
  
  GTK_WINDOW (clue_hunter)->type = GTK_WINDOW_POPUP;

  gtk_widget_set (GTK_WIDGET (clue_hunter),
		  "allow_shrink", FALSE,
		  "allow_grow", FALSE,
		  NULL);
  parent = GTK_WIDGET (clue_hunter);
  parent = gtk_widget_new (GTK_TYPE_FRAME,
			   "visible", TRUE,
			   "label", NULL,
			   "shadow", GTK_SHADOW_OUT,
			   "parent", parent,
			   NULL);
  clue_hunter->scw = gtk_widget_new (GTK_TYPE_SCROLLED_WINDOW,
				     "visible", TRUE,
				     "hscrollbar_policy", GTK_POLICY_AUTOMATIC,
				     "vscrollbar_policy", GTK_POLICY_AUTOMATIC,
				     "parent", parent,
				     NULL);
  clue_hunter->clist = NULL;
  clue_hunter->entry = NULL;
  clist = gtk_widget_new (GTK_TYPE_CLIST,
			  "n_columns", 1,
			  "titles_active", FALSE,
			  NULL);
  gtk_clist_set_auto_sort (GTK_CLIST (clist), TRUE);
  gtk_clist_set_sort_type (GTK_CLIST (clist), GTK_SORT_ASCENDING);
  gtk_clist_column_titles_hide (GTK_CLIST (clist));
  gtk_clue_hunter_set_clist (clue_hunter, clist, 0);
}

static void
gtk_clue_hunter_destroy (GtkObject *object)
{
  GtkClueHunter *clue_hunter;
  
  g_return_if_fail (object != NULL);
  g_return_if_fail (GTK_IS_CLUE_HUNTER (object));
  
  clue_hunter = GTK_CLUE_HUNTER (object);
  
  if (clue_hunter->popped_up)
    gtk_clue_hunter_popdown (clue_hunter);
  
  clue_hunter->scw = NULL;
  if (clue_hunter->clist)
    gtk_widget_unref (clue_hunter->clist);
  clue_hunter->clist = NULL;
  
  if (clue_hunter->entry)
    gtk_clue_hunter_set_entry (clue_hunter, NULL);
  
  GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static gint
gtk_clue_hunter_clist_click (GtkClueHunter  *clue_hunter,
			     GdkEvent	    *event,
			     GtkCList	    *clist)
{
  gboolean handled = FALSE;

  if (event->type == GDK_2BUTTON_PRESS &&
      event->button.button == 1 && clist->selection)
    {
      gchar *string;

      handled = TRUE;
      string = gtk_clue_hunter_try_complete (clue_hunter);
      gtk_entry_set_text (GTK_ENTRY (clue_hunter->entry), string ? string : "");
      g_free (string);

      gtk_clue_hunter_popdown (clue_hunter);
      gtk_widget_activate (clue_hunter->entry);
    }

  return handled;
}

void
gtk_clue_hunter_set_clist (GtkClueHunter *clue_hunter,
			   GtkWidget     *clist,
			   guint16	  column)
{
  g_return_if_fail (clue_hunter != NULL);
  g_return_if_fail (GTK_IS_CLUE_HUNTER (clue_hunter));
  g_return_if_fail (clist != NULL);
  g_return_if_fail (GTK_IS_CLIST (clist));
  g_return_if_fail (clist->parent == NULL);
  g_return_if_fail (column < GTK_CLIST (clist)->columns);

  if (clue_hunter->clist)
    {
      if (clue_hunter->clist->parent)
	gtk_container_remove (GTK_CONTAINER (clue_hunter->clist->parent), clue_hunter->clist);
      if (clue_hunter->clist)
	gtk_signal_disconnect_by_func (GTK_OBJECT (clue_hunter->clist),
				       GTK_SIGNAL_FUNC (gtk_clue_hunter_clist_click),
				       clue_hunter);
      gtk_widget_unref (clue_hunter->clist);
    }
  clue_hunter->clist = clist;
  gtk_widget_ref (clue_hunter->clist);
  g_object_set (clue_hunter->clist,
		"visible", TRUE,
		"selection_mode", GTK_SELECTION_EXTENDED,
		"parent", clue_hunter->scw,
		NULL);
  g_object_connect (clue_hunter->clist,
		    "swapped_signal::event-after", gtk_clue_hunter_clist_click, clue_hunter,
		    NULL);
  clue_hunter->clist_column = column;
}

static void
gtk_clue_hunter_popdown (GtkClueHunter *clue_hunter)
{
  g_return_if_fail (clue_hunter != NULL);
  g_return_if_fail (GTK_IS_CLUE_HUNTER (clue_hunter));
  g_return_if_fail (clue_hunter->popped_up == TRUE);
  
  gtk_signal_emit (GTK_OBJECT (clue_hunter), clue_hunter_signals[SIGNAL_POPDOWN]);
}

static void
gtk_clue_hunter_finalize (GObject *object)
{
  GtkClueHunter *clue_hunter;
  
  g_return_if_fail (object != NULL);
  g_return_if_fail (GTK_IS_CLUE_HUNTER (object));
  
  clue_hunter = GTK_CLUE_HUNTER (object);
  
  g_free (clue_hunter->cstring);
  
  G_OBJECT_CLASS (parent_class)->finalize (object);
}

void
gtk_clue_hunter_popup (GtkClueHunter *clue_hunter)
{
  g_return_if_fail (clue_hunter != NULL);
  g_return_if_fail (GTK_IS_CLUE_HUNTER (clue_hunter));
  g_return_if_fail (clue_hunter->popped_up == FALSE);
  
  if (clue_hunter->entry && GTK_WIDGET_DRAWABLE (clue_hunter->entry))
    gtk_signal_emit (GTK_OBJECT (clue_hunter), clue_hunter_signals[SIGNAL_POPUP]);
}

void
gtk_clue_hunter_select_on (GtkClueHunter *clue_hunter,
			   const gchar   *string)
{
  g_return_if_fail (clue_hunter != NULL);
  g_return_if_fail (GTK_IS_CLUE_HUNTER (clue_hunter));
  g_return_if_fail (string != NULL);
  
  gtk_signal_emit (GTK_OBJECT (clue_hunter), clue_hunter_signals[SIGNAL_SELECT_ON], string);
}

GtkWidget*
gtk_clue_hunter_create_arrow (GtkClueHunter *clue_hunter)
{
  GtkWidget *button, *arrow;

  g_return_val_if_fail (GTK_IS_CLUE_HUNTER (clue_hunter), NULL);

  arrow = gtk_widget_new (GTK_TYPE_ARROW,
			  "arrow_type", GTK_ARROW_DOWN,
			  "shadow_type", GTK_SHADOW_ETCHED_IN,
			  "visible", TRUE,
			  NULL);
  button = gtk_widget_new (GTK_TYPE_BUTTON,
			   "child", arrow,
			   "visible", TRUE,
			   "can_focus", FALSE,
			   NULL);
  gtk_signal_connect_object_while_alive (GTK_OBJECT (button),
					 "clicked",
					 GTK_SIGNAL_FUNC (gtk_clue_hunter_popup),
					 GTK_OBJECT (clue_hunter));

  return button;
}

static void
gtk_clue_hunter_entry_changed (GtkClueHunter *clue_hunter)
{
  clue_hunter->completion_tag = FALSE;
  g_free (clue_hunter->cstring);
  clue_hunter->cstring = g_strdup (gtk_entry_get_text (GTK_ENTRY (clue_hunter->entry)));
  gtk_clue_hunter_select_on (clue_hunter, clue_hunter->cstring);
}

static gint
gtk_clue_hunter_entry_key_press (GtkClueHunter *clue_hunter,
				 GdkEventKey   *event,
				 GtkEntry      *entry)
{
  gboolean handled = FALSE;
  
  if ((event->keyval == GDK_Tab || event->keyval == GDK_ISO_Left_Tab) &&
      !(event->state & (GDK_SHIFT_MASK | GDK_CONTROL_MASK | GDK_MOD1_MASK)))
    {
      handled = TRUE;
      
      if (event->type == GDK_KEY_PRESS)
	{
	  gchar *cstring;
	  gchar *ostring;
	  
	  cstring = gtk_clue_hunter_try_complete (clue_hunter);
	  ostring = gtk_entry_get_text (GTK_ENTRY (clue_hunter->entry));
	  if (!ostring)
	    ostring = "";
	  if (cstring && strcmp (ostring, cstring))
	    {
	      gtk_entry_set_text (GTK_ENTRY (clue_hunter->entry), cstring);
	      clue_hunter->completion_tag = clue_hunter->popped_up;
	      gtk_entry_set_position (GTK_ENTRY (entry), -1);
	    }
	  else
	    {
	      if (clue_hunter->completion_tag)
		gtk_widget_activate (GTK_WIDGET (clue_hunter));
	      else
		clue_hunter->completion_tag = TRUE;
	    }
	  g_free (cstring);
	}
    }
  
  return handled;
}

static void
gtk_clue_hunter_entry_destroyed (GtkClueHunter *clue_hunter)
{
  gtk_object_destroy (GTK_OBJECT (clue_hunter));
}

void
gtk_clue_hunter_set_entry (GtkClueHunter *clue_hunter,
			   GtkWidget     *entry)
{
  g_return_if_fail (clue_hunter != NULL);
  g_return_if_fail (GTK_IS_CLUE_HUNTER (clue_hunter));
  if (entry)
    {
      g_return_if_fail (GTK_IS_ENTRY (entry));
      g_return_if_fail (gtk_clue_hunter_from_entry (entry) == NULL);
    }
  
  if (clue_hunter->entry)
    {
      if (!clue_hunter->entry)
	{
	  gtk_signal_disconnect_by_func (GTK_OBJECT (clue_hunter->entry),
					 GTK_SIGNAL_FUNC (gtk_clue_hunter_entry_changed),
					 clue_hunter);
	  gtk_signal_disconnect_by_func (GTK_OBJECT (clue_hunter->entry),
					 GTK_SIGNAL_FUNC (gtk_clue_hunter_entry_key_press),
					 clue_hunter);
	  gtk_signal_disconnect_by_func (GTK_OBJECT (clue_hunter->entry),
					 GTK_SIGNAL_FUNC (gtk_clue_hunter_entry_destroyed),
					 clue_hunter);
	}
      gtk_object_set_data (GTK_OBJECT (clue_hunter->entry), "GtkClueHunter", NULL);
      gtk_widget_unref (clue_hunter->entry);
    }
  clue_hunter->entry = entry;
  if (clue_hunter->entry)
    {
      gtk_widget_ref (clue_hunter->entry);
      gtk_object_set_data (GTK_OBJECT (clue_hunter->entry), "GtkClueHunter", clue_hunter);
      gtk_signal_connect_object (GTK_OBJECT (clue_hunter->entry),
				 "destroy",
				 GTK_SIGNAL_FUNC (gtk_clue_hunter_entry_destroyed),
				 GTK_OBJECT (clue_hunter));
      gtk_signal_connect_object (GTK_OBJECT (clue_hunter->entry),
				 "changed",
				 GTK_SIGNAL_FUNC (gtk_clue_hunter_entry_changed),
				 GTK_OBJECT (clue_hunter));
      gtk_signal_connect_object (GTK_OBJECT (clue_hunter->entry),
				 "key_press_event",
				 GTK_SIGNAL_FUNC (gtk_clue_hunter_entry_key_press),
				 GTK_OBJECT (clue_hunter));
    }
  clue_hunter->completion_tag = FALSE;
}

GtkClueHunter*
gtk_clue_hunter_from_entry (GtkWidget *entry)
{
  g_return_val_if_fail (GTK_IS_ENTRY (entry), NULL);

  return gtk_object_get_data (GTK_OBJECT (entry), "GtkClueHunter");
}

void
gtk_clue_hunter_add_string (GtkClueHunter *clue_hunter,
			    const gchar   *string)
{
  GtkCList *clist;
  gchar **text;
  
  g_return_if_fail (clue_hunter != NULL);
  g_return_if_fail (GTK_IS_CLUE_HUNTER (clue_hunter));
  g_return_if_fail (string != NULL);
  
  clist = GTK_CLIST (clue_hunter->clist);
  
  text = g_new0 (gchar*, clist->columns);
  text[clue_hunter->clist_column] = (gchar*) string;

  gtk_clist_insert (clist, 0, text);
  g_free (text);
}

void
gtk_clue_hunter_remove_string (GtkClueHunter *clue_hunter,
			       const gchar   *string)
{
  GtkCList *clist;
  GList *list;
  guint n = 0;
  
  g_return_if_fail (clue_hunter != NULL);
  g_return_if_fail (GTK_IS_CLUE_HUNTER (clue_hunter));
  g_return_if_fail (string != NULL);
  
  clist = GTK_CLIST (clue_hunter->clist);
  
  for (list = clist->row_list; list; list = list->next)
    {
      GtkCListRow *clist_row = list->data;
      gint cmp;
      
      cmp = strcmp (string, clist_row->cell[clue_hunter->clist_column].u.text);
      if (cmp == 0)
	{
	  gtk_clist_remove (clist, n);
	  break;
	}
      n++;
    }
}

void
gtk_clue_hunter_remove_matches (GtkClueHunter *clue_hunter,
				const gchar   *pattern)
{
  GPatternSpec *pspec;
  GtkCList *clist;
  GList *list;
  guint n = 0;
  
  g_return_if_fail (clue_hunter != NULL);
  g_return_if_fail (GTK_IS_CLUE_HUNTER (clue_hunter));
  if (!pattern)
    pattern = "*";
  
  pspec = g_pattern_spec_new (pattern);
  
  clist = GTK_CLIST (clue_hunter->clist);
  
  gtk_clist_freeze (clist);

  list = clist->row_list;
  while (list)
    {
      GtkCListRow *clist_row = list->data;

      list = list->next;
      
      if (g_pattern_match_string (pspec, clist_row->cell[clue_hunter->clist_column].u.text))
	gtk_clist_remove (clist, n);
      else
	n++;
    }

  gtk_clist_thaw (clist);

  g_pattern_spec_free (pspec);
}

static gchar*
gtk_clue_hunter_intersect (guint   max_len,
			   GSList *strings)
{
  gchar *completion;
  guint l = 0;
  
  if (!strings || !max_len)
    return NULL;
  
  completion = g_new (gchar, max_len + 1);
  
  while (l < max_len)
    {
      gchar *s = strings->data;
      GSList *slist;
      
      s += l;
      completion[l] = *s;
      
      for (slist = strings->next; slist; slist = slist->next)
	{
	  s = slist->data;
	  s += l;
	  if (completion[l] != *s)
	    completion[l] = 0;
	}
      if (!completion[l])
	break;
      l++;
    }
  completion[l] = 0;
  
  return g_renew (gchar, completion, completion[0] ? l + 1 : 0);
}

gchar*
gtk_clue_hunter_try_complete (GtkClueHunter *clue_hunter)
{
  GtkCList *clist;
  GList *list;
  GSList *strings;
  guint max_len, n;
  gchar *completion;
  
  g_return_val_if_fail (clue_hunter != NULL, NULL);
  g_return_val_if_fail (GTK_IS_CLUE_HUNTER (clue_hunter), NULL);
  
  clist = GTK_CLIST (clue_hunter->clist);
  
  strings = NULL;
  max_len = 0;
  n = 0;
  for (list = clist->row_list; list; list = list->next)
    {
      GtkCListRow *clist_row = list->data;
      
      if (g_list_find (clist->selection, GINT_TO_POINTER (n)))
	{
	  guint l;
	  
	  l = strlen (clist_row->cell[clue_hunter->clist_column].u.text);
	  max_len = MAX (max_len, l);
	  strings = g_slist_prepend (strings, clist_row->cell[clue_hunter->clist_column].u.text);
	}
      n++;
    }
  
  completion = gtk_clue_hunter_intersect (max_len, strings);
  g_slist_free (strings);
  
  return completion;
}

void
gtk_clue_hunter_set_pattern_matching (GtkClueHunter *clue_hunter,
				      gboolean       on_off)
{
  g_return_if_fail (clue_hunter != NULL);
  g_return_if_fail (GTK_IS_CLUE_HUNTER (clue_hunter));
  
  clue_hunter->pattern_matching = on_off != FALSE;
}

void
gtk_clue_hunter_set_keep_history (GtkClueHunter *clue_hunter,
				  gboolean       on_off)
{
  g_return_if_fail (clue_hunter != NULL);
  g_return_if_fail (GTK_IS_CLUE_HUNTER (clue_hunter));
  
  clue_hunter->keep_history = on_off != FALSE;
}

void
gtk_clue_hunter_set_align_width (GtkClueHunter *clue_hunter,
				 gboolean       on_off)
{
  g_return_if_fail (clue_hunter != NULL);
  g_return_if_fail (GTK_IS_CLUE_HUNTER (clue_hunter));
  
  clue_hunter->align_width = on_off != FALSE;
}

static void
gtk_clue_hunter_do_activate (GtkClueHunter *clue_hunter)
{
  if (clue_hunter->popped_up)
    gtk_clue_hunter_popdown (clue_hunter);
  else if (clue_hunter->entry)
    gtk_clue_hunter_popup (clue_hunter);
}

static void
gtk_clue_hunter_do_popup (GtkClueHunter *clue_hunter)
{
  GtkWidget *widget;
  gint x = 0, y = 0, width = 0, height = 0;
  
  g_return_if_fail (!clue_hunter->popped_up);
  
  widget = GTK_WIDGET (clue_hunter);
  
  gtk_widget_grab_focus (clue_hunter->entry);

  if (!clue_hunter->cstring)
    clue_hunter->cstring = g_strdup ("");

  gtk_clist_columns_autosize (GTK_CLIST (clue_hunter->clist));
  gtk_widget_queue_resize (clue_hunter->clist);	/* work around gtk+ optimizations */
  gtk_widget_size_request (clue_hunter->clist, NULL);
  gtk_widget_set_size_request (clue_hunter->clist,
			       clue_hunter->clist->requisition.width,
			       clue_hunter->clist->requisition.height +
			       2 * widget->style->ythickness);
  g_print ("clist height: %d\n", clue_hunter->clist->requisition.height);

  gdk_window_get_origin (clue_hunter->entry->window, &x, &y);
  gdk_window_get_size (clue_hunter->entry->window, &width, &height);

  height = MIN (height, gdk_screen_height ());
  if (y < 0)
    {
      height = MAX (0, height + y);
      y = 0;
    }
  else if (y > gdk_screen_height ())
    {
      height = 0;
      y = gdk_screen_height ();
    }
  else if (y + height > gdk_screen_height ())
    height = gdk_screen_height () - y;
  width = MIN (width, gdk_screen_width ());
  x = CLAMP (x, 0, gdk_screen_width () - width);
  
  if (widget->requisition.height > gdk_screen_height () - (y + height))
    {
      if (y + height / 2 > gdk_screen_height () / 2)
	{
	  height = MIN (y, widget->requisition.height);
	  y -= height;
	}
      else
	{
	  y += height;
	  height = gdk_screen_height () - y;
	}
    }
  else
    {
      y += height;
      height = -1;
    }
  
  if (!clue_hunter->align_width && widget->requisition.width > width)
    {
      if (widget->requisition.width <= gdk_screen_width () - x)
	width = widget->requisition.width;
      else if (x + width / 2 > gdk_screen_width () / 2)
	{
	  x += width;
	  width = MIN (x, widget->requisition.width);
	  x -= width;
	}
      else
	width = MIN (gdk_screen_width () - x, widget->requisition.width);
    }

  gtk_widget_set_uposition (widget, x, y);
  gtk_widget_set_size_request (widget, width, height);

  gtk_grab_add (widget);
  
  gtk_widget_grab_focus (clue_hunter->clist);
  
  clue_hunter->popped_up = TRUE;
  clue_hunter->completion_tag = FALSE;
  
  gtk_clue_hunter_select_on (clue_hunter, clue_hunter->cstring);
  
  gtk_widget_show (widget);
  
  while (gdk_pointer_grab (widget->window, TRUE,
			   (GDK_POINTER_MOTION_HINT_MASK |
			    GDK_BUTTON1_MOTION_MASK |
			    GDK_BUTTON2_MOTION_MASK |
			    GDK_BUTTON3_MOTION_MASK |
			    GDK_BUTTON_PRESS_MASK |
			    GDK_BUTTON_RELEASE_MASK),
			   NULL,
			   NULL,
			   GDK_CURRENT_TIME) != 0)
    ;
}

static void
gtk_clue_hunter_do_popdown (GtkClueHunter *clue_hunter)
{
  GtkWidget *widget;
  
  g_return_if_fail (clue_hunter->popped_up);
  
  widget = GTK_WIDGET (clue_hunter);
  
  /* gdk_pointer_ungrab (GDK_CURRENT_TIME); */
  gtk_widget_hide (widget);

  gdk_flush ();
  
  gtk_grab_remove (widget);
  
  clue_hunter->popped_up = FALSE;
  clue_hunter->completion_tag = FALSE;
}

static void
gtk_clue_hunter_add_history (GtkClueHunter *clue_hunter,
			     const gchar   *string)
{
  GtkCList *clist;
  GList *list;

  clist = GTK_CLIST (clue_hunter->clist);

  for (list = clist->row_list; list; list = list->next)
    {
      GtkCListRow *clist_row = list->data;
      
      if (strcmp (string, clist_row->cell[clue_hunter->clist_column].u.text) == 0)
	return;
    }
  gtk_clue_hunter_add_string (clue_hunter, string);
}

static void
gtk_clue_hunter_do_select_on (GtkClueHunter *clue_hunter,
			      const gchar   *cstring)
{
  GtkCList *clist;
  GList *list;
  guint len;
  
  clist = GTK_CLIST (clue_hunter->clist);
  
  len = strlen (cstring);
  
  gtk_clist_freeze (clist);
  
  gtk_clist_undo_selection (clist);
  gtk_clist_unselect_all (clist);
  
  if (len && clue_hunter->pattern_matching)
    {
      GPatternSpec *pspec;
      guint n = 0;
      gboolean check_visibility = TRUE;
      gchar *pattern;
      
      pattern = g_strconcat (cstring, "*", NULL);
      pspec = g_pattern_spec_new (pattern);
      g_free (pattern);
      
      for (list = clist->row_list; list; list = list->next)
	{
	  GtkCListRow *clist_row = list->data;
	  
	  if (g_pattern_match_string (pspec, clist_row->cell[clue_hunter->clist_column].u.text))
	    {
	      gtk_clist_select_row (clist, n, 0);
	      
	      if (check_visibility &&
		  gtk_clist_row_is_visible (clist, n) != GTK_VISIBILITY_FULL)
		gtk_clist_moveto (clist, n, -1, 0.5, 0);
	      check_visibility = FALSE;
	    }
	  n++;
	}
      g_pattern_spec_free (pspec);
    }
  else if (len)
    {
      guint n = 0;
      gboolean check_visibility = TRUE;
      
      for (list = clist->row_list; list; list = list->next)
	{
	  GtkCListRow *clist_row = list->data;
	  gint cmp;
	  
	  cmp = strncmp (cstring, clist_row->cell[clue_hunter->clist_column].u.text, len);
	  if (cmp == 0)
	    {
	      gtk_clist_select_row (clist, n, 0);
	      
	      if (check_visibility &&
		  gtk_clist_row_is_visible (clist, n) != GTK_VISIBILITY_FULL)
		gtk_clist_moveto (clist, n, -1, 0.5, 0);
	      check_visibility = FALSE;
	    }
	  n++;
	}
    }
  
  gtk_clist_thaw (clist);
}

static gint
gtk_clue_hunter_event (GtkWidget *widget,
		       GdkEvent  *event)
{
  GtkClueHunter *clue_hunter;
  gboolean handled = FALSE;
  
  clue_hunter = GTK_CLUE_HUNTER (widget);
  switch (event->type)
    {
      GtkWidget *ev_widget;
      guint n_retries;
    case GDK_KEY_PRESS:
      if (event->key.keyval == GDK_Escape)
	{
	  handled = TRUE;
	  gtk_clue_hunter_popdown (clue_hunter);
	}
      else if (event->key.keyval == GDK_Return ||
	       event->key.keyval == GDK_KP_Enter)
	{
	  gchar *string;
	  
	  handled = TRUE;
	  string = gtk_clue_hunter_try_complete (clue_hunter);
	  if (string)
	    {
	      if (string[0])
		gtk_entry_set_text (GTK_ENTRY (clue_hunter->entry), string);
	      g_free (string);
	    }
	  else if (clue_hunter->keep_history)
	    {
	      string = gtk_entry_get_text (GTK_ENTRY (clue_hunter->entry));
	      if (string && string[0])
		gtk_clue_hunter_add_history (clue_hunter, string);
	    }
	  gtk_clue_hunter_popdown (clue_hunter);
	  if (string)
	    gtk_widget_activate (clue_hunter->entry);
	}
      else
	handled = gtk_widget_event (clue_hunter->entry, event);
      break;
      
    case GDK_KEY_RELEASE:
      if (event->key.keyval == GDK_Escape ||
	  event->key.keyval == GDK_Return ||
	  event->key.keyval == GDK_KP_Enter)
	handled = TRUE;
      else
	handled = gtk_widget_event (clue_hunter->entry, event);
      break;
      
    case GDK_BUTTON_PRESS:
    case GDK_BUTTON_RELEASE:
      n_retries = 0;
      while (gdk_pointer_grab (widget->window, TRUE,
			       (GDK_POINTER_MOTION_HINT_MASK |
				GDK_BUTTON1_MOTION_MASK |
				GDK_BUTTON2_MOTION_MASK |
				GDK_BUTTON3_MOTION_MASK |
				GDK_BUTTON_PRESS_MASK |
				GDK_BUTTON_RELEASE_MASK),
			       NULL,
			       NULL,
			       GDK_CURRENT_TIME) != 0)
	{
	  if (n_retries++ > 5)
	    {
	      g_warning ("unable to get Xserver pointer grab");
	      n_retries = 100;
	      break;
	    }
	}
      ev_widget = n_retries < 100 ? gtk_get_event_widget (event) : NULL;
      if (ev_widget == widget &&
	  event->type == GDK_BUTTON_PRESS)
	{
	  gint w, h;
	  
	  gdk_window_get_size (widget->window, &w, &h);
	  if (event->button.x > w || event->button.y > h ||
	      event->button.x < 0 || event->button.y < 0)
	    ev_widget = NULL;
	}
      else if (ev_widget)
	while (ev_widget->parent)
	  ev_widget = ev_widget->parent;
      if (ev_widget != widget)
	{
	  gtk_clue_hunter_popdown (clue_hunter);
	  handled = TRUE;
	}
      break;
      
    case GDK_DELETE:
      gtk_clue_hunter_popdown (clue_hunter);
      handled = TRUE;
      break;
      
    default:
      break;
    }
  
  return handled;
}
