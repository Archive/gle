/* GLE - GObject Layout Editor
 * Copyright (C) 1998-1999, 2001-2002 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 59 Temple Place - Suite 330,
 * Boston, MA 02111-1307, USA.
 */
#include	<gle/gleconfig.h>

#include	"gleshell.h"
#include	"gleeditor.h"
#include	"glestock.h"
#include	"gleted.h"
#include	"gleselector.h"
#include	"gledocu.inc"
#include	<gdk/gdk.h>
#include	<string.h>
#include	<stdio.h>



/* --- prototypes --- */
static void		gle_shell_class_init		(GleShellClass	*class);
static void		gle_shell_init			(GleShell	*shell);
static void		gle_shell_destroy		(GtkObject	*object);
static void		gle_shell_realize		(GtkWidget	*widget);
static void		gle_shell_unrealize		(GtkWidget	*widget);
static gint		gle_shell_configure_event	(GtkWidget	*widget,
							 GdkEventConfigure *event);
static gint		gle_shell_delete_event		(GtkWidget	*widget,
							 GdkEventAny	*dummy);
static void		gle_shell_do_selection		(GleShell	*shell);
static void		gle_shell_candidate_check	(GtkObject	*caller,
							 GtkWidget     **new_candidate,
							 gint		*candidate_ok);
static gint		gle_shell_plist_button_press	(GleShell	*shell,
							 GdkEventButton *event);
static GQuark		gle_shell_plist_check_tag	(GleShell	*shell,
							 GleProxy	*proxy);
static void		gle_shell_popup_finish		(GleShell	*shell);
static gboolean		gle_shell_popup_check		(GleShell	*shell,
							 GleProxySet	*pset,
							 guint		 pop);
static void		gle_shell_popup_operation	(GleShell	*shell,
							 guint		 shell_op,
							 GtkWidget	*item);


/* --- variables --- */
static GtkWindowClass	*parent_class = NULL;
static GleShellClass	*gle_shell_class = NULL;
static GleShell		*gle_shell = NULL;
static gchar		*gle_rc_file = NULL;
static const gchar *gle_rc_string =
( "style'GLE-DefaultStyle'"
  "{"
  "font='-adobe-helvetica-medium-r-normal--*-120-*-*-*-*-*-*'"
  "fg[NORMAL]={0.,0.,0.}"
  "fg[ACTIVE]={0.,0.,0.}"
  "fg[PRELIGHT]={0.,0.,0.}"
  "fg[SELECTED]={1.,1.,1.}"
  "fg[INSENSITIVE]={.457,.457,.457}"
  "bg[NORMAL]={.83,.83,.87}"
  "bg[ACTIVE]={.76,.76,.80}"
  "bg[PRELIGHT]={.91,.91,.95}"
  "bg[SELECTED]={0.,0.,.65}"
  "bg[INSENSITIVE]={.83,.83,.87}"
  "base[NORMAL]={1.,1.,1.}"
  "base[PRELIGHT]={1.,0.,0.}"
  "base[INSENSITIVE]={.83,.83,.87}"
  "text[INSENSITIVE]={.83,.83,.87}"
  "}"
  /* catch normal GLE dialogs */
  "widget'GLE-*'style'GLE-DefaultStyle'"
  /* catch ordinary GLE menus */
  "widget'GtkWindow.GLE-*'style'GLE-DefaultStyle'"
  /* catch GLE item factory menus */
  "widget'GtkWindow.<GLE-*'style'GLE-DefaultStyle'"
  "" );

/* --- menus --- */
static gchar	*gle_shell_factories_path = "<GLE-GleShell>";
static GtkItemFactoryEntry menubar_entries[] =
{
#define SHELL_OP(shell_op)	(gle_shell_operation), (GLE_SHELL_OP_ ## shell_op)
  { "/_File/_New",	"<ctrl>N",	SHELL_OP (NONE),	"<Item>" },
  { "/File/_Open...",	"<ctrl>O",	SHELL_OP (OPEN),	"<Item>" },
  { "/File/_Save...",	"<ctrl>S",	SHELL_OP (SAVE),	"<Item>" },
  // { "/File/-----",	"",		NULL, 0,		"<Separator>" },
  // { "/File/_Preferences...", "",	SHELL_OP (RC_PREF),	"<Item>" },
  { "/File/-----",	"",		NULL, 0,		"<Separator>" },
  { "/File/_Quit GLE",	"<ctrl>Q",	SHELL_OP (DELETE),	"<Item>" },
  // { "/_Dialogs/_Widget Palette...", "",	SHELL_OP (WPALETTE),	"<Item>" },
  // { "/_Proxy/_New Window", "",		SHELL_OP (NEW_WINDOW),	NULL,	},
  { "/_Help",		"",		NULL,	0,		"<LastBranch>" },
  { "/Help/_Intro...",	"",		SHELL_OP (HELP_INTRO),	"<Item>" },
  { "/Help/_About...",	"",		SHELL_OP (HELP_ABOUT),	"<Item>" },
#undef	SHELL_OP
};
static guint n_menubar_entries = sizeof (menubar_entries) / sizeof (menubar_entries[0]);
enum
{
  POPUP_OP_NONE,
  POPUP_OP_P_TED,
  POPUP_OP_P_RERIP,
  POPUP_OP_LAST
};
static GtkItemFactoryEntry popup_entries[] =
{
#define POP(popup_op)	(gle_shell_popup_operation), (POPUP_OP_ ## popup_op)
  { "/Proxy Operations", "",		POP (NONE),			"<Title>" },
  { "/---",		  "",		NULL, 0,			"<Separator>" },
  { "/Tree Editor...",	"<ctrl>T",	POP (P_TED),			NULL },
  { "/Rebuild Tree",	"<ctrl>T",	POP (P_RERIP),			NULL },
#if 0
  { "/Instantiate",	NULL,		POP (P_INSTANTIATE),		NULL },
  { "/Disassociate",	NULL,		POP (P_DISASSOCIATE),		NULL },
  { "/Stdout Dump",	NULL,		POP (P_DUMP),			NULL },
  { "/PP",		NULL,		POP (P_PP),			NULL },
  { "/Destroy",		NULL,		POP (P_DESTROY),		NULL },
  { "/---",		"",		NULL, 0,			"<Separator>" },
  { "/Widget Operations", "",		POP (NONE),			"<Title>" },
  { "/---",		"",		NULL, 0,			"<Separator>" },
  { "/Show Widget",	"<ctrl>S",	POP (W_SHOW),			NULL },
  { "/Hide Widget",	"<ctrl>H",	POP (W_HIDE),			NULL },
  { "/Destroy Widget",	NULL,		POP (W_DESTROY),		NULL },
#endif
#undef	POP
};
static guint n_popup_entries = (sizeof (popup_entries) / sizeof (popup_entries[0]));


/* --- functions --- */
GtkType
gle_shell_get_type (void)
{
  static GtkType shell_type = 0;
  
  if (!shell_type)
    {
      GtkTypeInfo shell_info =
      {
	"GleShell",
	sizeof (GleShell),
	sizeof (GleShellClass),
	(GtkClassInitFunc) gle_shell_class_init,
	(GtkObjectInitFunc) gle_shell_init,
	/* reserved_1 */ NULL,
	/* reserved_2 */ NULL,
	(GtkClassInitFunc) NULL,
      };
      
      shell_type = gtk_type_unique (GTK_TYPE_WINDOW, &shell_info);
    }
  
  return shell_type;
}

static void
gle_shell_class_init (GleShellClass *class)
{
  GtkObjectClass *object_class = GTK_OBJECT_CLASS (class);
  GtkWidgetClass *widget_class = GTK_WIDGET_CLASS (class);
  
  parent_class = g_type_class_peek_parent (class);
  gle_shell_class = class;
  
  object_class->destroy = gle_shell_destroy;
  
  widget_class->realize = gle_shell_realize;
  widget_class->unrealize = gle_shell_unrealize;
  widget_class->delete_event = gle_shell_delete_event;
  widget_class->configure_event = gle_shell_configure_event;
  
  class->factories_path = gle_shell_factories_path;
  
  gtk_rc_parse_string (gle_rc_string);
}

static void
gle_shell_init (GleShell *shell)
{
  GtkWidget *shell_vbox;
  GtkWidget *main_vbox;
  GtkWidget *hbox;
  GtkWidget *button;
  GtkWidget *any;
  gchar *string;
  
  if (gle_shell)
    g_error ("GleShell is a singleton");
  else
    gle_shell = shell;
  
  GLE_TAG (shell);
  
  /* structure setup */
  shell->plist = NULL;
  shell->update_button = NULL;
  shell->selector_button = NULL;
  shell->selector = NULL;
  shell->menubar_factory = NULL;
  shell->popup_factory = NULL;
  shell->popup_data = NULL;
  shell->rc_pref = NULL;
  shell->wpalette = NULL;
  shell->file_open_dialog = NULL;
  shell->file_save_dialog = NULL;
  shell->help_intro = NULL;
  shell->help_about = NULL;
  
#if 0
  /* GLE rc parsing
   */
  if (!gle_rc_file)
    {
      gle_rc_args_init ();
      gle_rc_file = g_strconcat (g_get_home_dir (), "/.glerc", NULL);
    }
  gle_rc_parse (gle_rc, gle_rc_file);
#endif
  
  /* create the popup factory
   */
  shell->popup_factory =
    gtk_item_factory_new (GTK_TYPE_MENU,
			  gle_shell_class->factories_path,
			  NULL);
  GLE_TAG (shell->popup_factory);
  gtk_signal_connect (GTK_OBJECT (shell->popup_factory),
		      "destroy",
		      GTK_SIGNAL_FUNC (gtk_widget_destroyed),
		      &shell->popup_factory);
  gtk_item_factory_create_items (shell->popup_factory,
				 n_popup_entries,
				 popup_entries,
				 shell);
  gtk_window_add_accel_group (GTK_WINDOW (shell), shell->popup_factory->accel_group);
  
  /* create GUI
   */
  string = g_strconcat ("GLE-Shell", "	 [", g_get_prgname (), "]", NULL);
  g_object_set (GTK_WIDGET (shell),
		"title", string,
		"type", GTK_WINDOW_TOPLEVEL,
		"allow_shrink", TRUE,
		"allow_grow", TRUE,
		NULL);
  g_free (string);
  gtk_widget_set_usize (GTK_WIDGET (shell),
			GLE_RCVAL_SHELL_WIDTH > 0 ? GLE_RCVAL_SHELL_WIDTH : -1,
			GLE_RCVAL_SHELL_HEIGHT > 0 ? GLE_RCVAL_SHELL_HEIGHT : -1);
  if (GLE_RCVAL_SHELL_USE_POSITION)
    gtk_widget_set_uposition (GTK_WIDGET (shell),
			      GLE_RCVAL_SHELL_INIT_X > 0 ? GLE_RCVAL_SHELL_INIT_X : -1,
			      GLE_RCVAL_SHELL_INIT_Y > 0 ? GLE_RCVAL_SHELL_INIT_Y : -1);
  shell_vbox = g_object_new (GTK_TYPE_VBOX,
			     "homogeneous", FALSE,
			     "spacing", 0,
			     "border_width", 0,
			     "parent", shell,
			     "visible", TRUE,
			     NULL);
  main_vbox = g_object_new (GTK_TYPE_VBOX,
			    "homogeneous", FALSE,
			    "spacing", 5,
			    "border_width", 5,
			    "parent", shell_vbox,
			    "visible", TRUE,
			    NULL);
  gtk_box_set_child_packing (GTK_BOX (shell_vbox), main_vbox, TRUE, TRUE, 0, GTK_PACK_START);
  
  /* toplevel proxy list
   */
  shell->plist = g_object_connect (g_object_new (GLE_TYPE_PLIST,
						 "visible", TRUE,
						 NULL),
				   "swapped_signal::button-press-event", gle_shell_plist_button_press, shell,
				   "swapped_signal::check-tag", gle_shell_plist_check_tag, shell,
				   "signal::destroy", gtk_widget_destroyed, &shell->plist,
				   NULL);
  any = g_object_new (GTK_TYPE_SCROLLED_WINDOW,
		      "visible", TRUE,
		      "hscrollbar_policy", GTK_POLICY_AUTOMATIC,
		      "vscrollbar_policy", GTK_POLICY_AUTOMATIC,
		      "parent", main_vbox,
		      NULL);
  gtk_container_add (GTK_CONTAINER (any), GTK_WIDGET (shell->plist));
  
  /* buttons
   */
  hbox = g_object_new (GTK_TYPE_HBOX,
		       "visible", TRUE,
		       "homogeneous", TRUE,
		       "spacing", 5,
		       "border_width", 0,
		       NULL);
  gtk_box_pack_start (GTK_BOX (main_vbox), hbox, FALSE, TRUE, 0);
  shell->update_button = g_object_connect (g_object_new (GTK_TYPE_BUTTON,
							 "visible", FALSE,
							 "label", "Update",
							 "parent", hbox,
							 NULL),
					   "swapped_signal::clicked", gle_plist_update_all, shell->plist,
					   NULL);
  shell->selector_button = g_object_connect (g_object_new (GTK_TYPE_BUTTON,
							   "visible", TRUE,
							   "label", "Selector",
							   "parent", hbox,
							   NULL),
					     "swapped_signal::clicked", gle_shell_do_selection, shell,
					     NULL);
  any = g_object_new (GTK_TYPE_HSEPARATOR,
		      "visible", TRUE,
		      NULL);
  gtk_box_pack_start (GTK_BOX (shell_vbox), any, FALSE, TRUE, 0);
  button = g_object_connect (g_object_new (GTK_TYPE_BUTTON,
					   "visible", TRUE,
					   "parent", shell_vbox,
					   "label", "Close",
					   "border_width", 5,
					   "can_default", TRUE,
					   "has_default", TRUE,
					   NULL),
			     "swapped_signal::clicked", gle_shell_delete_event, shell,
			     NULL);
  gtk_box_set_child_packing (GTK_BOX (shell_vbox), button, FALSE, TRUE, 0, GTK_PACK_START);
  
  /* MenuBar
   */
  shell->menubar_factory = gtk_item_factory_new (GTK_TYPE_MENU_BAR,
						 gle_shell_factories_path,
						 NULL);
  GLE_TAG (shell->menubar_factory);
  gtk_window_add_accel_group (GTK_WINDOW (shell),
			      GTK_ITEM_FACTORY (shell->menubar_factory)->accel_group);
  gtk_item_factory_create_items (GTK_ITEM_FACTORY (shell->menubar_factory),
				 n_menubar_entries,
				 menubar_entries,
				 shell);
  gtk_container_add_with_properties (GTK_CONTAINER (shell_vbox),
				     GTK_ITEM_FACTORY (shell->menubar_factory)->widget,
				     "expand", FALSE,
				     "position", 0,
				     NULL);
  gtk_widget_show (GTK_ITEM_FACTORY (shell->menubar_factory)->widget);
  gtk_signal_connect (GTK_OBJECT (shell->menubar_factory),
		      "destroy",
		      GTK_SIGNAL_FUNC (gtk_widget_destroyed),
		      &shell->menubar_factory);
  
  /* setup the GLE selector
   */
  shell->selector = gle_selector_new ("GLE Selector", "Select a window by clicking");
  GLE_TAG (shell->selector);
  GLE_TAG (GLE_SELECTOR (shell->selector)->warning_window);
  g_object_connect (shell->selector,
		    "swapped_signal::candidate_check", gle_shell_candidate_check, shell,
		    "signal::destroy", gtk_widget_destroyed, &shell->selector,
		    NULL);
}

static void
gle_shell_destroy (GtkObject *object)
{
  GleShell *shell = GLE_SHELL (object);

  if (gle_shell)
    g_assert (shell == gle_shell);
  
  gle_util_set_flash_widget (NULL);
  gdk_flush ();

  if (shell->plist)
    gle_plist_clear (shell->plist);
  
  /* get rid of the menu factories */
  if (shell->popup_factory)
    {
      g_object_unref (shell->popup_factory);
      shell->popup_factory = NULL;
      g_object_unref (shell->menubar_factory);
      shell->menubar_factory = NULL;
    }
  
  /* destroy associated dialogs */
  if (shell->rc_pref)
    gtk_widget_destroy (shell->rc_pref);
  if (shell->wpalette)
    gtk_widget_destroy (shell->wpalette);
  if (shell->file_open_dialog)
    gtk_widget_destroy (shell->file_open_dialog);
  if (shell->file_save_dialog)
    gtk_widget_destroy (shell->file_save_dialog);
  
  if (shell->selector)
    gtk_widget_destroy (shell->selector);

  if (gle_shell)
    {
#if 0
      /* save ~/.glerc
       */
      if (GLE_RCVAL_AUTO_SAVE_GLERC)
	gle_rc_dump (gle_rc, gle_rc_file);
#endif
      gle_shell = NULL;
    }
  
  GTK_OBJECT_CLASS (parent_class)->destroy (object);
}

static gint
gle_shell_delete_event (GtkWidget   *widget,
			GdkEventAny *dummy)
{
  g_return_val_if_fail (widget != NULL, TRUE);
  g_return_val_if_fail (GLE_IS_SHELL (widget), TRUE);
  
  /* FIXME: need "want to save" popup here */
  
  gle_shell_operation (GLE_SHELL (widget), GLE_SHELL_OP_DELETE);
  
  return TRUE;
}

static gint
gle_shell_configure_event (GtkWidget	       *widget,
			   GdkEventConfigure   *event)
{
#if 0
  if (GLE_RCVAL_SHELL_TRACK_MOVEMENTS)
    {
      gint x, y;
      
      gdk_window_get_root_origin (widget->window, &x, &y);
      gle_rc_set_long (gle_rc, _GLE_RCARG_SHELL_INIT_X, x);
      gle_rc_set_long (gle_rc, _GLE_RCARG_SHELL_INIT_Y, y);
    }
#endif
  
  if (GTK_WIDGET_CLASS (parent_class)->configure_event)
    return GTK_WIDGET_CLASS (parent_class)->configure_event (widget, event);
  else
    return FALSE;
}

static void
gle_shell_realize (GtkWidget *widget)
{
  GleShell *shell = GLE_SHELL (widget);
  GList *list, *node;

  GTK_WIDGET_CLASS (parent_class)->realize (widget);
  
  /* fill in the plist */
  list = gle_proxy_sets_list ();
  for (node = list; node; node = node->next)
    gle_plist_prepend (shell->plist, node->data);
  g_list_free (list);
}

static void
gle_shell_unrealize (GtkWidget *widget)
{
  GleShell *shell = GLE_SHELL (widget);
  
  gle_plist_clear (shell->plist);
  GTK_WIDGET_CLASS (parent_class)->unrealize (widget);
}

GleShell*
gle_shell_popup (void)
{
  if (!gle_shell)
    g_object_new (GLE_TYPE_SHELL, NULL);
  
  gtk_widget_show (GTK_WIDGET (gle_shell));
  gdk_window_raise (GTK_WIDGET (gle_shell)->window);
  
  return gle_shell;
}

gboolean
gle_shell_is_popped_up (void)
{
  return gle_shell != NULL;
}

void
gle_shell_addremove_proxy_set (GleProxySet *pset,
			       gboolean     is_add)
{
  if (gle_shell && GTK_WIDGET_REALIZED (gle_shell))
    (is_add ? gle_plist_prepend : gle_plist_remove) (gle_shell->plist, pset);
}

void
gle_shell_rip_gtk_window (GtkWindow *window)
{
  GleForeign *foreign;
  GleProxySet *pset;
  GleProxy *proxy;
  gchar *name;

  g_return_if_fail (GTK_IS_WINDOW (window));

  foreign = gle_foreign_from_class (G_OBJECT_GET_CLASS (window));
  proxy = gle_proxy_get_from_object (G_OBJECT (window), foreign);
  if (proxy)
    {
      if (gle_shell && gle_shell->plist)
	gle_plist_mark (gle_shell->plist, proxy->pset);
      return;
    }

  if (gle_shell && gle_shell->plist)
    gle_plist_freeze (gle_shell->plist);
  
  name = window->title;
  if (!name)
    name = window->wmclass_name;
  if (!name)
    name = window->wmclass_class;
  if (!name)
    name = window->wm_role;
  if (!name)
    name = gtk_widget_get_name (GTK_WIDGET (window));
  pset = gle_proxy_set_new (GLE_PROXY_SET_SOURCE_RIPPED, name, foreign);
  proxy = gle_proxy_new_from_object (pset, G_OBJECT (window));

  gle_foreign_sync_rip (proxy, TRUE);

  if (gle_shell && gle_shell->plist)
    {
      gle_plist_mark (gle_shell->plist, pset);
      gle_plist_thaw (gle_shell->plist);
    }
}

static GQuark
gle_shell_plist_check_tag (GleShell *shell,
			   GleProxy *proxy)
{
  return g_quark_try_string (GLE_XPM_CODE);
  // return g_quark_try_string (GLE_XPM_BOMB);
  // return g_quark_try_string (GLE_XPM_SHELL_OWNED);
}

static void
gle_shell_popup_finish (GleShell *shell)
{
  guint i;
  
  g_return_if_fail (GLE_IS_SHELL (shell));
  
  shell->popup_data = NULL;

  if (shell->popup_factory)
    for (i = 0; i < POPUP_OP_LAST; i++)
      gle_item_factory_adjust (shell->popup_factory,
			       i,
			       TRUE,
			       NULL);
}

static gint
gle_shell_plist_button_press (GleShell	     *shell,
			      GdkEventButton *event)
{
  gboolean handled = FALSE;
  
  g_return_val_if_fail (GLE_IS_SHELL (shell), FALSE);

  if (event->type == GDK_BUTTON_PRESS)
    {
      gint row = 0;
      gint column;
      
      if (gle_clist_selection_info (GTK_CLIST (shell->plist),
				    event->window,
				    event->x, event->y,
				    &row, &column))
	{
	  if (event->button == 3)
	    {
	      GleProxySet *pset = gle_plist_get_proxy_set (shell->plist, row);
	      guint i;
	      
	      handled = TRUE;
	      
	      g_return_val_if_fail (GLE_IS_PROXY_SET (pset), TRUE);	/* paranoid */
	      
	      /* we temporarily adjust the popup menu for the proxy set
	       * gle_shell_popup_finish() cares about readjustment
	       */
	      shell->popup_data = pset;
	      for (i = 0; i < POPUP_OP_LAST; i++)
		gle_item_factory_adjust (shell->popup_factory,
					 i,
					 gle_shell_popup_check (shell, pset, i),
					 NULL);
	      gtk_item_factory_popup_with_data (shell->popup_factory,
						shell,
						(GtkDestroyNotify) gle_shell_popup_finish,
						event->x_root,
						event->y_root,
						event->button,
						event->time);
	    }
#if 0
	  else if (column == GLE_PLIST_COL_TAG)
	    {
	      GleGObject *gobject;
	      
	      gobject = gle_plist_get_gobject (shell->plist, row);
	      g_return_val_if_fail (gobject != NULL, TRUE);
	      g_return_val_if_fail (GLE_IS_GWIDGET (gobject), TRUE);

	      handled = TRUE;
	      
	      if (event->button == 1)
		{
		  if (GLE_GOBJECT_SHELL_OWNED (gobject))
		    {
		      if (GLE_NOTIFIER_CHECK_F (gobject, gle_bomb_shell))
			GLE_NOTIFIER_REMOVE_F (gobject, gle_bomb_shell);
		      else
			{
			  GLE_NOTIFIER_INSTALL (gobject, "pre_disassociate", gle_bomb_shell, NULL);
			  GLE_NOTIFIER_INSTALL (gobject, "pre_destroy", gle_bomb_shell, NULL);
			}
		      
		      gle_plist_update (shell->plist, gobject);
		    }
		}
	      else if (event->button == 2)
		{
		  if (GLE_GOBJECT_SHELL_OWNED (gobject))
		    gle_shell_give_up_gtoplevel (GLE_GWIDGET (gobject));
		  else
		    gle_shell_own_gtoplevel (GLE_GWIDGET (gobject));
		}
	    }
#endif
	}
    }
  
  return handled;
}

static void
gle_shell_do_selection (GleShell *shell)
{
  gboolean do_selection;
  GtkWidget *customer;
  
  g_return_if_fail (GLE_IS_SHELL (shell));
  
  do_selection = !gle_selector_in_selection (NULL);
  
  if (do_selection && gdk_pointer_is_grabbed () &&
      !GLE_SELECTOR (shell->selector)->warning_visible)
    {
      gle_selector_popup_warning (GLE_SELECTOR (shell->selector));
      do_selection = FALSE;
    }
  
  g_object_ref (shell);
  if (do_selection)
    {
      if (!gdk_pointer_is_grabbed () && !GTK_WIDGET_VISIBLE (shell))
	{
	  gtk_widget_show (GTK_WIDGET (shell));
	  gdk_window_raise (GTK_WIDGET (shell)->window);
	}
      
      customer = gle_selector_make_selection (GLE_SELECTOR (shell->selector));
      
      if (shell->selector)
	{
	  if (customer && GTK_WIDGET_REALIZED (customer))
	    gle_shell_rip_gtk_window (GTK_WINDOW (customer));
	  else
	    gle_plist_mark (shell->plist, NULL);
	  
	  gle_selector_reset (GLE_SELECTOR (shell->selector));

	  if (!GTK_WIDGET_VISIBLE (shell))
	    gtk_widget_show (GTK_WIDGET (shell));
	}
      
    }
  g_object_unref (shell);
}

static void
gle_shell_candidate_check (GtkObject  *caller,
			   GtkWidget **new_candidate,
			   gint	      *candidate_ok)
{
  GleShell *shell = GLE_SHELL (caller);
  GtkWidget *toplevel;
  
  g_return_if_fail (caller != NULL);
  
  toplevel = *new_candidate ? gtk_widget_get_toplevel (*new_candidate) : NULL;
  if (!toplevel || GLE_HAS_TAG (toplevel))
    *candidate_ok = FALSE;
  
  *new_candidate = toplevel;
  if (*candidate_ok)
    {
      *candidate_ok = (*new_candidate &&
		       *new_candidate != GTK_WIDGET (shell) &&
		       !GLE_HAS_TAG (*new_candidate));
      
      if (*candidate_ok)
	{
	  GtkItemFactory *ifactory;
	  
	  ifactory = gtk_item_factory_from_widget (*new_candidate);
	  if (ifactory && GLE_HAS_TAG (ifactory))
	    *candidate_ok = FALSE;
	}
    }
  
  if (*candidate_ok)
    {
      GleForeign *foreign = gle_foreign_from_class (G_OBJECT_GET_CLASS (*new_candidate));
      GleProxy *proxy = gle_proxy_get_from_object (G_OBJECT (*new_candidate), foreign);

      if (proxy)
	gle_plist_mark (shell->plist, proxy->pset);
    }
  else
    gle_plist_mark (shell->plist, NULL);
}

#if 0
static void
gle_bomb_shell (GleProxy *proxy)
{
  if (GLE_PROXY_HAS_OBJECT (proxy) &&
      !GTK_OBJECT_DESTROYED (gobject->object))
    {
    }
  else
    {
      gle_gobject_destroy (gobject);
      if (gle_shell)
	gtk_widget_destroy (GTK_WIDGET (gle_shell));
    }
}

void
gle_shell_own_gtoplevel (GleGWidget *gwidget)
{
  g_return_if_fail (gwidget != NULL);
  g_return_if_fail (GLE_IS_GTOPLEVEL (gwidget));

  if (!GLE_GOBJECT_SHELL_OWNED (gwidget))
    {
      gle_gwidget_ref (gwidget);

      GLE_GOBJECT_SET_FLAG (gwidget, GLE_GOBJECT_SHELL_OWNED);
      
      GLE_NOTIFIER_INSTALL (gwidget, "pre_disassociate", gle_bomb_shell, NULL);
      GLE_NOTIFIER_INSTALL (gwidget, "pre_destroy", gle_bomb_shell, NULL);

      if (gle_shell && !GTK_OBJECT_DESTROYED (gle_shell))
	gle_plist_update (gle_shell->plist, GLE_GOBJECT (gwidget));
    }
}

void
gle_shell_give_up_gtoplevel (GleGWidget *gwidget)
{
  g_return_if_fail (gwidget != NULL);
  g_return_if_fail (GLE_IS_GTOPLEVEL (gwidget));

  if (GLE_GOBJECT_SHELL_OWNED (gwidget))
    {
      GLE_GOBJECT_UNSET_FLAG (gwidget, GLE_GOBJECT_SHELL_OWNED);

      if (GLE_NOTIFIER_CHECK_MFD (gwidget, "pre_disassociate", gle_bomb_shell, NULL))
	GLE_NOTIFIER_REMOVE_MFD (gwidget, "pre_disassociate", gle_bomb_shell, NULL);
      if (GLE_NOTIFIER_CHECK_MFD (gwidget, "pre_destroy", gle_bomb_shell, NULL))
	GLE_NOTIFIER_REMOVE_MFD (gwidget, "pre_destroy", gle_bomb_shell, NULL);

      if (gle_shell && !GTK_OBJECT_DESTROYED (gle_shell))
	gle_plist_update (gle_shell->plist, GLE_GOBJECT (gwidget));

      gle_gwidget_unref (gwidget);
    }
}

GList*
gle_shell_list_selected (void)
{
  if (gle_shell && !GTK_OBJECT_DESTROYED (gle_shell))
    return gle_plist_list_selected (gle_shell->plist);
  else
    return NULL;
}

GList*
gle_shell_list_all (void)
{
  if (gle_shell && !GTK_OBJECT_DESTROYED (gle_shell))
    return gle_plist_list_all (gle_shell->plist);
  else
    return NULL;
}
#endif

void
gle_shell_manage_object (GtkObject *object)
{
  g_return_if_fail (GTK_IS_OBJECT (object));
  g_return_if_fail (GLE_HAS_TAG (object));
  
  if (!gle_shell)
    g_error ("No GleShell present");
  
  g_object_connect (gle_shell,
		    "swapped_object_signal::destroy", gtk_object_destroy, object,
		    NULL);
}

static void
gle_shell_popup_operation (GleShell	  *shell,
			   guint	   popup_op,
			   GtkWidget	  *item)
{
  GList *pset_list, *list;
  
  g_return_if_fail (shell != NULL);
  g_return_if_fail (GLE_IS_SHELL (shell));
  g_return_if_fail (popup_op < POPUP_OP_LAST);
  
  if (shell == gtk_item_factory_popup_data_from_widget (item))
    {
      /* we are invoked from the popup menu */
      g_object_ref (shell->popup_data);
      pset_list = g_list_prepend (NULL, shell->popup_data);
    }
  else
    {
      /* we are invoked via accelerator, in this case
       * we group-operate on all selected psets
       */
      
      pset_list = gle_plist_list_selected (shell->plist);
      for (list = pset_list; list; list = list->next)
	g_object_ref (list->data);
    }
  
  g_object_ref (shell);
  for (list = pset_list; list; list = list->next)
    {
      GleProxySet *pset;
      
      pset = list->data;
      if (!gle_shell_popup_check (shell, pset, popup_op))
	{
	  g_object_unref (pset);
	  continue;
	}

      switch (popup_op)
	{
	  GtkWidget *dialog;
	  GList *proxy_list, *node;
	case POPUP_OP_NONE:
	  break;
	case POPUP_OP_P_TED:
	  proxy_list = gle_proxy_set_list_toplevels (pset);
	  for (node = proxy_list; node; node = node->next)
	    {
	      dialog = (GtkWidget*) gle_ted_from_proxy (node->data);
	      if (!dialog)
		dialog = gle_ted_new (node->data);
	      gtk_widget_show (dialog);
	      gdk_window_raise (dialog->window);
	    }
	  g_list_free (proxy_list);
	  break;
	case POPUP_OP_P_RERIP:
          proxy_list = gle_proxy_set_list_toplevels (pset);
	  for (node = proxy_list; node; node = node->next)
	    {
	      GleProxy *cproxy = node->data;

	      if (GLE_PROXY_HAS_OBJECT (cproxy))
		gle_foreign_sync_rip (cproxy, TRUE);
	    }
	  g_list_free (proxy_list);
	  break;
	default:
	  fprintf (stderr, "GleShell: invalid popup operation: %u\n", popup_op);
	  break;
	}
      g_object_unref (pset);
    }
  g_list_free (pset_list);
  g_object_unref (shell);
}

static gboolean
gle_shell_popup_check (GleShell	   *shell,
		       GleProxySet *pset,
		       guint	    pop)
{
  g_return_val_if_fail (GLE_IS_SHELL (shell), FALSE);
  g_return_val_if_fail (GLE_IS_PROXY_SET (pset), FALSE);

  switch (pop)
    {
      GList *proxy_list;
    case POPUP_OP_P_TED:
      return TRUE;
    case POPUP_OP_P_RERIP:
      proxy_list = gle_proxy_set_list_toplevels (pset);
      g_list_free (proxy_list);
      return proxy_list != NULL;
    default:
      return FALSE;
    }
}

void
gle_shell_operation (GleShell	*shell,
		     GleShellOps shell_op)
{
  g_return_if_fail (GLE_IS_SHELL (shell));
  g_return_if_fail (shell_op < GLE_SHELL_OP_LAST);
  
  g_object_ref (shell);
  switch (shell_op)
    {
    case GLE_SHELL_OP_NONE:
      break;
    case GLE_SHELL_OP_DELETE:
      gtk_widget_destroy (GTK_WIDGET (shell));
      break;
    case GLE_SHELL_OP_SELECTOR:
#if 0
      gle_shell_do_selection (shell);
#endif
      break;
    case GLE_SHELL_OP_UPDATE:
      gle_plist_update_all (shell->plist);
      break;
    case GLE_SHELL_OP_NEW_WINDOW:
#if 0
      gle_shell_add_toplevel (shell, gtk_widget_new (GTK_TYPE_WINDOW,
						     "title", "User Window",
						     "visible", TRUE,
						     NULL));
#endif
      break;
    case GLE_SHELL_OP_WPALETTE:
#if 0
      if (!shell->wpalette)
	{
	  shell->wpalette =
	    gtk_widget_new (GLE_TYPE_WPALETTE,
			    "signal::destroy", gtk_widget_destroyed, &shell->wpalette,
			    NULL);
	}
      gtk_widget_show (shell->wpalette);
      gdk_window_raise (shell->wpalette->window);
#endif
      break;
    case GLE_SHELL_OP_RC_PREF:
#if 0
      if (!shell->rc_pref)
	{
	  shell->rc_pref = gle_rc_pref_dialog_new (gle_rc, "GLE-RcPreferences");
	  GLE_TAG (shell->rc_pref);
	  gtk_signal_connect (GTK_OBJECT (shell->rc_pref),
			      "destroy",
			      gtk_widget_destroyed,
			      &shell->rc_pref);
	}
      gtk_widget_show (shell->rc_pref);
      gdk_window_raise (shell->rc_pref->window);
#endif
      break;
    case GLE_SHELL_OP_OPEN:
#if 0
      if (!shell->file_open_dialog)
	{
	  shell->file_open_dialog = gle_file_dialog_new_open ("*.glr");
	  gtk_signal_connect (GTK_OBJECT (shell->file_open_dialog),
			      "destroy",
			      gtk_widget_destroyed,
			      &shell->file_open_dialog);
	}
      gtk_widget_show (shell->file_open_dialog);
      gdk_window_raise (shell->file_open_dialog->window);
#endif
      break;
    case GLE_SHELL_OP_SAVE:
#if 0
      if (!shell->file_save_dialog)
	{
	  shell->file_save_dialog = gle_file_dialog_new_save ("unnamed.glr");
	  gtk_signal_connect (GTK_OBJECT (shell->file_save_dialog),
			      "destroy",
			      gtk_widget_destroyed,
			      &shell->file_save_dialog);
	}
      gtk_widget_show (shell->file_save_dialog);
      gdk_window_raise (shell->file_save_dialog->window);
#endif
      break;
    case GLE_SHELL_OP_HELP_INTRO:
      if (!shell->help_intro)
	{
	  shell->help_intro = gle_info_window ("GLE Introduction", gle_docus[0].text);
	  GLE_TAG (shell->help_intro);
          g_object_connect (shell->help_intro,
			    "signal::destroy", gtk_widget_destroyed, &shell->help_intro,
			    NULL);
	}
      gtk_widget_show (shell->help_intro);
      gdk_window_raise (shell->help_intro->window);
      break;
    case  GLE_SHELL_OP_HELP_ABOUT:
    default:
      fprintf (stderr, "GleShellOps: unimplemented %u\n", shell_op);
      break;
    }
  g_object_unref (shell);
}
