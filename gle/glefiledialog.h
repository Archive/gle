/* GLE - The GTK+ Layout Engine
 * Copyright (C) 1998, 1999 Tim Janik
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307, USA.
 */
#ifndef __GLE_FILE_DIALOG_H__
#define __GLE_FILE_DIALOG_H__


#include        <gle/glemisc.h>


#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */


/* --- type macros --- */
#define GLE_TYPE_FILE_DIALOG		(gle_file_dialog_get_type ())
#define GLE_FILE_DIALOG(object)		(GTK_CHECK_CAST ((object), GLE_TYPE_FILE_DIALOG, GleFileDialog))
#define GLE_FILE_DIALOG_CLASS(klass)	(GTK_CHECK_CLASS_CAST ((klass), GLE_TYPE_FILE_DIALOG, GleFileDialogClass))
#define GLE_IS_FILE_DIALOG(object)	(GTK_CHECK_TYPE ((object), GLE_TYPE_FILE_DIALOG))
#define GLE_IS_FILE_DIALOG_CLASS(klass)	(GTK_CHECK_CLASS_TYPE ((klass), GLE_TYPE_FILE_DIALOG))

/* --- typedefs --- */
typedef struct  _GleFileDialog		GleFileDialog;
typedef struct  _GleFileDialogClass	GleFileDialogClass;


/* --- structures --- */
struct _GleFileDialog
{
  GtkFileSelection file_selection;
};

struct _GleFileDialogClass
{
  GtkFileSelectionClass	parent_class;
};


/* --- prototypes --- */
GtkType		gle_file_dialog_get_type	(void);
GtkWidget*	gle_file_dialog_new_open	(const gchar *pattern);
GtkWidget*	gle_file_dialog_new_save	(const gchar *pattern);



#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif  /* __GLE_FILE_DIALOG_H__ */
