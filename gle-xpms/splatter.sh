#!/bin/sh

test -z "$1" && exit 1
INCLUDES="$1"
shift
test -z "$1" && exit 1
ARRAY="$1"
shift
test -z "$1" && exit 1
FILE="$1"
shift

touch "$INCLUDES" "$ARRAY"

NAME=`fgrep static $FILE | sed -e 's/.*\b\([A-Za-z_0-9]\+\)_xpm.*/\1/'`

test -z "$NAME" && { echo "unmatched $FILE"; exit 0 ; }

grep '\b'"$NAME"'_xpm\b' $ARRAY >/dev/null && {
	echo "skipping $FILE"
	exit 0
}

echo "adding gtk_"$NAME"_get_type for "$NAME"_xpm"

echo -e "  { 0, gtk_"$NAME"_get_type, &"$NAME"_xpm }," >> $ARRAY
echo -e "#include \<gle-xpms/"$FILE"\>" >> $INCLUDES

# invokation:
# for x in *.xpm; do splatter.sh glexpm_inc.c glexpm_p.c $x; done
